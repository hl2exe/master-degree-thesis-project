"""


"""

import re
import string
from functools import reduce
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS


class Cleaner(object):

    def __init__(self, tokenizer=None, balancing_amount=30000):
        if tokenizer is None:
            tokenizer = re.compile(r"(?u)\b\w\w+\b")

        self.tokenizer = tokenizer
        self.balancing_amount = balancing_amount

    def tokenize_filtering(self, couples, tokenizer=None):
        """
        Tries to tokenize given sentences a tokenizer.
        Failed attempts are discarded.

        :param couples: dictionary containing evidences and claims
        :param tokenizer: custom tokenizer object or, if None, the default regex tokenizer
        :return: dictionary containing filtered evidences and claims
        """

        if tokenizer is None:
            tokenizer = self.tokenizer

        filtered_couples = {}
        for key in couples:
            filtered_couples[key] = [couple for couple in couples[key]
                                     if len(tokenizer.findall(couple[0].lower())) > 0
                                     and len(tokenizer.findall(couple[1].lower())) > 0]

        return filtered_couples

    def stop_words_filtering(self, couples, tokenizer=None):
        """
        Filters given sentences by removing english stop words

        :param couples: dictionary containing evidences and claims
        :param tokenizer: custom tokenizer object or, if None, the default regex tokenizer
        :return: dictionary containing filtered evidences and claims
        """

        if tokenizer is None:
            tokenizer = self.tokenizer

        filtered_couples = {}
        for key in couples:
            filtered_couples[key] = [couple for couple in couples[key]
                                     if len([token for token in tokenizer.findall(couple[0].lower())
                                             if token not in ENGLISH_STOP_WORDS]) > 0
                                     and len([token for token in tokenizer.findall(couple[1].lower())
                                              if token not in ENGLISH_STOP_WORDS]) > 0]

        return filtered_couples

    def punctuation_filtering(self, couples):
        """
        Filters given sentences by removing punctuation

        :param couples: dictionary containing evidences and claims
        :return: dictionary containing filtered evidences and claims
        """

        def _strip_couple(couple):
            return tuple([text.translate(None, string.punctuation) for text in couple])

        filtered_couples = {}
        for key in couples:
            filtered_couples[key] = [_strip_couple(couple) for couple in couples[key]]

        return filtered_couples

    def balancing_filtering(self, couples):
        """
        Balances given sentences by selecting only a limited portion of them

        :param couples: dictionary containing evidences and claims
        :return: dictionary containing filtered evidences and claims
        """

        for key in couples:
            couples[key].sort(key=lambda couple: (len(couple[0]), len(couple[1])), reverse=True)
            couples[key] = couples[key][:self.balancing_amount]

        return couples

    def filter_couples(self, couples, function_names):
        """
        General filtering proxy function that applies a sub-set of the supported
        filtering methods to given sentences.

        :param couples: dictionary containing evidences and claims
        :param function_names: list of methods keys as shown in filter_methods dictionary
        :return: dictionary containing filtered evidences and claims
        """

        filter_methods = {
            'tokenizer': self.tokenize_filtering,
            'stop_words': self.stop_words_filtering,
            'punctuation_filtering': self.punctuation_filtering,
            'balancing': self.balancing_filtering
        }

        functions = [filter_methods[name] for name in function_names]
        return reduce(lambda r, f: f(r), functions, couples)
