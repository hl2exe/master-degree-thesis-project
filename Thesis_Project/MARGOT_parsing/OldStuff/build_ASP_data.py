"""

Builds evidence-claim couples csv files with associated relation stance

"""

from MARGOT_parsing.analyzer import Analyzer
from itertools import product
from Supervised_Approach.Utils.iterable_utils import pairwise
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
import os
import unicodecsv as csv
import re
from MARGOT_parsing.path_definitions import DATASETS_DIR, PARSED_DATASETS_DIR
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS


def get_file_metadata(filename, path, topic):

    meta_filename = filename.replace('.txt', '.meta')
    stance = None
    rebuttal = None

    with open(os.path.join(path, topic, meta_filename)) as f:
        lines = f.readlines()
        try:
            stance = int(lines[2].split('=')[1])
            rebuttal = lines[3].split('=')[1].strip()
        except Exception, e:
            print(e.message)

    return stance, rebuttal


def extract_arguments_from_file(filename):

    all_claims = None
    all_evidences = None
    with open(filename, 'r') as selected:
        argumentative_lines = [line for line in selected.readlines() if line.startswith('CLAIM')
                               or line.startswith('EVIDENCE')]

        claims = [line[len('CLAIM '):] for line in argumentative_lines
                  if line.startswith('CLAIM ')]
        evidences = [line[len('EVIDENCE '):] for line in argumentative_lines
                     if line.startswith('EVIDENCE')]
        claim_evidences = [line[len('CLAIM_EVIDENCE '):] for line in argumentative_lines
                           if line.startswith('CLAIM_EVIDENCE')]

        all_claims = claims + claim_evidences
        all_evidences = evidences + claim_evidences

    return all_evidences, all_claims


parsed_path = os.path.join(PARSED_DATASETS_DIR, 'CreateDebateCustomDataset')
default_path = os.path.join(DATASETS_DIR, 'CreateDebateCustomDataset')

# Step 1: positive examples

analyzer = Analyzer()
positive_files = analyzer.analyze(parsed_path)
# positive_files = ["/home/frgg/Desktop/ParsedDatasets/CreateDebate custom dataset/abortion_A183.txt"]
positive_couples = []

for file in positive_files:
    with open(os.path.join(parsed_path, file), mode='r') as selected:
        argumentative_lines = [line for line in selected.readlines() if line.startswith('CLAIM')
                               or line.startswith('EVIDENCE')]

        claims = [line[len('CLAIM '):] for line in argumentative_lines
                  if line.startswith('CLAIM ')]
        evidences = [line[len('EVIDENCE '):] for line in argumentative_lines
                     if line.startswith('EVIDENCE')]
        claim_evidences = [line[len('CLAIM_EVIDENCE '):] for line in argumentative_lines
                           if line.startswith('CLAIM_EVIDENCE')]

        all_claims = claims + claim_evidences
        all_evidences = evidences + claim_evidences

        for couple in product(all_evidences, all_claims):
            positive_couples.append(couple)

print("Built {} positive couples".format(len(positive_couples)))
print("Built {} distinct positive couples".format(len(set(positive_couples))))

# Step 2: negative examples

# Separating files by topic -> prefix

topics = ['abortion', 'gayRights', 'marijuana', 'obama']

files_by_topic = {}

for topic in topics:
    files_by_topic[topic] = []

for path, subdirs, files in os.walk(parsed_path):
    for filename in files:
        for topic in topics:
            if filename.startswith(topic):
                files_by_topic[topic].append(filename)

for topic in files_by_topic:
    print('Found {} files for topic {}'.format(len(files_by_topic[topic]), topic))

# Building opposing stance file couples

negative_couples = []

for topic in topics:
    domains = []
    for meta in os.listdir(os.path.join(default_path, topic)):
        if meta.endswith('.meta'):
            domains.append(meta[0])

    domains = set(domains)

    for domain in domains:
        domain_files = [file for file in files_by_topic[topic]
                        if file.replace("{}_".format(topic), '').startswith(domain)]
        domain_files.sort(key=lambda filename:
        int(filename.replace("{}_".format(topic), '').replace('.txt', '')[1:]))

        for (head, tail) in pairwise(domain_files):
            head_stance, _ = get_file_metadata(head.replace("{}_".format(topic), ''),
                                               default_path, topic)
            tail_stance, tail_rebuttal = get_file_metadata(tail.replace("{}_".format(topic), ''),
                                                           default_path, topic)

            if head_stance != tail_stance and (tail_rebuttal.startswith('oppose') or tail_rebuttal.startswith('null')):
                head_evidences, head_claims = extract_arguments_from_file(os.path.join(parsed_path, head))
                tail_evidences, tail_claims = extract_arguments_from_file(os.path.join(parsed_path, tail))

                for couple in product(head_evidences, tail_claims):
                    negative_couples.append(couple)
                for couple in product(tail_evidences, head_claims):
                    negative_couples.append(couple)

            # I may apply the same reasoning for positive couples
            if head_stance == tail_stance and (tail_rebuttal.startswith('support') or
                                               tail_rebuttal.startswith('null')):
                head_evidences, head_claims = extract_arguments_from_file(os.path.join(parsed_path, head))
                tail_evidences, tail_claims = extract_arguments_from_file(os.path.join(parsed_path, tail))

                for couple in product(head_evidences, tail_claims):
                    positive_couples.append(couple)
                for couple in product(tail_evidences, head_claims):
                    positive_couples.append(couple)

print("Built {} negative couples".format(len(negative_couples)))

print("Updated positive couples: {}".format(len(positive_couples)))


positive_couples = list(set(positive_couples))
negative_couples = list(set(negative_couples))

# Step 3: saving couples to csv file

positive_couples = [(evidence, claim, 'link') for (evidence, claim) in positive_couples]
negative_couples = [(evidence, claim, 'no-link') for (evidence, claim) in negative_couples]

print("Updated positive distinct couples: {}".format(len(set(positive_couples))))
print("Built {} distinct negative couples".format(len(set(negative_couples))))

# Cleaning data

# Tokenizer filtering
tokenizer = re.compile(r"(?u)\b\w\w+\b")

positive_couples = [couple for couple in positive_couples
                    if len(tokenizer.findall(couple[0].lower())) > 0
                    and len(tokenizer.findall(couple[1].lower())) > 0]

negative_couples = [couple for couple in negative_couples
                    if len(tokenizer.findall(couple[0].lower())) > 0
                    and len(tokenizer.findall(couple[1].lower())) > 0]

print("Positive couples after tokenizer cleaning: {}".format(len(positive_couples)))
print("Negative couples after tokenizer cleaning: {}".format(len(negative_couples)))

# Stop words filtering

positive_couples = [couple for couple in positive_couples
                    if len([token for token in tokenizer.findall(couple[0].lower())
                            if token not in ENGLISH_STOP_WORDS]) > 0
                    and len([token for token in tokenizer.findall(couple[1].lower())
                             if token not in ENGLISH_STOP_WORDS]) > 0]

negative_couples = [couple for couple in negative_couples
                    if len([token for token in tokenizer.findall(couple[0].lower())
                            if token not in ENGLISH_STOP_WORDS]) > 0
                    and len([token for token in tokenizer.findall(couple[1].lower())
                             if token not in ENGLISH_STOP_WORDS]) > 0]

print("Positive couples after stop words cleaning: {}".format(len(positive_couples)))
print("Negative couples after stop words cleaning: {}".format(len(negative_couples)))

# Balancing data-set: sorting by text length and then selecting the longest ones

positive_couples.sort(key=lambda couple: (len(couple[0]), len(couple[1])), reverse=True)
negative_couples.sort(key=lambda couple: (len(couple[0]), len(couple[1])), reverse=True)

positive_couples = positive_couples[:30000]
negative_couples = negative_couples[:30000]

print("Balanced data-set:\nPositive: {}\nNegative {}".format(len(positive_couples), len(negative_couples)))

file_path = os.path.join(POST_PROCESS_DIR, 'MARGOT_argument_structure_prediction_training_balanced_clean_all.csv')

print("Saving valid entries to: {}".format(file_path))
with open(file_path, 'wb') as out:
    csv_out = csv.writer(out, encoding="utf-8")
    csv_out.writerow(['Evidence', 'Claim', 'Argument relation'])
    for row in positive_couples + negative_couples:
        csv_out.writerow(row)
