import csv
import os


class CsvExtractor(object):

    def __init__(self, idfield, bodyfield):
        self.idfield = idfield
        self.bodyfield = bodyfield

    def extract(self, filepath, destpath):
        with open(filepath, 'rb') as csvfile:
            reader = csv.DictReader(csvfile)

            for row in reader:
                tempFilename = "{}{}.txt".format(destpath, row[self.idfield])
                with open(tempFilename, 'w') as tempFile:
                    tempFile.write(row[self.bodyfield] + os.linesep)
