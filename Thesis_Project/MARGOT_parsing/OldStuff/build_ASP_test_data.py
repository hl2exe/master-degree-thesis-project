"""

Builds MARGOT ASP evidence - claim couples.
Couples here are used for rebuttal validation test.

"""

import os
from MARGOT_parsing.path_definitions import DATASETS_DIR, PARSED_DATASETS_DIR
from Supervised_Approach.Utils.iterable_utils import pairwise
from itertools import product
import re
from sklearn.feature_extraction.stop_words import ENGLISH_STOP_WORDS
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
import unicodecsv as csv


def get_file_metadata(filename, path, topic):

    meta_filename = filename.replace('.txt', '.meta')
    stance = None
    rebuttal = None
    with open(os.path.join(path, topic, meta_filename)) as selected:
        lines = selected.readlines()
        try:
            stance = int(lines[2].split('=')[1])
        except Exception, e:
            print(path, topic, meta_filename)
            print(lines, lines[2].split('=')[1])
            print(e.message)

        rebuttal = lines[3].split('=')[1].strip()

    return stance, rebuttal


def extract_arguments_from_file(filename):

    all_claims = None
    all_evidences = None
    with open(filename, 'r') as selected:
        argumentative_lines = [line for line in selected.readlines() if line.startswith('CLAIM')
                               or line.startswith('EVIDENCE')]

        claims = [line[len('CLAIM '):] for line in argumentative_lines
                  if line.startswith('CLAIM ')]
        evidences = [line[len('EVIDENCE '):] for line in argumentative_lines
                     if line.startswith('EVIDENCE')]
        claim_evidences = [line[len('CLAIM_EVIDENCE '):] for line in argumentative_lines
                           if line.startswith('CLAIM_EVIDENCE')]

        all_claims = claims + claim_evidences
        all_evidences = evidences + claim_evidences

    return all_evidences, all_claims


parsed_path = os.path.join(PARSED_DATASETS_DIR, 'CreateDebateCustomDataset')
default_path = os.path.join(DATASETS_DIR, 'CreateDebateCustomDataset')
topics = ['abortion', 'gayRights', 'marijuana', 'obama']

files_by_topic = {}

for topic in topics:
    files_by_topic[topic] = []

for path, subdirs, files in os.walk(parsed_path):
    for filename in files:
        for topic in topics:
            if filename.startswith(topic):
                files_by_topic[topic].append(filename)

for topic in files_by_topic:
    print('Found {} files for topic {}'.format(len(files_by_topic[topic]), topic))

all_couples = []

for topic in topics:
    domains = []
    for meta in os.listdir(os.path.join(default_path, topic)):
        if meta.endswith('.meta'):
            domains.append(meta[0])

    domains = set(domains)

    for domain in domains:
        domain_files = [file for file in files_by_topic[topic]
                        if file.replace("{}_".format(topic), '').startswith(domain)]
        domain_files.sort(key=lambda filename: int(filename.replace("{}_".format(topic), '').replace('.txt', '')[1:]))

        for (head, tail) in pairwise(domain_files):
            head_evidences, head_claims = extract_arguments_from_file(os.path.join(parsed_path, head))
            tail_evidences, tail_claims = extract_arguments_from_file(os.path.join(parsed_path, tail))

            post_couple_name = '{}_{}_{}'.format(topic, head.replace('{}_'.format(topic), '').replace('.txt', ''),
                                                 tail.replace('{}_'.format(topic), '').replace('.txt', ''))

            for couple in product(head_evidences, tail_claims):
                all_couples.append((couple[0], couple[1], post_couple_name, 'head to tail'))
            for couple in product(tail_evidences, head_claims):
                all_couples.append((couple[0], couple[1], post_couple_name, 'tail to head'))

print("Total evidence-claim couples found: {}".format(len(all_couples)))

all_couples = set(all_couples)
print("Total distinct evidence-claim couples found: {}".format(len(all_couples)))

# Cleaning data

# Tokenizer filtering
tokenizer = re.compile(r"(?u)\b\w\w+\b")

all_couples = [couple for couple in all_couples
               if len(tokenizer.findall(couple[0].lower())) > 0
               and len(tokenizer.findall(couple[1].lower())) > 0]

print("Total couples after tokenizer cleaning: {}".format(len(all_couples)))

# Stop words filtering

all_couples = [couple for couple in all_couples
               if len([token for token in tokenizer.findall(couple[0].lower())
                       if token not in ENGLISH_STOP_WORDS]) > 0
               and len([token for token in tokenizer.findall(couple[1].lower())
                       if token not in ENGLISH_STOP_WORDS]) > 0]

print("Total couples after stop words cleaning: {}".format(len(all_couples)))

all_couples.sort(key=lambda couple: (len(couple[0]), len(couple[1])), reverse=True)
all_couples = all_couples[:50000]

# Saving data to file

file_path = os.path.join(POST_PROCESS_DIR, 'MARGOT_argument_structure_prediction_test_clean_reduced.csv')

print("Saving valid entries to: {}".format(file_path))
with open(file_path, 'wb') as out:
    csv_out = csv.writer(out, encoding="utf-8")
    csv_out.writerow(['Evidence', 'Claim', 'Post couple', 'Evidence and Claim post'])
    for row in all_couples:
        csv_out.writerow(row)
