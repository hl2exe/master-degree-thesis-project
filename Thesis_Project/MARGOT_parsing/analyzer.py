import os
from fnmatch import fnmatch
from itertools import chain


class Analyzer(object):

    MARGOT_keywords = {
        'claim': 'CLAIM',
        'evidence': 'EVIDENCE',
        'claim_evidence': 'CLAIM_EVIDENCE'
    }

    default_pairings = {
        'claim': ['claim', 'claim_evidence'],
        'evidence': ['evidence', 'claim_evidence']
    }

    def __init__(self, keywords=None, pairings=None, pattern='*.txt'):
        if keywords is None:
            self.keywords = Analyzer.MARGOT_keywords
        else:
            self.keywords = keywords

        if pairings is None:
            self.pairings = Analyzer.default_pairings
        else:
            self.pairings = pairings

        self.pattern = pattern

    def analyze(self, path, topics):
        """
        Filters MARGOT parsed documents by selecting the ones that have
        at least 1 Evidence and 1 Claim.

        :param path: path to folder containing all parsed documents
        :param topics: topics to consider during filtering
        :returns list of valid file names
        """

        filenames = []

        if not os.path.isdir(path):
            print("Invalid path! Can't analyze...")
            return

        for path, subdirs, files in os.walk(path):
            for filename in files:
                if fnmatch(filename, self.pattern):

                    occurrences = self.count_occurrences(filename, path)

                    # Checking file validity
                    # Rules:
                    # number of evidences > 1 and (number of claims > 1 or number of claim_evidences > 1)
                    # number of claims > 1 and (number of evidences > 1 or number of claim_evidences > 1)
                    rule1 = occurrences['EVIDENCE'] >= 1 and (occurrences['CLAIM'] >= 1
                                                              or occurrences['CLAIM_EVIDENCE'] >= 1)
                    rule2 = occurrences['CLAIM'] >= 1 and (occurrences['EVIDENCE'] >= 1
                                                           or occurrences['CLAIM_EVIDENCE'] >= 1)

                    if rule1 or rule2:
                        topic_name = [topic for topic in topics if filename.startswith(topic)][0]
                        filenames.append({'file': filename,
                                          'topic': topic_name})

        return filenames

    def count_occurrences(self, filename, path):
        """
        Counts the occurrences of keywords in file
        Note: keywords are considered as mutually exclusive

        :param filename: name of the file that has to be analysed
        :param path: path to folder containing the file to analyse
        :returns list of occurrences
        """

        occurrences = {}
        verify = {}

        for keyword in self.keywords.values():
            occurrences[keyword] = 0

        filePath = os.path.join(path, filename)

        with open(filePath, 'r') as f:
            for line in f.readlines():

                # Verifying multiple keyword for single line
                for keyword in self.keywords.values():
                    verify[keyword] = 0

                for keyword in self.keywords.values():
                    if line.startswith(keyword):
                        verify[keyword] = 1

                found = [(keyword, value) for keyword, value in verify.items() if value >= 1]

                matches = sum(verify.values())
                # Case: only one keyword matched the initial part of the line
                if matches == 1:
                    occurrences[found[0][0]] += 1
                elif matches > 1:
                    # Case: multiple keywords matched the initial part of the line.
                    # Selecting the longest one (hypothesis: mutually exclusive keywords)
                    selected = max([couple[0] for couple in found], key=len)
                    occurrences[selected] += 1

        return occurrences

    def extract_arguments_from_file(self, filename):
        """
        Extracts all Evidences and Claims in a given file.

        :param filename: name of the file to analyse
        :return: list of extracted evidences, list of extracted claims
        """

        with open(filename, 'r') as selected:
            argumentative_lines = [line for line in selected.readlines()
                                   if any([line.startswith(self.keywords[keyword]) for keyword in self.keywords])]

            argumentative_dict = {}
            for keyword in self.keywords:
                argumentative_dict[keyword] = [line[len(self.keywords[keyword]) + 1:]
                                               for line in argumentative_lines
                                               if line.startswith(self.keywords[keyword] + ' ')]    # mini-hack

            pairings_dict = {}
            for pairing_key in self.pairings:
                pairings_dict[pairing_key] = list(chain(*[argumentative_dict[keyword]
                                                          for keyword in self.pairings[pairing_key]]))

        return pairings_dict['evidence'], pairings_dict['claim']
