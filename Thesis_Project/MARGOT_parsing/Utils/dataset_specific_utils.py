################################
# Create Debate Custom Dataset
################################

import os


class CreateDebateCustom(object):

    @staticmethod
    def get_files_by_topic(topics, parsed_path):
        """
        Extracts file names according to given topics list

        :param topics: list of topics of CreateDebateCustom data-set to consider
        :param parsed_path: path to parsed documents folder
        :return: dictionary containing for each topic the related file names
        """

        files_by_topic = {}

        for topic in topics:
            files_by_topic[topic] = []

        for path, subdirs, files in os.walk(parsed_path):
            for filename in files:
                for topic in topics:
                    if filename.startswith(topic):
                        files_by_topic[topic].append(filename)

        return files_by_topic

    @staticmethod
    def get_file_metadata(filename, path, topic):
        """
        Reads information from the meta-data file related to the given file name

        :param filename: name of the .data file name
        :param path: path to CreateDebateCustom data-set folder
        :param topic: topic folder containing the given file name
        :return: stance and rebuttal values described in the .meta file
        """

        meta_filename = filename.replace('.txt', '.meta')
        stance = None
        rebuttal = None

        with open(os.path.join(path, topic, meta_filename)) as f:
            lines = f.readlines()
            try:
                stance = int(lines[2].split('=')[1])
                rebuttal = lines[3].split('=')[1].strip()
            except Exception, e:
                print(e.message)

        return stance, rebuttal
