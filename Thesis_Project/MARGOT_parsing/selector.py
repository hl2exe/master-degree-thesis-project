"""

Selects texts to parse in accordance to specific data-set format

"""

import shutil
import re
import os


class Selector(object):

    def __init__(self, pattern):
        self.pattern = pattern
        self.tempFiles = []

    def selectArguAna(self, filepath):
        with open(filepath, 'r') as targetFile:
            temp_filename = filepath.replace(".xmi", ".txt")
            with open(temp_filename, 'w') as tempFile:
                shutil.copyfileobj(targetFile, tempFile)

        regex = re.compile("sofaString=.*?\"/>")

        with open(temp_filename, 'r') as tempFile:
            text = tempFile.read().replace('\n', '')
            result = regex.search(text).group(0)

        result = result[len("sofaString=\""):]
        result = result[:-len("\"/>")]
        print("Result: {}".format(result))

        with open(temp_filename, 'w') as tempFile:
            tempFile.write(result)
            self.tempFiles.append(temp_filename)

        return temp_filename

    def processFile(self, filepath):
        if self.pattern == "*.xmi":
            return self.selectArguAna(filepath)
        if self.pattern == "*.data":
            return filepath
        if self.pattern == "*.txt":
            return filepath

    def flush(self):
        for file in self.tempFiles:
            os.remove(file)

        del self.tempFiles[:]
