"""

Data-set documents extractor and parser via MARGOT tool.
Supported data-sets: ArguAna TripAdvisor corpus, CreateDebateCustom data-set

"""

import os
from fnmatch import fnmatch
from shutil import copyfile
from subprocess import Popen


class Reader(object):

    def __init__(self, data_path, extractor_path, extraction_dir, save_dir, selector):

        if not os.path.exists(data_path) or not os.path.exists(extractor_path):
            print("Paths are not correct")
            print("Usage: dataPath extractorPath extractionDir saveDir selector. Paths were not correctly set.")
            return

        if not os.path.isdir(extraction_dir) or not os.path.isdir(save_dir):
            print("Directories are not valid")
            print("Usage: dataPath extractorPath extractionDir saveDir selector. Paths were not correctly set.")
            return

        self.dataPath = data_path
        self.extractorPath = extractor_path
        self.extractionDir = extraction_dir
        self.saveDir = save_dir
        self.selector = selector

    def extractData(self, dataset, extensions=['.txt']):
        """
        Applies MARGOT parsing to each document contained in the given data-set.
        First, it extracts the file name from each document analysed.
        Second, MARGOT process is invoked, which eventually produces a structured output.
        Third, MARGOT parsed document is saved in a specific folder.

        :param dataset: name of the folder in which to save results
        :param extensions: files extensions to consider while analysing data-set.
        :return: None
        """

        directory_path = os.path.join(self.saveDir, dataset)
        if not os.path.isdir(directory_path):
            os.mkdir(directory_path)
            print("Creating {} folder".format(directory_path))
        else:
            print("Folder {} already exists".format(directory_path))

        # Step 1: retrieving files and extracting data via extractor (shell script)
        for path, subdirs, files in os.walk(self.dataPath):
            for filename in files:
                if fnmatch(filename, self.selector.pattern):
                    print("Retrieving file: {}".format(filename))   # debug

                    file_path = os.path.join(path, filename)

                    # Processing file
                    print("File path: {}".format(file_path))
                    file_path = self.selector.processFile(file_path)

                    # MARGOT
                    print("File path: {}".format(file_path))
                    process = Popen(self.extractorPath + "run_margot.sh %s %s" % (file_path, self.extractionDir),
                                    shell=True, cwd=self.extractorPath)
                    process.wait()

                    # Flushing
                    self.selector.flush()

                    # Step 2: moving MARGOT output to desired folder + renaming
                    basename, file_extension = os.path.splitext(filename)
                    for ext in extensions:
                        copyfile(self.extractionDir + "OUTPUT{}".format(ext), directory_path + "/OUTPUT{}".format(ext))
                        print("Copying file {} to {}".format(self.extractionDir + "OUTPUT{}".format(ext),
                                                             directory_path + "/OUTPUT{}".format(ext)))
                        new_name = ''.join([directory_path, "/", basename, ext])
                        old_name = directory_path + "/OUTPUT{}".format(ext)
                        os.rename(old_name, new_name)
