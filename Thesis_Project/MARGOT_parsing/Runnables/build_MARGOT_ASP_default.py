"""

:author: Federico Ruggeri
:date: 14th March 2018
:description:

"""


import os
from itertools import product

import unicodecsv as csv

from MARGOT_parsing.analyzer import Analyzer
from MARGOT_parsing.cleaner import Cleaner
from MARGOT_parsing.Utils.dataset_specific_utils import CreateDebateCustom
from MARGOT_parsing.path_definitions import DATASETS_DIR, PARSED_DATASETS_DIR, DATASET_CREATION_INFO_DIR
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from Supervised_Approach.Utils.iterable_utils import pairwise
from Supervised_Approach.collector import Collector
from collections import OrderedDict

################################
# Variables Section
################################

# Dataset

dataset_name = 'CreateDebateCustomDataset'
parsed_path = os.path.join(PARSED_DATASETS_DIR, dataset_name)
default_path = os.path.join(DATASETS_DIR, dataset_name)

#  Extraction

topics = ['abortion', 'gayRights', 'marijuana', 'obama']

# Filtering

tokenizer = None
balancing_amount = 30000
cleaner = Cleaner(tokenizer=tokenizer, balancing_amount=balancing_amount)
filters = ['tokenizer', 'stop_words', 'balancing']

# Saving

dataset_filename = 'MARGOT_ASP_default_training_balanced.csv'
creation_info = OrderedDict()

################################
# System Section
################################

# Positive examples

analyzer = Analyzer()
positive_files = analyzer.analyze(parsed_path, topics)
keywords = analyzer.keywords
positive_couples = []

for file_info in positive_files:
    file_path = os.path.join(parsed_path, file_info['file'])
    all_evidences, all_claims = analyzer.extract_arguments_from_file(file_path)
    positive_couples += [(evidence, claim, file_info['topic'])
                         for (evidence, claim) in product(all_evidences, all_claims)]

print("Built {} positive couples".format(len(positive_couples)))
print("Built {} distinct positive couples".format(len(set(positive_couples))))

creation_info['initial_positive_couples'] = len(positive_couples)
creation_info['initial_distinct_positive_couples'] = len(set(positive_couples))

# Negative examples

files_by_topic = CreateDebateCustom.get_files_by_topic(topics, parsed_path)
creation_info['topic_info'] = {}
for topic in files_by_topic:
    print('Found {} files for topic {}'.format(len(files_by_topic[topic]), topic))
    creation_info['topic_info'].update({topic: len(files_by_topic[topic])})

negative_couples = []

for topic in topics:
    domains = []
    for meta in os.listdir(os.path.join(default_path, topic)):
        if meta.endswith('.meta'):
            domains.append(meta[0])

    domains = set(domains)

    for domain in domains:
        domain_files = [f for f in files_by_topic[topic]
                        if f.replace("{}_".format(topic), '').startswith(domain)]
        domain_files.sort(key=lambda filename: int(filename.replace("{}_".format(topic), '').replace('.txt', '')[1:]))

        for (head, tail) in pairwise(domain_files):
            head_stance, _ = CreateDebateCustom.get_file_metadata(head.replace("{}_".format(topic), ''),
                                                                  default_path, topic)
            tail_stance, tail_rebuttal = CreateDebateCustom.get_file_metadata(tail.replace("{}_".format(topic), ''),
                                                                              default_path, topic)

            if head_stance != tail_stance and (tail_rebuttal.startswith('oppose') or tail_rebuttal.startswith('null')):
                head_evidences, head_claims = analyzer.extract_arguments_from_file(os.path.join(parsed_path, head))
                tail_evidences, tail_claims = analyzer.extract_arguments_from_file(os.path.join(parsed_path, tail))

                for evidence, claim in product(head_evidences, tail_claims):
                    negative_couples.append((evidence, claim, topic))
                for evidence, claim in product(tail_evidences, head_claims):
                    negative_couples.append((evidence, claim, topic))

            # Extra: positive examples
            # I may apply the same reasoning for positive couples
            if head_stance == tail_stance and (tail_rebuttal.startswith('support') or
                                               tail_rebuttal.startswith('null')):
                head_evidences, head_claims = analyzer.extract_arguments_from_file(os.path.join(parsed_path, head))
                tail_evidences, tail_claims = analyzer.extract_arguments_from_file(os.path.join(parsed_path, tail))

                for evidence, claim in product(head_evidences, tail_claims):
                    positive_couples.append((evidence, claim, topic))
                for evidence, claim in product(tail_evidences, head_claims):
                    positive_couples.append((evidence, claim, topic))

print("Built {} negative couples".format(len(negative_couples)))
print("Updated positive couples: {}".format(len(positive_couples)))

creation_info['initial_negative_couples'] = len(negative_couples)
creation_info['updated_positive_couples'] = len(positive_couples)

positive_couples = list(set(positive_couples))
negative_couples = list(set(negative_couples))

print("Updated positive distinct couples: {}".format(len(positive_couples)))
print("Built {} distinct negative couples".format(len(negative_couples)))

creation_info['updated_distinct_positive_couples'] = len(positive_couples)
creation_info['initial_distinct_negative_couples'] = len(negative_couples)

################################
# Output Section
################################

# Cleaning data

positive_couples = [(evidence, claim, topic, 'link') for (evidence, claim, topic) in positive_couples]
negative_couples = [(evidence, claim, topic, 'no-link') for (evidence, claim, topic) in negative_couples]

couples = {'positive': positive_couples,
           'negative': negative_couples}

if len(filters) > 0:
    couples = cleaner.filter_couples(couples, filters)
    print("Positive couples after cleaning: {}".format(len(couples['positive'])))
    print("Negative couples after cleaning: {}".format(len(couples['negative'])))
    creation_info['positive_couples_after_cleaning'] = len(couples['positive'])
    creation_info['negative_couples_after_cleaning'] = len(couples['negative'])

# Saving results

file_path = os.path.join(POST_PROCESS_DIR, dataset_filename)

print("Saving valid entries to: {}".format(file_path))
with open(file_path, 'wb') as out:
    csv_out = csv.writer(out, encoding="utf-8")
    csv_out.writerow(['Evidence', 'Claim', 'Topic', 'Argument relation'])
    for row in couples['positive'] + couples['negative']:
        csv_out.writerow(row)

path = DATASET_CREATION_INFO_DIR
Collector.save_results(creation_info, 'MARGOT_default', path)
