from MARGOT_parsing.path_definitions import CONFIGS_DIR
from MARGOT_parsing.reader import Reader
from MARGOT_parsing.selector import Selector
from Supervised_Approach.Utils.config_utils import ConfigUtils

"""

Tested on POSIX or OSX only!

"""

if __name__ == '__main__':

    sections = ['Emergent', 'ArguAna', 'CreateDebate', 'Test']

    config = ConfigUtils.load_config('reader.ini', CONFIGS_DIR)

    section = input('Please select section:\n{}\n: '.format(sections))

    while section not in sections:
        print("Invalid section!")
        section = input('Please select section:\n{}\n: '.format(sections))

    dataPath = config.get(section, 'datapath')
    extractorPath = config.get('reader', 'extractorpath')
    extractionDir = config.get('reader', 'extractiondir')
    saveDir = config.get(section, 'savedir')
    dataset = config.get(section, 'dataset')
    pattern = config.get(section, 'pattern')

    reader = Reader(
        data_path=dataPath,
        extractor_path=extractorPath,
        extraction_dir=extractionDir,
        save_dir=saveDir,
        selector=Selector(pattern=pattern))

    reader.extractData(dataset=dataset)
