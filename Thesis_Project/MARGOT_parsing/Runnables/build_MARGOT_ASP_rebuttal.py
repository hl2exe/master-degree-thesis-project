"""

:author: Federico Ruggeri
:date: 14th March 2018
:description:

"""


import os
from itertools import product

import unicodecsv as csv


from MARGOT_parsing.analyzer import Analyzer
from MARGOT_parsing.cleaner import Cleaner
from MARGOT_parsing.Utils.dataset_specific_utils import CreateDebateCustom
from MARGOT_parsing.path_definitions import DATASETS_DIR, PARSED_DATASETS_DIR, DATASET_CREATION_INFO_DIR
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from Supervised_Approach.Utils.iterable_utils import pairwise
from Supervised_Approach.collector import Collector
from collections import OrderedDict

################################
# Variables Section
################################

# Dataset

dataset_name = 'CreateDebateCustomDataset'
parsed_path = os.path.join(PARSED_DATASETS_DIR, dataset_name)
default_path = os.path.join(DATASETS_DIR, dataset_name)

#  Extraction

topics = ['abortion', 'gayRights', 'marijuana', 'obama']

# Filtering

tokenizer = None
balancing_amount = 50000
cleaner = Cleaner(tokenizer=tokenizer, balancing_amount=balancing_amount)
filters = ['tokenizer', 'stop_words', 'punctuation_filtering']

# Saving

dataset_filename = 'MARGOT_ASP_rebuttal_training_balanced.csv'
creation_info = OrderedDict()

################################
# System Section
################################

files_by_topic = CreateDebateCustom.get_files_by_topic(topics, parsed_path)
creation_info['topic_info'] = {}
for topic in files_by_topic:
    print('Found {} files for topic {}'.format(len(files_by_topic[topic]), topic))
    creation_info['topic_info'].update({topic: len(files_by_topic[topic])})

all_couples = []
analyzer = Analyzer()

for topic in topics:
    domains = []
    for meta in os.listdir(os.path.join(default_path, topic)):
        if meta.endswith('.meta'):
            domains.append(meta[0])

    domains = set(domains)

    for domain in domains:
        domain_files = [f for f in files_by_topic[topic]
                        if f.replace("{}_".format(topic), '').startswith(domain)]
        domain_files.sort(key=lambda filename: int(filename.replace("{}_".format(topic), '').replace('.txt', '')[1:]))

        for (head, tail) in pairwise(domain_files):
            head_evidences, head_claims = analyzer.extract_arguments_from_file(os.path.join(parsed_path, head))
            tail_evidences, tail_claims = analyzer.extract_arguments_from_file(os.path.join(parsed_path, tail))

            tail_stance, tail_rebuttal = CreateDebateCustom.get_file_metadata(tail.replace("{}_".format(topic), ''),
                                                                              default_path, topic)

            post_couple_name = '{}_{}_{}'.format(topic, head.replace('{}_'.format(topic), '').replace('.txt', ''),
                                                 tail.replace('{}_'.format(topic), '').replace('.txt', ''))

            # Head to Tail
            for evidence, claim in product(head_evidences, tail_claims):
                all_couples.append((evidence, claim, topic, post_couple_name, tail_rebuttal))

################################
# Output Section
################################

# Cleaning data

creation_info['initial_couples'] = len(all_couples)
all_couples = list(set(all_couples))
creation_info['initial_distinct_couples'] = len(all_couples)

couples = {'all': all_couples}

if len(filters) > 0:
    couples = cleaner.filter_couples(couples, filters)
    creation_info['all_couples_after_cleaning'] = len(couples['all'])

# Saving results

file_path = os.path.join(POST_PROCESS_DIR, dataset_filename)

print("Saving valid entries to: {}".format(file_path))
with open(file_path, 'wb') as out:
    csv_out = csv.writer(out, encoding="utf-8")
    csv_out.writerow(['Evidence', 'Claim', 'Topic', 'Post couple', 'Rebuttal'])
    for row in couples['all']:
        csv_out.writerow(row)

path = DATASET_CREATION_INFO_DIR
Collector.save_results(creation_info, 'MARGOT_rebuttal', path)
