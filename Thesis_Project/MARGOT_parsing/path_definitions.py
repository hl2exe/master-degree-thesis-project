"""

Simple scripts which defines all directories used in this project for saving/using data.

"""

import os

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

DATASETS_DIR = os.path.join(PROJECT_DIR, 'Datasets')

PARSED_DATASETS_DIR = os.path.join(PROJECT_DIR, 'ParsedDatasets')

DATASET_CREATION_INFO_DIR = os.path.join(PROJECT_DIR, 'Dataset_creation_info')

CONFIGS_DIR = os.path.join(PROJECT_DIR, 'Configs')

UTILS_DIR = os.path.join(PROJECT_DIR, 'Utils')
