import os
import pickle
from Supervised_Approach.path_definitions import PICKLE_DIR


class PickleUtility(object):

    @staticmethod
    def check_pickle(filename):
        """
        Checks whether the given filename extension is '.pickle' or not.

        :param filename: filename to check
        :return: True if filename extension is of type '.pickle', False otherwise
        """
        return str(filename).rsplit('.', 1)[-1:][0] == "pickle"

    @staticmethod
    def load_pickle(filename, folder=None, mode='rb'):
        """
        Loads given pickle file.

        :param filename:
        :param folder:
        :param mode:
        :return:
        """

        if folder is not None:
            path_to_save = os.path.join(PICKLE_DIR, folder, filename)
        else:
            path_to_save = os.path.join(PICKLE_DIR, filename)

        with open(path_to_save, mode) as f:
            return pickle.load(f)

    @staticmethod
    def load_pickles(filename, folders=None, mode='rb'):
        """
        Loads a sub-set of pickle files listed in default pickle folder and sub-folders
        that have the same given name.

        :param filename:
        :param folders:
        :param mode:
        :return:
        """

        if not PickleUtility.check_pickle(filename):
            raise ValueError('Given file name does not end with .pickle. Got {}'.format(filename))

        if folders is None:
            return PickleUtility.load_all_pickles(filename, mode)

        union_data = {}

        for path, subdirs, files in os.walk(PICKLE_DIR):
            for f in files:
                if f == filename and any([path.endswith(folder) for folder in folders]):
                    path_to_file = os.path.join(path, f)
                    with open(path_to_file, mode=mode) as pickle_file:
                        union_data.update(pickle.load(pickle_file))

        return union_data

    @staticmethod
    def load_all_pickles(filename, mode='rb'):
        """
        Loads all pickle files listed in default pickle folder and sub-folders that have
        the same given name

        :param filename:
        :param mode:
        :return:
        """

        if not PickleUtility.check_pickle(filename):
            raise ValueError('Given file name does not end with .pickle. Got {}'.format(filename))

        union_data = {}

        for path, subdirs, files in os.walk(PICKLE_DIR):
            for f in files:
                if f == filename:
                    path_to_file = os.path.join(path, f)
                    with open(path_to_file, mode=mode) as pickle_file:
                        union_data.update(pickle.load(pickle_file))

        return union_data

    @staticmethod
    def verify_file(filename):
        """
        Verifies if given filename exists.

        :param filename:
        :return: True if filename exists, False otherwise
        """
        return os.path.isfile(os.path.join(PICKLE_DIR, filename))

    @staticmethod
    def save_to_file(data, filename, folder=None, mode='wb'):
        """
        Saves given data to given pickle file.

        :param data: data structure to store in file
        :param filename: pickle file in which to save data
        :param folder
        :param mode:
        :return: None
        """

        if folder is not None:
            path_to_check = os.path.join(PICKLE_DIR, folder)
            path_to_save = os.path.join(PICKLE_DIR, folder, filename)
        else:
            path_to_check = PICKLE_DIR
            path_to_save = os.path.join(PICKLE_DIR, filename)

        if not os.path.isdir(path_to_check):
            os.mkdir(path_to_check)

        with open(path_to_save, mode) as f:
            pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)
