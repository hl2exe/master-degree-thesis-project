import itertools
import os
import traceback

import nltk
import numpy as np
from nltk.stem.porter import PorterStemmer
from scipy.sparse import csr_matrix
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from config_utils import ConfigUtils
from function_decorators import timed
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from Supervised_Approach.path_definitions import PPDB_DIR


class FeatureTransformerUtils(object):

    pre_loaded_data = {}

    entailment_map = {
        'ReverseEntailment': 0,
        'ForwardEntailment': 1,
        'Equivalence':       2,
        'OtherRelated':      2,
        'Independence':      3
    }

    def __init__(self):

        config = ConfigUtils.load_config('transformer.ini')

        self.pickle_files = dict(config.items('pickle'))

        self.stemmer = PorterStemmer()

        self.path_to_ppdb = os.path.join(PPDB_DIR, config.get('ppdb', 'path'))
        self.ppdb_data = None

    """

    PUBLIC METHODS

    """

    @staticmethod
    def load_pickle_data(utilities_names):

        print("PRE-LOADING PICKLE DATA EXTRACTORS...")
        for name in utilities_names:
            filename = '{}.pickle'.format(name)
            FeatureTransformerUtils.pre_loaded_data[filename] = PickleUtility.load_pickle(filename)

        print("PRE-LOADING COMPLETED. Loaded the following pickle data: \n{}".format(
            FeatureTransformerUtils.pre_loaded_data.keys()))

    @staticmethod
    def calc_entailment_vec(v, w):
        """
        Computes the entailment vector between two words via
        Paraphrase Database.

        :param v: word
        :param w: word
        :return: integer vector
        """

        vec = np.zeros((1, len(set(FeatureTransformerUtils.entailment_map.values()))))

        if v == w:
            vec[0, FeatureTransformerUtils.entailment_map['Equivalence']] = 1
            return vec

        relationships = [(x, s, e) for (x, s, e) in
                         FeatureTransformerUtils.pre_loaded_data['ppdb.pickle'].get(v, [])
                         if e in FeatureTransformerUtils.entailment_map.keys() and x == w]
        if relationships:
            relationship = max(relationships, key=lambda t: t[1])[2]
            vec[0, FeatureTransformerUtils.entailment_map[relationship]] = 1

        return vec

    def to_float(self, s):
        """
        Parses given value to float if possible

        :param s: text
        :return: NaN if not given value is not a number else value to float
        """

        return np.nan if s == 'NA' else float(s)

    def process_ppdb_data(self):
        """
        Builds Paraphrase data from Paraphrase Database (PPDB)

        :return: dictionary whose keys are PPDB sources and whose values are lists containing the following PPDB
         elements:

            1. Target
            2. Abstract value
            3. Entailment values
        """

        with open(self.path_to_ppdb, 'r') as f:
            ppdb = {}
            for line in f:
                data = line.split('|||')
                text_lhs = data[1].strip(' ,')
                text_rhs = data[2].strip(' ,')

                if len(text_lhs.split(' ')) > 1 or len(text_rhs.split(' ')) > 1:
                    continue

                ppdb_score = self.to_float(data[3].strip().split()[0].split('=')[1])
                entailment = data[-1].strip()
                paraphrases = ppdb.setdefault(text_lhs, list())
                paraphrases.append((text_rhs, ppdb_score, entailment))

        return ppdb

    def get_ppdb_processed_data(self):
        """
        Loads and processes ppdb data.

        :return:
        """

        if PickleUtility.verify_file(self.pickle_files['ppdb']):
            return PickleUtility.load_pickle(self.pickle_files['ppdb'])

        return self.process_ppdb_data()

    def tokenize_text(self, text):
        """
        Text is tokenized and stemmed via nltk.

        :param text: text to parse
        :return: list of stemmed tokens
        """

        tokens = nltk.word_tokenize(text)
        stemmed = [self.stemmer.stem(token) for token in tokens]

        return stemmed

    """

    PUBLIC METHODS

    """

    def compute_cosine_similarity_matrix_from_pickle(self):
        """
        Computes the cosine similarity matrix.

        :return: cosine similarity matrix
        """

        cosine_similarity_data = PickleUtility.load_pickle(self.pickle_files['cosine-similarity'])

        mat = np.zeros((len(cosine_similarity_data), 1))
        for i, entry_id in zip(range(0, len(cosine_similarity_data)), cosine_similarity_data):
            mat[i] = cosine_similarity_data[entry_id]

        return csr_matrix(mat)

    @timed
    def compute_cosine_similarity_matrix_from_data(self, cosine_similarity_data):
        """
        Computes the cosine similarity matrix

        :param cosine_similarity_data:
        :return:
        """

        mat = np.zeros((len(cosine_similarity_data), 1))
        for i, item in enumerate(cosine_similarity_data):
            mat[i] = item

        return csr_matrix(mat)

    @timed
    def compute_SVO_entailment_matrix_from_pickle(self, target_list):
        """
        Computes SVO entailment matrix. For each SVO triples couple obtained by the product of
        the first target SVO triples and the second's, the PPDB relationship data is used in order to
        identify the entailment score between each S,V,O couple.

        :param target_list: list of tuples containing targets ids
        :return: SVO entailment matrix
        """

        columns = 3 * len(set(self.entailment_map.values()))
        svo_triples_data = PickleUtility.load_pickle(self.pickle_files['svo-triples'])

        mat = np.zeros((len(target_list), columns))

        for idx, (target1_ID, target2_ID) in enumerate(target_list):
            # print("Considering ID: {}".format(id))
            try:
                target_1_svos = svo_triples_data[target1_ID]
                target_2_svos = svo_triples_data[target2_ID]

                vec = np.zeros((1, columns))

                for (target1_svo, target2_svo) in itertools.product(target_1_svos, target_2_svos):

                    if not target1_svo or not target2_svo:
                        continue

                    entailments = {}
                    for key in target1_svo[0]:
                        entailments[key] = self.calc_entailment_vec(target1_svo[0][key][0], target2_svo[0][key][0])

                    for index, key in zip(range(0, 3), entailments):
                        start_value = len(set(self.entailment_map.values())) * index
                        end_value = start_value + len(set(self.entailment_map.values()))
                        vec[0, start_value:end_value] += entailments[key][0]

                mat[idx, :] = vec

            except Exception, e:
                print(e)
                traceback.print_exc()
                pass

        return csr_matrix(mat)

    def compute_SVO_entailment_matrix_from_data(self, triples_couple_list):
        """
        Computes SVO entailment matrix. For each SVO triples couple obtained by the product of
        the first target SVO triples and the second's, the PPDB relationship data is used in order to
        identify the entailment score between each S,V,O couple.

        :param triples_couple_list: list of tuples containing targets svo triples
        :return: SVO entailment matrix
        """

        columns = 3 * len(set(FeatureTransformerUtils.entailment_map.values()))
        mat = np.zeros((len(triples_couple_list), columns))

        for idx, (target_1_svos, target_2_svos) in enumerate(triples_couple_list):
            # print("Considering ID: {}".format(id))
            try:
                vec = np.zeros((1, columns))

                for (target1_svo, target2_svo) in itertools.product(target_1_svos, target_2_svos):

                    if not target1_svo or not target2_svo:
                        continue

                    entailments = {}
                    for key in target1_svo[0]:
                        entailments[key] = FeatureTransformerUtils.calc_entailment_vec(target1_svo[0][key][0],
                                                                                       target2_svo[0][key][0])

                    for index, key in zip(range(0, 3), entailments):
                        start_value = len(set(FeatureTransformerUtils.entailment_map.values())) * index
                        end_value = start_value + len(set(FeatureTransformerUtils.entailment_map.values()))
                        vec[0, start_value:end_value] += entailments[key][0]

                mat[idx, :] = vec

            except Exception, e:
                print(e)
                traceback.print_exc()
                pass

        return csr_matrix(mat)

    def compute_basic_matrix(self, target_list):
        """
        Computes basic feature matrix via DictVectorizer

        :param target_list:
        :return: csr matrix
        """

        vectorizer = DictVectorizer()

        return vectorizer.fit_transform(target_list)

    def compute_simple_concatenation_matrix_from_pickle(self, target_list, pickle_file):
        """
        Generic transform function that column stacks targets' pickle values into a single matrix.

        :param target_list:
        :param pickle_file:
        :return: csr_matrix
        """

        features = PickleUtility.load_pickle(pickle_file)
        columns = 2 * len(features.values()[0])
        mat = np.zeros((len(target_list), columns))

        for idx, (target1_ID, target2_ID) in enumerate(target_list):

            vec = np.zeros((1, columns))

            for index, target_id in enumerate([target1_ID, target2_ID]):
                for feature_pos, key in enumerate(features[target_id]):
                    vec_pos = feature_pos + len(features[target_id]) * index
                    vec[0, vec_pos] = features[target_id][key]

            mat[idx, :] = vec

        return csr_matrix(mat)

    def compute_ngram_matrix_from_data(self, data):
        """
        Extracts n-grams of a given dataset via sklearn

        :param data: csr matrix obtained by TfidfVectorizer
        :return: raw matrix computed by TfidfTransformer
        """

        transformer = TfidfTransformer()

        return transformer.fit_transform(data)

    def compute_ngram_matrix_from_pickle(self):

        data = PickleUtility.load_pickle(self.pickle_files['ngrams'])
        transformer = TfidfTransformer()

        return transformer.fit_transform(data)

    def compute_skipgrams_matrix_from_data(self, data):
        """
        Extracts skip-grams from a given dataset.

        :param data: csr matrix obtained by TfidfVectorizer
        :return: raw matrix computed by TfidfTransformer
        """

        transformer = TfidfTransformer()
        return transformer.fit_transform(data)

    def compute_skipgrams_matrix_from_pickle(self):
        """
        Loads pre-computed skipgrams from pickle file and
        computes the associated feature matrix via TfidfTransformer

        :return: csr matrix
        """

        data = PickleUtility.load_pickle(self.pickle_files['skipgrams'])
        transformer = TfidfTransformer()
        return transformer.fit_transform(data)
