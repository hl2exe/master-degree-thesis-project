import functools
import operator as op
import os
import re
import sys
from collections import Counter

import gensim
import nltk
import nltk.data
import nltk.draw
import numpy as np
from nltk import word_tokenize, sent_tokenize
from nltk.parse.stanford import StanfordDependencyParser, StanfordParser
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.stem.porter import PorterStemmer
from nltk.util import ngrams

from config_utils import ConfigUtils
from function_utils import FunctionUtils
from pickle_utility import PickleUtility
from Supervised_Approach.Ext.svo import SVO
from Supervised_Approach.path_definitions import NLTK_DIR, STANFORD_PARSER_DIR
from gensim.test.utils import get_tmpfile
from gensim.scripts.glove2word2vec import glove2word2vec

# TODO: move to function
config = ConfigUtils.load_config('extractor.ini')

reload(sys)
sys.setdefaultencoding("utf-8")

# Change this if necessary
java_path = config.get(os.name, 'java_path')
os.environ['JAVAHOME'] = java_path

# Change this if necessary
nltk.data.path.append(NLTK_DIR)


class FeatureExtractorUtils(object):
    punctuations_marks = ['.', ',', '!', '?', ':', ';', '\'', '\"', '-', '(', ')', '[', ']', '{', '}']
    modals = ['can', 'could', 'may', 'might', 'must', 'will', 'would', 'should']

    # Used for pickle pre-loading
    pre_loaded_data = {}

    def __init__(self):

        config = ConfigUtils.load_config('extractor.ini')

        section = os.name

        self.tokenizer_path = 'file:///' + os.path.join(NLTK_DIR, config.get(section, 'tokenizer_path'))
        self.sent_detector = nltk.data.load(self.tokenizer_path)
        stanford_parser_jar = os.path.join(STANFORD_PARSER_DIR,
                                           config.get(section, 'stanford_parser_jar'))
        stanford_parser_models_jar = os.path.join(STANFORD_PARSER_DIR,
                                                  config.get(section, 'stanford_parser_models_jar'))
        self.dependency_parser = StanfordDependencyParser(path_to_jar=stanford_parser_jar,
                                                          path_to_models_jar=stanford_parser_models_jar)

        self.parser = StanfordParser(path_to_jar=stanford_parser_jar, path_to_models_jar=stanford_parser_models_jar)
        self.wordnet_lemmatizer = nltk.WordNetLemmatizer()
        self.stemmer = PorterStemmer()

        self.mpqa_path = config.get(section, 'path_mpqa')

        self.svo = SVO(self.tokenizer_path, stanford_parser_jar, stanford_parser_models_jar)

        self.w2v_path = config.get(section, 'path_w2v')
        self.glove_path = config.get(section, 'path_glove')

        self.embeddings_model = None

    """

    PUBLIC METHODS

    """

    @staticmethod
    def load_pickle_data(features_names, folders=None):

        print("PRE-LOADING PICKLE DATA EXTRACTORS...")

        for name in features_names:
            filename = '{}.pickle'.format(name)
            FeatureExtractorUtils.pre_loaded_data[filename] = PickleUtility.load_pickles(filename, folders)

        print("PRE-LOADING COMPLETED. Loaded the following pickle data: \n{}".format(
            FeatureExtractorUtils.pre_loaded_data.keys()))

    def load_embeddings_model(self, mode='word2vec'):
        """
        Utility function for loading a custom word embedding pre-trained model

        :param mode: either 'word2vec' or 'glove'
        :return: None
        """

        if mode == 'word2vec':
            self.load_w2v_model()
        elif mode == 'glove':
            self.load_glove_model()
        else:
            print("Unknown mode found! Found {}".format(mode))

    def load_glove_model(self):
        """
        Loads GloVe pre-trained embedding model via gensim

        :return: None
        """

        if self.embeddings_model is None:

            print("Loading GloVe model..")
            tmp_file = get_tmpfile('temp_glove_w2v_format.txt')
            glove2word2vec(self.glove_path, tmp_file)

            self.embeddings_model = gensim.models.KeyedVectors.load_word2vec_format(tmp_file)
            print("GloVe model successfully loaded")

        else:
            print("GloVe model already loaded")


    def load_w2v_model(self):
        """
        Loads GoogleNews pre-trained Word2Vec model via gensim.

        :return: None
        """

        if self.embeddings_model is None:
            print("Loading Word2Vec model..")
            self.embeddings_model = gensim.models.KeyedVectors.load_word2vec_format(self.w2v_path, binary=True)
            print("Word2Vec model successfully loaded!")
        else:
            print("Word2Vec model already loaded")

    def get_tokenized_lemmas(self, text):
        """
        Returns lemmas extract from text via WordNet lemmatizer.

        :param text: text to analyse
        :return: list of lemmas extracted from text
        """

        return [self.wordnet_lemmatizer.lemmatize(token).lower() for token in nltk.word_tokenize(text)]

    def convert_word_to_vec(self, word, dim=300):
        """
        Convertes given word to its related word embedding via Word2Vec

        :param word: word to convert
        :param dim: word embedding dimesion
        :return: word embedding vector
        """

        default_value = np.zeros(dim)

        try:
            vec = self.embeddings_model[word]
        except KeyError:
            vec = default_value

        return vec

    def convert_text_to_vec_via_SE(self, text, a, probabilities_dict, dim=300, mode=op.add):
        """
        Sentence embedding as described in "A simple but though-to-beat baseline for
        sentence embeddings", Arora, Liang and Ma, 2017

        :param text: text to convert
        :param a: word weight parameter
        :param probabilities_dict: dictionary containing for each word its corpus probability
        :param dim: word embeddings dimension
        :param mode: sentence embedding mode
        :return: sentence embedding vector
        """

        default_value = np.zeros(dim)

        if not text:
            print("Default vector returned")
            return default_value

        m = []

        tokens = 0
        exceptions = 0

        for token in self.get_tokenized_lemmas(text):
            tokens += 1
            try:
                vec = (a / (a + probabilities_dict[token])) * self.embeddings_model[token]
            except KeyError:
                exceptions += 1
                vec = default_value
                # print("Exception: ", e)

            m.append(vec)

        print("Tokens: {} ---- Exceptions: {}".format(tokens, exceptions))

        return functools.reduce(mode, m) if m else default_value

    def convert_text_to_vec_ABTT(self, text, word_embeddings_dict, mode=op.add):
        """
        Sentence embedding as described in "All-but-the-top: Simple and effective
        postprocessing for word representations", Mu, Bhat and Viswanath, 2017

        :param text: text to convert
        :param word_embeddings_dict: sub-set of the Word2Vec dictionary, computed
        for the given specific scenario
        :param mode: sentence embedding mode
        :return: sentence embedding vector
        """

        dim = word_embeddings_dict[word_embeddings_dict.keys()[0]].shape
        default_value = np.zeros(dim)

        if not text:
            print("Default vector returned")
            return default_value

        m = []

        tokens = 0
        exceptions = 0

        for token in self.get_tokenized_lemmas(text):
            tokens += 1
            try:
                vec = word_embeddings_dict[token]
            except KeyError:
                exceptions += 1
                vec = default_value
                # print("Exception: ", e)

            m.append(vec)

        print("Tokens: {} ---- Exceptions: {}".format(tokens, exceptions))

        return functools.reduce(mode, m) if m else default_value

    def convert_text_to_vec(self, text, dim=300, mode=op.add):
        """
        Maps a given plain text to its word vector obtained by combining each text token word vector
        by a given operator.

        :param text: text to convert to vector
        :param dim: dimension of each word vector
        :param mode: reduction operator to apply to list of vectors in order to obtain a single vector
        :return: vector representation of text
        """

        default_value = np.zeros(dim)

        if not text:
            print("Default vector returned")
            return default_value

        m = []

        tokens = 0
        exceptions = 0

        for token in self.get_tokenized_lemmas(text):
            tokens += 1
            try:
                vec = self.embeddings_model[token]
            except KeyError:
                exceptions += 1
                vec = default_value
                # print("Exception: ", e)

            m.append(vec)

        # print("Tokens: {} ---- Exceptions: {}".format(tokens, exceptions))

        return functools.reduce(mode, m) if m else default_value

    def calculate_cosine_similarity(self, u, v):
        """
        Computes the cosine similarity between two 1-D vectors, u and v"

        :param u: first 1-D vector
        :param v: second 1-D vector
        :return: cosine similarity of given 1-D vectors
        """

        cosine_similarity = np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v))
        return np.nan_to_num(cosine_similarity)

    def compute_cosine_similarity(self, text1, text2):
        """
        Compute the cosine similarity between two texts.
        Wrapper function of _calculate_cosine_similarity

        :param text1:
        :param text2:
        :return: float value
        """

        vector1 = self.convert_text_to_vec(text1)
        vector2 = self.convert_text_to_vec(text2)

        return self.calculate_cosine_similarity(vector1, vector2)

    def compute_unigrams_corpus_probability(self, corpus):
        """
        Computes for each word its probability in the entire corpus

        :param corpus: list of sentences
        :return: dictionary containing for each word its corpus probability
        """

        probabilities_dict = {}
        ngrams_list = []
        for item in corpus:
            ngrams_list += self.get_tokenized_lemmas(item)

        counts = Counter(ngrams_list)
        total_counts = sum(counts.values())

        for key in counts:
            probabilities_dict[key] = float(counts[key]) / total_counts

        return probabilities_dict

    def average_word_length(self, text):
        """
        Calculates the average word length of a given text using NLTK

        :param text: text to analyse
        :return: average word length in characters
        """

        words = nltk.word_tokenize(text)
        return sum(len(word) for word in words) / float(len(words))

    def average_sentence_length(self, text):
        """
        Calculates the average sentence length in words of a given text using NLTK.

        :param text: text to analyse
        :return: average sentence length in words
        """

        # sentences = text.split('.')       naive
        sentences = self.sent_detector.tokenize(text.strip())
        return sum(len(nltk.word_tokenize(sentence)) for sentence in sentences) / float(len(sentences))

    def calculate_long_words_percentage(self, text):
        """
        Calculates the percentage of long words, i.e. words of six or more characters.
        [Anand et al.'s feature]

        :param text: text to analyse
        :return: percentage of long words
        """

        words = nltk.word_tokenize(text)
        return len([word for word in words if len(word) >= 6]) / float(len(words))

    def calculate_pronouns_percentage(self, text):
        """
        Calculates the percentage of pronouns of a given text.
        [Anand et al.'s feature]

        :param text: text to analyse
        :return: percentage of pronouns
        """

        tokenized = nltk.word_tokenize(text)
        tagged = nltk.pos_tag(tokenized)

        pronouns = ["PRP", "PRP$", "WP", "WP$"]

        pronouns_count = len([tag for item, tag in tagged if tag in pronouns])

        return pronouns_count / float(len(tokenized))

    def calculate_sentiment_words_percentage(self, text):
        """
        Calculates the percentage of sentiment words of a given text via VADER analyzer.
        [Anand et al.'s feature]

        :param text: text to analyse
        :return: percentage of sentiment words
        """

        tokenized = nltk.word_tokenize(text)
        sid = SentimentIntensityAnalyzer()
        scores = [sid.polarity_scores(word) for word in tokenized]

        sentiment_words_count = len([item for item in scores if item["compound"] >= 0.5 or item["compound"] <= -0.5])

        return sentiment_words_count / float(len(tokenized))

    def extract_basic_features_from_text(self, data):
        """
        Extracts the following basic features:

            - Number of characters
            - Number of words
            - Number of sentences
            - Average word length
            - Average sentence length
            * Number of question marks ('?')
            * Number of exclamation marks ('!')
            * Number of apostrophes ('"', ''')
            * Number of parentheses couples ('()', '[]', '{}')

        Each * feature is normalized with respect to the text length in terms of characters.

        :param data: text to analyse.
        :return: dictionary whose keys are feature names and whose values are their associated results
        """

        # TODO: parentheses count could be better

        features = {
            'character_amount': lambda text: len(text),
            'words_amount': lambda text: len(nltk.word_tokenize(text)),
            'sentences_amount': lambda text: len(self.sent_detector.tokenize(text)),
            'average_word_length': lambda text: self.average_word_length(text),
            'average_sentence_length': lambda text: self.average_sentence_length(text),
            'long_words_percentage': lambda text: self.calculate_long_words_percentage(text),
            'pronouns_percentage': lambda text: self.calculate_pronouns_percentage(text),
            'sentiment_words_percentage': lambda text: self.calculate_sentiment_words_percentage(text),
            'question_marks': lambda text: float(text.count('?')) / len(text),
            'exclamation_marks': lambda text: float(text.count('!')) / len(text),
            'apostrophes': lambda text: (text.count('\"') + text.count('\'')) / float(len(text)),
            'parentheses': lambda text:
            (text.count('(') + text.count('[') + text.count('{')) / float(len(text))  # naive method
        }

        args = {
            'character_amount': [data],
            'words_amount': [data],
            'sentences_amount': [data],
            'average_word_length': [data],
            'average_sentence_length': [data],
            'long_words_percentage': [data],
            'pronouns_percentage': [data],
            'sentiment_words_percentage': [data],
            'question_marks': [data],
            'exclamation_marks': [data],
            'apostrophes': [data],
            'parentheses': [data]
        }

        return FunctionUtils.apply_functions(features, args)

    def extract_svo_triples(self, text):
        """
        Extract SVO triplets from given text by exploiting external SVO library

        :param text: text to parse in order to extract SVO triplets
        :return: list of SVO triples extracted from text
        """

        sentences = self.svo.sentence_split(text)
        triples = []
        for sent in sentences:
            root_tree = self.svo.get_parse_tree(sent)
            triples.append(self.svo.process_parse_tree(next(root_tree)))

        return triples

    def count_repeated_punctuation(self, text, targets=("?!", "??", "!!", "!?")):
        """
        Counts how many times given sequences of symbols are repeated within given text.
        Each sequence counter is normalized by the number of unigrams extracted from the same text.

        :param text: text to analyse
        :param targets: sequences of symbols to count in the text
        :return: dictionary that for each target sequence of symbols associates its normalized count
        """

        query = re.compile("[^\s\w]+")
        repeated_punctuation = query.findall(text)

        targets_count = {}

        # Normalize
        for match in repeated_punctuation:
            for target in targets:
                targets_count[target] = 0.
                if match.startswith(target):
                    if target in targets_count:
                        targets_count[target] += 1

        unigrams = list(ngrams(nltk.word_tokenize(text), 1))

        for key in targets_count:
            targets_count[key] = float(targets_count[key]) / len(unigrams)

        return targets_count

    def parse_mpqa_lexicon(self):
        """
        Parses MPQA lexicon in order to extracts words polarity indicators.
        Line structure: type=*type* len=*len* word1=*word1* pos1=*pos1* stemmed1=*stemmed* priorpolarity=*priorpolarity

        :return: dictionary whose keys are word terms and whose values are their associated polarities.
        """

        lexicon = {}

        with open(self.mpqa_path, 'r') as f:
            for line in f.readlines():
                tokens = line.split(' ')
                word = tokens[2].split('=')[1].strip()
                polarity = tokens[5].split('=')[1].strip()

                lexicon[word] = polarity

        return lexicon

    ##############################
    # Stab and Gurevych features #
    ##############################

    @staticmethod
    def count_tokens(text):
        """
        Counts the number of tokens in given text

        :param text:
        :return: integer
        """

        tokens_amount = 0
        sentences = sent_tokenize(text)
        for sentence in sentences:
            tokens = word_tokenize(sentence)
            tokens_amount += len(tokens)

        return tokens_amount

    @staticmethod
    def count_punctuation_marks(text):
        """
        Counts the number of punctuation marks in given text.

        :param text:
        :return:
        """

        punctuation_marks_count = 0
        for punctuation_mark in FeatureExtractorUtils.punctuations_marks:
            punctuation_marks_count += text.count(punctuation_mark)

        return punctuation_marks_count
