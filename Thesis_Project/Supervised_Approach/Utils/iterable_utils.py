from itertools import tee, izip


# s -> (s0,s1), (s1,s2), (s2, s3), ...
def pairwise(iterable):
    """
    Iterates over adjacent couples within given iterable

    :param iterable:
    :return: list of adjacent couples
    """

    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)
