
import os

import pandas as pd

from Supervised_Approach.path_definitions import DATASETS_DICT
from collections import OrderedDict


class DatasetUtils(object):

    index_marker = "s_index"

    @staticmethod
    def _consider_index(columns):
        """
        Returns an empty list or one containing index_marker value only.
        Used in order to verify whether to add index column or not

        :param columns: list of column names
        :return: list
        """

        return [value for value in columns if value is DatasetUtils.index_marker]

    @staticmethod
    def get_path_to_dataset(filename, directory='complete'):
        return os.path.join(DATASETS_DICT[directory],
                            filename)

    @staticmethod
    def extract_columns(dataframe, columns):
        """
        Return a new pandas DataFrame with selected given columns.
        In order to extract index column, it is necessary to use DatasetUtils.index_marker field value.

        :param dataframe: pandas DataFrame instance associated to target dataset of interest
        :param columns: list of column names
        :return: pandas DataFrame
        """

        filtered = dataframe[[col for col in columns if col is not DatasetUtils.index_marker]]

        index = DatasetUtils._consider_index(columns)

        if index:
            filtered["ID"] = pd.Series(range(0, len(dataframe))).values
            original_name = filtered.index.names[0]
            filtered["{}_temp".format(original_name)] = filtered.index
            filtered = filtered.set_index("ID")
            filtered = filtered.rename(columns={"{}_temp".format(original_name): original_name})

        return filtered

    @staticmethod
    def build_column_associations(target_dict):
        """
        Builds a dictionary that associates each column of interest to its associated ID column.

        :param target_dict: dictionary as described in dataset_extractor_*.ini files
        :return: dictionary: column ID name : column name
        """

        associations = OrderedDict()

        for index in [1, 2]:
            current_targets = [key for key in target_dict if key.startswith('target{}'.format(index))]
            idx = [target for target in current_targets if target.lower().endswith("id")]
            target = [target for target in current_targets if target not in idx]
            associations[target_dict[idx[0]]] = target_dict[target[0]]

        return associations

    @staticmethod
    def iterate_over_target(path_to_csv, target_name):
        """
        Builds an iterator view for specific target values.

        :param path_to_csv: path to csv file
        :param target_name: column in dataframe instance
        :return: iterator
        """

        data = pd.DataFrame.from_csv(path_to_csv, encoding="utf-8")

        for value in data[target_name].values:
            yield value

    @staticmethod
    def iterate_over_dataset(path_to_csv, target_dict):
        """
        Returns a generator over a given dataset, viewed as a pandas DataFrame.
        generated values are: entry_ID (i.e. pandas DataFrame row index), target columns, target id columns

        :param path_to_csv: path to dataset
        :param target_dict: dictionary as described in dataset_extractor_*.ini files
        :return: generator over dataset rows
        """

        data = pd.DataFrame.from_csv(path_to_csv, encoding="utf-8")
        filtered = DatasetUtils.extract_columns(data, target_dict.values())

        associations = DatasetUtils.build_column_associations(target_dict)

        for idx, row in filtered.iterrows():
            ids = [row[idx] for idx in associations]
            targets = [row[associations[idx]] for idx in associations]

            yield idx, targets, ids

    @staticmethod
    def iterate_over_targets(path_to_csv, target_dict):
        """
        Returns a generator over a given dataset, viewed as a pandas DataFrame.
        generated values are: target ID, i.e. concatenated list of targets ID columns

        :param path_to_csv: path to dataset
        :param target_dict
        :return: generator
        """

        data = pd.DataFrame.from_csv(path_to_csv, encoding="utf-8")
        filtered = DatasetUtils.extract_columns(data, target_dict.values())

        id_dict = {}

        associations = DatasetUtils.build_column_associations(target_dict)

        for _, row in filtered.iterrows():
            for idx in associations:
                id_dict[row[idx]] = row[associations[idx]]

        for idx in id_dict:
            yield idx, id_dict[idx]

    @staticmethod
    def get_proportional_dataframe_slice(dataframe, label_fractions, label_column):

        total_samples = len(dataframe)
        return pd.concat(dataframe.sample(n=int(label_fractions.get(i) * total_samples))
                         for i, dff in dataframe.groupby(label_column))
