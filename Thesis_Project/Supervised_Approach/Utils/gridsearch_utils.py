import os
from itertools import combinations, product

import pandas as pd

from json_utils import load_json_from_file, load_simplejson_from_file
from Supervised_Approach.classifier_factory import ClassifierFactory
from Supervised_Approach.path_definitions import PREBUILT_CV_DIR
from sklearn.model_selection._split import _BaseKFold


def build_emergent_predefined_folds():
    prefix = 'url-versions-2015-06-14-clean-test-fold'
    test_ids = []

    for fold in range(1, 11):
        filename = os.path.join(PREBUILT_CV_DIR, '{}-{}.csv'.format(prefix, fold))
        df = pd.read_csv(filename, encoding='utf-8')
        test_id_values = df[df.columns[0]].values
        test_ids.append(test_id_values)

    return 'Unnamed: 0', test_ids


class PrebuiltCV(_BaseKFold):

    predefined_dict = {
        'emergent': build_emergent_predefined_folds
    }

    def __init__(self, predefined_key):
        _BaseKFold.__init__(self, 10, False, None)
        self.predefined_key = predefined_key
        self.index_name, self.test_folds = PrebuiltCV.predefined_dict[self.predefined_key]()
        self.n_splits = len(self.test_folds)

    def _iter_test_indices(self, X=None, y=None, groups=None):

        for test_fold in self.test_folds:
            yield X[X[self.index_name].isin(test_fold)].index.values

#####################
# Utility Methods
#####################


class GridSearchUtils(object):
    gridsearch_info = load_simplejson_from_file('features_gridsearch.json')
    classifier_info = load_json_from_file('classifiers_gridsearch.json')

    @staticmethod
    def verify_gridsearch_info(gridsearch_features_set, features_set,
                               gridsearch_features_targets_info, features_targets_info):
        """
        Checks whether grid-search feature info is a sub-set of the classifier feature info

        :param gridsearch_features_set: set of grid-search features
        :param features_set: set of features used by the classifier
        :param gridsearch_features_targets_info: dictionary that specifies for each feature
        its classification targets during grid-search
        :param features_targets_info: dictionary that specifies for each feature
        its classification targets
        :return: whether the grid-search feature info is a sub-set of the classifier feature info
        """

        if not gridsearch_features_set:
            return True

        features_set_difference = gridsearch_features_set - features_set

        if features_set_difference:
            print('Detected gridsearch features that do not belong to features_set:\n {}'
                  .format(features_set_difference))
            return False

        for key in gridsearch_features_targets_info:
            difference = gridsearch_features_targets_info[key] - features_targets_info[key]
            if difference:
                print('Detected gridsearch targets that do not belong to features_targets_info:\n {}'
                      .format(difference))
                return False

        return True

    @staticmethod
    def get_features_transformer_weights_combinations(features, verbose=0):
        """
        Computes all possibile features combinations.

        :param features: list of features
        :param verbose
        :return: list of dictionaries entries used as gridsearch parameters.
        """

        # Building weights
        gridsearch_transformer_weights_combinations = []
        for key in features:
            key_dict = dict({(f_key, 1) if f_key == key else (f_key, 0) for f_key in features})
            gridsearch_transformer_weights_combinations.append(key_dict)

        all_features = dict({(f_key, 1) for f_key in features})
        gridsearch_transformer_weights_combinations.append(all_features)

        for dim in range(2, len(features)):
            dim_combinations = combinations(features, dim)
            for item in dim_combinations:
                key_dict = dict({(f_key, 1) if f_key in item else (f_key, 0) for f_key in features})
                gridsearch_transformer_weights_combinations.append(key_dict)

        print("Weight combinations: {}".format(len(gridsearch_transformer_weights_combinations)))

        if verbose == 1:
            for item in gridsearch_transformer_weights_combinations:
                print(item)

        return {'featureunion__transformer_weights': gridsearch_transformer_weights_combinations}

    @staticmethod
    def get_features_gridsearch_parameters(features):
        """
        Retrieves for each feature its grid-search parameters

        :param features: list of features
        :return: dictionary for GridSearchCV object
        """

        suffix = 'featureunion'
        parameters = {}
        for key in features:
            if features[key] in GridSearchUtils.gridsearch_info:
                for param in GridSearchUtils.gridsearch_info[features[key]]['params']:
                    block_name = GridSearchUtils.gridsearch_info[features[key]]['block']
                    param_key = '{0}__{1}__{2}__{3}'.format(suffix, key, block_name, param)
                    parameters[param_key] = GridSearchUtils.gridsearch_info[features[key]]['params'][param]

        return parameters

    @staticmethod
    def get_classifier_algorithm_gridsearch_parameters(classifier):
        """
        Retrieves classifier algorithm grid-search parameters

        :param classifier: classifier instance
        :return: dictionary for GridSearchCV object
        """

        classifier_name = type(classifier).__name__.lower()
        parameters = {}

        if classifier_name in GridSearchUtils.classifier_info:
            for key in GridSearchUtils.classifier_info[classifier_name]:
                param_key = '{}__{}'.format(classifier_name, key)
                parameters[param_key] = GridSearchUtils.classifier_info[classifier_name][key]
        else:
            raise ValueError('Classifier name ({}) not found!'.format(classifier_name))

        return parameters

    @staticmethod
    def get_classifier_types_as_gridsearch_parameter(pipeline, classifiers=None, classifiers_info=None):
        """
        Retrieves all or a sub-set of the supported classifiers for grid-search

        :param pipeline: classifier
        :param classifiers: custom list of classifiers to consider (sub-set of supported types)
        :param classifiers_info: custom info for each classifier init
        :return: dictionary for GridSearchCV object
        """

        classifier_block_name = pipeline.steps[-1][0]
        if classifiers is None:
            classifiers = ClassifierFactory.get_all_classifiers_types(classifiers_info)

        return {classifier_block_name: classifiers}

    @staticmethod
    def get_gridsearch_parameters(args_dict):
        """
        General proxy function that allows to get different types of grid-search parameters
        in accordance to given information (args_dict)

        :param args_dict: dictionary that for each method key (see gridsearch_parameters_methods keys)
        contains the related method args.
        :return: dictionary for GridSearchCV object
        """

        gridsearch_parameters_methods = {
            'features': GridSearchUtils.get_features_gridsearch_parameters,
            'classifier_algorithm': GridSearchUtils.get_classifier_algorithm_gridsearch_parameters,
            'feature_weights': GridSearchUtils.get_features_transformer_weights_combinations,
            'classifier_types': GridSearchUtils.get_classifier_types_as_gridsearch_parameter
        }

        parameters = {}

        for key in args_dict:
            parameters.update(gridsearch_parameters_methods[key](**args_dict[key]))

        return parameters

    @staticmethod
    def get_network_parameters(network_type, gridsearch_params=None):
        """
        Retrieves neural network grid-search parameters

        :param network_type: neural network type
        :param gridsearch_params: custom parameters set to consider
        :return: dictionary for grid-search activity
        """

        name_to_lookup = network_type.lower()
        if name_to_lookup in GridSearchUtils.classifier_info:
            if gridsearch_params is None:
                parameters = GridSearchUtils.classifier_info[name_to_lookup]
            else:
                parameters = {key: value for key, value in GridSearchUtils.classifier_info[name_to_lookup]
                              if key in gridsearch_params}
        else:
            raise ValueError('Network type ({}) not found!'.format(network_type))

        return parameters

    @staticmethod
    def get_network_gridsearch_parameters(args_dict):
        """
        General proxy function that allows to get different types of grid-search parameters
        in accordance to given information (args_dict). Neural network version

        :param args_dict: dictionary that for each method key (see gridsearch_parameters_methods keys)
        contains the related method args.
        :return: dictionary for grid-search activity
        """

        gridsearch_parameters_methods = {
            'params': GridSearchUtils.get_network_parameters
        }

        parameters = {}

        for key in args_dict:
            parameters.update(gridsearch_parameters_methods[key](**args_dict[key]))

        return parameters

    @staticmethod
    def get_network_params_combinations(param_dict):
        """
        Computes all possible parameters combinations for grid-search

        :param param_dict: parameters dictionary obtained from
        get_network_gridsearch_parameters method
        :return: list of parameters combinations for grid-search
        """

        params_combinations = []

        keys = sorted(param_dict)
        comb_tuples = product(*(param_dict[key] for key in keys))

        for comb_tuple in comb_tuples:
            instance_params = {dict_key: comb_item for dict_key, comb_item in zip(keys, comb_tuple)}
            params_combinations.append(instance_params)

        return params_combinations
