import time


def timed(method):
    """
    Alternatively see: profilehooks by Marius Gedminas

    :param method:
    :return:
    """

    def timed(*args, **kw):
        """
        Computes the computation time of given method

        :param args:
        :param kw:
        :return: method result
        """

        start_time = time.time()
        result = method(*args, **kw)
        end_time = time.time()

        print("Execution completed: %r %2.2f sec" % (method.__name__, end_time - start_time))

        return result

    return timed
