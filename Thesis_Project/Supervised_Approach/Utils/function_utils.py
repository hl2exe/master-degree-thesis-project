
class FunctionUtils(object):

    @staticmethod
    def apply_functions(functions, arguments):
        """
        Applies each of the given functions with their associated parameters.

        :param functions: dictionary of functions to apply
        :param arguments: dictionary of arguments to apply to their respective functions. For this reason,
        dictionary key list must coincide with functions dictionary key list.
        :return: dictionary whose keys are feature names and whose values are their associated results
        """

        result = {}

        for key in functions:
            result[key] = functions[key](*arguments[key])

        return result

    @staticmethod
    def build_functions(functions, key, args):
        """
        Builds given functions args with given arguments. Successively returns a nested dictionary like follows:

           { functions_key (i.e. key of functions dictionary) : { key : functions[functions_key] }  }

        :param functions: dictionary whose values are functions
        :param key: any kind of python accepted dict key
        :param args: list of arguments
        :return: dictionary specified as above
        """

        result = {}

        for f_key in functions:
            inner_dict = {key: functions[f_key](*args)}
            result[f_key] = inner_dict

        return result
