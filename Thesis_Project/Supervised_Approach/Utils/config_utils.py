import os

from configparser import ConfigParser

from Supervised_Approach import path_definitions


class ConfigUtils(object):

    @staticmethod
    def load_config(config_file, folder=None):
        """
        Reads given configuration file located in Configs directory.

        :param config_file: name of the configuration file to read
        :param folder: folder in which the config file is located. Default folder is CONFIG_DIR
        :return: ConfigParser instance set to given configuration file
        """

        if folder is None:
            folder = path_definitions.CONFIG_DIR

        config = ConfigParser()
        path_to_file = os.path.join(folder, config_file)
        config.read(path_to_file)

        return config
