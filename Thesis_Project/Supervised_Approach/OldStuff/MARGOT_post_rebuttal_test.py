"""

Rebuttal validation test.

1. Predicting argument relation of evidence - claim couples
2. Building predicted post argument relation
3. Associating true rebuttal values
4. Computing precision/recall

"""


import os

import pandas as pd

from MARGOT_parsing.path_definitions import DATASETS_DIR
from Supervised_Approach.classifier_factory import ClassifierType, ClassifierFactory
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils
from sklearn.metrics import precision_score, recall_score


def get_file_metadata(filename, path, topic):

    meta_filename = '{}.meta'.format(filename)
    stance = None
    rebuttal = None
    with open(os.path.join(path, topic, meta_filename)) as selected:
        lines = selected.readlines()
        try:
            stance = int(lines[2].split('=')[1])
        except Exception, e:
            print(path, topic, meta_filename)
            print(lines, lines[2].split('=')[1])
            print(e.message)

        rebuttal = lines[3].split('=')[1].strip()

    return stance, rebuttal


# Windows multiprocessing requires this, since every process runs the whole module.
if __name__ == "__main__":

    default_path = os.path.join(DATASETS_DIR, 'CreateDebateCustomDataset')

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, inverted_columns_mapping = ScenarioManager\
        .get_associations_mapping(config_file)

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'ce_emnlp_15_argument_structure_prediction_training.csv')
    train_data = pd.read_csv(training_data_path, encoding="utf-8")

    train_data.name = 'argument-structure-prediction'
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data["Argument relation"].values

    # classifier_info = {'class_weight': 'balanced'}
    classifier_info = {'class_weight': 'balanced', 'alpha': 1e-06, 'loss': 'log', 'penalty': 'l1'}
    classifier = ClassifierFactory.factory(ClassifierType.linear_svm, **classifier_info)
    trainer = Trainer()

    custom_pipelines = CustomPipelines(associations_mapping=associations_mapping)
    args, features = custom_pipelines.get_all_features_args()

    # pipeline_info = custom_pipelines.get_pipeline_info('argument-structure-prediction')
    # pipeline = trainer.pipeline(features=pipeline_info['features'], args=pipeline_info['args'], classifier=classifier)

    test_features_list = ['cosine_similarity']
    # test_features_list = [
    #                       'single_ngrams_first',
    #                       'single_ngrams_second',
    #                       # 'single_skipgrams_first',
    #                       # 'single_skipgrams_second',
    #                       'svo_triples',
    #                       'basic_first',
    #                       'basic_second',
    #                       'cosine_similarity',
    #                       'POS_generalized_first',
    #                       'POS_generalized_second',
    #                       'repeated_punctuation_first',
    #                       'repeated_punctuation_second',
    #                       'sngrams_first',
    #                       'sngrams_second',
    #                       'syntactic_first',
    #                       'syntactic_second'
    #                       ]
    # test_features_list = ['count_tokens_first',
    #                       'count_tokens_second',
    #                       'difference_tokens',
    #                       'punctuation_marks_first',
    #                       'punctuation_marks_second',
    #                       'difference_punctuation_marks',
    #                       'word_pairs_binary',
    #                       'first_word_binary_first',
    #                       'first_word_binary_second',
    #                       'couple_first_word_binary',
    #                       'modal_verbs_binary_first',
    #                       'modal_verbs_binary_second',
    #                       'common_elements',
    #                       'production_rules_binary_first',
    #                       'production_rules_binary_second']
    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    # test_args.update(custom_pipelines.get_argument_structure_prediction_specific_args())

    # test_args.update({
    #         'single_ngrams_first': [{'key_mapping': custom_pipelines.first_key_mapping},
    #                                 {'ngram_range': (1, 2),
    #                                  'max_features': 20000}],
    #         'single_ngrams_second': [{'key_mapping': custom_pipelines.second_key_mapping},
    #                                  {'ngram_range': (1, 1),
    #                                   'max_features': 20000}],
    #     })

    pipeline = trainer.build_pipeline(features=test_features, args=test_args, classifier=classifier)

    print("Training classifier..")
    # print("Features: {}".format(pipeline_info['features']))
    print("Features: {}".format(test_features))
    pipeline.fit(train_data, train_target)
    print("Training completed!")

    # Predicting on given data-set

    print("Predicting stances on specified dataset")
    test_data = pd.read_csv(os.path.join(POST_PROCESS_DIR, 'MARGOT_argument_structure_prediction_test_clean_reduced.csv'),
                            encoding="utf-8")
    df_name = "margot-argument-structure-prediction"
    test_data = test_data.rename(columns=columns_mapping[df_name])
    test_data.name = df_name

    predictions = pipeline.predict(test_data)

    # Calculating precision and recall
    # support link predicted: given two consecutive posts p1 and p2 -> evidence (p1) - claim (p2) predicted as support
    # support link true: rebuttal = support

    test_data['Argument Relation'] = predictions

    grouped_test_data = test_data.groupby('Post couple')
    post_dict = {}

    print("Total post couples: ", len(grouped_test_data))
    for key, item in grouped_test_data:
        if len(item.loc[(item['Evidence and Claim post'] == 'head to tail') & (item['Argument Relation'] == 'link')]) > 0:
            post_dict[key] = ['link']
        else:
            post_dict[key] = ['no-link']

    rebuttal_info = {'support': 0, 'non-support': 0}

    for post_data in post_dict:
        topic = post_data.split('_')[0]
        head = post_data.split('_')[1]
        tail = post_data.split('_')[2]
        _, rebuttal = get_file_metadata(tail, default_path, topic)
        if rebuttal == 'support':
            post_dict[post_data].append('link')
            rebuttal_info['support'] += 1
        else:
            post_dict[post_data].append('no-link')
            rebuttal_info['non-support'] += 1

    print("Total support rebuttal: ", rebuttal_info['support'])
    print("Total non-support rebuttal: ", rebuttal_info['non-support'])

    y_pred = [post_dict[key][0] for key in post_dict]
    y_true = [post_dict[key][1] for key in post_dict]

    precision = precision_score(y_true=y_true, y_pred=y_pred, pos_label='link')
    recall = recall_score(y_true=y_true, y_pred=y_pred, pos_label='link')

    print("Precision: {}\nRecall: {}".format(precision, recall))
