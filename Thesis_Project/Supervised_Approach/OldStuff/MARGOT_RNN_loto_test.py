"""

Training classifiers on MARGOT custom data-set.
Successively predictions are made for each topic
related data belonging to CE-EMNLP-15 train and
test set. Precisely, classification report,
confusion matrix, accuracy and f1 score are
computed for each data associated with the
currently selected topic out of the 39 of
interest.
Averages are also computed

"""

import os

import numpy as np
import pandas as pd
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn import metrics

from Supervised_Approach.path_definitions import POST_PROCESS_DIR, COMPLETE_DIR
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from Supervised_Approach.Neural_networks.rnn_models import\
    ArgumentStructurePrediction_RNN
from Supervised_Approach.Neural_networks.Utils import model_metrics
from sklearn.utils import class_weight
from sklearn.model_selection import LeaveOneGroupOut
from Supervised_Approach.collector import Collector
import pprint


def get_vocab_size(sequence):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sequence)

    return len(tokenizer.word_index) + 1


def preprocess_sequence(sequence, model, key, load_pickle=False, pickle_file=None,
                        vocab_size=None, padding_max_length=None, embedding_matrix=None):

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sequence)
    if vocab_size is None:
        vocab_size = len(tokenizer.word_index) + 1
    encoded_data = tokenizer.texts_to_sequences(sequence)
    if padding_max_length is None:
        # padding_max_length = len(max(encoded_data, key=len))
        padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
    print("Padding max length: {}".format(padding_max_length))
    padded_docs = pad_sequences(encoded_data, maxlen=padding_max_length, padding='post')
    errors = 0

    if not load_pickle:
        if embedding_matrix is None:
            embedding_matrix = np.zeros((vocab_size, 300))
        for word, i in tokenizer.word_index.items():
            try:
                embedding_vector = model[word]
            except KeyError:
                errors += 1
                embedding_vector = np.zeros(300)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        if pickle_file is not None:
            if PickleUtility.verify_file(pickle_file):
                existing_data = PickleUtility.load_pickle(pickle_file)
                existing_data.update({key: embedding_matrix})
                PickleUtility.save_to_file(existing_data, pickle_file)
            else:
                PickleUtility.save_to_file({key: embedding_matrix}, pickle_file)
    else:
        embedding_matrix = PickleUtility.load_pickle(pickle_file)[key]

    print("Key errors: {}".format(errors))
    return vocab_size, padding_max_length, padded_docs, embedding_matrix


# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Repeatability
    np.random.seed(7)

    # Step 0: Retrieving positive and negative examples

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'MARGOT_argument_structure_prediction_training_balanced_clean.csv')
    data = pd.read_csv(training_data_path, encoding="utf-8")
    target = data['Argument relation'].values

    claim_data = data['Claim'].values
    evidence_data = data['Evidence'].values

    # Step 1: Loading Word2Vec data

    # mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    # while mode not in [1, 2]:
    #     mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    mode = 2

    pickle_file = None
    utils = FeatureExtractorUtils()

    if mode == 2:
        utils.load_w2v_model()
        # pickle_file = 'MARGOT_argument_structure_prediction_training_balanced_clean.pickle'
        load_pickle = False
    else:
        # pickle_file = input("Please specify pickle file containing pre-computed embedding matrix")
        pickle_file = 'MARGOT_argument_structure_prediction_training_balanced_clean.pickle'
        load_pickle = True

    # Step 2: Pre-processing data for classifier

    # Test data
    test_data = pd.read_csv(os.path.join(POST_PROCESS_DIR,
                                         'ce_emnlp_15_ASP_train_and_test_training.csv'),
                            encoding="utf-8")
    test_target = test_data['Argument relation'].values
    claim_test_data = test_data['Claim'].values
    evidence_test_data = test_data['Evidence'].values

    train_claim_vocab_size = get_vocab_size(claim_data)
    train_evidence_vocab_size = get_vocab_size(evidence_data)
    test_claim_vocab_size = get_vocab_size(claim_test_data)
    test_evidence_vocab_size = get_vocab_size(evidence_test_data)

    selected_claim_vocab_size = max(train_claim_vocab_size, test_claim_vocab_size)
    selected_evidence_vocab_size = max(train_evidence_vocab_size, test_evidence_vocab_size)

    # Building input data

    # Claim
    claim_vocab_size, claim_padding_max_length, claim_padded_docs, claim_embedding_matrix = \
        preprocess_sequence(claim_data, utils.embeddings_model, pickle_file=pickle_file, key='Claim',
                            load_pickle=load_pickle,
                            vocab_size=selected_claim_vocab_size)

    print("Claim embedding matrix shape: {}".format(claim_embedding_matrix.shape))

    # Evidence
    evidence_vocab_size, evidence_padding_max_length, evidence_padded_docs, evidence_embedding_matrix = \
        preprocess_sequence(evidence_data, utils.embeddings_model, pickle_file=pickle_file, key='Evidence',
                            load_pickle=load_pickle,
                            vocab_size=selected_evidence_vocab_size)

    print("Evidence embedding matrix shape: {}".format(evidence_embedding_matrix.shape))

    _, _, claim_test_padded_docs, selected_claim_embedding_matrix = \
        preprocess_sequence(claim_test_data, utils.embeddings_model, key='Claim', vocab_size=selected_claim_vocab_size,
                            padding_max_length=claim_padding_max_length,
                            embedding_matrix=claim_embedding_matrix)

    _, _, evidence_test_padded_docs, selected_evidence_embedding_matrix = \
        preprocess_sequence(evidence_test_data, utils.embeddings_model, key='Evidence',
                            vocab_size=selected_evidence_vocab_size,
                            padding_max_length=evidence_padding_max_length,
                            embedding_matrix=evidence_embedding_matrix)

    print("Claim embedding matrix shape: {}".format(selected_claim_embedding_matrix.shape))
    print("Evidence embedding matrix shape: {}".format(selected_evidence_embedding_matrix.shape))

    print(claim_padded_docs.shape)
    print(evidence_padded_docs.shape)

    # Step 3: Defining classifier
    utils.embeddings_model = None
    classifier = ArgumentStructurePrediction_RNN(ids=['Claim', 'Evidence'],
                                                 vocab_size={'Claim': claim_vocab_size,
                                                             'Evidence': evidence_vocab_size},
                                                 input_length={'Claim': claim_padding_max_length,
                                                               'Evidence': evidence_padding_max_length},
                                                 embedding_vector_length={'Claim': 300,
                                                                          'Evidence': 300},
                                                 weights={'Claim': claim_embedding_matrix,
                                                          'Evidence': evidence_embedding_matrix},
                                                 lstm_neurons={'Claim':    100,
                                                               'Evidence': 100},
                                                 metrics=['accuracy', model_metrics.f1]).build_model()

    # Step 4: Training
    target_tran = [1 if val == 'link' else 0 for val in target]

    weights = class_weight.compute_class_weight('balanced', np.unique(target_tran), target_tran)
    weights_dict = dict(enumerate(weights))
    print("weight classes: {}".format(np.unique(target_tran)))
    print(weights_dict)

    classifier.fit([claim_padded_docs, evidence_padded_docs], target_tran, batch_size=64, epochs=3, verbose=2,
                   shuffle=True, class_weight=weights_dict)

    # Step 5: Predicting data

    logo = LeaveOneGroupOut()
    topic_map = {topic: idx for idx, topic in enumerate(np.unique(test_data['Topic'].values))}
    groups = [topic_map[topic] for topic in test_data['Topic'].values]
    n_splits = logo.get_n_splits(X=test_data, y=test_target, groups=groups)

    scores = {}

    for idx, (_, test_index) in enumerate(logo.split(X=test_data, y=test_target, groups=groups)):
        current_group = np.unique(test_data.iloc[test_index]['Topic'].values)[0]
        print("Predicting on group: {}".format(current_group))

        predictions = classifier.predict_classes([claim_test_padded_docs[test_index], evidence_test_padded_docs[test_index]],
                                                 batch_size=64,
                                                 verbose=2)
        print(predictions.shape)
        predictions = ['link' if item == 1 else 'no-link' for item in predictions]
        print("Total predictions: {}".format(len(predictions)))

        f1 = metrics.f1_score(y_true=test_target[test_index], y_pred=predictions, pos_label='link')
        acc = metrics.accuracy_score(y_true=test_target[test_index], y_pred=predictions)

        predicted_series = pd.Series(predictions, name='Predicted')
        true_series = pd.Series(test_target[test_index], name='True')

        precision, recall, f1score, support = metrics.precision_recall_fscore_support(y_true=test_target[test_index],
                                                                                      y_pred=predictions,
                                                                                      average=None,
                                                                                      pos_label='link',
                                                                                      labels=['link', 'no-link'])
        confusion_matrix = pd.crosstab(true_series, predicted_series)

        scores[current_group] = {'f1-score': f1,
                                 'accuracy': acc,
                                 'report': {'precision': precision,
                                            'recall': recall,
                                            'f1-score': f1score,
                                            'support': support},
                                 'confusion_matrix': confusion_matrix}

    # Step 6: average metrics

    print("Average f1: {}".format(np.mean([scores[idx]['f1-score'] for idx in scores])))
    print("Average accuracy: {}".format(np.mean([scores[idx]['accuracy'] for idx in scores])))

    keys = list(scores.keys())
    accumulator_confusion_matrix = scores[keys[0]]['confusion_matrix']
    for idx in range(1, len(keys)):
        accumulator_confusion_matrix = accumulator_confusion_matrix.add(scores[keys[idx]]['confusion_matrix'],
                                                                        fill_value=0)

    print("Confusion matrix:\n {}".format(accumulator_confusion_matrix))

    per_class_avg_precision = np.mean([scores[idx]['report']['precision'] for idx in scores], axis=0)
    per_class_avg_recall = np.mean([scores[idx]['report']['recall'] for idx in scores], axis=0)
    per_class_avg_f1 = np.mean([scores[idx]['report']['f1-score'] for idx in scores], axis=0)
    per_class_support = np.sum([scores[idx]['report']['support'] for idx in scores], axis=0)
    total_support = np.sum(per_class_support)

    print("Classification report:\n"
          "          precision recall    f1-score   support\n"
          "link:        {0}       {1}      {2}        {3}\n"
          "no-link:     {4}       {5}      {6}        {7}\n"
          "avg/total:   {8}       {9}      {10}       {11}"
        .format(per_class_avg_precision[0],
                per_class_avg_recall[0],
                per_class_avg_f1[0],
                per_class_support[0],
                per_class_avg_precision[1],
                per_class_avg_recall[1],
                per_class_avg_f1[1],
                per_class_support[1],
                (per_class_avg_precision[0] * per_class_support[0] +
                 per_class_avg_precision[1] * per_class_support[1]) / total_support,
                (per_class_avg_recall[0] * per_class_support[0] +
                 per_class_avg_recall[1] *  per_class_support[1]) / total_support,
                (per_class_avg_f1[0] * per_class_support[0] +
                 per_class_avg_f1[1] * per_class_support[1]) / total_support,
                total_support))

    # Saving results

    filename = 'margot-topic-results-ce-emnlp-15-train-and-test.txt'
    folder = None   # default: STATISTICS_DIR
    header_data = 'Classifier: Neural Network'

    # If you want to use saved data I suggest saving to pickle format or csv
    Collector.save_results(pprint.pformat(scores, width=1), filename, folder, header_data)