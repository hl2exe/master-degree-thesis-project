"""

Stance classification gridsearch process.

Examples:

    1. Features calibration:

        classifier_info = {'class_weight': 'balanced'}
        classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info)
        pipeline = trainer.pipeline(test_features, test_args, classifier)

        [Optional]
        transformer_weight_combinations, _ = custom_pipelines.build_transformer_weights_combinations(test_features)

        parameters = custom_pipelines.get_gridsearch_parameters(test_features, classifier_block_name='clf',
                                                                transformer_weights=transformer_weight_combinations)


    2. Feature calibration and classifier choice:


        classifier_info = {'class_weight': 'balanced'}
        classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info)
        pipeline = trainer.pipeline(test_features, test_args, classifier)

        [Optional]
        transformer_weight_combinations, _ = custom_pipelines.build_transformer_weights_combinations(test_features)

        classifiers = [classifier,
                       ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info),
                       ClassifierFactory.factory(ClassifierType.linear_svc, **classifier_info)
                      ]

        parameters = custom_pipelines.get_gridsearch_parameters(test_features, classifier_block_name='clf',
                                                                transformer_weights=transformer_weight_combinations,
                                                                classifiers=classifiers)


    3. Classifier calibration:

        test_args.update(custom_pipelines.specific_args[config_section]())

        classifier_info = {'class_weight': 'balanced'}
        classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info)
        pipeline = trainer.pipeline(test_features, test_args, classifier)

        parameters = {'logisticregression__C': [0.001, 0.01, 0.1, 1.0, 10],
                      'logisticregression__penalty': ['l1', 'l2']}

"""

# TODO: aggiungere richiesta di scelta della CV da usare: default -> StratifiedKFold; custom -> customCV

import pandas as pd

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierFactory, ClassifierType
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.gridsearch_utils import PrebuiltCV

# Windows multiprocessing requires this, since every process runs the whole module.
if __name__ == '__main__':

    train_index = input("Please choose train dataset.."
                        "\nAvailable:\n{}\n: "
                        .format(ScenarioManager.supported_train_datasets))

    while not ScenarioManager.validate_scenario(train_index, ScenarioManager.supported_train_datasets):
        print("Invalid dataset! Please retry..")
        train_index = input("Please choose train dataset.."
                            "\nAvailable:\n{}\n: "
                            .format(ScenarioManager.supported_train_datasets))

    config_section = ScenarioManager.supported_train_datasets[train_index]
    target_column = ScenarioManager.supported_train_targets[train_index]

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    target_dict = dict(config.items(config_section))
    associations = DatasetUtils.build_column_associations(target_dict)

    associations_mapping, columns_mapping, _ = ScenarioManager.get_associations_mapping(config_file)

    # Train data
    train_file = config.get('train', ScenarioManager.supported_train_datasets[train_index])
    path_to_csv_train = DatasetUtils.get_path_to_dataset(train_file, 'train')
    train_data = pd.read_csv(path_to_csv_train, encoding="utf-8")
    train_data.name = ScenarioManager.supported_train_datasets[train_index]
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data[target_column].values

    trainer = Trainer()

    custom_pipelines = CustomPipelines(associations_mapping)
    args, features = custom_pipelines.get_all_features_args()

    # Selecting ones of interest
    test_features_list = [
                          'single_ngrams_first',
                          'single_ngrams_second',
                          'single_skipgrams_first',
                          'single_skipgrams_second',
                          'svo_triples',
                          'basic_first',
                          'basic_second',
                          'cosine_similarity',
                          'POS_generalized_first',
                          'POS_generalized_second',
                          'repeated_punctuation_first',
                          'repeated_punctuation_second',
                          'sngrams_first',
                          'sngrams_second',
                          'syntactic_first',
                          'syntactic_second'
                          ]
    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    # Uncomment this if you have already performed gridsearch on feature parameters
    # test_args.update(custom_pipelines.specific_args[config_section]())
    print(len(test_features))

    # Test data
    # test_file = config.get('test', ScenarioManager.supported_train_datasets[train_index])
    # path_to_csv_test = DatasetUtils.get_path_to_dataset(test_file, 'test')
    # test_data = pd.read_csv(path_to_csv_test, encoding="utf-8")
    # test_data.name = ScenarioManager.supported_train_datasets[train_index]
    # test_data = test_data.rename(columns=columns_mapping[test_data.name])
    # test_target = test_data[target_column].values

    # Classifier
    classifier_info = {'class_weight': 'balanced'}
    classifier = ClassifierFactory.factory(ClassifierType.linear_svm, **classifier_info)
    pipeline = trainer.build_pipeline(test_features, test_args, classifier)

    # Gridsearch on classifier types
    # classifiers = [classifier,
    #                ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info),
    #                ClassifierFactory.factory(ClassifierType.linear_svc, **classifier_info)
    #                ]

    # Get all possible features weights combinations
    # transformer_weight_combinations, _ = custom_pipelines.build_transformer_weights_combinations(test_features)
    # parameters = {'featureunion__transformer_weights': transformer_weight_combinations}

    # Specific classifiers parameters
    # parameters = {'linearsvc__C': [0.001, 0.01, 0.1, 1.0, 10],
    #               'linearsvc__loss': ['hinge', 'squared_hinge']
    #               }
    # parameters = {'logisticregression__C': [0.001, 0.01, 0.1, 1.0, 10],
    #              'logisticregression__penalty': ['l1', 'l2']}
    # parameters = {'sgdclassifier__loss': ['log', 'hinge'],
    #               'sgdclassifier__penalty': ['l1', 'l2', 'elasticnet'],
    #               'sgdclassifier__alpha': [0.001, 0.0001, 0.00001, 0.000001]}

    parameters = {'featureunion__single_ngrams_first__tfidfvectorizer__ngram_range': [(1, 1), (1, 2), (1, 3)],
                  'featureunion__single_ngrams_first__tfidfvectorizer__max_features': [None, 10000, 20000],
                  'featureunion__single_skipgrams_first__skipgramvectorizer__ngram_range': [(2, 2), (2, 3)],
                  'featureunion__single_skipgrams_first__skipgramvectorizer__skip_range': [(2, 2), (2, 3), (2, 4), (2, 5), (2, 6)],
                  'featureunion__single_skipgrams_first__skipgramvectorizer__max_features': [None, 10000, 20000]}

    # Get gridsearch parameters. Classifiers and transformer weights are optional.
    # parameters = custom_pipelines.get_gridsearch_parameters(test_features, classifier_block_name='clf')
    # parameters = custom_pipelines.get_gridsearch_parameters(test_features, classifier_block_name='clf',
    #                                                         transformer_weights=transformer_weight_combinations,
    #                                                         classifiers=classifiers)
    print(parameters)

    print("Running GridSearchCV with following configuration\n"
          "Features: {}\n"
          "Dataset: {}\n".format(test_features,
                                 ScenarioManager.supported_train_datasets[train_index]))

    scoring = {'Accuracy': 'accuracy'}
    tuned = trainer.gridsearch(pipeline, parameters, train_data, train_target, test_features, save_cv_result=True,
                               n_jobs=3,
                               # cv=ms.StratifiedKFold(shuffle=True, random_state=42, n_splits=3),
                               cv=PrebuiltCV('emergent').fold_iterator(train_data, train_target),
                               verbose=5,
                               scoring=scoring,
                               refit='Accuracy')
