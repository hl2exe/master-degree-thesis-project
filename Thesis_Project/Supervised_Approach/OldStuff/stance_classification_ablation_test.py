"""
Ablation test on available stance classification train data-sets.
All the available features are used.
Results are saved in 'ablation' folder in .csv format.

Step 1: determining training data-set
Step 2: retrieving associated test data-set
Step 3: determining feature set
Step 4: ablation test
Step 5: saving results

"""

import argparse
import os
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierFactory, ClassifierType
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import ABLATION_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils
from Supervised_Approach.Utils.dataset_utils import DatasetUtils

# Windows multiprocessing requires this, since every process runs the whole module.
if __name__ == "__main__":

    # Step 1: determining training data-set

    train_index = input("Please choose train dataset.."
                        "\nAvailable:\n{}\n: "
                        .format(ScenarioManager.supported_train_datasets))

    while not ScenarioManager.validate_scenario(train_index, ScenarioManager.supported_train_datasets):
        print("Invalid dataset! Please retry..")
        train_index = input("Please choose train dataset.."
                            "\nAvailable:\n{}\n: "
                            .format(ScenarioManager.supported_train_datasets))

    config_section = ScenarioManager.supported_train_datasets[train_index]
    target_column = ScenarioManager.supported_train_targets[train_index]

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, inverted_columns_mapping = ScenarioManager\
        .get_associations_mapping(config_file)

    # Train data
    train_file = config.get('train', config_section)
    path_to_csv_train = DatasetUtils.get_path_to_dataset(train_file, 'train')
    train_data = pd.read_csv(path_to_csv_train, encoding="utf-8")
    train_data.name = config_section
    train_target = train_data[target_column].values
    train_data = train_data.rename(columns=columns_mapping[train_data.name])

    # Step 2: retrieving associated test data-set

    # Test data
    test_file = config.get('test', config_section)
    path_to_csv_test = DatasetUtils.get_path_to_dataset(test_file, 'test')
    test_data = pd.read_csv(path_to_csv_test, encoding="utf-8")
    test_data.name = config_section
    test_target = test_data[target_column].values
    test_data = test_data.rename(columns=columns_mapping[test_data.name])

    # Step 3: determining feature set

    custom_pipelines = CustomPipelines(associations_mapping=associations_mapping)

    all_args, all_features = custom_pipelines.get_all_features_args()

    test_features_list = [
                          'single_ngrams_first',
                          'single_ngrams_second',
                          'single_skipgrams_first',
                          'single_skipgrams_second',
                          'svo_triples',
                          'basic_first',
                          'basic_second',
                          'cosine_similarity',
                          'POS_generalized_first',
                          'POS_generalized_second',
                          'repeated_punctuation_first',
                          'repeated_punctuation_second',
                          'sngrams_first',
                          'sngrams_second',
                          'syntactic_first',
                          'syntactic_second'
                          ]
    test_args = dict({key: all_args[key] for key in all_args if key in test_features_list})
    test_features = dict({key: all_features[key] for key in all_features if key in test_features_list})

    if config_section in custom_pipelines.specific_args:
        print("Replacing args with emergent specific args")
        test_args.update(custom_pipelines.specific_args[config_section]())
        print(test_args)

    parser = argparse.ArgumentParser(description='run_baseline cmd-line arguments.')
    parser.add_argument('-f',
                        default=','.join(test_features.keys()),
                        type=str)
    parser.add_argument('-s',
                        default=ABLATION_DIR,
                        type=str)
    parser.add_argument('-n',
                        type=str)

    args = parser.parse_args()

    if args.f:
        selected_features = [feature for feature in args.f.split(',')]
        selected_args = dict({key: all_args[key] for key in all_args if key in selected_features})
    else:
        selected_features = test_features.keys()
        selected_args = test_args

    print("Selected features: {}".format(selected_features))

    df_out = pd.DataFrame(index=['{}'.format(feature) for feature in selected_features],
                          columns=['score-cv', 'accuracy-test'], data=np.nan)

    # Step 4: ablation test

    classifier = ClassifierFactory.factory(ClassifierType.linear_svc, class_weight='balanced')
    trainer = Trainer()

    print("Starting ablation test..")
    for to_exclude in selected_features:
        print("######\nExcluding feature: {}\n######".format(to_exclude))
        current_feature_set = [feature for feature in selected_features if feature != to_exclude]
        pipeline_feature_dict = {f_key: test_features[f_key] for f_key in current_feature_set}
        pipeline = trainer.build_pipeline(features=pipeline_feature_dict,
                                          args=selected_args,
                                          classifier=classifier)

        # Cross-validation
        scores = cross_val_score(estimator=pipeline,
                                 X=train_data,
                                 y=train_target,
                                 cv=StratifiedKFold(shuffle=True, random_state=42, n_splits=10))
        cross_validation_score = np.mean(scores)

        # Test score
        pipeline.fit(train_data, train_target)
        test_score = pipeline.score(test_data, test_target)

        df_out.ix[to_exclude, 'score-cv'] = cross_validation_score
        df_out.ix[to_exclude, 'accuracy-test'] = test_score

    print("Ablation test ended!")

    # Step 5: saving results

    if args.s:
        destination_folder = args.s
    else:
        destination_folder = ABLATION_DIR

    if not os.path.isdir(destination_folder):
        print("{} folder does not exist! Creating...".format(destination_folder))
        os.mkdir(destination_folder)

    if args.n:
        filename = args.n
    else:
        date = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        filename = "ablation_test_{}.csv".format(date)

    path = os.path.join(destination_folder, filename)
    print("Saving results to: {}".format(path))
    df_out.to_csv(path, encoding="utf-8")
