"""

Simple MARGOT prediction test
Classifiers are trained on MARGOT data-set and then evaluated on
another corpus.

"""

import os

import pandas as pd
from Supervised_Approach.Utils.config_utils import ConfigUtils
from Supervised_Approach.Utils.dataset_utils import DatasetUtils

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierType, ClassifierFactory
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, PREDICTIONS_DIR
from sklearn import metrics
import numpy as np

# Windows multiprocessing requires this, since every process runs the whole module.
if __name__ == "__main__":

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, inverted_columns_mapping = ScenarioManager\
        .get_associations_mapping(config_file)

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'ce_emnlp_15_ASP_train_and_test_training.csv')
    train_data = pd.read_csv(training_data_path, encoding="utf-8")

    train_data.name = 'argument-structure-prediction'
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data["Argument relation"].values

    # classifier_info = {'class_weight': 'balanced'}
    classifier_info = {'class_weight': 'balanced', 'C': 1.0, 'penalty': 'l1'}
    classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info)
    # classifier = ClassifierFactory.factory(ClassifierType.linear_svm')
    trainer = Trainer()

    custom_pipelines = CustomPipelines(associations_mapping=associations_mapping)
    args, features = custom_pipelines.get_all_features_args()

    # pipeline_info = custom_pipelines.get_pipeline_info('argument-structure-prediction')
    # pipeline = trainer.pipeline(features=pipeline_info['features'], args=pipeline_info['args'], classifier=classifier)

    # test_features_list = ['cosine_similarity']
    test_features_list = [
                          'single_ngrams_first',
                          'single_ngrams_second',
                          # 'single_skipgrams_first',
                          # 'single_skipgrams_second',
                          'svo_triples',
                          'basic_first',
                          'basic_second',
                          'cosine_similarity',
                          'POS_generalized_first',
                          'POS_generalized_second',
                          'repeated_punctuation_first',
                          'repeated_punctuation_second',
                          'sngrams_first',
                          'sngrams_second',
                          'syntactic_first',
                          'syntactic_second'
                          ]
    # test_features_list = ['count_tokens_first',
    #                       'count_tokens_second',
    #                       'difference_tokens',
    #                       'punctuation_marks_first',
    #                       'punctuation_marks_second',
    #                       'difference_punctuation_marks',
    #                       'word_pairs_binary',
    #                       'first_word_binary_first',
    #                       'first_word_binary_second',
    #                       'couple_first_word_binary',
    #                       'modal_verbs_binary_first',
    #                       'modal_verbs_binary_second',
    #                       'common_elements',
    #                       'production_rules_binary_first',
    #                       'production_rules_binary_second']
    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    # test_args.update(custom_pipelines.get_argument_structure_prediction_specific_args())

    test_args.update({
            'single_ngrams_first': [{'key_mapping': custom_pipelines.first_key_mapping},
                                    {'ngram_range': (1, 1),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': custom_pipelines.second_key_mapping},
                                     {'ngram_range': (1, 3),
                                      'max_features': 10000}],
        })

    pipeline = trainer.build_pipeline(features=test_features, args=test_args, classifier=classifier)

    print("Training classifier..")
    # print("Features: {}".format(pipeline_info['features']))
    print("Features: {}".format(test_features))
    pipeline.fit(train_data, train_target)
    print("Training completed!")

    # Predicting on given data-set
    predict_dataset = input("On which dataset do you want to make predictions?"
                            "\nAvailable:\n"
                            "{}\n: ".format(ScenarioManager.supported_predict_datasets))

    while not ScenarioManager.validate_scenario(predict_dataset, ScenarioManager.supported_predict_datasets):
        predict_dataset = input("On which dataset do you want to make predictions?"
                                "\nAvailable:\n"
                                "{}\n: ".format(ScenarioManager.supported_predict_datasets))

    print("Predicting stances on specified dataset")
    test_file = config.get('complete', ScenarioManager.supported_predict_datasets[predict_dataset])
    path_to_csv_test = DatasetUtils.get_path_to_dataset(test_file, 'post_process')
    test_data = pd.read_csv(path_to_csv_test, encoding="utf-8")
    df_name = "margot-argument-structure-prediction"
    test_data = test_data.rename(columns=columns_mapping[df_name])
    test_data.name = df_name
    test_target = test_data['Argument relation'].values
    print(test_data.columns)

    unique, counts = np.unique(test_target, return_counts=True)
    print("True targets stats:\n{}".format(dict(zip(unique, counts))))

    predictions = pipeline.predict(test_data)
    print("predictions labels: {}".format(list(set(predictions))))
    unique, counts = np.unique(predictions, return_counts=True)
    print("predictions stats:\n{}".format(dict(zip(unique, counts))))

    columns = associations_mapping[df_name]
    print("Columns: {}".format(columns))

    # Viewing classification report
    report, confusion_matrix = trainer.view_sklearn_metrics(classifier=pipeline,
                                                            test_data=test_data,
                                                            stance_column='Argument relation')

    accuracy = metrics.accuracy_score(y_pred=predictions, y_true=test_target)
    f1 = metrics.f1_score(y_pred=predictions, y_true=test_target, pos_label='link')

    print("Classification report: \n{}".format(report))
    print("Confusion matrix: \n{}".format(confusion_matrix))
    print("Overall accuracy: {}".format(accuracy))
    print("Overall f1: {}".format(f1))

    # Saving results

    to_save = test_data[columns]
    to_save = to_save.rename(columns=inverted_columns_mapping[df_name])
    prediction_column = "{} to {} stance".format(*inverted_columns_mapping[df_name].values())
    to_save[prediction_column] = pd.Series(predictions).values

    if not os.path.isdir(PREDICTIONS_DIR):
        os.mkdir(PREDICTIONS_DIR)
        print("Creating {} folder!".format(PREDICTIONS_DIR))

    to_save_path = os.path.join(PREDICTIONS_DIR, "{}_train_and_test_stance_classifier_scenario_{}_predictions.csv"
                                .format(test_data.name, 'margot-argument-structure-prediction'))
    to_save.to_csv(to_save_path, encoding="utf-8")
