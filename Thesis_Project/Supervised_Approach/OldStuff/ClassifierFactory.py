from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC

"""

Simple class which implements the factory pattern.

"""


class ClassifierType(object):

    linear_svm = SGDClassifier
    #multinomial_NB = MultinomialNB
    linear_svc = LinearSVC
    logistic_regression = LogisticRegression


class ClassifierFactory(object):

    supported_classifiers = {
        ClassifierType.linear_svm: SGDClassifier,
        #ClassifierType.multinomial_NB: MultinomialNB,
        ClassifierType.linear_svc: LinearSVC,
        ClassifierType.logistic_regression: LogisticRegression
    }

    @staticmethod
    def factory(cl_type, **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: instance type -> ClassifierType field
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        if cl_type in ClassifierFactory.supported_classifiers:
            return ClassifierFactory.supported_classifiers[cl_type](**kwargs)
        else:
            assert 0, "Bad type creation: {}".format(cl_type)

    @staticmethod
    def get_all_classifiers_types(**kwargs):
        """

        :return:
        """

        types = [value for key, value in ClassifierType.__dict__.items() if not key.startswith("__") and
                 not callable(key)]

        return [ClassifierFactory.factory(classifier_type, **kwargs) for classifier_type in types]

    @staticmethod
    def get_instance_from_string(name):

        return eval(name)


if __name__ == '__main__':

    print(ClassifierFactory.get_instance_from_string('sgdclassifier'))