from collections import OrderedDict
from itertools import combinations

import pandas as pd
import sklearn.model_selection as ms
from sklearn import metrics
from sklearn.model_selection import train_test_split, GridSearchCV

from ClassifierFactory import ClassifierFactory
from Collector import Collector
from Path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from PipelineBuilder import PipelineBuilder
from Utils.FunctionDecorators import timed
from Utils.Gridsearch_utils import PrebuiltCV
from Utils.JsonUtils import load_json_from_file, load_simplejson_from_file, save_simplejson_to_file


class Trainer(object):
    """

    PUBLIC METHODS

    """

    features_info = load_json_from_file('features.json')
    args_info = load_simplejson_from_file('features_args.json')
    gridsearch_info = load_simplejson_from_file('features_gridsearch.json')
    classifier_info = load_json_from_file('classifiers_gridsearch.json')
    classification_default_targets_suffixes = load_json_from_file('trainer.json')

    @staticmethod
    def split_and_score(classifier, train_data, target_data, **kwargs):
        """
        Splits given data, i.e. feature_matrix and target_data, into training and test sets respectively.
        Successively, fits classifier on training dat and evaluates its score on test data.

        :param classifier: classifier instance
        :param train_data:
        :param target_data:
        :param test_size: percentage dimension of test set with respect to given data
        :param random_state:
        :return: score on testa data
        """

        X_train, X_test, y_train, y_test = train_test_split(train_data, target_data, **kwargs)
        classifier.fit(X_train, y_train)

        return classifier.score(X_test, y_test)

    @staticmethod
    def split_and_test(classifier, train_data, target_data, **kwargs):
        """
        Splits given data, i.e. feature_matrix and target_data, into training and test sets respectively.
        Successively, fits classifier on training dat and returns its predictions over test data.

        :param classifier:
        :param train_data:
        :param target_data:
        :param test_size:
        :param random_state:
        :return: predictions on test data and true values
        """

        X_train, X_test, y_train, y_test = train_test_split(train_data, target_data, **kwargs)
        classifier.fit(X=X_train, y=y_train)

        return classifier.predict(X_test), y_test

    @staticmethod
    def score_on_test(classifier, train_data, train_targets, test_data, test_targets):
        """
        Fits classifier on train data and scores it on test data.

        :param classifier:
        :param train_data:
        :param train_targets:
        :param test_data:
        :param test_targets:
        :return: score on test data
        """

        classifier.fit(train_data, train_targets)
        return classifier.score(test_data, test_targets)

    @staticmethod
    def view_sklearn_metrics(classifier, test_data, stance_column):
        """
        Computes sklearn.metrics.classification_report function.

        :param classifier:
        :param test_data:
        :param stance_column:
        :return: result of metrics.classification_report function
        """

        target_names = list(set(test_data[stance_column].values))
        predicted = classifier.predict(test_data)
        y_true = test_data[stance_column].values

        predicted_series = pd.Series(predicted, name='Predicted')
        true_series = pd.Series(y_true, name='True')

        report = metrics.classification_report(y_true=y_true, y_pred=predicted, target_names=target_names)
        confusion_matrix = pd.crosstab(true_series, predicted_series)

        return report, confusion_matrix

    @staticmethod
    def build_pipeline(features, args, classifier, transformer_weights=None):
        """
        Builds a pipeline through PipelineBuilder

        :param features:
        :param args:
        :param classifier:
        :param transformer_weights
        :return: pipeline object
        """

        builder = PipelineBuilder(features, args)
        pipeline = builder.build(classifier, transformer_weights)

        return pipeline

    @staticmethod
    def save_pipeline_configuration(pipeline, filename, folder=CLASSIFIER_CONFIGURATIONS_DIR):
        """

        :param pipeline:
        :param filename:
        :param folder:
        :return:
        """

        data_to_save = {'features': {},
                        'classifier': {}}

        pipeline_params = pipeline.get_params(deep=True)
        features_dict = {name: pipeline for name, pipeline in pipeline_params['featureunion__transformer_list']}

        features_set = set()
        features_targets_info = {}
        for feature_name in features_dict:
            for suffix in Trainer.classification_default_targets_suffixes['classification']:
                if feature_name.lower().endswith(suffix.lower()):
                    general_feature_name = feature_name[:len(feature_name) - (len(suffix) + 1)]
                    features_set.add(general_feature_name)
                    if feature_name in features_targets_info:
                        features_targets_info[feature_name].append(suffix)
                    else:
                        features_targets_info[feature_name] = [suffix]

                    block_name = Trainer.gridsearch_info[general_feature_name]['block']
                    args_list = Trainer.gridsearch_info[general_feature_name]['params'].keys()

                    pipeline_steps_dict = {name: step for name, step in
                                           features_dict[feature_name].get_params(deep=True)['steps']}

                    args_step = pipeline_steps_dict[block_name]
                    args_dict = {name: getattr(args_step, name) for name in args_list}

                    data_to_save['features'].update({general_feature_name: {suffix: args_dict}})

                    # Next feature
                    break

        classifier_name = type(pipeline_params['steps'][-1][1]).__name__.lower()
        classifier_step = pipeline_params['steps'][-1][1]
        args_list = Trainer.classifier_info[classifier_name].keys()
        classifier_dict = {name: getattr(classifier_step, name) for name in args_list}

        data_to_save['classifier'][classifier_name] = classifier_dict

        save_simplejson_to_file(filename, data_to_save, folder)

    # TODO: documentation
    @staticmethod
    def load_pipeline_configuration(configuration_file, folder=None):
        """

        :param configuration_file:
        :param folder:
        :return:
        """

        if folder is None:
            folder = CLASSIFIER_CONFIGURATIONS_DIR

        configuration_info = load_simplejson_from_file(configuration_file, folder)

        features_set = set(configuration_info['features'].keys())
        features_targets_info = {}
        additional_args = {}

        for feature_name in configuration_info['features']:
            features_targets_info[feature_name] = list(configuration_info['features'][feature_name].keys())
            additional_args[feature_name] = dict(configuration_info['features'][feature_name].items())

        classifier_name = configuration_info['classifier'].keys()[0]
        classifier_args = configuration_info['classifier'][classifier_name]

        return features_set, features_targets_info, additional_args, classifier_name, classifier_args

    # TODO: documentation
    @staticmethod
    def verify_gridsearch_info(gridsearch_features_set, features_set):
        """

        :param gridsearch_features_set:
        :param features_set:
        :return:
        """

        pass

    # TODO: documentation
    @staticmethod
    def get_associations_mapping(df_name, targets):
        """

        :param df_name:
        :param targets:
        :return:
        """

        return Trainer.get_associations_mapping_from_dict({df_name: targets})

    @staticmethod
    def get_associations_mapping_from_dict(datasets_info):
        """
        Builds data-set and scenario mappings for general classification behaviour.
        Obtained mappings are used by data selectors inside the pipeline in order to filter data correctly in
        accordance to specific case of interest.

        :param datasets_info: dict whose keys are datasets names and whose values are the columns names chosen for
        classification
        :return: 3 dictionaries: associations mapping, columns mapping and inverse columns mapping
        """

        associations_mapping = OrderedDict()
        columns_mapping = OrderedDict()
        inverted_columns_mapping = OrderedDict()

        for df_name in datasets_info:
            associations_mapping[df_name] = ["{}-{}".format(target, df_name) for target in datasets_info[df_name]]
            columns_mapping[df_name] = OrderedDict({target: "{}-{}".format(target, df_name)
                                                    for target in datasets_info[df_name]})
            inverted_columns_mapping[df_name] = OrderedDict({"{}-{}".format(target, df_name): target
                                                             for target in datasets_info[df_name]})

        return associations_mapping, columns_mapping, inverted_columns_mapping

    # TODO: documentation
    @staticmethod
    def build_pipeline_info(features_set, features_targets_info, associations_mapping, additional_args=None,
                            classifier_info=None, classifier_type=None):
        """

        :param features_set:
        :param features_targets_info
        :param associations_mapping:
        :param additional_args:
        :param classifier_info:
        :param classifier_type:
        :return:
        """

        # Step 1: building features -> for each feature in features_list checks whether is appears as a key in
        # features_targets_info. If yes: add feature for each given suffix; if no: add feature for each default suffix

        features = {}
        args = {}

        for feature in features_set:

            if Trainer.features_info[feature]['category'] == 'single_target':
                targets = features_targets_info[feature] if feature in features_targets_info \
                    else Trainer.classification_default_targets_suffixes
                for target_info, suffix in zip([dict((df_name, associations_mapping[df_name][idx])
                                                     for df_name in associations_mapping
                                                     for idx in range(0, len(targets)))],
                                               targets):
                    current_key = '{}_{}'.format(feature, suffix)

                    args[current_key] = [{'key_mapping': target_info}]

                    # pickle + Additional args
                    second_block_dict = {}
                    if Trainer.features_info[feature]['has_pickle']:
                        second_block_dict.update({'utils': None,
                                                  'pre_loaded': True,
                                                  'pickle_mode': True,
                                                  'pickle_file': Trainer.features_info[feature]['pickle_file']})

                    if additional_args is None and Trainer.features_info[feature]['additional_args']:
                        second_block_dict.update(Trainer.args_info[feature])
                    else:
                        second_block_dict.update(additional_args[feature][suffix])

                    args[current_key].append(second_block_dict)

                    # Type
                    features[current_key] = Trainer.features_info[feature]['type']

            else:

                current_key = feature

                args[feature] = [{'key_mapping': associations_mapping}]

                second_block_dict = {}
                if Trainer.features_info[feature]['has_pickle']:
                    second_block_dict.update({'utils': None,
                                              'pre_loaded': True,
                                              'pickle_mode': True,
                                              'pickle_file': Trainer.features_info[feature]['pickle_file']})

                if additional_args is None and Trainer.features_info[feature]['additional_args']:
                    second_block_dict.update(Trainer.args_info[feature])
                else:
                    second_block_dict.update(additional_args[feature])

                args[current_key].append(second_block_dict)

                # Type
                features[current_key] = Trainer.features_info[feature]['type']

        # [Step 2]: define classifier

        classifier = None
        if classifier_type and classifier_info:
            classifier = ClassifierFactory.factory(classifier_type, **classifier_info)

        return features, args, classifier

    # TODO: documentation
    @staticmethod
    def retrieve_cv(cv_mode, prebuilt_cv_key=None, **kwargs):
        """

        :param cv_mode:
        :param prebuilt_cv_key:
        :return:
        """

        if cv_mode == 'default':
            return ms.StratifiedKFold(**kwargs)
        else:
            if cv_mode == 'prebuilt_cv':
                return PrebuiltCV(prebuilt_cv_key).fold_iterator(**kwargs)
            else:
                raise ValueError('Invalid cv_mode!! Possible values: default, prebuilt_cv')

    # TODO: documentation
    @staticmethod
    def transform_to_params(features, classifier):
        """

        :param features:
        :param classifier:
        :return:
        """

        suffix = 'featureunion'
        parameters = {}

        for feature in features:
            if features[feature] in Trainer.gridsearch_info:
                for param in Trainer.gridsearch_info[features[feature]]['params']:
                    block_name = Trainer.gridsearch_info[features[feature]]['block']
                    param_key = '{0}__{1}__{2}__{3}'.format(suffix, feature, block_name, param)
                    parameters[param_key] = Trainer.gridsearch_info[features[feature]]['params'][param]

        classifier_name = type(classifier).__name__.lower()

        for param in Trainer.classifier_info[classifier_name]:
            param_key = '{0}__{1}__{2}'.format(suffix, classifier_name, param)
            parameters[param_key] = Trainer.classifier_info[classifier_name][param]

        return parameters

    @staticmethod
    def get_features_transformer_weights_combinations(features, verbose=0):
        """
        Computes all possibile features combinations.

        :param features: list of features
        :param verbose
        :return: list of dictionaries entries used as gridsearch parameters.
        """

        # Building weights
        gridsearch_transformer_weights_combinations = []
        for key in features:
            key_dict = dict({(f_key, 1) if f_key == key else (f_key, 0) for f_key in features})
            gridsearch_transformer_weights_combinations.append(key_dict)

        all_features = dict({(f_key, 1) for f_key in features})
        gridsearch_transformer_weights_combinations.append(all_features)

        for dim in range(2, len(features)):
            dim_combinations = combinations(features, dim)
            for item in dim_combinations:
                key_dict = dict({(f_key, 1) if f_key in item else (f_key, 0) for f_key in features})
                gridsearch_transformer_weights_combinations.append(key_dict)

        print("Weight combinations: {}".format(len(gridsearch_transformer_weights_combinations)))

        if verbose == 1:
            for item in gridsearch_transformer_weights_combinations:
                print(item)

        return gridsearch_transformer_weights_combinations, all_features

    # TODO: documentation
    @staticmethod
    def get_features_gridsearch_parameters(features):
        """

        :param features:
        :return:
        """

        suffix = 'featureunion'
        parameters = {}
        for key in features:
            if features[key] in Trainer.gridsearch_info:
                for param in Trainer.gridsearch_info[features[key]]['params']:
                    block_name = Trainer.gridsearch_info[features[key]]['block']
                    param_key = '{0}__{1}__{2}__{3}'.format(suffix, key, block_name, param)
                    parameters[param_key] = Trainer.gridsearch_info[features[key]]['params'][param]

        return parameters

    # TODO: documentation
    @staticmethod
    def get_classifier_algorithm_gridsearch_parameters(classifier):
        """

        :param classifier:
        :return:
        """

        classifier_name = type(classifier).__name__.lower()
        parameters = {}

        if classifier_name in Trainer.classifier_info:
            for key in Trainer.classifier_info[classifier_name]:
                parameters[key] = Trainer.classifier_info[classifier_name][key]
        else:
            raise ValueError('Classifier name ({}) not found!'.format(classifier_name))

        return parameters

    # TODO: documentation
    @staticmethod
    def get_classifier_types_as_gridsearch_parameter(pipeline, classifiers=None, classifiers_info=None):
        """

        :param pipeline
        :param classifiers:
        :param classifiers_info
        :return:
        """

        classifier_block_name = pipeline.steps[-1][0]
        if classifiers is None:
            classifiers = ClassifierFactory.get_all_classifiers_types(**classifiers_info)

        return {classifier_block_name: classifiers}

    # TODO: documentation
    @staticmethod
    def get_gridsearch_parameters(args_dict):
        """

        :param args_dict
        :return:
        """

        gridsearch_parameters_methods = {
            'features': Trainer.get_features_gridsearch_parameters,
            'classifier_algorithm': Trainer.get_classifier_algorithm_gridsearch_parameters,
            'feature_weights': Trainer.get_features_transformer_weights_combinations,
            'classifier_types': Trainer.get_classifier_types_as_gridsearch_parameter
        }

        parameters = {}

        for key in args_dict:
            parameters.update(gridsearch_parameters_methods[key](**args_dict[key]))

        return parameters

    # TODO: si puo' rimuovere e tutto il contenuto va messo per esplicito in gridsearch_test
    @staticmethod
    @timed
    def gridsearch(classifier, parameters, train_data, target_data, features,
                   save_cv_result=False, plot=False, index=None, info_verbose=False,
                   gridsearch_results_filename='gridsearch.txt', gridsearch_cv_results_filename='gridsearch', **kwargs):
        """
        Runs a gridsearch on given classifier via given set of parameters.
        Results are then handled by Collector which saves them both in txt and csv format

        :param classifier:
        :param parameters:
        :param train_data:
        :param target_data:
        :param features: set of features used in classifier and that are used by Collector for log purposes
        :param save_cv_result: whether to save gridsearch results in csv format or not
        :param plot
        :param index: Collector's parameter for grouping results.
        :param info_verbose
        :param gridsearch_results_filename
        :param gridsearch_cv_results_filename
        :return: best estimator achieved during grid-search
        """

        gridsearch = GridSearchCV(classifier, parameters, **kwargs)
        gridsearch.fit(train_data, target_data)

        gridsearch_info = Collector.retrieve_gridsearch_info(gridsearch=gridsearch,
                                                             parameters=parameters,
                                                             verbose=info_verbose)
        Collector.save_classifier_results(gridsearch_info, features, gridsearch_results_filename)

        if save_cv_result:
            Collector.save_dict_to_csv(gridsearch.cv_results_, parameters, index, gridsearch_cv_results_filename,
                                       plot=plot, scoring=kwargs['scoring'])

        return gridsearch.best_estimator_
