"""

Leave One Topic Out test on CE-EMNLP-15 train and test sub data-set after
parameter calibration on held-out sub data-set.

"""

import os

import pandas as pd
from sklearn.model_selection import LeaveOneGroupOut

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierFactory, ClassifierType
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils
from sklearn.metrics import f1_score, accuracy_score
from Supervised_Approach.collector import Collector
import pprint
import numpy as np


def f1_scorer(estimator, X, y):
    y_pred = estimator.predict(X)
    return f1_score(y_pred=y_pred, y_true=y, pos_label='link')


# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Variables: change these according to your scenario
    dataset_name = 'ce_emnlp_15_ASP_train_and_test_training.csv'
    train_data_target_column = "Argument relation"
    # classifier_info = {'class_weight': 'balanced'}
    classifier_info = {'class_weight': 'balanced', 'C': 1.0, 'penalty': 'l1'}

    # Step 1: Retrieving positive and negative examples

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, _ = ScenarioManager.get_associations_mapping(config_file)

    training_data_path = os.path.join(POST_PROCESS_DIR, dataset_name)
    train_data = pd.read_csv(training_data_path, encoding="utf-8")

    train_data.name = 'argument-structure-prediction'
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data[train_data_target_column].values

    # Step 2: Defining the classifier

    customPipelines = CustomPipelines(associations_mapping=associations_mapping)
    args, features = customPipelines.get_all_features_args()

    classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info)
    trainer = Trainer()

    # Selecting ones of interest
    # test_features_list = ['cosine_similarity']
    test_features_list = [
                          'single_ngrams_first',
                          'single_ngrams_second',
                          # 'single_skipgrams_first',
                          # 'single_skipgrams_second',
                          'svo_triples',
                          'basic_first',
                          'basic_second',
                          'cosine_similarity',
                          'POS_generalized_first',
                          'POS_generalized_second',
                          'repeated_punctuation_first',
                          'repeated_punctuation_second',
                          'sngrams_first',
                          'sngrams_second',
                          'syntactic_first',
                          'syntactic_second'
                          ]
    # test_features_list = ['count_tokens_first',
    #                       'count_tokens_second',
    #                       'difference_tokens',
    #                       'punctuation_marks_first',
    #                       'punctuation_marks_second',
    #                       'difference_punctuation_marks',
    #                       'word_pairs_binary',
    #                       'first_word_binary_first',
    #                       'first_word_binary_second',
    #                       'couple_first_word_binary',
    #                       'modal_verbs_binary_first',
    #                       'modal_verbs_binary_second',
    #                       'common_elements',
    #                       'production_rules_binary_first',
    #                       'production_rules_binary_second']
    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    # test_args.update(customPipelines.get_argument_structure_prediction_specific_args())

    test_args.update({
            'single_ngrams_first': [{'key_mapping': customPipelines.first_key_mapping},
                                    {'ngram_range': (1, 1),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': customPipelines.second_key_mapping},
                                     {'ngram_range': (1, 3),
                                      'max_features': 10000}],
        })

    pipeline = trainer.build_pipeline(features=test_features, args=test_args, classifier=classifier)

    # pipeline_info = customPipelines.get_pipeline_info('argument-structure-prediction')

    # Step 3: Training

    logo = LeaveOneGroupOut()
    topic_map = {topic: idx for idx, topic in enumerate(np.unique(train_data['Topic'].values))}
    groups = [topic_map[topic] for topic in train_data['Topic'].values]
    n_splits = logo.get_n_splits(X=train_data, y=train_target, groups=groups)

    scores = {}

    for idx, (train_index, test_index) in enumerate(logo.split(X=train_data, y=train_target, groups=groups)):
        print "Running Fold", idx+1, "/", n_splits

        group_excluded = np.unique(train_data.iloc[test_index]['Topic'].values)[0]
        print("Group excluded: {}".format(group_excluded))

        # pipeline = trainer.pipeline(features=pipeline_info['features'], args=pipeline_info['args'],
        #                             classifier=classifier)
        pipeline = trainer.build_pipeline(features=test_features, args=test_args, classifier=classifier)

        X_train, X_test = train_data.iloc[train_index], train_data.iloc[test_index]
        y_train, y_test = train_target[train_index], train_target[test_index]
        pipeline.fit(X=X_train, y=y_train)
        predictions = pipeline.predict(X_test)

        f1 = f1_score(y_pred=predictions, y_true=y_test, pos_label='link')
        accuracy = accuracy_score(y_pred=predictions, y_true=y_test)
        print("Fold scores:\n F1: {}\nAccuracy: {}".format(f1, accuracy))
        scores[group_excluded] = {'f1-score': f1, 'accuracy': accuracy}

    # [Step 4]: Viewing classification results
    print("All scores: {}".format(scores))
    print("Average f1: {}".format(np.mean([scores[idx]['f1-score'] for idx in scores])))
    print("Average accuracy: {}".format(np.mean([scores[idx]['accuracy'] for idx in scores])))

    # Saving results
    filename = 'loto-topic-results.txt'
    folder = None   # default: STATISTICS_DIR
    header_data = 'Classifier: Stance classifier\n' \
                  'LogisticRegression: C: 1.0, penalty: l1\n' \
                  'single_ngrams_first: (1, 1), None\n' \
                  'single_ngrams_second: (1, 3), 10000\n'
    Collector.save_results(pprint.pformat(scores, width=1), filename, folder, header_data)


