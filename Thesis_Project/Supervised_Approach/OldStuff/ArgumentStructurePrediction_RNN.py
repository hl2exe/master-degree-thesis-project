from keras.layers import Dense, Dropout, LSTM, Merge
from keras.layers.embeddings import Embedding
from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM, Input, Lambda, Merge
from keras.layers.embeddings import Embedding
from keras.layers.merge import concatenate
from keras.models import Model, Sequential
import keras.backend as K


class ArgumentStructurePrediction_RNN(object):

    def __init__(self, ids, vocab_size, input_length, embedding_vector_length, weights,
                 lstm_neurons,
                 loss='binary_crossentropy',
                 optimizer='adam', metrics=['accuracy'], add_dropout=False,
                 recurrent_dropout=0., dropout=0.):
        self.ids = ids
        self.vocab_size = vocab_size
        self.input_length = input_length
        self.embedding_vector_length = embedding_vector_length
        self.weights = weights
        self.loss = loss
        self.optimizer = optimizer
        self.metrics = metrics
        self.lstm_neurons = lstm_neurons
        self.add_dropout = add_dropout
        self.recurrent_dropout = recurrent_dropout
        self.dropout = dropout

    def build_model(self):
        branches = []

        for idx in self.ids:
            model = Sequential()
            model.add(Embedding(self.vocab_size[idx], self.embedding_vector_length[idx], weights=[self.weights[idx]],
                                input_length=self.input_length[idx], trainable=False))
            if self.add_dropout:
                model.add(Dropout(self.dropout))

            model.add(LSTM(self.lstm_neurons[idx], recurrent_dropout=self.recurrent_dropout, dropout=self.dropout))

            if self.add_dropout:
                model.add(Dropout(self.dropout))

            # print(model.summary())

            branches.append(model)

        merged = Sequential()
        merged.add(Merge(branches, mode='concat'))
        merged.add(Dense(1, activation='sigmoid'))

        merged.compile(loss=self.loss, optimizer=self.optimizer, metrics=self.metrics)

        # print(merged.summary())

        return merged


if __name__ == '__main__':

    import numpy as np
    classifier = ArgumentStructurePrediction_RNN(ids=['Claim', 'Evidence'],
                                                 vocab_size={'Claim': 6267,
                                                             'Evidence': 8752},
                                                 input_length={'Claim': 11,
                                                               'Evidence': 29},
                                                 embedding_vector_length={'Claim': 300,
                                                                          'Evidence': 300},
                                                 weights={'Claim': np.zeros((6267, 300)),
                                                          'Evidence': np.zeros((8752, 300))},
                                                 lstm_neurons={'Claim': 100,
                                                               'Evidence': 100},
                                                 metrics=['accuracy']).build_model()
