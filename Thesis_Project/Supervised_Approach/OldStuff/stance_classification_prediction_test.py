"""

Stance classification test in which a trained classifier predicts stance towards a given target of
a given input data-set

Step 1: determining training data-set
Step 2: training classifier
Step 3: determining prediction data-set
Step 4: making predictions
Step 5: saving results

Training data-sets: Emergent, FNC-1 (not implemented yet)
Prediction data-sets: CE-ACL-14, CE-EMNLP-15

Please check Supervised_Approach/Configs/trainer.ini file for more info about classification targets

"""

import os

import pandas as pd

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import PREDICTIONS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.classifier_factory import ClassifierType, ClassifierFactory

# Windows multiprocessing requires this, since every process runs the whole module.
if __name__ == "__main__":

    # Step 1: determining training data-set

    scenario = input("Please insert scenario..\nAvailable:\n{}\n: ".format(ScenarioManager.supported_scenarios))

    while not ScenarioManager.validate_scenario(scenario, ScenarioManager.supported_scenarios):
        print("Invalid scenario! Please retry..")
        scenario = input("Please insert scenario..\nAvailable:\n{}\n: ".format(ScenarioManager.supported_scenarios))

    config_section = ScenarioManager.supported_scenario_datasets[scenario]
    target_column = ScenarioManager.supported_scenario_targets[scenario]

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, inverted_columns_mapping = ScenarioManager.\
        get_associations_mapping(config_file)

    train_file = config.get('train', ScenarioManager.supported_scenario_datasets[scenario])
    path_to_csv_train = DatasetUtils.get_path_to_dataset(train_file, 'train')
    train_data = pd.read_csv(path_to_csv_train, encoding="utf-8")
    train_data.name = ScenarioManager.supported_scenario_datasets[scenario]
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data[target_column].values

    # Step 2: training classifier

    # Modify this with your specific classifier
    classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, class_weight='balanced',
                                           C=10, penalty='l1')
    trainer = Trainer()

    custom_pipelines = CustomPipelines(associations_mapping=associations_mapping)
    pipeline_info = custom_pipelines.get_pipeline_info(key=ScenarioManager.supported_scenarios[scenario])
    pipeline = trainer.build_pipeline(features=pipeline_info['features'],
                                      args=pipeline_info['args'],
                                      classifier=classifier,
                                      transformer_weights=pipeline_info['transformer_weights'])

    print("Training classifier..")
    print("Features: {}".format(pipeline_info['features']))
    pipeline.fit(train_data, train_target)
    print("Training completed!")

    # Step 3: determining prediction data-set

    # Classifier performance on test data
    test_train_file = config.get('test',  ScenarioManager.supported_scenario_datasets[scenario])
    path_to_csv_test_train = DatasetUtils.get_path_to_dataset(test_train_file, 'test')
    test_train_data = pd.read_csv(path_to_csv_test_train, encoding="utf-8")
    test_train_data.name = ScenarioManager.supported_scenario_datasets[scenario]
    test_train_data = test_train_data.rename(columns=columns_mapping[test_train_data.name])
    test_train_target = test_train_data[target_column].values

    score = pipeline.score(test_train_data, test_train_target)
    report, confusion_matrix = trainer.view_sklearn_metrics(pipeline, test_train_data,
                                                            ScenarioManager.supported_scenario_targets[scenario])
    print("Test score: {}".format(score))
    print("Classification report on test set:\n{}\nConfusion matrix on test set:\n{}".format(report, confusion_matrix))
    input()

    predict_dataset = input("On which dataset do you want to make predictions?"
                            "\nAvailable:\n"
                            "{}\n: ".format(ScenarioManager.supported_predict_datasets))

    while not ScenarioManager.validate_scenario(predict_dataset, ScenarioManager.supported_predict_datasets):
        predict_dataset = input("On which dataset do you want to make predictions?"
                                "\nAvailable:\n"
                                "{}\n: ".format(ScenarioManager.supported_predict_datasets))

    print("Predicting stances on specified dataset")
    test_file = config.get('complete', ScenarioManager.supported_predict_datasets[predict_dataset])
    path_to_csv_test = DatasetUtils.get_path_to_dataset(test_file, 'complete')
    test_data = pd.read_csv(path_to_csv_test, encoding="utf-8")
    df_name = "{}-{}".format(ScenarioManager.supported_predict_datasets[predict_dataset],
                             ScenarioManager.supported_scenarios[scenario])
    test_data = test_data.rename(columns=columns_mapping[df_name])
    test_data.name = df_name
    print(test_data.columns)

    # Step 4: making predictions

    predictions = pipeline.predict(test_data)
    columns = associations_mapping[df_name]
    print("Columns: {}".format(columns))

    # Step 5: saving results

    to_save = test_data[columns]
    to_save = to_save.rename(columns=inverted_columns_mapping[df_name])
    prediction_column = "{} to {} stance".format(*inverted_columns_mapping[df_name].values())
    to_save[prediction_column] = pd.Series(predictions).values

    if not os.path.isdir(PREDICTIONS_DIR):
        os.mkdir(PREDICTIONS_DIR)
        print("Creating {} folder!".format(PREDICTIONS_DIR))

    to_save_path = os.path.join(PREDICTIONS_DIR, "{}_scenario_predictions.csv"
                                .format(test_data.name))
    to_save.to_csv(to_save_path, encoding="utf-8")
