import os
import numpy as np
import pandas as pd
from datetime import datetime
from Path_definitions import STATISTICS_DIR, CLASSIFIER_CONFIGURATIONS_DIR
from matplotlib import pyplot as plt
import Trainer
from Utils.JsonUtils import save_simplejson_to_file


class Collector(object):

    """
    Simple utility class that save to file given data.
    Used in particular to save gridsearch and classifier results.
    """

    separator_length = 20
    separator_symbol = '#'

    @staticmethod
    def get_default_scorers():

        return [
            'accuracy',
            'average_precision',
            'f1',
            'f1_micro',
            'f1_macro',
            'precision',
            'recall',
            'roc_auc'
        ]

    @staticmethod
    def get_default_colours():

        return [
            'b',
            'g',
            'r',
            'c',
            'm',
            'y',
            'k',
            'w'
        ]

    # TODO: to test
    # @staticmethod
    # def plot_cv_results(cv_results, param, scoring, colours):
    #     """
    #     Taken from: http://scikit-learn.org/stable/auto_examples/model_selection/plot_multi_metric_evaluation.html
    #     :param cv_results:
    #     :param scoring
    #     :param colours
    #     :return:
    #     """
    #
    #     plt.figure(figsize=(13, 13))
    #     plt.title("GridSearchCV evaluating using multiple scorers simultaneously",
    #               fontsize=16)
    #
    #     plt.xlabel(param)
    #     plt.ylabel("Score")
    #     plt.grid()
    #
    #     ax = plt.axes()
    #     ax.set_xlim(0, 402)
    #     ax.set_ylim(0.65, 1)
    #
    #     # Get the regular numpy array from the MaskedArray
    #     X_axis = np.array(cv_results[param].data, dtype=float)
    #
    #     for scorer, color in zip(sorted(scoring), colours):
    #         for sample, style in (('train', '--'), ('test', '-')):
    #             sample_score_mean = cv_results['mean_%s_%s' % (sample, scorer)]
    #             sample_score_std = cv_results['std_%s_%s' % (sample, scorer)]
    #             ax.fill_between(X_axis, sample_score_mean - sample_score_std,
    #                             sample_score_mean + sample_score_std,
    #                             alpha=0.1 if sample == 'test' else 0, color=color)
    #             ax.plot(X_axis, sample_score_mean, style, color=color,
    #                     alpha=1 if sample == 'test' else 0.7,
    #                     label="%s (%s)" % (scorer, sample))
    #
    #         best_index = np.nonzero(cv_results['rank_test_%s' % scorer] == 1)[0][0]
    #         best_score = cv_results['mean_test_%s' % scorer][best_index]
    #
    #         # Plot a dotted vertical line at the best score for that scorer marked by x
    #         ax.plot([X_axis[best_index], ] * 2, [0, best_score],
    #                 linestyle='-.', color=color, marker='x', markeredgewidth=3, ms=8)
    #
    #         # Annotate the best score for that scorer
    #         ax.annotate("%0.2f" % best_score,
    #                     (X_axis[best_index], best_score + 0.005))
    #
    #     plt.legend(loc="best")
    #     plt.grid('off')
    #     plt.show()

    @staticmethod
    def save_results(results, filename, folder=None, header_data=None):
        """
        Save generic given results as string into given filename.

        :param results:
        :param filename:
        :param folder: default folder is Path_definitions.STATISTICS_DIR
        :param header_data: extra descriptive data
        :return: None
        """

        if folder is None:
            folder = STATISTICS_DIR

        if not os.path.isdir(folder):
            print("{} folder does not exist! Creating...")
            os.mkdir(folder)

        file_path = os.path.join(folder, filename)
        print("Saving results to: {}".format(file_path))

        header = Collector.compute_header(header_data)
        to_write = '\n'.join([Collector.compute_separator(),
                              header,
                              str(results)])
        to_write += '\n'

        mode = 'w'
        if os.path.isfile(file_path):
            print("{} file does exist! Appending results...".format(file_path))
            mode = 'a'

        with open(file_path, mode=mode) as f:
            f.write(to_write)

    @staticmethod
    def save_classifier_results(results, features, filename):
        """
        Saves given data to file.
        Collector's results are stored in statistics directory.

        :param results:
        :param features:
        :param filename:
        :return:
        """

        if not os.path.isdir(CLASSIFIER_CONFIGURATIONS_DIR):
            print("{} folder does not exist! Creating...")
            os.mkdir(CLASSIFIER_CONFIGURATIONS_DIR)

        file_path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, filename)
        print("Saving classifier results to: {}".format(file_path))

        header = Collector.compute_header(features)
        to_write = '\n'.join([Collector.compute_separator(),
                              header,
                              results])
        to_write += '\n'

        mode = 'w'
        if os.path.isfile(file_path):
            print("{} file does exist! Appending results...".format(file_path))
            mode = 'a'

        with open(file_path, mode=mode) as f:
            f.write(to_write)

    @staticmethod
    def save_dict_to_csv(dict_data, parameters, index, filename, plot=False, scoring=None):
        """
        Saves gridsearch cv_results.

        :param dict_data:
        :param index
        :param filename:
        :param plot:
        :param scoring:
        :return: None
        """

        if plot and scoring:
            for param in parameters:
                Collector.plot_cv_results(dict_data, param, scoring, Collector.get_default_colours()[:len(scoring)])

        df_out = pd.DataFrame.from_dict(dict_data)
        if index is not None:
            df_out = df_out.set_index(index)
        date = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

        df_out.to_csv(os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, '{}_{}.csv'.format(filename, date)), encoding="utf-8")

    @staticmethod
    def retrieve_gridsearch_info(gridsearch, parameters, verbose=False):
        """
        Retrieves main gridsearch statistics and saves them.

        :param gridsearch:
        :param parameters:
        :param verbose:
        :return:
        """

        to_save = list()
        to_save.append("Best score: {}".format(gridsearch.best_score_))
        to_save.append("Best estimator: {}".format(gridsearch.best_estimator_))
        to_save.append("Best parameters: ")
        for param_name in sorted(parameters.keys()):
            to_save.append("%s: %r" % (param_name, gridsearch.best_params_[param_name]))

        to_save.append("Scorer: {}".format(gridsearch.scorer_.keys()))
        to_save.append("N-splits: {}".format(gridsearch.n_splits_))

        if verbose:
            to_save.append("\n\nCV_RESULTS:\n{}".format(gridsearch.cv_results_))

        return "\n".join(element for element in to_save)

    @staticmethod
    def compute_header(header_data=None):
        date = datetime.now().strftime("%Y-%m-%d %H-%M-%S")

        if header_data is None:
            return '\n'.join([date,
                              "Results:"])
        else:
            return '\n'.join([date,
                              str(header_data),
                              "Results:"])

    @staticmethod
    def compute_separator():
        return '\n'.join([Collector.separator_length * Collector.separator_symbol,
                          'AUTOMATED SEPARATOR',
                          Collector.separator_length * Collector.separator_symbol])
