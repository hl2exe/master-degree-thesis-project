# def extract_crossed_pairs(self, path_to_csv, group_by_columns, stance_column, evidence_claim_columns,
#                          trainer_type, filename, path_to_incoherent_csv):
#
#     # TODO: documentazione
#
#     df = pd.read_csv(path_to_csv, encoding="utf-8")
#
#     # Step 0: incoherent predictions elimination
#
#     print("Removing incoherent rows..")
#     incoherent_df = pd.read_csv(path_to_incoherent_csv, encoding="utf-8")
#     print("Old shape: {}".format(df.shape))
#     df.drop(incoherent_df['ID'].values, inplace=True)
#     print("New shape: {}".format(df.shape))
#
#     # Step 1: selecting original rows that have positive or negative stance
#
#     classes = ScenarioManager.scenario_classes[trainer_type]
#     filtered = df.loc[(df[stance_column] == classes['positive']) | (df[stance_column] == classes['negative'])]
#
#     # Step 2: grouping by main index, i.e. Topic/Article
#
#     grouped = filtered.groupby(group_by_columns)
#
#     # Step 3: for each grouped row, count the occurrences of each class.
#     #         selecting only entries that have #positive >= 1 AND #negative >= 1
#
#     positive = {}
#     negative = {}
#     total_positive = 0
#     total_negative = 0
#
#     for idx, _ in grouped:
#         positive[idx] = 0
#         negative[idx] = 0
#
#     valid_entries = []
#
#     for key, item in grouped:
#         for id, row in item.iterrows():
#             if row[stance_column] == classes['positive']:
#                     positive[key] += 1
#                     total_positive += 1
#
#             if row[stance_column] == classes['negative']:
#                     negative[key] += 1
#                     total_negative += 1
#
#         print("Stats for {}: \nPositive: {}\nNegative: {}".format(key, positive[key], negative[key]))
#         if positive[key] >= 1 and negative[key] >= 1:
#             valid_entries.append((key, positive[key], negative[key]))
#
#     # Debug
#     print("Total positive: {}\nTotal negative: {}".format(total_positive, total_negative))
#     print("Number of valid entries: {}".format((len(valid_entries))))
#
#     if not valid_entries:
#         print("No valid entries were found! Terminating..")
#         return
#     else:
#         print("Valid Entries: {}".format(valid_entries))
#
#     # Step 4: building pairs combinations
#
#     valid_filtered = filtered.loc[filtered[group_by_columns].isin(x[0] for x in valid_entries)]
#     filtered_grouped = valid_filtered.groupby(group_by_columns)
#     crossed_pairs = []
#
#     for key, item in filtered_grouped:
#         for ((id1, item1), (id2, item2)) in itertools.combinations(item.iterrows(), 2):
#             # print("Couple: \n({}, {})".format(item1, item2))
#             if item1[stance_column] != item2[stance_column]:
#                 first_couple = (item1[evidence_claim_columns['claim']],
#                                 item2[evidence_claim_columns['evidence']])
#                 second_couple = (item1[evidence_claim_columns['claim']],
#                                  item2[evidence_claim_columns['evidence']])
#                 crossed_pairs.append(first_couple)
#                 crossed_pairs.append(second_couple)
#
#     # Step 5: saving built crossed pairs
#
#     if not valid_entries:
#         print("No valid entries were found! I'm sorry..")
#     else:
#         file_path = os.path.join(POST_PROCESS_DIR, filename)
#         print("Saving valid entries to: {}".format(file_path))
#         with open(file_path, 'wb') as out:
#             csv_out = csv.writer(out, encoding="utf-8")
#             csv_out.writerow(['Claim', 'Evidence'])
#             for row in crossed_pairs:
#                 csv_out.writerow(row)
#
# # def extract_valid_entries(self, file):
# #     """
# #     Step 1: open instance dataset
# #     Step 2: add predictions column to instance dataset
# #     Step 3: group instance dataset by body ID
# #     Step 4: for each body ID count number of "agree" and "disagree"
# #     Step 5: if #agree >= 1 and #disagree >= 1 then save rows
# #
# #     :return: csv file: Body ID, Claim, Stance
# #     """
# #
# #     # Step 1
# #     instance_data = pd.DataFrame.from_csv(self.instance_dataset)
# #
# #     # Step 2
# #     predictions_data = pd.DataFrame.from_csv(self.predictions_dataset)
# #     instance_data["Stance"] = pd.Series(predictions_data.index.values).values
# #
# #     # Step 3
# #     grouped = instance_data.groupby(instance_data.index)
# #
# #     # Step 4
# #     agree = {}
# #     disagree = {}
# #     total_agreements = 0
# #     total_disagreements = 0
# #
# #     for idx,_ in grouped:
# #         agree[idx] = 0
# #         disagree[idx] = 0
# #
# #     for key, item in grouped:
# #         for id,row in item.iterrows():
# #             if row["Stance"] == "agree":
# #                 agree[key] += 1
# #                 total_agreements += 1
# #
# #             if row["Stance"] == "disagree":
# #                 disagree[key] += 1
# #                 total_disagreements += 1
# #
# #     # Debug
# #     print("Total agreements: {}\nTotal disagreements: {}".format(total_agreements, total_disagreements))
# #
# #     # Step 5
# #     valid_entries = []
# #
# #     for key in agree:
# #         if agree[key] >= 1 and disagree[key] >= 1:
# #             valid_entries.append((key, agree[key], disagree[key]))
# #
# #     print("Number of valid entries: {}".format((len(valid_entries))))
# #     print("Valid Entries: {}".format(valid_entries))
# #
# #     to_save = instance_data.loc[instance_data.index.isin(x[0] for x in valid_entries)]
# #     to_save = to_save.loc[(to_save["Stance"] == "agree") | (to_save["Stance"] == "disagree")]
# #     to_save.index.names = ['Topic']
# #     # to_save = to_save.rename(columns={'Body ID' : 'Claim'})
# #     to_save.to_csv(file)
# #
# # def extract_crossed_pairs(self, claim_dataset, evidence_dataset, article_predictions, claim_predictions, filename):
# #     """
# #     Step 1: open claim dataset
# #     Step 2: add stances columns
# #     Step 3: group by Topic (index)
# #     Step 4: for each topic evaluate boolean expression for each entry
# #     Step 5: open evidence dataset
# #     Step 6: for each topic, for each claim find associated evidence
# #     Step 7: for each valid pair (see schema below), save crossed claim-evidence pairs
# #
# #     Schema:
# #
# #         Stance_Article_1 (w.r.t. Topic)     Stance_Article_2    Stance_Claim_1 (w.r.t. Article)     Stance_Claim_2
# #
# #             Agree                               Agree               Agree                               Disagree
# #
# #             Agree                               Agree               Disagree                            Agree
# #
# #             Agree                               Disagree            Agree                               Agree
# #
# #             Disagree                            Agree               Agree                               Agree
# #
# #     -> Each stance value is interpreted as a boolean value: Agree -> True ; Disagree -> False
# #     -> Stances are evaluated via a logic AND
# #     -> A pair is valid if the two related boolean expressions have different values:
# #
# #                     Valid: False - True
# #                     Valid: True - False
# #
# #     :param claim_dataset:
# #     :param evidence_dataset:
# #     :param claim_predictions: dataset containing predicted stances towards articles
# #     :param article_predictions: dataset containing predicted stances towards topic
# #     :param filename: result file name
# #     :return: csv file containing claims and opposing evidences pairs
# #     """
# #
# #     # Step 1
# #     claim_data = pd.DataFrame.from_csv(claim_dataset, encoding="utf-8")
# #
# #     # Step 2
# #     article_predictions_data = pd.DataFrame.from_csv(article_predictions)
# #     claim_predictions_data = pd.DataFrame.from_csv(claim_predictions)
# #
# #     claim_data["Stance_Article"] = pd.Series(article_predictions_data.index.values).values
# #     claim_data["Stance_Claim"] = pd.Series(claim_predictions_data.index.values).values
# #
# #     claim_data = claim_data.loc[(claim_data["Stance_Article"].isin(["agree", "disagree"])) & (claim_data["Stance_Claim"].isin(["agree", "disagree"]))]
# #
# #     # Step 3
# #     grouped = claim_data.groupby(claim_data.index)
# #
# #     # Step 4
# #     bool_exprs = {}
# #
# #     for key, item in grouped:
# #         for id, row in item.iterrows():
# #             claim = row["Claim"]
# #
# #             if not key in bool_exprs:
# #                 bool_exprs[key] = {}
# #
# #             print("Topic: {} -- Stance_Article: {} -- Stance_Claim: {}".format(key, row["Stance_Article"], row["Stance_Claim"]))
# #             expr = self.bool_dict[row["Stance_Article"]] and self.bool_dict[row["Stance_Claim"]]
# #
# #             if not claim in bool_exprs[key]:
# #                     bool_exprs[key][claim] = {}
# #
# #             bool_exprs[key][claim] = {"Expr" : expr}
# #
# #     # Step 5
# #     evidence_data = pd.DataFrame.from_csv(evidence_dataset, encoding="utf-8")
# #
# #     # Step 6
# #     grouped = evidence_data.groupby(evidence_data.index)
# #
# #     for key, item in grouped:
# #         for id, row in item.iterrows():
# #             if key in bool_exprs:
# #                 claim = row["Claim"]
# #                 if claim in bool_exprs[key]:
# #                     bool_exprs[key][claim]["Evidence"] = row["CDE"]
# #
# #     filtered_bool_expr = deepcopy(bool_exprs)
# #
# #     for key in bool_exprs:
# #         for claim in bool_exprs[key]:
# #             if not "Evidence" in bool_exprs[key][claim]:
# #                 del filtered_bool_expr[key][claim]
# #
# #     # Step 7
# #     valid_entries = []
# #
# #     for key in filtered_bool_expr:
# #         for pair in map(dict, itertools.combinations(filtered_bool_expr[key].items(), 2)):
# #             first_claim = list(pair)[0]
# #             second_claim = list(pair)[1]
# #             if pair[first_claim]["Expr"] != pair[second_claim]["Expr"]:    # boolean values
# #                 first_evidence = pair[first_claim]["Evidence"]
# #                 second_evidence = pair[second_claim]["Evidence"]
# #                 valid_entries.append((first_claim, second_evidence))
# #                 valid_entries.append((second_claim, first_evidence))
# #
# #     print("Number of valid entries: {}".format(len(valid_entries)))
# #     print("Valid entries: {}".format(valid_entries))
# #
# #     if not valid_entries:
# #         print("No valid entries were found! I'm sorry..")
# #     else:
# #         print("Saving valid entries to: {}".format(filename))
# #         with open(filename, 'wb') as out:
# #             csv_out = csv.writer(out)
# #             csv_out.writerow(['Claim', 'Evidence'])
# #             for row in valid_entries:
# #                 csv_out.writerow(row)
# #
# # def extract_crossed_pair_ce_emnlp_15(self, claim_dataset, evidence_dataset, failed_entries, replicate_dataset, claim_predictions, filename):
# #
# #     claim_data = pd.DataFrame.from_csv(claim_dataset, encoding="utf-8", sep="\t")
# #
# #     failed_data = pd.DataFrame.from_csv(failed_entries, encoding="utf-8")
# #     to_remove = failed_data["Claim original text"].values
# #
# #     claim_data = claim_data[claim_data["Claim original text"].isin(to_remove) == False]
# #
# #     replicate_data = pd.DataFrame.from_csv(replicate_dataset, encoding="utf-8")
# #     claim_predictions_data = pd.DataFrame.from_csv(claim_predictions)
# #
# #     claim_data["Stance_Article"] = pd.Series(replicate_data["Stance_Article"].values).values
# #     claim_data["Stance_Claim"] = pd.Series(claim_predictions_data.index.values).values
# #
# #     claim_data = claim_data.loc[(claim_data["Stance_Article"].isin(["agree", "disagree"])) & (claim_data["Stance_Claim"].isin(["agree", "disagree"]))]
# #
# #     # Step 3
# #     grouped = claim_data.groupby(claim_data.index)
# #
# #     # Step 4
# #     bool_exprs = {}
# #
# #     for key, item in grouped:
# #         for id, row in item.iterrows():
# #             claim = row["Claim original text"]
# #
# #             if not key in bool_exprs:
# #                 bool_exprs[key] = {}
# #
# #             expr = self.bool_dict[row["Stance_Article"]] and self.bool_dict[row["Stance_Claim"]]
# #
# #             # print("Topic: {} -- Stance_Article: {} -- Stance_Claim: {} -- Expr: {}".format(key, row["Stance_Article"], row["Stance_Claim"], expr))
# #
# #             if not claim in bool_exprs[key]:
# #                     bool_exprs[key][claim] = {}
# #
# #             bool_exprs[key][claim] = {"Expr": expr}
# #
# #     # Step 5
# #     evidence_data = pd.DataFrame.from_csv(evidence_dataset, encoding="utf-8", sep="\t")
# #
# #     # Step 6
# #     grouped = evidence_data.groupby(evidence_data.index)
# #
# #     for key, item in grouped:
# #         if key in bool_exprs:
# #             for id, row in item.iterrows():
# #                 claim = row["Claim"]
# #                 if claim in bool_exprs[key]:
# #                     bool_exprs[key][claim]["Evidence"] = row["Evidence"]
# #
# #     filtered_bool_expr = deepcopy(bool_exprs)
# #
# #     for key in bool_exprs:
# #         for claim in bool_exprs[key]:
# #             if not "Evidence" in bool_exprs[key][claim]:
# #                 del filtered_bool_expr[key][claim]
# #
# #     # Step 7
# #     valid_entries = []
# #
# #     for key in filtered_bool_expr:
# #         for pair in map(dict, itertools.combinations(filtered_bool_expr[key].items(), 2)):
# #             first_claim = list(pair)[0]
# #             second_claim = list(pair)[1]
# #             # print("Pair Boolean Expressions: {} and {}".format(pair[first_claim]["Expr"], pair[second_claim]["Expr"]) + os.linesep)
# #             if pair[first_claim]["Expr"] != pair[second_claim]["Expr"]:  # boolean values
# #                 print("Pair: {}".format(pair) + os.linesep)
# #                 first_evidence = pair[first_claim]["Evidence"]
# #                 second_evidence = pair[second_claim]["Evidence"]
# #                 valid_entries.append((first_claim, second_evidence))
# #                 valid_entries.append((second_claim, first_evidence))
# #
# #     print("Number of valid entries: {}".format(len(valid_entries)))
# #     # print("Valid entries: {}".format(valid_entries))
# #
# #     if not valid_entries:
# #         print("No valid entries were found! I'm sorry..")
# #     else:
# #         print("Saving valid entries to: {}".format(filename))
# #         with open(filename, 'w') as out:
# #             csv_out = csv.writer(out)
# #             csv_out.writerow(['Claim', 'Evidence'])
# #             for row in valid_entries:
# #                 csv_out.writerow(row)