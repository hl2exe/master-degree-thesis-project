import pandas as pd
import sklearn.model_selection as ms
from sklearn import metrics
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV

from DatasetExtractor import DatasetExtractor
from PipelineBuilder import PipelineBuilder
from Utils.DatasetUtils import DatasetUtils
from Utils.FunctionDecorators import timed


class Trainer(object):

    # TODO: score analyzer (grafici?) -> classe a parte

    def __init__(self, extractor_config_file, extractor_config_section):
        self.extractor = DatasetExtractor(extractor_config_file, extractor_config_section)

    """

    PUBLIC METHODS

    """

    def cross_validate(self, classifier, train_data, target_data, cv=None):
        """
        Returns sklearn cross validation result.

        :param classifier: classifier instance
        :param feature_matrix: train data
        :param target_data: vector of target elements
        :param cv: sklearn.model_selection.cross_val_score cross validation splitting strategy.
        :return: array of scores (floats) of the estimator for each run of the cross validation.
        """

        return cross_val_score(estimator=classifier, X=train_data, y=target_data, cv=cv)

    def split_and_score(self, classifier, train_data, target_data, test_size=0.3, random_state=0):
        """
        Splits given data, i.e. feature_matrix and target_data, into training and test sets respectively.
        Successively, fits classifier on training dat and evaluates its score on test data.

        :param classifier: classifier instance
        :param feature_matrix:
        :param target_data:
        :param test_size: percentage dimension of test set with respect to given data
        :param random_state:
        :return:
        """

        X_train, X_test, y_train, y_test = train_test_split(train_data, target_data, test_size=test_size, random_state=random_state)
        classifier.fit(X_train, y_train)

        return classifier.score(X_test, y_test)

    def split_and_test(self, classifier, train_data, target_data, test_size=0.3, random_state=0):
        """
        Splits given data, i.e. feature_matrix and target_data, into training and test sets respectively.
        Successively, fits classifier on training dat and returns its predictions over test data.

        :param classifier:
        :param feature_matrix:
        :param target_data:
        :param test_size:
        :param random_state:
        :return:
        """

        X_train, X_test, y_train, y_test = train_test_split(train_data, target_data, test_size=test_size, random_state=random_state)
        classifier.fit(X=X_train, y=y_train)

        print(classifier.score(X_test, y_test))
        return classifier.predict(X_test), y_test

    def view_sklearn_metrics(self, classifier, train_data, target_data):
        """
        Computes confusion matrix via sklearn.metrics.classification_report function.

        :param classifier:
        :param feature_matrix:
        :param target_data:
        :return:
        """

        target_names = list(set(target_data))
        predicted, y_values = self.split_and_test(classifier, train_data, target_data)

        return metrics.classification_report(y_values, predicted, target_names=target_names)

    @timed
    def train_classifier(self, classifier, target_column, path_to_csv, features,
                         test_csv, test_target_column, additional_args = None, save_to_pickle = None):
        """
        Sums up the whole classification pipeline: feature extraction [optional],
        matrix computation, classification

        :param path_to_csv:
        :param features:
        :param target_column:
        :param classifier:
        :param extract_features:
        :return:
        """

        print("Extracting features.. This may take a while..")
        extracted_features = self.extractor.extract_features(path_to_csv=path_to_csv,
                                                             features=features,
                                                             additional_args=additional_args,
                                                             save_to_pickle=save_to_pickle)

        feature_matrix = self.extractor.compute_transforms(extracted_features)

        data = pd.DataFrame.from_csv(path_to_csv, encoding="utf-8")
        target_data = data[target_column].values

        print("Target data shape: {}".format(target_data.shape))
        print("Feature matrix shape: {}".format(feature_matrix.shape))

        # scores = self.cross_validate(classifier, feature_matrix, target_data, cv=ms.StratifiedKFold(shuffle=True, random_state=42))
        # print("Scores: {}".format(scores))
        # print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

        classifier.fit(feature_matrix, target_data)
        # print("Score on train data: ", classifier.score(X=feature_matrix, y=target_data))
        #
        # # tests
        #
        # print(self.split_and_score(classifier, feature_matrix, target_data))
        #
        # print(self.view_sklearn_metrics(classifier, feature_matrix, target_data))


        test_data = pd.DataFrame.from_csv(test_csv, encoding="utf-8")
        test_target = test_data[test_target_column].values
        test_features = dataset_extractor.extract_features(test_csv, features, additional_args=additional_args)
        test_matrix = dataset_extractor.compute_transforms(test_features)

        predictions = classifier.predict(test_matrix)
        print(metrics.classification_report(y_true=test_target, y_pred=predictions, target_names=list(set(test_target))))

    def pipeline(self, path_to_csv, target_data, target_dict, features, args, classifier, transformer_weights = None):
        """
        Builds a pipeline through PipelineBuilder and fits it with given training data

        :param path_to_csv:
        :param target_data:
        :param target_dict:
        :param features:
        :param args:
        :param classifier:
        :return: pipeline object
        """

        associations = DatasetUtils.build_column_associations(target_dict)

        builder = PipelineBuilder(features, args)
        pipeline = builder.build(classifier, associations, transformer_weights)

        return pipeline

    @timed
    def gridsearch(self, classifier, parameters, train_data, target_data):

        classifier = GridSearchCV(classifier,
                                  parameters,
                                  cv= ms.StratifiedKFold(shuffle=True, random_state=42),
                                  n_jobs=-1,
                                  verbose=1)
        classifier.fit(train_data, target_data)

        print("Best score: {}".format(classifier.best_score_))
        print("Best estimator: {}".format(classifier.best_estimator_))
        print("Best parameters: ")
        for param_name in sorted(parameters.keys()):
            print("%s: %r" % (param_name, classifier.best_params_[param_name]))

        return classifier

# Test

if __name__ == "__main__":
    path_to_csv = "/home/frgg/Downloads/mscproject/data/emergent/url-versions-2015-06-14-clean.csv"
    config_file = "dataset_extractor_training"
    config_section = "emergent"
    target_column = "articleHeadlineStance"

    df = pd.DataFrame.from_csv(path_to_csv, encoding="utf-8")
    train_target = df[target_column].values

    dataset_extractor = DatasetExtractor(config_file, config_section)

    # train_data = list(DatasetUtils.iterate_over_dataset(path_to_csv, dataset_extractor.targets))

    trainer = Trainer(config_file, config_section)
    associations = DatasetUtils.build_column_associations(dataset_extractor.targets)
    print(associations)

    from FeatureTransformer import FeatureTransformerUtils
    tran = FeatureTransformerUtils()

    features = {
        "article_ngrams": "single_ngrams",
        "article_skipgrams": "single_skipgrams",
        # "basic_1": "basic",
        # "basic_2": "basic"
        # 'my_cs' : "cosine_similarity",
        # 'svo': 'svo_triples'
    }

    args = {
        'article_ngrams': [{'key': associations.values()[0]},
                       {'ngram_range': (1,3)}],
        'article_skipgrams': [{'key': associations.values()[0]},
                         {'ngram_range': (2,3),
                          'skip_range': (2,6)}],
        # 'basic_1': [{'key': associations.keys()[0]},
        #             {'utils': dataset_extractor.f_extractor}],
        # 'basic_2': [{'key': associations.keys()[1]},
        #             {'utils': dataset_extractor.f_extractor}],
        # 'my_cs': [{'key_list': associations.keys()},
        #           {'utils': dataset_extractor.f_extractor},
        #           {'utils': dataset_extractor.f_transformer}],
        # 'svo': [{'key_list': associations.keys()},
        #         {'utils': dataset_extractor.f_extractor},
        #         {'utils': dataset_extractor.f_transformer}]
    }

    transformer_weights = {
        "article_ngrams": 1.,
        "article_skipgrams": 1.
    }

    test_csv = "/home/frgg/Downloads/mscproject/data/emergent/url-versions-2015-06-14-clean-test.csv"
    test_df = pd.DataFrame.from_csv(test_csv, encoding="utf-8")
    test_target = test_df[target_column].values

    # dataset_extractor.f_extractor.load_w2v_model()

    from sklearn.naive_bayes import MultinomialNB
    classifier = MultinomialNB()
    pipeline = trainer.pipeline(path_to_csv, train_target, dataset_extractor.targets, features,
                                args, classifier, None)
    # pipeline.fit(df, train_target)
    # print(pipeline.score(df, train_target))
    # print(pipeline.score(test_df, test_target))
    # print(trainer.cross_validate(pipeline, df, train_target, cv= ms.StratifiedKFold(shuffle=True, random_state=42)))
    # predictions = pipeline.predict(test_df)
    # print(metrics.classification_report(y_true=test_target, y_pred=predictions, target_names=list(set(test_target))))


    # from sklearn.linear_model import SGDClassifier
    # from sklearn.svm import SVC

    parameters = {
        'featureunion__article_ngrams__tfidfvectorizer__ngram_range': [(1,1),(1,2),(1,3)],
        'featureunion__article_ngrams__tfidfvectorizer__max_features': [None, 10000, 20000],
        'featureunion__article_skipgrams__skipgramvectorizer__ngram_range': [(2,2), (2,3)],
        'featureunion__article_skipgrams__skipgramvectorizer__skip_range': [(2,2), (2,3), (2,4), (2,5), (2,6)],

        'featureunion__article_skipgrams__skipgramvectorizer__max_features': [None, 10000, 20000],
        'featureunion__transformer_weights': [{"article_ngrams": 1., "article_skipgrams": 1.},
                                              {"article_ngrams": 0, "article_skipgrams": 1.},
                                              {"article_ngrams": 1, "article_skipgrams": 0}]
        # 'classifier' : [classifier, SVC(), SGDClassifier()]
    }

    tuned = trainer.gridsearch(pipeline,
                       parameters,
                       df,
                       train_target)
