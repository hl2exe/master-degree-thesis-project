from configparser import ConfigParser
from psutil import virtual_memory
from scipy.sparse import hstack, csr_matrix

from Supervised_Approach.feature_extractor import FeatureExtractorUtils
from Supervised_Approach.feature_transformer import FeatureTransformerUtils
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.function_utils import FunctionUtils
from Supervised_Approach.Utils.pickle_utility import PickleUtility


class DatasetExtractor(object):


    def __init__(self, config_file, config_section):

        config = ConfigParser()
        config.read(config_file, encoding='utf-8')

        self.targets = dict(config.items(config_section))

        self.f_extractor = FeatureExtractorUtils()
        self.f_transformer = FeatureTransformerUtils()

        self.features_info = {
            'cosine_similarity': {
                'extractor_type': 'text_couple',
                # 'extractor': self.f_extractor.extract_cosine_similarity_feature,
                'pickle_file': 'cosine_similarity.pickle',
                'requires_w2v': True,
                'transformer': self.f_transformer.compute_cosine_similarity_matrix_from_data,
            },
            'basic': {
                'extractor_type': 'single_target',
                # 'extractor': self.f_extractor.extract_basic_features,
                'pickle_file': 'basic.pickle',
                'requires_w2v': False,
                'transformer': self.f_transformer.compute_basic_matrix,
            },
            'svo_triples': {
                'extractor_type': 'text_couple',
                # 'extractor': self.f_extractor.extract_SVO_triples,
                'pickle_file': 'svo_triples.pickle',
                'requires_w2v': False,
                'transformer': self.f_transformer.compute_SVO_entailment_matrix_from_data,
            },
            'ngrams': {
                'extractor_type': 'concatenated',
                # 'extractor': self.f_extractor.extract_ngrams,
                'pickle_file': 'ngrams.pickle',
                'requires_w2v': False,
                'transformer': self.f_transformer.compute_ngram_matrix_from_data,
            },
            'skipgrams': {
                'extractor_type': 'concatenated',
                # 'extractor':      self.f_extractor.extract_skipgrams,
                'pickle_file':    'skipgrams.pickle',
                'requires_w2v':   False,
                'transformer':    self.f_transformer.compute_skipgrams_matrix_from_data,
            }
        }

    """
    
    PRIVATE METHODS
    
    """

    def _verify_load_w2v_model(self, feature_info):
        """
        Verifies if any value in given extractors dictionary has the
        'requires_w2v' key value equal to True.

        :param feature_info: dictionary structured as self.feature_info shown in __init__ method
        :return: True if given dictionary has at least one value with 'requires_w2v'
        key value equal to True, False otherwise.
        """

        for key in feature_info:
            if feature_info[key]['requires_w2v']:
                return True

        return False

    """
    
    PUBLIC METHODS
    
    """

    def extract_features(self, path_to_csv, features, additional_args = None, save_to_pickle = False):
        """
        Extracts a sub-set of features given by 'features' list parameter.
        Lastly, all extracted features are saved into their pickle files respectively.

        :param path_to_csv: path to dataset csv file in order to iterate over its rows
        :param features: list of features to select
        :return: dictionary: feature_type -> dictionary containing results of associated
        feature extraction process
        """

        filtered_feature_info = {}

        for key in self.features_info:
            if key in features:
                filtered_feature_info[key] = self.features_info[key]

        if self._verify_load_w2v_model(filtered_feature_info):
            print("Detected features that require Word2Vec model. Loading...")
            self.f_extractor.load_w2v_model()

            if virtual_memory().percent >= 90:
                print("Warning! High memory consumption! Feature extraction may throw OSError 12 "
                      "Please extract features that require Word2Vec model separately.")

        extractors_data = {}
        associations = DatasetUtils.build_column_associations(self.targets)

        # Couple extractors
        dataset_couples = [(target1, target2) for entry_ID, [target1, target2], _ in
                     DatasetUtils.iterate_over_dataset(path_to_csv, self.targets)]

        couple_extractor_data = {}
        for key in filtered_feature_info:
            if filtered_feature_info[key]['extractor_type'] == 'text_couple':
                additional_function_args = additional_args[key] if additional_args is not None and key in additional_args else {}
                couple_extractor_data[key] = filtered_feature_info[key]['extractor'](dataset_couples, **additional_function_args)

        extractors_data.update(couple_extractor_data)

        # Single target extractors
        target_lists = dict((associations[key], list(DatasetUtils.iterate_over_target(path_to_csv, associations[key]))) for key in associations)

        single_target_extractors_data = {}
        for key in filtered_feature_info:
            if filtered_feature_info[key]['extractor_type'] == 'single_target':
                additional_function_args = additional_args[key] if additional_args is not None and key in additional_args else {}
                for target_name in target_lists:
                    single_target_extractors_data['{}_{}'.format(key, target_name)] = \
                        filtered_feature_info[key]['extractor'](target_lists[target_name], **additional_function_args)

        extractors_data.update(single_target_extractors_data)

        # Concatenated targets extractors
        concatenated_targets = dict({(entry_ID, "{} {}".format(target1, target2)) for entry_ID, [target1, target2], _ in
                     DatasetUtils.iterate_over_dataset(path_to_csv, self.targets)})

        concatenated_extractors_data = {}
        for key in filtered_feature_info:
            if filtered_feature_info[key]['extractor_type'] == 'concatenated':
                additional_function_args = additional_args[key] if additional_args is not None and key in additional_args else {}
                concatenated_extractors_data[key] = filtered_feature_info[key]['extractor'](concatenated_targets, **additional_function_args)

        extractors_data.update(concatenated_extractors_data)

        # Saving data to files
        if save_to_pickle:
            for key in extractors_data:
                print("Saving to file: {}".format(filtered_feature_info[key]['pickle_file']))
                PickleUtility.save_to_file(extractors_data[key], filtered_feature_info[key]['pickle_file'])

        # print(extractors_data)
        return extractors_data

    def compute_transforms(self, extractors_data):
        """
        Compute all features matrixes from given feature extraction data.
        The latter is the one obtained by self.extract_features

        :param extractors_data: dictionary whose keys are features names and whose
        values are the results obtained by associated feature extraction functions
        :return: union feature matrix obtained by horizontal stacking each computed feature matrix
        """

        transforms_data = {}
        for key in self.features_info:
            if key in extractors_data:
                transforms_data[key] = self.features_info[key]['transformer'](extractors_data[key])

        if len(transforms_data.keys()) >= 2:
            union_matrix = hstack([transforms_data[key] for key in transforms_data])
        else:
            union_matrix = transforms_data.values()[0]

        return csr_matrix(union_matrix)


    def load_features_and_compute_transforms(self, path_to_csv, features, additional_args = {}, use_saved_data = False):
        """
        Computes features associated transformations in order to use given results
        for classification.
        Each transformation output is of type scipy.sparse.csr_matrix

        :param features: list of features to select
        :param additional_args: dictionary that for each feature (key) associates a list
        of additional parameters.
        :return: scipy.sparse.csr_matrix obtained by horizontal stacking all computed
        matrixes
        """

        #TODO: il codice puo' essere migliorato

        filtered_feature_info = {}

        for key in self.features_info:
            if key in features:
                filtered_feature_info[key] = self.features_info[key]

        matrixes = {}

        # No args transformations
        no_args_data = FunctionUtils.apply_functions(
            dict((key, self.features_info[key]['transformer']) for key in filtered_feature_info
                 if filtered_feature_info[key]['transform_type'] == "no_args"),
            dict((key, []) for key in filtered_feature_info
                 if filtered_feature_info[key]['transform_type'] == "no_args")
        )

        matrixes.update(no_args_data)

        # Transformations that require targets list
        targets_list = [(id1, id2) for _, _, [id1, id2] in DatasetUtils.iterate_over_dataset(path_to_csv, self.targets)]

        args_dict = {}
        for key in filtered_feature_info:
            if filtered_feature_info[key]['transform_type'] == 'target_list':
                if key in additional_args:
                    args_dict[key] = [targets_list, filtered_feature_info[key]['pickle_file'], additional_args[key]]
                else:
                    args_dict[key] = [targets_list, filtered_feature_info[key]['pickle_file']]

        targets_list_data = FunctionUtils.apply_functions(
            dict((key, self.features_info[key]['transformer']) for key in filtered_feature_info
                 if filtered_feature_info[key]['transform_type'] == "target_list"),
            args_dict
        )

        matrixes.update(targets_list_data)

        # Transformations that require combined targets texts
        dataset_couples = dict({(entry_ID, "{} {}".format(target1, target2)) for entry_ID, [target1, target2], _ in
                     DatasetUtils.iterate_over_dataset(path_to_csv, self.targets)})

        args_dict = {}
        for key in filtered_feature_info:
            if filtered_feature_info[key]['transform_type'] == 'combined_texts':
                if key in additional_args:
                    args_dict[key] = [dataset_couples, additional_args[key]]
                else:
                    args_dict[key] = [dataset_couples]

        full_data = FunctionUtils.apply_functions(
            dict((key, self.features_info[key]['transformer']) for key in filtered_feature_info
                 if filtered_feature_info[key]['transform_type'] == "combined_texts"),
            args_dict
        )

        matrixes.update(full_data)

        if len(matrixes.keys()) >= 2:
            union_matrix = hstack([matrixes[key] for key in matrixes])
        else:
            union_matrix = matrixes.values()[0]

        return csr_matrix(union_matrix)

# Test

if __name__ == "__main__":

    config_file = "dataset_extractor_training"
    config_section = "emergent"
    extractor = DatasetExtractor(config_file, config_section)

    # test extractor
    path_to_csv = "/home/frgg/Downloads/mscproject/data/emergent/url-versions-2015-06-14-clean.csv"
    features = ["ngrams", "skipgrams"]

    additional_args = {'ngrams' : {'ngram_range' : (1,3)}}

    extractors_data = extractor.extract_features(path_to_csv=path_to_csv, features=features, additional_args=additional_args)
    matrix = extractor.compute_transforms(extractors_data)