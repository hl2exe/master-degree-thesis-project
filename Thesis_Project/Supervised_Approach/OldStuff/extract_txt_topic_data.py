"""

If you are using Collector save results method then this script can extract the txt data

"""

from Supervised_Approach.path_definitions import STATISTICS_DIR, LOTO_DIR, MARGOT_TOPIC_DIR, COMPLETE_DIR
import os
import ast
import pandas as pd
import numpy as np


class LessPrecise(float):
    def __repr__(self):
        return str(self)


def roundingVals_toTwoDeci(data):
    for k, d in data.iteritems():
        d['accuracy'] = LessPrecise(round(d['accuracy'], 4))
        d['f1-score'] = LessPrecise(round(d['f1-score'], 4))
        # print d
        data[k] = d


def flat_list(l):
    return [item for sublist in l for item in sublist]


motions_df = pd.read_csv(os.path.join(COMPLETE_DIR, 'motions.csv'), encoding="utf-8")
train_and_test_topics = motions_df.loc[motions_df['Data-set'] == 'train and test'][['Topic id', 'Topic']].to_dict()
topic_mapping_ids = train_and_test_topics['Topic id']
print(topic_mapping_ids)
# topic_mapping_values = train_and_test_topics['Topic']
# topic_mapping = {topic_mapping_values[key]: topic_mapping_ids[key] for key in topic_mapping_values}
topic_mapping = {train_and_test_topics['Topic'][key]: key for key in train_and_test_topics['Topic']}

# loto_data_file = os.path.join(STATISTICS_DIR, 'loto-topic-results.txt')
loto_data_file = os.path.join(STATISTICS_DIR, 'margot-topic-results-ce-emnlp-15-train-and-test.txt')
# loto_data_file = os.path.join(STATISTICS_DIR, 'test.txt')
classifier_key = ""
folder = MARGOT_TOPIC_DIR
all_data = {}

with open(loto_data_file, mode='r') as loto_file:
    lines = loto_file.readlines()
    for idx, line in enumerate(lines):

        if line.startswith('Classifier:'):
            classifier_key = line.split('Classifier:')[1].strip()

        if line.startswith('Results:'):
            # print('Stampo', lines[idx+274])
            classifier_dict = ''.join(lines[idx+1:idx+274])
            # print(data_dict + '\n\n')
            classifier_dict = ast.literal_eval(classifier_dict)
            # print(classifier_dict)
            # print("\n\n")

            roundingVals_toTwoDeci(classifier_dict)
            all_data[classifier_key] = {topic_mapping[topic] + 1: classifier_dict[topic] for topic in classifier_dict}

            # Accuracy
            acc_data = []
            for topic in classifier_dict:
                acc_data.append([classifier_dict[topic]['accuracy'], topic_mapping[topic] + 1])

            acc_data.sort(key=lambda x: x[1])
            classifier__accuracy_file = '{}-accuracy.dat'.format(classifier_key)
            with open(os.path.join(folder, classifier__accuracy_file), mode='w') as accuracy_file:
                accuracy_file.write('Accuracy\tTopic\n')
                for data in acc_data:
                    accuracy_file.write('{}\n'.format('\t'.join(map(str, data))))

            # F1-score
            f1_data = []
            for topic in classifier_dict:
                f1_data.append([classifier_dict[topic]['f1-score'], topic_mapping[topic] + 1])

            f1_data.sort(key=lambda x: x[1])
            classifier__f1_file = '{}-f1-score.dat'.format(classifier_key)
            with open(os.path.join(folder, classifier__f1_file), mode='w') as f1score_file:
                f1score_file.write('F1-score\tTopic\n')
                for data in f1_data:
                    f1score_file.write('{}\n'.format('\t'.join(map(str, data))))

# Accuracy & F1-score table
df_out = pd.DataFrame(index=[x for x in range(1, 40)],
                      columns=['Topic graph id', 'Topic id', 'Baseline CS acc', 'Baseline CS f1',
                               'Baseline n-gram acc', 'Baseline n-gram f1', 'Stab and Gurevych acc',
                               'Stab and Gurevych f1', 'RNN acc', 'RNN f1', 'SC acc',
                               'SC f1'], data=np.nan)

df_out['Topic id'] = pd.Series(topic_mapping_ids.values()).values
df_out['Topic graph id'] = pd.Series([x for x in range(1, 40)]).values

df_out['Baseline CS acc'] = pd.Series([item['accuracy']
                                       for item in all_data['baseline-cosine-similarity'].values()]).values
df_out['Baseline n-gram acc'] = pd.Series([item['accuracy']
                                       for item in all_data['baseline-n-grams'].values()]).values
df_out['Stab and Gurevych acc'] = pd.Series([item['accuracy']
                                       for item in all_data['Stab-and-Gurevych'].values()]).values
df_out['RNN acc'] = pd.Series([item['accuracy']
                                       for item in all_data['RNN'].values()]).values
df_out['SC acc'] = pd.Series([item['accuracy']
                                       for item in all_data['SC'].values()]).values

df_out['Baseline CS f1'] = pd.Series([item['f1-score']
                                       for item in all_data['baseline-cosine-similarity'].values()]).values
df_out['Baseline n-gram f1'] = pd.Series([item['f1-score']
                                       for item in all_data['baseline-n-grams'].values()]).values
df_out['Stab and Gurevych f1'] = pd.Series([item['f1-score']
                                       for item in all_data['Stab-and-Gurevych'].values()]).values
df_out['RNN f1'] = pd.Series([item['f1-score']
                                       for item in all_data['RNN'].values()]).values
df_out['SC f1'] = pd.Series([item['f1-score']
                                       for item in all_data['SC'].values()]).values

df_out = df_out.set_index('Topic graph id')
df_out.to_csv(os.path.join(folder, 'margot_dataframe.csv'), encoding='utf-8')

# Confusion matrix
df_out = pd.DataFrame(index=[x for x in range(1, 40) for _ in (0, 1)],
                      columns=['Topic graph id', 'Topic id', 'Baseline CS link', 'Baseline CS no-link',
                               'Baseline n-gram link', 'Baseline n-gram no-link', 'Stab and Gurevych link',
                               'Stab and Gurevych no-link', 'RNN link', 'RNN no-link', 'SC link',
                               'SC no-link', 'Link', 'True'], data=np.nan)

df_out['Topic id'] = pd.Series([val for val in topic_mapping_ids.values() for _ in (0, 1)]).values
df_out['Topic graph id'] = pd.Series([x for x in range(1, 40) for _ in (0, 1)]).values

df_out['Baseline CS link'] = pd.Series(flat_list([item['confusion_matrix']['link']
                                       for item in all_data['baseline-cosine-similarity'].values()])).values
df_out['Baseline n-gram link'] = pd.Series(flat_list([item['confusion_matrix']['link']
                                       for item in all_data['baseline-n-grams'].values()])).values
df_out['Stab and Gurevych link'] = pd.Series(flat_list([item['confusion_matrix']['link']
                                       for item in all_data['Stab-and-Gurevych'].values()])).values
df_out['RNN link'] = pd.Series(flat_list([item['confusion_matrix']['link']
                                       for item in all_data['RNN'].values()])).values
df_out['SC link'] = pd.Series(flat_list([item['confusion_matrix']['link']
                                       for item in all_data['SC'].values()])).values

df_out['Baseline CS no-link'] = pd.Series(flat_list([item['confusion_matrix']['no-link']
                                       for item in all_data['baseline-cosine-similarity'].values()])).values
df_out['Baseline n-gram no-link'] = pd.Series(flat_list([item['confusion_matrix']['no-link']
                                       for item in all_data['baseline-n-grams'].values()])).values
df_out['Stab and Gurevych no-link'] = pd.Series(flat_list([item['confusion_matrix']['no-link']
                                       for item in all_data['Stab-and-Gurevych'].values()])).values
df_out['RNN no-link'] = pd.Series(flat_list([item['confusion_matrix']['no-link']
                                       for item in all_data['RNN'].values()])).values
df_out['SC no-link'] = pd.Series(flat_list([item['confusion_matrix']['no-link']
                                       for item in all_data['SC'].values()])).values

df_out['Link'] = pd.Series(['no-link' if x % 2 == 0 else 'link' for x in range(1, 79)]).values
df_out['True'] = pd.Series(['True' for x in range(1, 40) for _ in (0, 1)]).values

df_out = df_out.set_index('Topic graph id')
df_out.to_csv(os.path.join(folder, 'margot_confusion_matrix_dataframe.csv'), encoding='utf-8')

# Classification Report Table
for classifier in all_data:

    df_out = pd.DataFrame(index=[x for x in range(1, 40)],
                          columns=['Topic graph id', 'Topic id', 'Precision link', 'Precision no-link',
                                   'Recall link', 'Recall no-link', 'F1-score link',
                                   'F1-score no-link', 'Support link', 'Support no-link'], data=np.nan)

    df_out['Topic id'] = pd.Series(topic_mapping_ids.values()).values
    df_out['Topic graph id'] = pd.Series([x for x in range(1, 40)]).values

    df_out['Precision link'] = pd.Series([round(item['report']['precision'][0], 4)
                                           for item in all_data[classifier].values()]).values
    df_out['Recall link'] = pd.Series([round(item['report']['recall'][0], 4)
                                           for item in all_data[classifier].values()]).values
    df_out['F1-score link'] = pd.Series([round(item['report']['f1-score'][0], 4)
                                           for item in all_data[classifier].values()]).values
    df_out['Support link'] = pd.Series([round(item['report']['support'][0], 4)
                                           for item in all_data[classifier].values()]).values

    df_out['Precision no-link'] = pd.Series([round(item['report']['precision'][1], 4)
                                           for item in all_data[classifier].values()]).values
    df_out['Recall no-link'] = pd.Series([round(item['report']['recall'][1], 4)
                                           for item in all_data[classifier].values()]).values
    df_out['F1-score no-link'] = pd.Series([round(item['report']['f1-score'][1], 4)
                                           for item in all_data[classifier].values()]).values
    df_out['Support no-link'] = pd.Series([round(item['report']['support'][1], 4)
                                           for item in all_data[classifier].values()]).values

    df_out = df_out.set_index('Topic graph id')
    df_out.to_csv(os.path.join(folder, 'margot-report-{}-dataframe.csv'.format(classifier)), encoding='utf-8')
