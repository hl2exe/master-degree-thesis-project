from sklearn.base import BaseEstimator, TransformerMixin


class ItemSelector(BaseEstimator):

    def __init__(self, key):
        self.key = key

    def fit(self, x, y=None):
        return self

    def transform(self, dataframe):
        return dataframe[self.key].values

class ConcatenateSelector(BaseEstimator):

    def __init__(self, key_list):
        self.key_list = key_list

    def fit(self, x, y=None):
        return self

    def transform(self, dataframe):
        lists = []
        for key in self.key_list:
            lists.append(dataframe[key].values)

        return [" ".join(values) for values in zip(*lists)]

class CoupleSelector(BaseEstimator):

    def __init__(self, key_list):
        self.key_list = key_list

    def fit(self, X, y=None):
        return self

    def transform(self, dataframe):
        lists = []
        for key in self.key_list:
            lists.append(dataframe[key].values)

        return [tuple(values) for values in zip(*lists)]

# class TargetExtractor(BaseEstimator):
#
#     def __init__(self, associations):
#         self.associations = associations
#
#     def fit(self, x, y=None):
#         return self
#
#     def transform(self, generator):
#
#         #TODO: documentazione
#
#         features = {}
#
#         for entry_ID, targets, ids in generator:
#
#             for current_key, current_target in zip(self.associations.values() + list(combinations(self.associations.keys(), 2)),
#                                                targets + list(combinations(targets, 2))):
#
#                 if not current_key in features:
#                     features[current_key] = {entry_ID : current_target}
#                 else:
#                     features[current_key][entry_ID] = current_target
#
#         return features

class Debugger(BaseEstimator, TransformerMixin):

    def fit(self, x, y=None):
        return self

    def transform(self, data):
        print("Debugger: ")
        print(type(data))
        print(data)
        return data