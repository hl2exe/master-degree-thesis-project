"""
Ablation test on available argument structure prediction (ASP) train data-sets.
All the available features are used.
Results are saved in 'ablation' folder in .csv format.

Step 1: Retrieving positive and negative examples
Step 2: building train/test sets
Step 3: determining feature set
Step 4: ablation test
Step 5: saving results

"""

import argparse
import os
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.model_selection import train_test_split

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierFactory, ClassifierType
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, ABLATION_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Variables: change these according to your scenario
    dataset_name = 'ce_emnlp_15_argument_structure_prediction_training.csv'
    target_column = 'Argument relation'
    test_size = 0.3
    random_state = 42   # repeatability

    # Step 1: Retrieving positive and negative examples

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, _ = ScenarioManager.get_associations_mapping(config_file)

    training_data_path = os.path.join(POST_PROCESS_DIR, dataset_name)
    data = pd.read_csv(training_data_path, encoding="utf-8")
    data.name = 'argument-structure-prediction'
    data = data.rename(columns=columns_mapping[data.name])
    target = data[target_column].values

    # Step 2: building train/test sets

    print("Splitting initial data into train/test sets respectively. Test size set to: {}".format(test_size))
    X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=test_size, random_state=random_state)

    # Step 3: determining feature set

    customPipelines = CustomPipelines(associations_mapping=associations_mapping)
    args, features = customPipelines.get_all_features_args()

    test_features_list = ['count_tokens_first',
                          'count_tokens_second',
                          'difference_tokens',
                          'punctuation_marks_first',
                          'punctuation_marks_second',
                          'difference_punctuation_marks',
                          'word_pairs_binary',
                          'first_word_binary_first',
                          'first_word_binary_second',
                          'couple_first_word_binary',
                          'modal_verbs_binary_first',
                          'modal_verbs_binary_second',
                          'common_elements',
                          'production_rules_binary_first',
                          'production_rules_binary_second']

    args.update(customPipelines.get_argument_structure_prediction_specific_args())

    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    parser = argparse.ArgumentParser(description='run_baseline cmd-line arguments.')
    parser.add_argument('-f',
                        default=','.join(test_features.keys()),
                        type=str)
    parser.add_argument('-s',
                        default=ABLATION_DIR,
                        type=str)
    parser.add_argument('-n',
                        type=str)

    parser_args = parser.parse_args()

    if parser_args.f:
        selected_features = [feature for feature in parser_args.f.split(',')]
        selected_args = dict({key: args[key] for key in args if key in selected_features})
    else:
        selected_features = test_features.keys()
        selected_args = test_args

    print("Selected features: {}".format(selected_features))

    df_out = pd.DataFrame(index=['{}'.format(feature) for feature in selected_features],
                          columns=['score-cv', 'accuracy-test'], data=np.nan)

    # Step 4: ablation test

    classifier = ClassifierFactory.factory(ClassifierType.linear_svm)
    trainer = Trainer()

    print("Starting ablation test..")
    for to_exclude in selected_features:
        print("Excluding feature: {}".format(to_exclude))
        current_feature_set = [feature for feature in selected_features if feature != to_exclude]
        pipeline_feature_dict = dict({(f_key, selected_features[f_key])
                                      for f_key in selected_features
                                      if f_key != to_exclude})
        pipeline = trainer.build_pipeline(features=pipeline_feature_dict,
                                          args=selected_args,
                                          classifier=classifier)

        # Cross-validation
        scores = cross_val_score(estimator=pipeline,
                                 X=X_train,
                                 y=y_train,
                                 cv=StratifiedKFold(shuffle=True,
                                                    random_state=random_state,
                                                    n_splits=10))
        cross_validation_score = np.mean(scores)

        # Test score
        pipeline.fit(X_train, y_train)
        test_score = pipeline.score(X_test, y_test)

        df_out.ix[to_exclude, 'score-cv'] = cross_validation_score
        df_out.ix[to_exclude, 'accuracy-test'] = test_score

    print("Ablation test ended!")

    # Step 5: saving results

    if parser_args.s:
        destination_folder = parser_args.s
    else:
        destination_folder = ABLATION_DIR

    if not os.path.isdir(destination_folder):
        print("{} folder does not exist! Creating...".format(destination_folder))
        os.mkdir(destination_folder)

    if parser_args.n:
        filename = parser_args.n
    else:
        date = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
        filename = "Ablation_test_{}.csv".format(date)

    path = os.path.join(destination_folder, filename)
    print("Saving results to: {}".format(path))
    df_out.to_csv(path, encoding="utf-8")
