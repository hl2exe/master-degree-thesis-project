"""

MARGOT Argument Structure Prediction classification test with RNN.

Input: positive/negative claim-evidence couples
Output: binary predictions: SUPPORTS/NOT SUPPORTS or LINK/NO-LINK

"""

import os

import numpy as np
import pandas as pd
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn import metrics

from Supervised_Approach.path_definitions import POST_PROCESS_DIR, COMPLETE_DIR
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from Supervised_Approach.Neural_networks.rnn_models import ArgumentStructurePrediction_RNN
from Supervised_Approach.Neural_networks.Utils import model_metrics


def get_vocab_size(sequence):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sequence)

    return len(tokenizer.word_index) + 1


def preprocess_sequence(sequence, model, key, load_pickle=False, pickle_file=None,
                        vocab_size=None, padding_max_length=None, embedding_matrix=None):

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sequence)
    if vocab_size is None:
        vocab_size = len(tokenizer.word_index) + 1
    encoded_data = tokenizer.texts_to_sequences(sequence)
    if padding_max_length is None:
        # padding_max_length = len(max(encoded_data, key=len))
        padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
    print("Padding max length: {}".format(padding_max_length))
    padded_docs = pad_sequences(encoded_data, maxlen=padding_max_length, padding='post')
    errors = 0

    if not load_pickle:
        if embedding_matrix is None:
            embedding_matrix = np.zeros((vocab_size, 300))
        for word, i in tokenizer.word_index.items():
            try:
                embedding_vector = model[word]
            except KeyError:
                errors += 1
                embedding_vector = np.zeros(300)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        if pickle_file is not None:
            if PickleUtility.verify_file(pickle_file):
                existing_data = PickleUtility.load_pickle(pickle_file)
                existing_data.update({key: embedding_matrix})
                PickleUtility.save_to_file(existing_data, pickle_file)
            else:
                PickleUtility.save_to_file({key: embedding_matrix}, pickle_file)
    else:
        embedding_matrix = PickleUtility.load_pickle(pickle_file)[key]

    print("Key errors: {}".format(errors))
    return vocab_size, padding_max_length, padded_docs, embedding_matrix


# TODO: prova a controllare la struttura dei padded docs -> quante righe tutte a zero? o con valori strani? Il confronto
# va fatto sia nel caso con pochi elementi (1000 link e 1000 no link) dove f1 != NaN e nel caso normale.


# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Repeatability
    np.random.seed(7)

    # Step 0: Retrieving positive and negative examples

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'MARGOT_argument_structure_prediction_training_balanced_clean.csv')
    data = pd.read_csv(training_data_path, encoding="utf-8")
    target = data['Argument relation'].values

    claim_data = data['Claim'].values
    evidence_data = data['Evidence'].values

    # Step 1: Loading Word2Vec data

    # mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    # while mode not in [1, 2]:
    #     mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    mode = 2

    pickle_file = None
    utils = FeatureExtractorUtils()

    if mode == 2:
        utils.load_w2v_model()
        # pickle_file = 'MARGOT_argument_structure_prediction_training_balanced_clean.pickle'
        load_pickle = False
    else:
        # pickle_file = input("Please specify pickle file containing pre-computed embedding matrix")
        pickle_file = 'MARGOT_argument_structure_prediction_training_balanced_clean.pickle'
        load_pickle = True

    # Step 2: Pre-processing data for classifier

    # Test data
    test_data = pd.read_csv(os.path.join(COMPLETE_DIR, 'ce_emnlp_15_joined-ids.csv'), encoding="utf-8")
    claim_test_data = test_data['Claim'].values
    evidence_test_data = test_data['Evidence'].values

    train_claim_vocab_size = get_vocab_size(claim_data)
    train_evidence_vocab_size = get_vocab_size(evidence_data)
    test_claim_vocab_size = get_vocab_size(claim_test_data)
    test_evidence_vocab_size = get_vocab_size(evidence_test_data)

    selected_claim_vocab_size = max(train_claim_vocab_size, test_claim_vocab_size)
    selected_evidence_vocab_size = max(train_evidence_vocab_size, test_evidence_vocab_size)

    # Building input data

    # Claim
    claim_vocab_size, claim_padding_max_length, claim_padded_docs, claim_embedding_matrix = \
        preprocess_sequence(claim_data, utils.w2v_model, pickle_file=pickle_file, key='Claim',
                            load_pickle=load_pickle,
                            vocab_size=selected_claim_vocab_size)

    print("Claim embedding matrix shape: {}".format(claim_embedding_matrix.shape))

    # Evidence
    evidence_vocab_size, evidence_padding_max_length, evidence_padded_docs, evidence_embedding_matrix = \
        preprocess_sequence(evidence_data, utils.w2v_model, pickle_file=pickle_file, key='Evidence',
                            load_pickle=load_pickle,
                            vocab_size=selected_evidence_vocab_size)

    print("Evidence embedding matrix shape: {}".format(evidence_embedding_matrix.shape))

    _, _, claim_test_padded_docs, selected_claim_embedding_matrix = \
        preprocess_sequence(claim_test_data, utils.w2v_model, key='Claim', vocab_size=selected_claim_vocab_size,
                            padding_max_length=claim_padding_max_length,
                            embedding_matrix=claim_embedding_matrix)

    _, _, evidence_test_padded_docs, selected_evidence_embedding_matrix = \
        preprocess_sequence(evidence_test_data, utils.w2v_model, key='Evidence',
                            vocab_size=selected_evidence_vocab_size,
                            padding_max_length=evidence_padding_max_length,
                            embedding_matrix=evidence_embedding_matrix)

    print("Claim embedding matrix shape: {}".format(selected_claim_embedding_matrix.shape))
    print("Evidence embedding matrix shape: {}".format(selected_evidence_embedding_matrix.shape))

    # Step 3: Defining classifier
    utils.w2v_model = None
    classifier = ArgumentStructurePrediction_RNN(ids=['Claim', 'Evidence'],
                                                 vocab_size={'Claim': selected_claim_vocab_size,
                                                             'Evidence': selected_evidence_vocab_size},
                                                 input_length={'Claim': claim_padding_max_length,
                                                               'Evidence': evidence_padding_max_length},
                                                 embedding_vector_length={'Claim': 300,
                                                                          'Evidence': 300},
                                                 weights={'Claim': selected_claim_embedding_matrix,
                                                          'Evidence': selected_evidence_embedding_matrix},
                                                 metrics=['accuracy', model_metrics.f1],
                                                 add_dropout=True,
                                                 dropout=0.2
                                                 ).build_model()

    # Step 4: Training
    train_target = [1 if val == 'link' else 0 for val in target]
    classifier.fit([claim_padded_docs, evidence_padded_docs], train_target, batch_size=64, epochs=3, verbose=2,
                   shuffle=True)

    # Step 5: Predicting data
    print("Predicting stances on specified dataset")
    predictions = classifier.predict_classes([claim_test_padded_docs, evidence_test_padded_docs],
                                             batch_size=64,
                                             verbose=1)
    print(predictions.shape)
    predictions = ['link' if item == 1 else 'no-link' for item in predictions]

    print("Total predictions: {}".format(len(predictions)))

    # Step 6: viewing classification report
    unique, counts = np.unique(predictions, return_counts=True)
    print("predictions stats:\n{}".format(dict(zip(unique, counts))))

    report = metrics.classification_report(y_true=['link' for _ in range(0, len(predictions))],
                                           y_pred=predictions,
                                           target_names=['link', 'no-link'])
    confusion_matrix = metrics.confusion_matrix(y_true=['link' for _ in range(0, len(predictions))],
                                                y_pred=predictions,
                                                labels=['link', 'no-link'])

    print("Classification report: \n{}".format(report))
    print("Confusion matrix: \n{}".format(confusion_matrix))
