from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from configparser import NoSectionError
from Supervised_Approach.Utils.config_utils import ConfigUtils
from collections import OrderedDict


class ScenarioManager(object):

    supported_train_datasets = {
        1: "emergent",
        2: "fnc-1"
    }

    supported_train_targets = {
        1: 'articleHeadlineStance',
        2: 'Stance'
    }

    supported_scenarios = {
        1: "claim-topic",
        2: "claim-article",
        3: "article-topic",
        4: "evidence-topic",
        5: "evidence-article"
    }

    supported_predict_datasets = {
        1: "ce_acl_14",
        2: "ce_emnlp_15",
        3: "ce_emnlp_15_train_and_test",
        4: 'MARGOT_ASP_balanced_clean'
    }

    supported_scenario_datasets = {
        1: "emergent",
        2: "fnc-1",
        3: "fnc-1",
        4: "emergent",
        5: "fnc-1"
    }

    extra_supported_scenarios = {
        1: 'argument-structure-prediction',
        2: 'margot-argument-structure-prediction',
        3: 'ce-acl-14-ASP',
        4: 'ce-emnlp-15-ASP'
    }

    supported_scenario_targets = {
        1: 'articleHeadlineStance',
        2: 'Stance',
        3: 'Stance',
        4: 'articleHeadlineStance',
        5: 'Stance'
    }

    scenario_couples = {
        'Topic': ('claim-topic', 'evidence-topic'),
        'Article': ('claim-article', 'evidence-article'),
        'Article-Topic': ('claim-article-topic', 'evidence-article-topic')
    }

    scenario_classes = {
        'emergent': {'positive': 'for',
                     'negative': 'against'},
        'fnc-1': {'positive': 'agree',
                  'negative': 'disagree'}
    }

    # TODO: modify the info setup with the added .ini file
    # TODO: potrei rimuovere questa parte di settaggio e mettere in chiaro le variabili in ogni runnable.
    @staticmethod
    def get_associations_mapping(config_file):
        """
        Builds data-set and scenario mappings for general classification behaviour.
        Obtained mappings are used by data selectors inside the pipeline in order to filter data correctly in
        accordance to specific case of interest.

        :param config_file: configuration file which stores columns associations. (trainer.ini)
        :return: 3 dictionaries: associations mapping, columns mapping and inverse columns mapping
        """

        dataset_names = []

        for scenario in ScenarioManager.supported_scenarios.values():
            for predict_dataset in ScenarioManager.supported_predict_datasets.values():
                dataset_names.append("{}-{}".format(predict_dataset, scenario))

        for train_dataset in ScenarioManager.supported_train_datasets.values():
            dataset_names.append(train_dataset)

        for extra_scenario in ScenarioManager.extra_supported_scenarios.values():
            dataset_names.append(extra_scenario)

        associations_mapping = OrderedDict()
        columns_mapping = OrderedDict()
        inverted_columns_mapping = OrderedDict()
        config = ConfigUtils.load_config(config_file)

        for df_name in dataset_names:
            try:
                target_dict = OrderedDict(config.items(df_name))
                associations = DatasetUtils.build_column_associations(target_dict)
                associations_mapping[df_name] = ["{}-{}".format(val, df_name) for val in associations.values()]
                columns_mapping[df_name] = OrderedDict({val: "{}-{}".format(val, df_name)
                                                        for val in associations.values()})
                inverted_columns_mapping[df_name] = OrderedDict({"{}-{}".format(val, df_name): val
                                                                 for val in associations.values()})
            except NoSectionError:
                print("Sorry, but it seems that section {} is not implemented yet".format(df_name))

        return associations_mapping, columns_mapping, inverted_columns_mapping

    @staticmethod
    def validate_scenario(index, dictionary):
        """
        Verifies if given index is present in given dictionary.

        :param index:
        :param dictionary:
        :return:
        """

        try:
            parsed = int(index)
            if parsed in dictionary.keys():
                return True
        except ValueError:
            print("Invalid scenario! Got: {}".format(index))
            return False
