"""

Argument Structure Prediction classification test with RNN.

Input: positive/negative claim-evidence couples
Output: binary classification: SUPPORTS/NOT SUPPORTS or LINK/NO-LINK

"""

from Supervised_Approach.path_definitions import POST_PROCESS_DIR, PREDICTIONS_DIR
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from Supervised_Approach.Neural_networks.rnn_models import ArgumentStructurePrediction_RNN
from Supervised_Approach.Neural_networks.ArgumentStructurePrediction_RNN_single_input import ArgumentStructurePrediction_RNN_single_input
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import os
import pandas as pd
import numpy as np
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score, StratifiedKFold
from Supervised_Approach.Neural_networks.Utils import model_metrics


def preprocess_sequence(sequence, model, key, load_pickle=False, pickle_file=None,
                        vocab_size=None, padding_max_length=None, add_stance=False, stance_vector=None,
                        embedding_matrix=None):

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sequence)
    if vocab_size is None:
        vocab_size = len(tokenizer.word_index) + 1
    encoded_data = tokenizer.texts_to_sequences(sequence)
    if padding_max_length is None:
        padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
    print("Padding max length: {}".format(padding_max_length))
    padded_docs = pad_sequences(encoded_data, maxlen=padding_max_length, padding='post')

    # Add stances values in padded_docs: shifting indexes by 3, i.e. reserving values for stances
    # print("Before ({}): {}".format(padded_docs.shape, padded_docs))
    if add_stance:
        padding_max_length += 1
        vocab_size += 3
        new_padded_docs = np.zeros((padded_docs.shape[0], padded_docs.shape[1] + 1),
                                   dtype=np.int32)
        for idx, row in enumerate(padded_docs):
            new_padded_docs[idx] = [val + 3 if val != 0 else val for val in row] + [stance_vector[key][idx]]
        padded_docs = new_padded_docs

    # print("After ({}): {}".format(padded_docs.shape, padded_docs))

    if not load_pickle:
        if embedding_matrix is None:
            embedding_matrix = np.zeros((vocab_size, 300))
        for word, i in tokenizer.word_index.items():
            try:
                embedding_vector = model[word]
            except KeyError:
                embedding_vector = np.zeros(300)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        if pickle_file is not None:
            if PickleUtility.verify_file(pickle_file):
                existing_data = PickleUtility.load_pickle(pickle_file)
                existing_data.update({key: embedding_matrix})
                PickleUtility.save_to_file(existing_data, pickle_file)
            else:
                PickleUtility.save_to_file({key: embedding_matrix}, pickle_file)
    else:
        embedding_matrix = PickleUtility.load_pickle(pickle_file)[key]

    return vocab_size, padding_max_length, padded_docs, embedding_matrix


# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Repeatability
    np.random.seed(7)

    # Step 0: Retrieving positive and negative examples

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'ce_emnlp_15_argument_structure_prediction_training.csv')
    data = pd.read_csv(training_data_path, encoding="utf-8")
    target = data['Argument relation'].values

    # Splitting data into training and test sets:
    X_train, X_test, y_train, y_test = train_test_split(data, target, test_size=0.4, random_state=42)

    claim_data = X_train['Claim'].values
    evidence_data = X_train['Evidence'].values

    # add_stance_mode = input("Do you want to add stance as feature? Y/N: ")
    add_stance_mode = 'n'

    if add_stance_mode.lower() == 'y':
        claim_stances = X_train['Claim stance']
        evidence_stances = X_train['Evidence stance']

        claim_stances = [1 if val == 'for' else 2 if val == 'against' else 3 for val in claim_stances]
        evidence_stances = [1 if val == 'for' else 2 if val == 'against' else 3 for val in evidence_stances]

        print("Claim stances length: {}".format(len(claim_stances)))
        print("Evidence stances length: {}".format(len(evidence_stances)))

        add_stance = True
        stance_vector = {'Claim': claim_stances,
                         'Evidence': evidence_stances}
    else:
        add_stance = False
        stance_vector = None

    # Step 1: Loading Word2Vec data

    # mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    # while mode not in [1, 2]:
    #     mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    mode = 2

    pickle_file = None
    utils = FeatureExtractorUtils()

    if mode == 2:
        utils.load_w2v_model()
        # pickle_file = 'ce_emnlp_15_argument_structure_prediction_training.pickle'
        load_pickle = False
    else:
        # pickle_file = input("Please specify pickle file containing pre-computed embedding matrix")
        pickle_file = 'ce_emnlp_15_argument_structure_prediction_training.pickle'
        load_pickle = True

    # Step 2: Pre-processing data for classifier

    # Claim
    claim_vocab_size, claim_padding_max_length, claim_padded_docs, claim_embedding_matrix = \
        preprocess_sequence(claim_data, utils.w2v_model, pickle_file=pickle_file, key='Claim',
                            load_pickle=load_pickle, add_stance=add_stance, stance_vector=stance_vector)

    print("Claim embedding matrix shape: {}".format(claim_embedding_matrix.shape))

    # Evidence
    evidence_vocab_size, evidence_padding_max_length, evidence_padded_docs, evidence_embedding_matrix = \
        preprocess_sequence(evidence_data, utils.w2v_model, pickle_file=pickle_file, key='Evidence',
                            load_pickle=load_pickle, add_stance=add_stance, stance_vector=stance_vector)

    print("Evidence embedding matrix shape: {}".format(evidence_embedding_matrix.shape))

    test_target = [1 if val == 'link' else 0 for val in y_test]

    claim_test_data = X_test['Claim'].values
    evidence_test_data = X_test['Evidence'].values

    if add_stance_mode == 'y':
        claim_stances = X_test['Claim stance']
        evidence_stances = X_test['Evidence stance']

        claim_stances = [1 if val == 'for' else 2 if val == 'against' else 3 for val in claim_stances]
        evidence_stances = [1 if val == 'for' else 2 if val == 'against' else 3 for val in evidence_stances]

        stance_vector = {'Claim': claim_stances,
                         'Evidence': evidence_stances}

        claim_padding_max_length -= 1
        evidence_padding_max_length -= 1
        claim_vocab_size -= 3
        evidence_vocab_size -= 3

    _, _, claim_test_padded_docs, selected_claim_embedding_matrix = preprocess_sequence(claim_test_data, utils.w2v_model, pickle_file=pickle_file,
                                                          load_pickle=load_pickle, key='Claim',
                                                          padding_max_length=claim_padding_max_length,
                                                          vocab_size=claim_vocab_size, add_stance=add_stance,
                                                          stance_vector=stance_vector,
                                                          embedding_matrix=claim_embedding_matrix)

    _, _, evidence_test_padded_docs, selected_evidence_embedding_matrix = preprocess_sequence(evidence_test_data, utils.w2v_model,
                                                             pickle_file=pickle_file, load_pickle=load_pickle,
                                                             key='Evidence',
                                                             padding_max_length=evidence_padding_max_length,
                                                             vocab_size=evidence_vocab_size, add_stance=add_stance,
                                                             stance_vector=stance_vector,
                                                             embedding_matrix=evidence_embedding_matrix)

    # Step 3: Defining classifier
    utils.w2v_model = None
    # classifier = ArgumentStructurePrediction_RNN(ids=['Claim', 'Evidence'],
    #                                              vocab_size={'Claim': claim_vocab_size,
    #                                                          'Evidence': evidence_vocab_size},
    #                                              input_length={'Claim': claim_padding_max_length,
    #                                                            'Evidence': evidence_padding_max_length},
    #                                              embedding_vector_length={'Claim': 300,
    #                                                                       'Evidence': 300},
    #                                              weights={'Claim': selected_claim_embedding_matrix,
    #                                                       'Evidence': selected_evidence_embedding_matrix},
    #                                              metrics=['accuracy', model_metrics.f1],
    #                                              add_dropout=True,
    #                                              dropout=0.2
    #                                              ).build_model()

    print(claim_padded_docs.shape)
    print(evidence_padded_docs.shape)

    input_array = np.concatenate((claim_padded_docs, evidence_padded_docs), axis=1)
    print(input_array.shape)

    # classifier = ArgumentStructurePrediction_RNN_single_input(ids=['Claim', 'Evidence'],
    #                                                           input_size=(input_array.shape[1],),
    #                                                           lambda_output_shape={'Claim': claim_padded_docs.shape,
    #                                                                                'Evidence': evidence_padded_docs.shape},
    #                                                           vocab_size={'Claim': claim_vocab_size,
    #                                                                       'Evidence': evidence_vocab_size},
    #                                                           embedding_input_length={'Claim': claim_padding_max_length,
    #                                                                                   'Evidence': evidence_padding_max_length},
    #                                                           embedding_vector_length={'Claim': 300,
    #                                                                                    'Evidence': 300},
    #                                                           weights={'Claim': selected_claim_embedding_matrix,
    #                                                                    'Evidence': selected_evidence_embedding_matrix},
    #                                                           metrics=['accuracy', model_metrics.f1]).build_model()

    ASP_RNN = ArgumentStructurePrediction_RNN_single_input(ids=['Claim', 'Evidence'],
                                                              input_size=(input_array.shape[1],),
                                                              lambda_output_shape={'Claim': claim_padded_docs.shape,
                                                                                   'Evidence': evidence_padded_docs.shape},
                                                              vocab_size={'Claim': claim_vocab_size,
                                                                          'Evidence': evidence_vocab_size},
                                                              embedding_input_length={'Claim': claim_padding_max_length,
                                                                                      'Evidence': evidence_padding_max_length},
                                                              embedding_vector_length={'Claim': 300,
                                                                                       'Evidence': 300},
                                                              weights={'Claim': selected_claim_embedding_matrix,
                                                                       'Evidence': selected_evidence_embedding_matrix},
                                                              metrics=['accuracy', model_metrics.f1])
    classifier = KerasClassifier(build_fn=ASP_RNN.build_model, epochs=3, verbose=2, batch_size=64)

    # Step 4: Training
    train_target = [1 if val == 'link' else 0 for val in y_train]
    classifier.fit(input_array, train_target, batch_size=64, epochs=3, verbose=2)

    # classifier.fit([claim_padded_docs, evidence_padded_docs], train_target)
    # scores = cross_val_score(classifier, input_array, train_target,
    #                          cv=StratifiedKFold(shuffle=True, n_splits=10))

    # Step 5: Evaluation on test data
    # test_input_array = np.concatenate((claim_test_padded_docs, evidence_test_padded_docs), axis=1)
    # scores = classifier.evaluate(test_input_array, test_target, batch_size=64, verbose=2)
    # print("Metrics: {}".format(classifier.metrics_names))
    # print("Scores: {}".format(scores))








