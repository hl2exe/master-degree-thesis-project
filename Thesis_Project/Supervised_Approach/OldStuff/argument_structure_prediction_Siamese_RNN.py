"""

Argument Structure Prediction classification test with RNN.

Input: positive/negative claim-evidence couples
Output: binary classification: SUPPORTS/NOT SUPPORTS or LINK/NO-LINK

"""

"""
Note di costruzione:

La vocab_size e' il max tra vocab_size_claim e vocab_size_evidence
La input_length puo' essere: min tra i due, max tra i due, media dei due
L'embedding vector length e' un solo valore: 300
La matrice dei pesi (weights) deve contenere i dati presi da entrambi (claim ed evidence)
"""

import os

import numpy as np
import pandas as pd
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
from sklearn.metrics import f1_score, accuracy_score
from sklearn.model_selection import StratifiedKFold
from sklearn.utils import class_weight

from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from Supervised_Approach.Neural_networks.rnn_models import \
    ArgumentStructurePrediction_Siamese_RNN
from Supervised_Approach.Neural_networks.Utils import model_metrics
from itertools import product


def preprocess_sequence(sequence, model, load_pickle=False, pickle_file=None,
                        vocab_size=None, padding_max_length=None, embedding_matrix=None):

    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(sequence)
    if vocab_size is None:
        vocab_size = len(tokenizer.word_index) + 1
    encoded_data = tokenizer.texts_to_sequences(sequence)
    if padding_max_length is None:
        padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
    print("Padding max length: {}".format(padding_max_length))

    if not load_pickle:
        if embedding_matrix is None:
            embedding_matrix = np.zeros((vocab_size, 300))
        for word, i in tokenizer.word_index.items():
            try:
                embedding_vector = model[word]
            except KeyError:
                embedding_vector = np.zeros(300)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector

        if pickle_file is not None:
            if PickleUtility.verify_file(pickle_file):
                existing_data = PickleUtility.load_pickle(pickle_file)
                existing_data.update(embedding_matrix)
                PickleUtility.save_to_file(existing_data, pickle_file)
            else:
                PickleUtility.save_to_file(embedding_matrix, pickle_file)
    else:
        embedding_matrix = PickleUtility.load_pickle(pickle_file)

    return vocab_size, padding_max_length, embedding_matrix, tokenizer


# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Repeatability
    np.random.seed(7)

    # Step 0: Retrieving positive and negative examples

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'ce_emnlp_15_argument_structure_prediction_training.csv')
    data = pd.read_csv(training_data_path, encoding="utf-8")
    target = data['Argument relation'].values

    claim_data = data['Claim'].values
    evidence_data = data['Evidence'].values

    # add_stance_mode = input("Do you want to add stance as feature? Y/N: ")
    add_stance_mode = 'n'

    if add_stance_mode.lower() == 'y':
        claim_stances = data['Claim stance']
        evidence_stances = data['Evidence stance']

        claim_stances = [1 if val == 'for' else 2 if val == 'against' else 3 for val in claim_stances]
        evidence_stances = [1 if val == 'for' else 2 if val == 'against' else 3 for val in evidence_stances]

        print("Claim stances length: {}".format(len(claim_stances)))
        print("Evidence stances length: {}".format(len(evidence_stances)))

        add_stance = True
        stance_vector = {'Claim': claim_stances,
                         'Evidence': evidence_stances}
    else:
        add_stance = False
        stance_vector = None

    # Step 1: Loading Word2Vec data

    # mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    # while mode not in [1, 2]:
    #     mode = input("What to you want to do?\n1. Loading pre-computed embedding matrix\n2. On-line computation\n")
    mode = 2

    pickle_file = None
    utils = FeatureExtractorUtils()

    if mode == 2:
        utils.load_w2v_model()
        # pickle_file = 'ce_emnlp_15_argument_structure_prediction_training.pickle'
        load_pickle = False
    else:
        # pickle_file = input("Please specify pickle file containing pre-computed embedding matrix")
        pickle_file = 'ce_emnlp_15_argument_structure_prediction_training.pickle'
        load_pickle = True

    # Step 2: Pre-processing data for classifier

    # Claim
    max_vocab_size, average_padding_max_length, total_embedding_matrix, tokenizer = \
        preprocess_sequence(claim_data + evidence_data, utils.embeddings_model, pickle_file=pickle_file,
                            load_pickle=load_pickle)

    print("Siamese embedding matrix shape: {}".format(total_embedding_matrix.shape))

    claim_padded_docs = build_padded_docs(claim_data, tokenizer, average_padding_max_length, add_stance=add_stance,
                                          stance_vector=stance_vector, vocab_size=max_vocab_size, key='Claim')
    evidence_padded_docs = build_padded_docs(evidence_data, tokenizer, average_padding_max_length, add_stance=add_stance,
                                          stance_vector=stance_vector, vocab_size=max_vocab_size, key='Evidence')

    print(claim_padded_docs.shape)
    print(evidence_padded_docs.shape)

    utils.embeddings_model = None

    # Step 3: Training
    translated_target = [1 if val == 'link' else 0 for val in target]
    target_tran = np.array(translated_target)
    target_tran = target_tran.reshape((len(target), 1))
    print(target_tran.shape)

    # test_mode = input("What do you want to do?\n1. Gridsearch\n2. Cross validation\n")
    #
    # while test_mode not in [1, 2]:
    #     print("Please select one of the options!")
    #     test_mode = input("What do you want to do?\n1. Gridsearch\n2. Cross validation\n")
    test_mode = 2

    # Gridsearch
    # if test_mode == 1:
    #     n_splits = 3
    #     parameters = {'lstm_neurons': [{'Claim': 100,
    #                                    'Evidence': 100},
    #                                    {'Claim': 500,
    #                                     'Evidence': 500},
    #                                    {'Claim': 1000,
    #                                     'Evidence': 1000}]
    #                   }
    #     if len(parameters) == 1:
    #         items = list(parameters.values())[0]
    #     else:
    #         items = product(*parameters.values())
    #
    #     print("Starting gridsearch..\nTotal parameters to test: {}\n"
    #           "Total fits: {}".format(len(items), len(items) * n_splits))
    #
    #     for item in items:
    #         print("Considering parameters: {}".format(item))
    #         if len(parameters) == 1:
    #             instance_args = {parameters.keys().pop(0): item}
    #         else:
    #             instance_args = {param: item_value for (param, item_value) in zip(parameters.keys(), item)}
    #         print("Instance args: {}".format(instance_args))
    #
    #         scores = []
    #         skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
    #         splitted_indices = skf.split(claim_padded_docs, target_tran)
    #
    #         for i, (train, test) in enumerate(splitted_indices):
    #             print "Running Fold", i + 1, "/", n_splits
    #             model = None
    #             model = ArgumentStructurePrediction_Siamese_RNN(ids=['Claim', 'Evidence'],
    #                                                     vocab_size=np.max([claim_vocab_size,
    #                                                                        evidence_vocab_size]),
    #                                                     input_length=siamese_padding_max_length,
    #                                                     embedding_vector_length=300,
    #                                                     weights=total_embedding_matrix,
    #                                                     metrics=['accuracy', model_metrics.f1],
    #                                                     **instance_args
    #                                                     ).build_model()
    #
    #             print("Fold shapes:\n"
    #                   "Train: {}, {}\nTest: {}, {}".format(claim_padded_docs[train].shape,
    #                                                        evidence_padded_docs[train].shape,
    #                                                        claim_padded_docs[test].shape,
    #                                                        evidence_padded_docs[test].shape))
    #
    #             labels = np.unique(translated_target)
    #             weights = class_weight.compute_class_weight('balanced', labels, translated_target)
    #             weights_dict = {label: weight for (label, weight) in zip(labels, weights)}
    #             print("weight classes: {}".format(labels))
    #             print(weights_dict)
    #
    #             model.fit(x=[claim_padded_docs[train], evidence_padded_docs[train]], y=target_tran[train],
    #                       batch_size=64, epochs=3, verbose=2, class_weight=weights_dict)
    #             predictions = model.predict_classes([claim_padded_docs[test], evidence_padded_docs[test]],
    #                                                 batch_size=64,
    #                                                 verbose=2)
    #             f1 = f1_score(y_true=target_tran[test], y_pred=predictions, labels=['1', '0'])
    #             acc = accuracy_score(y_true=target_tran[test], y_pred=predictions)
    #             print("Fold Scores:\nF1-score: {}\nAccuracy: {}".format(f1, acc))
    #             scores.append([f1, acc])
    #
    #         print(scores)
    #         print("Average score: {}".format(np.mean(scores, axis=0)))

    # Cross validation
    if test_mode == 2:
        n_splits = 3
        skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=42)
        splitted_indices = skf.split(claim_padded_docs, target_tran)
        scores = []

        for i, (train, test) in enumerate(splitted_indices):
            print "Running Fold", i+1, "/", n_splits
            model = None
            model = ArgumentStructurePrediction_Siamese_RNN(ids=['Claim', 'Evidence'],
                                                    vocab_size=max_vocab_size,
                                                    input_length=average_padding_max_length,
                                                    embedding_vector_length=300,
                                                    weights=total_embedding_matrix,
                                                    lstm_neurons=100,
                                                    metrics=['accuracy', model_metrics.f1]).build_model()
            print("Fold shapes:\n"
                  "Train: {}, {}\nTest: {}, {}".format(claim_padded_docs[train].shape,
                                                       evidence_padded_docs[train].shape,
                                                       claim_padded_docs[test].shape,
                                                       evidence_padded_docs[test].shape))

            labels = np.unique(translated_target)
            weights = class_weight.compute_class_weight('balanced', labels, translated_target)
            weights_dict = {label: weight for (label, weight) in zip(labels, weights)}
            print("weight classes: {}".format(labels))
            print(weights_dict)

            model.fit(x=[claim_padded_docs[train], evidence_padded_docs[train]], y=target_tran[train],
                      batch_size=256, epochs=3, verbose=2, class_weight=weights_dict)
            predictions = model.predict_classes([claim_padded_docs[test], evidence_padded_docs[test]], batch_size=256,
                                                verbose=2)
            f1 = f1_score(y_true=target_tran[test], y_pred=predictions, labels=['1', '0'])
            acc = accuracy_score(y_true=target_tran[test], y_pred=predictions)
            print("Fold Scores:\nF1-score: {}\nAccuracy: {}".format(f1, acc))
            scores.append([f1, acc])

        # Step 5: Evaluation on test data
        print(scores)
        print("Average score: {}".format(np.mean(scores, axis=0)))
