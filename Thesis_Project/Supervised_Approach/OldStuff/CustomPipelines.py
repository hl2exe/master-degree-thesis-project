"""

Contains a set of specific pipeline info.
Scenarios of interest:

    1. Claim - Topic
    2. Claim - Article
    3. Article - Topic
    4. Evidence - Topic
    5. Evidence - Article

"""

#  emergent -> associations_emergent.values()
#  ce_acl_14 -> associations_ce_acl_14.values()
#  ce_emnlp_15 -> associations_ce_emnlp_15.values()

from itertools import combinations


# TODO: sistema funzioni per ottenere i parametri relativi a ciascun classificatore in modo tale che sia uniforme
# TODO: definire sistema di caricamento e lettura di tali configurazioni (sostituisce questo file)
# TODO: spostare funzioni di utility (per gridsearch e altro) in trainer.py
# TODO: tutti i dizionari privati possono essere messi in un file di configurazione
class CustomPipelines(object):

    def __init__(self, associations_mapping):
        self.associations_mapping = associations_mapping

        # Build ItemSelector key_mappings
        # [{dataframe_name -> value}]
        self.first_key_mapping = dict((df_name, associations_mapping[df_name][0]) for df_name in associations_mapping)
        self.second_key_mapping = dict((df_name, associations_mapping[df_name][1]) for df_name in associations_mapping)

        self.getters = {
            'claim-topic': self._get_claim_topic_pipeline_info,
            'claim-article': self._get_claim_article_pipeline_info,
            'article-topic': self._get_article_topic_pipeline_info,
            'evidence-topic': self._get_evidence_topic_pipeline_info,
            'evidence-article': self._get_evidence_article_pipeline_info,
            'argument-structure-prediction': self._get_argument_structure_prediction_pipeline_info,
            'margot-argument-structure-prediction': self._get_margot_argument_structure_prediction_pipeline_info
        }

        self.features_info = {
            'single_ngrams': {'has_pickle': False,
                              'additional_args': True,
                              'type': 'single_ngrams',
                              'category': 'single_target'},

            'single_skipgrams': {'has_pickle': False,
                                 'additional_args': True,
                                 'type': 'single_skipgrams',
                                 'category': 'single_target'},

            'basic': {'has_pickle': True,
                      'additional_args': False,
                      'pickle_file': 'basic.pickle',
                      'type': 'basic',
                      'category': 'single_target'},

            'repeated_punctuation': {'has_pickle': True,
                                     'additional_args': False,
                                     'pickle_file': 'repeated_punctuation.pickle',
                                     'type': 'repeated_punctuation',
                                     'category': 'single_target'},

            'sngrams': {'has_pickle': True,
                        'additional_args': False,
                        'pickle_file': 'sngrams.pickle',
                        'type': 'sngrams',
                        'category': 'single_target'},

            'POS_generalized': {'has_pickle': True,
                                'additional_args': True,
                                'type': 'dependency_relations',
                                'pickle_file': 'POS_generalized.pickle',
                                'category': 'single_target'},

            'syntactic': {'has_pickle': True,
                          'additional_args': True,
                          'type': 'dependency_relations',
                          'pickle_file': 'syntactic.pickle',
                          'category': 'single_target'},

            'cosine_similarity': {'has_pickle': True,
                                  'additional_args': False,
                                  'type': 'cosine_similarity',
                                  'pickle_file': 'cosine_similarity.pickle',
                                  'category': 'both_targets'},

            'svo_triples': {'has_pickle': True,
                            'additional_args': False,
                            'type': 'svo_triples',
                            'pickle_file': 'svo_triples.pickle',
                            'category': 'both_targets'},

            'count_tokens': {'has_pickle': True,
                             'additional_args': False,
                             'pickle_file': 'token_count.pickle',
                             'type': 'count_tokens',
                             'category': 'single_target'},

            'difference_tokens': {'has_pickle': True,
                                  'additional_args': False,
                                  'pickle_file': 'token_count_difference.pickle',
                                  'type': 'difference_tokens',
                                  'category': 'both_targets'},

            'punctuation_marks': {'has_pickle': True,
                                  'additional_args': False,
                                  'pickle_file': 'punctuation_marks.pickle',
                                  'type': 'punctuation_marks',
                                  'category': 'single_target'},

            'difference_punctuation_marks': {'has_pickle': True,
                                             'additional_args': False,
                                             'pickle_file': 'punctuation_marks_difference.pickle',
                                             'type': 'difference_punctuation_marks',
                                             'category': 'both_targets'},

            'word_pairs_binary': {'has_pickle': True,
                                  'additional_args': True,
                                  'pickle_file': 'word_pairs_binary.pickle',
                                  'type': 'word_pairs_binary',
                                  'category': 'both_targets'},

            'first_word_binary': {'has_pickle': False,
                                  'additional_args': True,
                                  'type': 'first_word_binary',
                                  'category': 'single_target'},

            'couple_first_word_binary': {'has_pickle': False,
                                         'additional_args': True,
                                         'type': 'couple_first_word_binary',
                                         'category': 'both_targets'},

            'modal_verbs_binary': {'has_pickle': True,
                                   'additional_args': False,
                                   'pickle_file': 'modal_verb_binary.pickle',
                                   'type': 'modal_verbs_binary',
                                   'category': 'single_target'},

            'common_elements': {'has_pickle': True,
                                'additional_args': False,
                                'pickle_file': 'common_elements.pickle',
                                'type': 'common_elements',
                                'category': 'both_targets'},

            'production_rules_binary': {'has_pickle': True,
                                        'additional_args': False,
                                        'pickle_file': 'production_rules_binary.pickle',
                                        'type': 'production_rules_binary',
                                        'category': 'single_target'},
        }

        self.specific_args = {
            'emergent': self._get_emergent_specific_args,
            'fnc-1': self._get_fnc_specific_args
        }

        self.additional_args = {
            'single_ngrams': {'ngram_range': (1, 2),
                              'max_features': 20000},
            'single_skipgrams': {'ngram_range': (2, 3),
                                 'skip_range': (2, 4),
                                 'max_features': 20000},
            'POS_generalized': {'key': 'POS_generalized'},
            'syntactic': {'key': 'syntactic'},
            'word_pairs_binary': {'stop_words': 'english'},
            'first_word_binary': {'stop_words': 'english'},
            'couple_first_word_binary': {'stop_words': 'english'},
        }

        self.gridsearch_parameters = {
            'single_ngrams': {'params': {'ngram_range': [(1, 1), (1, 2), (1, 3)],
                              'max_features': [None, 10000, 20000]},
                              'block': 'tfidfvectorizer'},
            'single_skipgrams': {'params': {'ngram_range': [(2, 2), (2, 3)],
                                            'skip_range': [(2, 2), (2, 3), (2, 4), (2, 5), (2, 6)],
                                            'max_features': [None, 10000, 20000]},
                                 'block': 'skipgramvectorizer'},
            'word_pairs_binary': {'params': {'stop_words': [None, 'english']},
                                  'block': 'wordpairsbinaryvectorizer'},
            'first_word_binary': {'params': {'stop_words': [None, 'english']},
                                  'block': 'firstwordbinaryextractor'},
            'couple_first_word_binary': {'params': {'stop_words': [None, 'english']},
                                         'block': 'firstwordbinarycoupleextractor'}
        }

    """
    
    PRIVATE METHODS
    
    """

    def _get_claim_topic_pipeline_info(self):
        """
        This is the set of features that has been achieved with gridsearch tests, i.e. finding the best combination
        of features.
        Scenario: Stance classification

        :return:
        """

        features = {
            "single_ngrams_first": 'single_ngrams',
            'basic_second': 'basic',
            'basic_first': 'basic',
            'single_skipgrams_first': 'single_skipgrams',
            'cosine_similarity': 'cosine_similarity',
            "svo_triples": 'svo_triples',
        }
        args = {
            'single_ngrams_first': [{'key_mapping': self.first_key_mapping},
                                    {'ngram_range': (1, 1),
                                     'max_features': None}],
            'basic_second': [{'key_mapping': self.second_key_mapping},
                             {'pre_loaded': True,
                              'pickle_mode': True,
                              'pickle_file': 'basic.pickle'}],
            'basic_first': [{'key_mapping': self.first_key_mapping},
                            {'pre_loaded': True,
                             'pickle_mode': True,
                             'pickle_file': 'basic.pickle'}],
            'single_skipgrams_first': [{'key_mapping': self.first_key_mapping},
                                       {'ngram_range': (2, 3),
                                        'skip_range': (2, 6),
                                        'max_features': 10000}],
            'cosine_similarity': [{'key_mapping': self.associations_mapping},
                                  {'pre_loaded': True,
                                   'pickle_mode': True,
                                   'pickle_file': 'cosine_similarity.pickle'}],
            'svo_triples': [{'key_mapping': self.associations_mapping},
                            {'pre_loaded': True,
                             'pickle_mode': True,
                             'pickle_file': 'svo_triples.pickle'}],
        }

        transformer_weights = {
            "single_ngrams_first": 1.,
            'basic_second': 1.,
            'basic_first': 1.,
            'single_skipgrams_first': 1.,
            'cosine_similarity': 1.,
            "svo_triples": 1,
        }

        return {
            'features': features,
            'args': args,
            'transformer_weights': transformer_weights
        }

    def _get_claim_article_pipeline_info(self):
        pass

    def _get_article_topic_pipeline_info(self):
        pass

    def _get_evidence_topic_pipeline_info(self):
        """
        This is the set of features that has been achieved with gridsearch tests, i.e. finding the best combination
        of features.
        Scenario: Stance classification

        :return:
        """

        features = {
            "single_ngrams_first": 'single_ngrams',
            'basic_second': 'basic',
            'basic_first': 'basic',
            'single_skipgrams_first': 'single_skipgrams',
            'cosine_similarity': 'cosine_similarity',
            "svo_triples": 'svo_triples',
        }
        args = {
            'single_ngrams_first': [{'key_mapping': self.first_key_mapping},
                                    {'ngram_range': (1, 1),
                                     'max_features': None}],
            'basic_second': [{'key_mapping': self.second_key_mapping},
                             {'pre_loaded': True,
                              'pickle_mode': True,
                              'pickle_file': 'basic.pickle'}],
            'basic_first': [{'key_mapping': self.first_key_mapping},
                            {'pre_loaded': True,
                             'pickle_mode': True,
                             'pickle_file': 'basic.pickle'}],
            'single_skipgrams_first': [{'key_mapping': self.first_key_mapping},
                                       {'ngram_range': (2, 3),
                                        'skip_range': (2, 6),
                                        'max_features': 10000}],
            'cosine_similarity': [{'key_mapping': self.associations_mapping},
                                  {'pre_loaded': True,
                                   'pickle_mode': True,
                                   'pickle_file': 'cosine_similarity.pickle'}],
            'svo_triples': [{'key_mapping': self.associations_mapping},
                            {'pre_loaded': True,
                             'pickle_mode': True,
                             'pickle_file': 'svo_triples.pickle'}],
        }

        transformer_weights = {
            "single_ngrams_first": 1.,
            'basic_second': 1.,
            'basic_first': 1.,
            'single_skipgrams_first': 1.,
            'cosine_similarity': 1.,
            "svo_triples": 1,
        }

        return {
            'features': features,
            'args': args,
            'transformer_weights': transformer_weights
        }

    def _get_evidence_article_pipeline_info(self):
        pass

    def _get_argument_structure_prediction_pipeline_info(self):
        """
        This is the set of features for the n-grams baseline.
        Scenario: ASP (CE-EMNLP-15)

        :return:
        """

        features = {
            "single_ngrams_first": 'single_ngrams',
            "single_ngrams_second": 'single_ngrams'
        }
        args = {
            'single_ngrams_first': [{'key_mapping': self.first_key_mapping},
                                    {'ngram_range': (1, 3),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': self.second_key_mapping},
                                     {'ngram_range': (1, 3),
                                      'max_features': 10000}],
        }

        transformer_weights = {
            "single_ngrams_first": 1,
            "single_ngrams_second": 1,
        }

        return {
            'features': features,
            'args': args,
            'transformer_weights': transformer_weights
        }

    def _get_margot_argument_structure_prediction_pipeline_info(self):
        """
        This is the set of features for the n-grams baseline.
        Scenario: MARGOT experiment

        :return:
        """

        features = {
            "single_ngrams_first": 'single_ngrams',
            "single_ngrams_second": 'single_ngrams'
        }
        args = {
            'single_ngrams_first': [{'key_mapping': self.first_key_mapping},
                                    {'ngram_range': (1, 3),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': self.second_key_mapping},
                                     {'ngram_range': (1, 3),
                                      'max_features': None}],
        }

        transformer_weights = {
            "single_ngrams_first": 1,
            "single_ngrams_second": 1,
        }

        return {
            'features': features,
            'args': args,
            'transformer_weights': transformer_weights
        }

    def _get_emergent_specific_args(self):
        """
        This is the specific set of features along with their parameters that has been achieved via gridsearch tests.
        Scenario: Stance Classification

        :return:
        """

        return {
            'single_ngrams_first': [{'key_mapping': self.first_key_mapping},
                                    {'ngram_range': (1, 1),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': self.second_key_mapping},
                                     {'ngram_range':  (1, 1),
                                      'max_features': None}],
            'single_skipgrams_first': [{'key_mapping': self.first_key_mapping},
                                       {'ngram_range':  (2, 3),
                                        'max_features': 10000,
                                        'skip_range': (2, 4)}],
            'single_skipgrams_second': [{'key_mapping': self.second_key_mapping},
                                        {'ngram_range':  (2, 3),
                                         'max_features': 10000,
                                         'skip_range':   (2, 6)}]
        }

    def _get_fnc_specific_args(self):
        pass

    """
    
    PUBLIC METHODS
    
    """

    def get_pipeline_info(self, key):
        """
        Returns specific pre-defined pipeline structure info by given key.
        The key indicates the scenario of interest.

        :param key: scenario of interest.
        :return: pipeline info, i.e. dictionary of 3 dictionaries: features, args, transformer weights.
        """

        return self.getters[key]()

    def build_transformer_weights_combinations(self, features):
        """
        Computes all possibile features combinations.

        :param features: list of features
        :return: list of dictionaries entries used as gridsearch parameters.
        """

        # Building weights
        gridsearch_trasnformer_weights_combinations = []
        for key in features:
            key_dict = dict({(f_key, 1) if f_key == key else (f_key, 0) for f_key in features})
            gridsearch_trasnformer_weights_combinations.append(key_dict)

        all_features = dict({(f_key, 1) for f_key in features})
        gridsearch_trasnformer_weights_combinations.append(all_features)

        for dim in range(2, len(features)):
            dim_combinations = combinations(features, dim)
            for item in dim_combinations:
                key_dict = dict({(f_key, 1) if f_key in item else (f_key, 0) for f_key in features})
                gridsearch_trasnformer_weights_combinations.append(key_dict)

        print("Weight combinations: {}".format(len(gridsearch_trasnformer_weights_combinations)))
        # for item in gridsearch_trasnformer_weights_combinations:
        #     print(item)

        return gridsearch_trasnformer_weights_combinations, all_features

    def get_gridsearch_parameters(self, features, classifier_block_name, transformer_weights=None, classifiers=None):
        """
        Builds gridsearch parameters from given list of features.
        Transformer weights and classifiers parameters can be added optionally.

        :param features:
        :param classifier_block_name: name of the classifier pipeline block
        :param transformer_weights:
        :param classifiers:
        :return: gridsearch paramaters dictionary
        """

        suffix = 'featureunion'
        parameters = {}
        for key in features:
            if features[key] in self.gridsearch_parameters:
                for param in self.gridsearch_parameters[features[key]]['params']:
                    block_name = self.gridsearch_parameters[features[key]]['block']
                    param_key = '{0}__{1}__{2}__{3}'.format(suffix, key, block_name, param)
                    parameters[param_key] = self.gridsearch_parameters[features[key]]['params'][param]

        if transformer_weights is not None:
            parameters.update({'{}__transformer_weights'.format(suffix): transformer_weights})

        if classifiers is not None:
            parameters.update({classifier_block_name: classifiers})

        return parameters

    def _filter_dict_by_condition(self, data, condition):

        return dict({key: data[key] for key in data if condition(key)})

    def split_args_and_features_by_target(self, args, features):
        """
        Splits args and features by target

        :param args:
        :param features:
        :return:
        """

        first_args = self._filter_dict_by_condition(args, lambda x: 'first' in x or
                                                                    ('first' not in x and 'second' not in x))

        second_args = self._filter_dict_by_condition(args, lambda x: 'second' in x or
                                                                     ('first' not in x and 'second' not in x))

        first_features = self._filter_dict_by_condition(features, lambda x: 'first' in x or
                                                                            ('first' not in x and 'second' not in x))

        second_features = self._filter_dict_by_condition(features, lambda x: 'second' in x or
                                                                             ('first' not in x and 'second' not in x))

        return first_args, first_features, second_args, second_features

    def get_all_features_args(self):
        """
        Builds features info for each supported feature. Arguments values are set to class default.

        :return: 2 dictionaries: features, args
        """

        args = {}
        features = {}

        for feature_info in self.features_info:
            # Category
            if self.features_info[feature_info]['category'] == 'single_target':
                for target_info, suffix in zip([self.first_key_mapping, self.second_key_mapping], ['first', 'second']):
                    current_key = '{}_{}'.format(feature_info, suffix)
                    args[current_key] = [{'key_mapping': target_info}]

                    # pickle + Additional args
                    second_block_dict = {}
                    if self.features_info[feature_info]['has_pickle']:
                        second_block_dict.update({'utils': None,
                                                  'pre_loaded': True,
                                                  'pickle_mode': True,
                                                  'pickle_file': self.features_info[feature_info]['pickle_file']})

                    if self.features_info[feature_info]['additional_args']:
                        second_block_dict.update(self.additional_args[feature_info])

                    args[current_key].append(second_block_dict)

                    # Type
                    features[current_key] = self.features_info[feature_info]['type']
            else:
                current_key = feature_info
                args[feature_info] = [{'key_mapping': self.associations_mapping}]

                # pickle + Additional args
                second_block_dict = {}
                if self.features_info[feature_info]['has_pickle']:
                    second_block_dict.update({'utils': None,
                                              'pre_loaded': True,
                                              'pickle_mode': True,
                                              'pickle_file': self.features_info[feature_info]['pickle_file']})

                if self.features_info[feature_info]['additional_args']:
                    second_block_dict.update(self.additional_args[feature_info])

                args[current_key].append(second_block_dict)

                # Type
                features[current_key] = self.features_info[feature_info]['type']

        return args, features

    def get_argument_structure_prediction_specific_args(self):
        """
        This is the specific set of features for the Stab & Gurevych classifier. The feature parameters have been
        determined via gridsearch test.
        Scenario: ASP (CE-EMNLP-15)

        :return:
        """

        return {
            'couple_first_word_binary': [{'key_mapping': self.associations_mapping},
                                         {'stop_words': None}],
            'first_word_binary_first': [{'key_mapping': self.first_key_mapping},
                                        {'stop_words': None}],
            'first_word_binary_second': [{'key_mapping': self.second_key_mapping},
                                         {'stop_words': None}],
            'word_pairs_binary': [{'key_mapping': self.associations_mapping},
                                  {'utils': None,
                                   'stop_words': None,
                                   'pre_loaded': True,
                                   'pickle_mode': True,
                                   'pickle_file': 'word_pairs_binary.pickle'}]
        }

    def get_margot_argument_structure_prediction_specific_args(self):
        """
        This is the specific set of features for the Stab & Gurevych classifier. The feature parameters have been
        determined via gridsearch test.
        Scenario: MARGOT experiment

        :return:
        """

        return {
            'couple_first_word_binary': [{'key_mapping': self.associations_mapping},
                                         {'stop_words': 'english'}],
            'first_word_binary_first': [{'key_mapping': self.first_key_mapping},
                                        {'stop_words': 'english'}],
            'first_word_binary_second': [{'key_mapping': self.second_key_mapping},
                                         {'stop_words': 'english'}],
            'word_pairs_binary': [{'key_mapping': self.associations_mapping},
                                  {'utils': None,
                                   'pre_loaded': True,
                                   'pickle_mode': True,
                                   'pickle_file': 'word_pairs_binary.pickle'}]
        }
