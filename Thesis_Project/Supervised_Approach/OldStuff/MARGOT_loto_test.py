"""

Training classifiers on MARGOT custom data-set.
Successively predictions are made for each topic
related data belonging to CE-EMNLP-15 train and
test set. Precisely, classification report,
confusion matrix, accuracy and f1 score are
computed for each data associated with the
currently selected topic out of the 39 of
interest.
Averages are also computed

"""


import os

import pandas as pd
from Supervised_Approach.Utils.config_utils import ConfigUtils
from Supervised_Approach.Utils.dataset_utils import DatasetUtils

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierType, ClassifierFactory
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from sklearn import metrics
import numpy as np
from sklearn.model_selection import LeaveOneGroupOut
from Supervised_Approach.collector import Collector
import pprint

# Windows multiprocessing requires this, since every process runs the whole module.
if __name__ == "__main__":

    # Step 1: Train data

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, inverted_columns_mapping = ScenarioManager\
        .get_associations_mapping(config_file)

    training_data_path = os.path.join(POST_PROCESS_DIR,
                                      'MARGOT_argument_structure_prediction_training_balanced_clean.csv')
    train_data = pd.read_csv(training_data_path, encoding="utf-8")

    train_data.name = 'margot-argument-structure-prediction'
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data["Argument relation"].values

    # classifier_info = {'class_weight': 'balanced'}
    classifier_info = {'class_weight': 'balanced', 'alpha': 0.0001, 'penalty': 'l2', 'loss': 'hinge'}
    classifier = ClassifierFactory.factory(ClassifierType.linear_svm, **classifier_info)
    # classifier = ClassifierFactory.factory(ClassifierType.linear_svm')
    trainer = Trainer()

    custom_pipelines = CustomPipelines(associations_mapping=associations_mapping)
    args, features = custom_pipelines.get_all_features_args()

    # pipeline_info = custom_pipelines.get_pipeline_info('margot-argument-structure-prediction')
    # pipeline = trainer.pipeline(features=pipeline_info['features'], args=pipeline_info['args'], classifier=classifier)

    # test_features_list = ['cosine_similarity']
    test_features_list = [
                          'single_ngrams_first',
                          'single_ngrams_second',
                          # 'single_skipgrams_first',
                          # 'single_skipgrams_second',
                          'svo_triples',
                          'basic_first',
                          'basic_second',
                          'cosine_similarity',
                          'POS_generalized_first',
                          'POS_generalized_second',
                          'repeated_punctuation_first',
                          'repeated_punctuation_second',
                          'sngrams_first',
                          'sngrams_second',
                          'syntactic_first',
                          'syntactic_second'
                          ]
    # test_features_list = ['count_tokens_first',
    #                       'count_tokens_second',
    #                       'difference_tokens',
    #                       'punctuation_marks_first',
    #                       'punctuation_marks_second',
    #                       'difference_punctuation_marks',
    #                       'word_pairs_binary',
    #                       'first_word_binary_first',
    #                       'first_word_binary_second',
    #                       'couple_first_word_binary',
    #                       'modal_verbs_binary_first',
    #                       'modal_verbs_binary_second',
    #                       'common_elements',
    #                       'production_rules_binary_first',
    #                       'production_rules_binary_second']
    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    # test_args.update(custom_pipelines.get_margot_argument_structure_prediction_specific_args())

    test_args.update({
            'single_ngrams_first': [{'key_mapping': custom_pipelines.first_key_mapping},
                                    {'ngram_range': (1, 3),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': custom_pipelines.second_key_mapping},
                                     {'ngram_range': (1, 2),
                                      'max_features': 20000}],
        })

    pipeline = trainer.build_pipeline(features=test_features, args=test_args, classifier=classifier)

    print("Training classifier..")
    # print("Features: {}".format(pipeline_info['features']))
    print("Features: {}".format(test_features))
    pipeline.fit(train_data, train_target)
    print("Training completed!")

    # Step 2: Test data

    test_file = 'ce_emnlp_15_ASP_train_and_test_training.csv'
    path_to_csv_test = DatasetUtils.get_path_to_dataset(test_file, 'post_process')
    test_data = pd.read_csv(path_to_csv_test, encoding="utf-8")
    df_name = "margot-argument-structure-prediction"
    test_data = test_data.rename(columns=columns_mapping[df_name])
    test_data.name = df_name
    test_target = test_data['Argument relation'].values

    logo = LeaveOneGroupOut()
    topic_map = {topic: idx for idx, topic in enumerate(np.unique(test_data['Topic'].values))}
    groups = [topic_map[topic] for topic in test_data['Topic'].values]
    n_splits = logo.get_n_splits(X=test_data, y=test_target, groups=groups)

    scores = {}

    for idx, (_, test_index) in enumerate(logo.split(X=test_data, y=test_target, groups=groups)):
        current_group = np.unique(test_data.iloc[test_index]['Topic'].values)[0]
        print("Predicting on group: {}".format(current_group))

        X_test = test_data.iloc[test_index]
        y_test = test_target[test_index]

        predictions = pipeline.predict(X_test)

        f1 = metrics.f1_score(y_pred=predictions, y_true=y_test, pos_label='link')
        accuracy = metrics.accuracy_score(y_pred=predictions, y_true=y_test)

        predicted_series = pd.Series(predictions, name='Predicted')
        true_series = pd.Series(y_test, name='True')

        precision, recall, f1score, support = metrics.precision_recall_fscore_support(y_true=y_test, y_pred=predictions,
                                                                                      average=None,
                                                                                      pos_label='link',
                                                                                      labels=['link', 'no-link'])
        confusion_matrix = pd.crosstab(true_series, predicted_series)

        scores[current_group] = {'f1-score': f1,
                                 'accuracy': accuracy,
                                 'report': {'precision': precision,
                                            'recall': recall,
                                            'f1-score': f1score,
                                            'support': support},
                                 'confusion_matrix': confusion_matrix}

    print("Average f1: {}".format(np.mean([scores[idx]['f1-score'] for idx in scores])))
    print("Average accuracy: {}".format(np.mean([scores[idx]['accuracy'] for idx in scores])))

    keys = list(scores.keys())
    accumulator_confusion_matrix = scores[keys[0]]['confusion_matrix']
    for idx in range(1, len(keys)):
        accumulator_confusion_matrix = accumulator_confusion_matrix.add(scores[keys[idx]]['confusion_matrix'],
                                                                        fill_value=0)

    print("Confusion matrix:\n {}".format(accumulator_confusion_matrix))

    per_class_avg_precision = np.mean([scores[idx]['report']['precision'] for idx in scores], axis=0)
    per_class_avg_recall = np.mean([scores[idx]['report']['recall'] for idx in scores], axis=0)
    per_class_avg_f1 = np.mean([scores[idx]['report']['f1-score'] for idx in scores], axis=0)
    per_class_support = np.sum([scores[idx]['report']['support'] for idx in scores], axis=0)
    total_support = np.sum(per_class_support)

    print("Classification report:\n"
          "          precision recall    f1-score   support\n"
          "link:        {0}       {1}      {2}        {3}\n"
          "no-link:     {4}       {5}      {6}        {7}\n"
          "avg/total:   {8}       {9}      {10}       {11}"
        .format(per_class_avg_precision[0],
                per_class_avg_recall[0],
                per_class_avg_f1[0],
                per_class_support[0],
                per_class_avg_precision[1],
                per_class_avg_recall[1],
                per_class_avg_f1[1],
                per_class_support[1],
                (per_class_avg_precision[0] * per_class_support[0] +
                 per_class_avg_precision[1] * per_class_support[1]) / total_support,
                (per_class_avg_recall[0] * per_class_support[0] +
                 per_class_avg_recall[1] *  per_class_support[1]) / total_support,
                (per_class_avg_f1[0] * per_class_support[0] +
                 per_class_avg_f1[1] * per_class_support[1]) / total_support,
                total_support))

    # Saving results

    filename = 'margot-topic-results-ce-emnlp-15-train-and-test.txt'
    folder = None   # default: STATISTICS_DIR
    header_data = 'Classifier: Stance classification\n' \
                  'LinearSVM: alpha: 0.0001, penalty: l2, loss: hinge'

    # If you want to use saved data I suggest saving to pickle format or csv
    Collector.save_results(pprint.pformat(scores, width=1), filename, folder, header_data)

