"""

Argument Structure Prediction classification test

Input: positive/negative claim-evidence couples
Output: binary classification: SUPPORTS/NOT SUPPORTS or LINK/NO-LINK

Step 1: Retrieving positive and negative examples
Step 2: Defining the classifier
Step 3: Training
[Step 4]: Viewing classification results

Examples:

    [Gridsearch]

    1. Feature calibration and classifier choice:

        classifier_info = {'class_weight': 'balanced'}
        classifier = ClassifierFactory.factory(ClassifierType.linear_svm, **classifier_info)

        pipeline = trainer.pipeline(features=test_features, args=test_args, classifier=classifier)

        classifiers = [classifier,
                       ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info),
                       ClassifierFactory.factory(ClassifierType.linear_svc, **classifier_info)
                       ]

        parameters = customPipelines.get_gridsearch_parameters(features=test_features,
                                                               classifier_block_name='sgdclassifier',
                                                               classifiers=classifiers)

        scoring = {'Accuracy': 'accuracy',
                   'F1-score': make_scorer(f1_score, pos_label='link')}
        tuned = trainer.gridsearch(pipeline, parameters, train_data, train_target, test_features,
                                   save_cv_result=True,
                                   cv=StratifiedKFold(shuffle=True, random_state=42),
                                   n_jobs=3,
                                   verbose=5,
                                   scoring=scoring,
                                   refit='F1-score')

    2. Classifier calibration:

        classifier_info = {'class_weight': 'balanced'}
        classifier = ClassifierFactory.factory(ClassifierType.linear_svm, **classifier_info)

        Stance classifier:
                                test_args.update({
                                'single_ngrams_first': [{'key_mapping': customPipelines.first_key_mapping},
                                                        {'ngram_range': (1, 1),
                                                         'max_features': None}],
                                'single_ngrams_second': [{'key_mapping': customPipelines.second_key_mapping},
                                                         {'ngram_range': (1, 3),
                                                          'max_features': 10000}],
                                })

        Stab & Gurevych classifier:
                                test_args.update(customPipelines.get_argument_structure_prediction_specific_args())

        [Occorre commentare tutta la parte relativa a test_features e test_args]
        Baseline n-gram classifier:
                                pipeline_info = customPipelines.get_pipeline_info('argument-structure-prediction')
                                print(pipeline_info['features'])
                                pipeline = trainer.pipeline(features=pipeline_info['features'], args=pipeline_info['args'], classifier=classifier)

        parameters = {'sgdclassifier__loss': ['log', 'hinge'],
                      'sgdclassifier__penalty': ['l1', 'l2', 'elasticnet'],
                      'sgdclassifier__alpha': [0.001, 0.0001, 0.00001, 0.000001]}


    [Scoring]

    scores = cross_val_score(estimator=pipeline, X=train_data, y=train_target,
                             cv=StratifiedKFold(shuffle=True, n_splits=10, random_state=42), verbose=5,
                             scoring=f1_scorer)

    [Step 4]: Viewing classification results
    print("Average score: {}".format(np.mean(scores)))

"""

import os

import pandas as pd
from sklearn.model_selection import StratifiedKFold

from Supervised_Approach.OldStuff.ScenarioManager import ScenarioManager
from Supervised_Approach.classifier_factory import ClassifierFactory, ClassifierType
from Supervised_Approach.OldStuff.CustomPipelines import CustomPipelines
from Supervised_Approach.path_definitions import POST_PROCESS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.config_utils import ConfigUtils
from sklearn.metrics import make_scorer, f1_score


def f1_scorer(estimator, X, y):
    y_pred = estimator.predict(X)
    return f1_score(y_pred=y_pred, y_true=y, pos_label='link')


# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    # Variables: change these according to your scenario
    dataset_name = 'ce_emnlp_15_ASP_held_out_training.csv'
    train_data_target_column = "Argument relation"
    classifier_info = {'class_weight': 'balanced'}
    # classifier_info = {'class_weight': 'balanced', 'C': 0.001, 'penalty': 'l2'}

    # Step 1: Retrieving positive and negative examples

    config_file = "trainer"
    config = ConfigUtils.load_config(config_file)

    associations_mapping, columns_mapping, _ = ScenarioManager.get_associations_mapping(config_file)

    training_data_path = os.path.join(POST_PROCESS_DIR, dataset_name)
    train_data = pd.read_csv(training_data_path, encoding="utf-8")

    train_data.name = 'argument-structure-prediction'
    train_data = train_data.rename(columns=columns_mapping[train_data.name])
    train_target = train_data[train_data_target_column].values

    # Step 2: Defining the classifier

    customPipelines = CustomPipelines(associations_mapping=associations_mapping)
    args, features = customPipelines.get_all_features_args()

    classifier = ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info)
    trainer = Trainer()

    # Selecting ones of interest
    test_features_list = [
                          'single_ngrams_first',
                          'single_ngrams_second',
                          # 'single_skipgrams_first',
                          # 'single_skipgrams_second',
                          'svo_triples',
                          'basic_first',
                          'basic_second',
                          'cosine_similarity',
                          'POS_generalized_first',
                          'POS_generalized_second',
                          'repeated_punctuation_first',
                          'repeated_punctuation_second',
                          'sngrams_first',
                          'sngrams_second',
                          'syntactic_first',
                          'syntactic_second'
                          ]
    # test_features_list = ['cosine_similarity']
    # test_features_list = ['count_tokens_first',
    #                       'count_tokens_second',
    #                       'difference_tokens',
    #                       'punctuation_marks_first',
    #                       'punctuation_marks_second',
    #                       'difference_punctuation_marks',
    #                       'word_pairs_binary',
    #                       'first_word_binary_first',
    #                       'first_word_binary_second',
    #                       'couple_first_word_binary',
    #                       'modal_verbs_binary_first',
    #                       'modal_verbs_binary_second',
    #                       'common_elements',
    #                       'production_rules_binary_first',
    #                       'production_rules_binary_second']
    test_args = dict({key: args[key] for key in args if key in test_features_list})
    test_features = dict({key: features[key] for key in features if key in test_features_list})

    # test_args.update(customPipelines.get_argument_structure_prediction_specific_args())
    #
    test_args.update({
            'single_ngrams_first': [{'key_mapping': customPipelines.first_key_mapping},
                                    {'ngram_range': (1, 1),
                                     'max_features': None}],
            'single_ngrams_second': [{'key_mapping': customPipelines.second_key_mapping},
                                     {'ngram_range': (1, 3),
                                      'max_features': 10000}],
        })

    pipeline = trainer.build_pipeline(features=test_features, args=test_args, classifier=classifier)

    # pipeline_info = customPipelines.get_pipeline_info('argument-structure-prediction')
    # print(pipeline_info['features'])
    # pipeline = trainer.pipeline(features=pipeline_info['features'], args=pipeline_info['args'], classifier=classifier)

    # Step 3: Training

    # classifiers = [classifier,
    #                ClassifierFactory.factory(ClassifierType.logistic_regression, **classifier_info),
    #                ClassifierFactory.factory(ClassifierType.linear_svc, **classifier_info)
    #                ]
    # parameters = customPipelines.get_gridsearch_parameters(features=test_features,
    #                                                        classifier_block_name='sgdclassifier',
    #                                                        classifiers=classifiers)

    #
    # parameters = {'linearsvc__C': [0.001, 0.01, 0.1, 1.0, 10],
    #               'linearsvc__loss': ['hinge', 'squared_hinge']
    #               }
    parameters = {'logisticregression__C': [0.001, 0.01, 0.1, 1.0, 10],
                  'logisticregression__penalty': ['l1', 'l2']}
    # parameters = {'sgdclassifier__loss': ['log', 'hinge'],
    #               'sgdclassifier__penalty': ['l1', 'l2', 'elasticnet'],
    #               'sgdclassifier__alpha': [0.001, 0.0001, 0.00001, 0.000001]}

    print(parameters)
    #
    scoring = {'Accuracy': 'accuracy',
               'F1-score': make_scorer(f1_score, pos_label='link')}
    tuned = trainer.gridsearch(pipeline, parameters, train_data, train_target, test_features,
                               save_cv_result=True,
                               cv=StratifiedKFold(shuffle=True, random_state=42),
                               n_jobs=3,
                               verbose=5,
                               scoring=scoring,
                               refit='F1-score')

    # scores = cross_val_score(estimator=pipeline, X=train_data, y=train_target,
    #                          cv=StratifiedKFold(shuffle=True, n_splits=10, random_state=42), verbose=5,
    #                          scoring=f1_scorer)

    # [Step 4]: Viewing classification results
    # print("Average score: {}".format(np.mean(scores)))
