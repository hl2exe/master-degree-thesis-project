"""
Simple selectors that pre-process given input (dataframe)

1. ItemSelector: selects one column of the given dataframe

2. ConcatenateSelector: selects two or more columns of the given
dataframe and builds up a list in which each i-th element is the concatenation
of the i-th elements of the selected columns

3. CoupleSelector: selects two or more columns of the given dataframe
and builds up a list of tuples in which each i-th tuple is composed by the
i-th elements of the selected columns
"""

from sklearn.base import BaseEstimator


class ItemSelector(BaseEstimator):
    """
    Selects all values from a specific dataframe column (key_index).
    """

    def __init__(self, key_mapping):
        self.key_mapping = key_mapping

    def fit(self, x, y=None):
        return self

    def transform(self, dataframe):
        try:
            key_index = dataframe.name
        except AttributeError:
            key_index = [key for key in self.key_mapping if self.key_mapping[key] in dataframe.columns.values][0]

        return dataframe[self.key_mapping[key_index]].values


class ConcatenateSelector(BaseEstimator):
    """
    Selects all values from a specific list of columns (key_list_index).
    All values are concatenated.
    """

    def __init__(self, key_mapping):
        self.key_mapping = key_mapping

    def fit(self, x, y=None):
        return self

    def transform(self, dataframe):
        try:
            key_list_index = dataframe.name
        except AttributeError:
            key_list_index = [key for key in self.key_mapping
                              if set(self.key_mapping[key].values()) < set(dataframe.columns.values)][0]

        lists = []
        for key in self.key_mapping[key_list_index].values():
            lists.append(dataframe[key].values)

        return [" ".join(values) for values in zip(*lists)]


class CoupleSelector(BaseEstimator):
    """
    Selects all values from a specific list of columns (key_list_index).
    Values are returned in tuple format.
    """

    def __init__(self, key_mapping):
        self.key_mapping = key_mapping

    def fit(self, X, y=None):
        return self

    def transform(self, dataframe):
        try:
            key_list_index = dataframe.name
        except AttributeError:
            key_list_index = [key for key in self.key_mapping
                              if set(self.key_mapping[key].values()) < set(dataframe.columns.values)][0]

        lists = []
        for key in self.key_mapping[key_list_index].values():
            lists.append(dataframe[key].values)

        return [tuple(values) for values in zip(*lists)]
