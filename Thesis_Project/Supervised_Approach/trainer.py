import os
from collections import OrderedDict

import numpy as np
import pandas as pd
import sklearn.model_selection as ms
from sklearn import metrics

from classifier_factory import ClassifierFactory
from path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Utils.gridsearch_utils import PrebuiltCV, GridSearchUtils
from Utils.json_utils import load_json_from_file, load_simplejson_from_file, save_simplejson_to_file
from Supervised_Approach.Neural_networks.rnn_models import BaseNetwork


class Trainer(object):
    """

    PUBLIC METHODS

    """

    features_info = load_json_from_file('features.json')
    args_info = load_simplejson_from_file('features_args.json')

    @staticmethod
    def view_sklearn_metrics(predictions, target_column):
        """
        Computes sklearn.metrics.classification_report function.

        :param predictions: array of classification predictions
        :param target_column: true values column name
        :return: result of metrics.classification_report function
        """

        target_names = list(set(target_column))

        predicted_series = pd.Series(predictions, name='Predicted')
        true_series = pd.Series(target_column, name='True')

        report = {}
        precision, recall, f1score, support = metrics.precision_recall_fscore_support(y_true=target_column,
                                                                                      y_pred=predictions,
                                                                                      labels=target_names, average=None)

        for header_name, value_array in zip(['precision', 'recall', 'f1score', 'support'],
                                            [precision, recall, f1score, support]):
            report[header_name] = {target_name: header_value for target_name, header_value
                                   in zip(target_names, value_array.tolist())}

        report['precision'].update({'average': float(np.average(precision, weights=support))})
        report['recall'].update({'average': float(np.average(recall, weights=support))})
        report['f1score'].update({'average': float(np.average(f1score, weights=support))})
        report['support'].update({'total': int(np.sum(support))})

        confusion_matrix = pd.crosstab(true_series, predicted_series)

        return report, confusion_matrix

    @staticmethod
    def save_pipeline_configuration(pipeline, configuration_name, num_targets, folder=None, weighted_features=None):
        """
        Saves pipeline configuration in JSON format

        :param pipeline: classifier pipeline
        :param configuration_name: name of the classifier configuration
        :param num_targets: number of targets of classification (usually it is equal to 2)
        :param folder: classifier configuration folder
        :return: None
        """

        data_to_save = {'features': {},
                        'classifier': {}}

        pipeline_params = pipeline.get_params(deep=True)
        features_dict = {name: pipeline for name, pipeline in pipeline_params['featureunion__transformer_list']}

        if weighted_features:
            features_dict = {name: pipeline for name, pipeline in features_dict.items() if name in weighted_features}

        features_set = set()
        features_targets_info = {}
        for feature_name in features_dict:
            suffix_search = False
            for suffix in range(num_targets):
                suffix = str(suffix + 1)
                if feature_name.lower().endswith(suffix.lower()):
                    general_feature_name = feature_name[:len(feature_name) - (len(suffix) + 1)]
                    features_set.add(general_feature_name)
                    if feature_name in features_targets_info:
                        features_targets_info[feature_name].append(suffix)
                    else:
                        features_targets_info[feature_name] = [suffix]

                    args_dict = {}
                    if Trainer.features_info[general_feature_name]['additional_args']:
                        block_name = Trainer.args_info[general_feature_name]['block']
                        args_list = Trainer.args_info[general_feature_name]['params'].keys()

                        pipeline_steps_dict = {name: step for name, step in
                                               features_dict[feature_name].get_params(deep=True)['steps']}

                        args_step = pipeline_steps_dict[block_name]
                        args_dict = {name: getattr(args_step, name) for name in args_list}

                    if general_feature_name in data_to_save['features']:
                        data_to_save['features'][general_feature_name][suffix] = args_dict
                    else:
                        data_to_save['features'][general_feature_name] = {suffix: args_dict}

                    # Next feature
                    suffix_search = True
                    break

            # not single_target features
            if not suffix_search:
                features_set.add(feature_name)

                if Trainer.features_info[feature_name]['additional_args']:
                    block_name = Trainer.args_info[feature_name]['block']
                    args_list = Trainer.args_info[feature_name]['params'].keys()

                    pipeline_steps_dict = {name: step for name, step in
                                           features_dict[feature_name].get_params(deep=True)['steps']}

                    args_step = pipeline_steps_dict[block_name]
                    args_dict = {name: getattr(args_step, name) for name in args_list}
                    data_to_save['features'][feature_name] = args_dict

        classifier_name = type(pipeline_params['steps'][-1][1]).__name__.lower()
        classifier_step = pipeline_params['steps'][-1][1]

        if classifier_name in GridSearchUtils.classifier_info:
            args_list = GridSearchUtils.classifier_info[classifier_name].keys()
            classifier_dict = {name: getattr(classifier_step, name) for name in args_list}

            data_to_save['classifier'][classifier_name] = classifier_dict

        if folder is None:
            folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
            if not os.path.isdir(folder):
                os.mkdir(folder)

        configuration_file = '{}.json'.format(configuration_name)
        save_simplejson_to_file(configuration_file, data_to_save, folder)

    @staticmethod
    def load_pipeline_configuration(configuration_name, folder=None):
        """
        Loads pipeline configuration from JSON file

        :param configuration_name: classifier configuration name
        :param folder: classifier configuration folder
        :return: features set, features targets info, additional features args,
        name of the classifier algorithm and additional classifier algorithm
        init args
        """

        if folder is None:
            folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)

        configuration_file = '{}.json'.format(configuration_name)
        configuration_info = load_simplejson_from_file(configuration_file, folder)

        features_set = set(configuration_info['features'].keys())
        features_targets_info = {}
        additional_args = {}

        for feature_name in configuration_info['features']:
            if configuration_info['features'][feature_name]:
                features_targets_info[feature_name] = [int(key) for key in
                                                       configuration_info['features'][feature_name].keys()]
                additional_args[feature_name] = {int(key): value for key, value in
                                                 configuration_info['features'][feature_name].items()}

        classifier_name = configuration_info['classifier'].keys()[0]
        classifier_args = configuration_info['classifier'][classifier_name]

        return features_set, features_targets_info, additional_args, classifier_name, classifier_args

    @staticmethod
    def save_network_configuration(network, configuration_name, folder=None):
        """
        Saves neural network configuration to JSON format

        :param network: neural network model
        :param configuration_name: classifier configuration name
        :param folder: classifier configuration folder
        :return: None
        """

        if not isinstance(network, BaseNetwork):
            raise ValueError('Network should derive from Neural_networks.rnn_models.BaseNetwork. Found {}'
                             .format(type(network)))

        network_type = type(network).__name__

        data_to_save = dict()
        data_to_save['params'] = network.__dict__
        data_to_save['network_type'] = network_type

        if folder is None:
            folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
            if not os.path.isdir(folder):
                os.mkdir(folder)

        configuration_file = '{}.json'.format(configuration_name)
        save_simplejson_to_file(configuration_file, data_to_save, folder)

    @staticmethod
    def load_network_configuration(configuration_name, folder=None):
        """
        Loads neural network configuration from JSON file

        :param configuration_name:
        :param folder:
        :return:
        """

        if folder is None:
            folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)

        configuration_file = '{}.json'.format(configuration_name)
        configuration_info = load_simplejson_from_file(configuration_file, folder)

        network_type = configuration_info['network_type']
        network = ClassifierFactory.factory(network_type, model_type='network', **configuration_info['params'])

        return network

    @staticmethod
    def get_associations_mapping(df_name, targets):
        """
        Single data-set version of get_associations_mapping_from_dict

        :param df_name: data-set name
        :param targets: targets list (column names)
        :return: 3 dictionaries: associations mapping, columns mapping and inverse columns mapping
        """

        datasets_info = {
            'train': {
                'df_name': df_name,
                'targets': targets
            }
        }

        return Trainer.get_associations_mapping_from_dict(datasets_info)

    @staticmethod
    def get_associations_mapping_from_dict(datasets_info):
        """
        Builds data-set and scenario mappings for general classification behaviour.
        Obtained mappings are used by data selectors inside the pipeline in order to filter data correctly in
        accordance to specific case of interest.

        :param datasets_info: dictionary that for each set (train or test) contains
        data-set information like data-set name and targets list
        :return: 3 dictionaries: associations mapping, columns mapping and inverse columns mapping
        """

        associations_mapping = OrderedDict()
        columns_mapping = OrderedDict()
        inverted_columns_mapping = OrderedDict()

        datasets_info_parsed = {datasets_info[key]['df_name']: datasets_info[key]['targets'] for key in datasets_info}

        for df_name in datasets_info_parsed:
            associations_mapping[df_name] = {idx + 1: "{}-{}".format(target, df_name)
                                             for idx, target in enumerate(datasets_info_parsed[df_name])}

            columns_mapping[df_name] = OrderedDict({target: "{}-{}".format(target, df_name)
                                                    for target in datasets_info_parsed[df_name]})

            inverted_columns_mapping[df_name] = OrderedDict({"{}-{}".format(target, df_name): target
                                                             for target in datasets_info_parsed[df_name]})

        return associations_mapping, columns_mapping, inverted_columns_mapping

    @staticmethod
    def build_pipeline_info(features_set, features_targets_info, associations_mapping, num_targets,
                            additional_args=None, classifier_info=None, classifier_type=None):
        """

        :param features_set: set of feature names
        :param features_targets_info: dictionary that specifies for each feature
        its classification targets
        :param associations_mapping: dictionary columns mapping
        :param num_targets: number of targets of classification (usually it is equal to 2)
        :param additional_args: additional features pipeline args
        :param classifier_info: args for classifier algorithm init method
        :param classifier_type: name (string) of the classifier class
        :return:
        """

        # Step 1: building features -> for each feature in features_list checks whether is appears as a key in
        # features_targets_info. If yes: add feature for each given suffix; if no: add feature for each default suffix

        features = {}
        args = {}

        for feature in features_set:

            if Trainer.features_info[feature]['category'] == 'single_target':
                targets = features_targets_info[feature] if feature in features_targets_info \
                    else [str(idx + 1) for idx in range(num_targets)]
                for target in targets:
                    current_key = '{}_{}'.format(feature, target)
                    args[current_key] = [{'key_mapping': {df_name: associations_mapping[df_name][target]
                                          for df_name in associations_mapping}}]

                    # pickle + Additional args
                    second_block_dict = {}
                    if Trainer.features_info[feature]['has_pickle']:
                        second_block_dict.update({'utils': None,
                                                  'pre_loaded': True,
                                                  'pickle_mode': True,
                                                  'pickle_file': Trainer.features_info[feature]['pickle_file']})

                    if additional_args is None and Trainer.features_info[feature]['additional_args']:
                        second_block_dict.update(Trainer.args_info[feature]['params'])
                    elif additional_args is not None and target in additional_args[feature]:
                        second_block_dict.update(additional_args[feature][target])

                    args[current_key].append(second_block_dict)

                    # Type
                    features[current_key] = Trainer.features_info[feature]['type']

            else:

                current_key = feature

                args[feature] = [{'key_mapping': associations_mapping}]

                second_block_dict = {}
                if Trainer.features_info[feature]['has_pickle']:
                    second_block_dict.update({'utils': None,
                                              'pre_loaded': True,
                                              'pickle_mode': True,
                                              'pickle_file': Trainer.features_info[feature]['pickle_file']})

                if additional_args is None and Trainer.features_info[feature]['additional_args']:
                    second_block_dict.update(Trainer.args_info[feature])
                elif additional_args is not None and feature in additional_args:
                    second_block_dict.update(additional_args[feature])

                args[current_key].append(second_block_dict)

                # Type
                features[current_key] = Trainer.features_info[feature]['type']

        # [Step 2]: define classifier

        classifier = None
        if classifier_type and classifier_info is not None:
            classifier = ClassifierFactory.factory(classifier_type, **classifier_info)

        return features, args, classifier

    @staticmethod
    def retrieve_cv(cv_mode, prebuilt_cv_key=None, **kwargs):
        """
        Retrieves custom cross validator

        :param cv_mode: either 'default' or 'prebuilt_cv' for pre-defined
        :param prebuilt_cv_key: which pre-defined
        :return: cv
        """

        if cv_mode == 'default':
            return ms.StratifiedKFold(**kwargs)
        else:
            if cv_mode == 'prebuilt_cv':
                return PrebuiltCV(prebuilt_cv_key)
            else:
                raise ValueError('Invalid cv_mode!! Possible values: default, prebuilt_cv')
