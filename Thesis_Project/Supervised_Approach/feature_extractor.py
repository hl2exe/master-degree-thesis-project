
import os
import numpy as np
from nltk import word_tokenize, sent_tokenize
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.stem.porter import PorterStemmer
from nltk.util import ngrams
from nltk.util import skipgrams
from sklearn.base import BaseEstimator
from sklearn.feature_extraction.text import CountVectorizer
from toolz import compose
from sklearn.decomposition import PCA

from Utils.iterable_utils import pairwise
from Utils.pickle_utility import PickleUtility

# required in order to load pickle data
from Utils.feature_extractor_utils import FeatureExtractorUtils
from itertools import product

# required when using DependencyVectorizer with multiple processes (gridsearch) -> lambda pickling
import dill

"""

Extractors

"""


class BaseCustomExtractor(BaseEstimator):
    """

    Basic abstract class which defines the general behaviour of a feature extractor

    """

    def __init__(self, utils=None):
        self.utils = utils

    def fit(self, X, y=None):
        return self


class PickleCompliantExtractor(BaseCustomExtractor):

    def __init__(self, utils=None, pickle_mode=False, pickle_file=None, save_to_pickle=False, pre_loaded=False,
                 pickle_folder=None):
        super(PickleCompliantExtractor, self).__init__(utils=utils)

        self.save_to_pickle = save_to_pickle
        self.pickle_mode = pickle_mode
        self.pre_loaded = pre_loaded
        self.pickle_folder = pickle_folder

        if (save_to_pickle or pickle_mode) and not PickleUtility.check_pickle(pickle_file):
            raise Exception("Invalid pickle file! Got {}".format(pickle_file))

        self.pickle_file = pickle_file

    def _online_transform(self, data):
        """
        Abstract methods which defines the extractor on-line feature computation behaviour.

        :param data: input data
        :return: None
        """

        pass

    def save_data_to_pickle(self, data):
        """
        If 'save to pickle' mode is enabled, it saves computed into associated pickle file.
        If the pickle files already exists, the new content is appended to the existing one.

        :param data: computed content by extractor
        :return: None
        """

        if self.save_to_pickle:
            # print(self.__class__.__name__, ": saving to {}".format(self.pickle_file))

            if self.pickle_folder is None:
                pickle_to_verify = self.pickle_file
            else:
                pickle_to_verify = os.path.join(self.pickle_folder, self.pickle_file)

            if PickleUtility.verify_file(pickle_to_verify):
                existing_data = PickleUtility.load_pickle(self.pickle_file, folder=self.pickle_folder)
                existing_data.update(data)
                PickleUtility.save_to_file(existing_data, self.pickle_file, folder=self.pickle_folder, mode='wb')
            else:
                PickleUtility.save_to_file(data, self.pickle_file, folder=self.pickle_folder, mode='wb')

    def _check_speedup(self, data):
        """
        Checks whether the extractor is configured for on-line feature computation or it has to load
        pre-computed data only.
        In particular if 'pickle mode' is enabled, it checks whether the data has already been pre-loaded externally.
        If yes, the pre-computed data is used in order to extract the data associated specific extractor features.
        Alternatively, it loads the pre-computed data and builds the data associated specific extractor features.
        In the case 'pickle mode' is not enabled, the extractor proceeds the feature on-line computation.

        :param data: input data
        :return: extractor data associated features.
        """

        if self.pickle_mode:
            if self.pre_loaded:
                print("{}: Using pre-loaded data..".format(self.__class__.__name__))
                return [FeatureExtractorUtils.pre_loaded_data[self.pickle_file][key] for key in data]
            else:
                print(self.__class__.__name__, ": loading pre-computed data...")
                if self.pickle_folder:
                    pre_computed_data = PickleUtility.load_pickle(os.path.join(self.pickle_folder, self.pickle_file))
                else:
                    pre_computed_data = PickleUtility.load_all_pickles(self.pickle_file)
                return [pre_computed_data[key] for key in data]
        else:
            print(self.__class__.__name__, ": on-line computation...")
            return self._online_transform(data)


class PickleCompliantVectorizer(BaseCustomExtractor):

    def __init__(self, utils=None, pickle_mode=False, pickle_file=None, save_to_pickle=False, pre_loaded=False,
                 pickle_folder=None):
        super(PickleCompliantVectorizer, self).__init__(utils=utils)

        self.save_to_pickle = save_to_pickle
        self.pickle_mode = pickle_mode
        self.pre_loaded = pre_loaded
        self.pickle_folder = pickle_folder

        if (save_to_pickle or pickle_mode) and not PickleUtility.check_pickle(pickle_file):
            raise Exception("Invalid pickle file! Got {}".format(pickle_file))

        self.pickle_file = pickle_file
        self.pickle_data = None

        if self.pickle_mode and not self.pre_loaded:
            if self.pickle_folder:
                self.pickle_data = PickleUtility.load_pickle(os.path.join(self.pickle_folder, self.pickle_file))
            else:
                self.pickle_data = PickleUtility.load_all_pickles(self.pickle_file)

    def _online_transform(self, data):
        """
        Abstract methods which defines the extractor on-line feature computation behaviour.

        :param data: input data
        :return: None
        """

        pass

    def save_data_to_pickle(self, data):
        """
        If 'save to pickle' mode is enabled, it saves computed into associated pickle file.
        If the pickle files already exists, the new content is appended to the existing one.

        :param data: computed content by extractor
        :return: None
        """

        if self.save_to_pickle:
            # print(self.__class__.__name__, ": saving to {}".format(self.pickle_file))

            if self.pickle_folder is None:
                pickle_to_verify = self.pickle_file
            else:
                pickle_to_verify = os.path.join(self.pickle_folder, self.pickle_file)

            if PickleUtility.verify_file(pickle_to_verify):
                existing_data = PickleUtility.load_pickle(self.pickle_file, folder=self.pickle_folder)
                existing_data.update(data)
                PickleUtility.save_to_file(existing_data, self.pickle_file, folder=self.pickle_folder, mode='wb')
            else:
                PickleUtility.save_to_file(data, self.pickle_file, folder=self.pickle_folder, mode='wb')

    def _check_speedup_analyzer(self, doc):
        """
        Equivalent behaviour of '_check_speedup()' function in the case of a CountVectorizer instance.

        :param doc: input text
        :return: extractor data associated features.
        """

        if self.pickle_mode:
            if self.pre_loaded:
                # print("{}: Using pre-loaded data..".format(self.__class__.__name__))
                return FeatureExtractorUtils.pre_loaded_data[self.pickle_file][doc]
            else:
                # print(self.__class__.__name__, ": loading pre-computed data...")
                return self.pickle_data[doc]
        else:
            # print(self.__class__.__name__, ": on-line computation...")
            return self._online_transform(doc)


class CosineSimilarityExtractor(PickleCompliantExtractor):

    def _online_transform(self, couple_list):
        """
        Computes cosine similarity for each item in given list.
        Each item is a 2D-tuple of texts

        :param couple_list:
        :return: list of computed cosine similarities
        """

        # If the dataframe has duplicates, returning dict.values() is not correct
        cosine_similarity_dict = {}
        cosine_similarity_list = []

        for couple in couple_list:
            cosine_similarity = self.utils.compute_cosine_similarity(*couple)
            cosine_similarity_dict[couple] = cosine_similarity
            cosine_similarity_list.append(cosine_similarity)

        self.save_data_to_pickle(cosine_similarity_dict)

        return cosine_similarity_list

    def transform(self, couple_list):
        return self._check_speedup(couple_list)


class SVOTriplesExtractor(PickleCompliantExtractor):

    def _online_transform(self, couple_list):
        """
        Compute SVO triples for each element of each couple in given list.

        :param couple_list:
        :return: list
        """

        # If the dataframe has duplicates, returning dict.values() is not correct
        triples_couple_dict = {}
        triples_couple_list = []

        for (text1, text2) in couple_list:
            text1_triples = self.utils.extract_svo_triples(text1)
            text2_triples = self.utils.extract_svo_triples(text2)
            triples_couple_dict[(text1, text2)] = (text1_triples, text2_triples)
            triples_couple_list.append((text1_triples, text2_triples))

        self.save_data_to_pickle(triples_couple_dict)

        return triples_couple_list

    def transform(self, couple_list):

        return self._check_speedup(couple_list)


class BasicExtractor(PickleCompliantExtractor):

    def _online_transform(self, target_list):
        """
        Extracts basic features for each item in given list.
        Each item is a text.


        :param target_list:
        :return: list
        """

        basic_data_list = []
        basic_data_dict = {}

        for target in target_list:
            target_features = self.utils.extract_basic_features_from_text(target)
            basic_data_list.append(target_features)
            basic_data_dict[target] = target_features

        self.save_data_to_pickle(basic_data_dict)

        print("Returning: {}".format(len(basic_data_list)))
        return basic_data_list

    def transform(self, target_list):

        return self._check_speedup(target_list)


class RepeatedPunctuationExtractor(PickleCompliantExtractor):

    def _online_transform(self, target_list):
        """
        Extracts repeated punctuation statistics for each item in given list.
        Each item is a text

        :param target_list:
        :return: list
        """

        repeated_punctuation_list = []
        repeated_punctuation_dict = {}

        for target in target_list:
            target_features = self.utils.count_repeated_punctuation(target)
            repeated_punctuation_list.append(target_features)
            repeated_punctuation_dict[target] = target_features

        self.save_data_to_pickle(repeated_punctuation_dict)

        return repeated_punctuation_list

    def transform(self, target_list):

        return self._check_speedup(target_list)


class PickleCountVectorizer(CountVectorizer, PickleCompliantVectorizer):
    """
    Simple CountVectorizer pickle-compliant extension.

    """

    def __init__(self, ngram_range=(1, 3), utils=None, save_to_pickle=False,
                 pickle_mode=False, pickle_file=None, pre_loaded=False, pickle_folder=None):

        CountVectorizer.__init__(self, ngram_range=ngram_range)
        PickleCompliantVectorizer.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                           pickle_mode=pickle_mode, pickle_file=pickle_file,
                                           pre_loaded=pre_loaded, pickle_folder=pickle_folder)

    def _online_transform(self, doc):
        preprocess = self.build_preprocessor()
        stop_words = self.get_stop_words()
        tokenize = self.build_tokenizer()
        result = self._word_ngrams(tokenize(preprocess(self.decode(doc))), stop_words)
        if self.pickle_data is None:
            self.pickle_data = {(doc, self.ngram_range): result}
        else:
            self.pickle_data.update({(doc, self.ngram_range): result})
        return result

    def build_analyzer(self):
        return lambda doc: self._check_speedup_analyzer(doc)


class SkipGramVectorizer(CountVectorizer, PickleCompliantVectorizer):
    """

    Simple CountVectorizer extension for skip n-grams extraction.

    """

    def __init__(self, ngram_range=(2, 3), skip_range=(2, 6), stemmer=None,
                 utils=None, save_to_pickle=False,
                 pickle_mode=False, pickle_file=None, pre_loaded=False, pickle_folder=None,
                 input='content', encoding='utf-8', decode_error='strict',
                 strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None,
                 stop_words=None, token_pattern='(?u)\b\w\w+\b', analyzer='word',
                 max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False,
                 dtype=np.int64):

        CountVectorizer.__init__(self, input=input, encoding=encoding, decode_error=decode_error,
                                 strip_accents=strip_accents, lowercase=lowercase,
                                 preprocessor=preprocessor, tokenizer=tokenizer,
                                 stop_words=stop_words, token_pattern=token_pattern, analyzer=analyzer,
                                 max_df=max_df, min_df=min_df, max_features=max_features,
                                 vocabulary=vocabulary, binary=binary,
                                 dtype=dtype, ngram_range=ngram_range)

        PickleCompliantVectorizer.__init__(self, utils=utils, pickle_mode=pickle_mode,
                                           pickle_file=pickle_file, save_to_pickle=save_to_pickle,
                                           pre_loaded=pre_loaded, pickle_folder=pickle_folder)

        self.skip_range = skip_range

        if stemmer is None:
            self.stemmer = PorterStemmer()
        else:
            self.stemmer = stemmer

    def _online_transform(self, doc):
        preprocess = self.build_preprocessor()
        stop_words = self.get_stop_words()
        tokenize = self._tokenize

        result = self._extract_skipgrams(compose(tokenize, preprocess)(doc), stop_words)
        if self.pickle_data is None:
            self.pickle_data = {(doc, self.ngram_range, self.skip_range): result}
        else:
            self.pickle_data.update({(doc, self.ngram_range, self.skip_range): result})
        return result

    def build_analyzer(self):
        return lambda doc: self._check_speedup_analyzer(doc)

    def _tokenize(self, text):
        tokens = word_tokenize(text)
        stemmed = [self.stemmer.stem(token) for token in tokens]

        return stemmed

    def _extract_skipgrams(self, tokens, stop_words=None):
        """
        Extracts skip n-grams from given tokenized sentence. Input data can be further pre-processed via given stop
        words.

        :param tokens: tokenized input via Stanford Parser
        :param stop_words: set of stop words used in order to filter input data
        :return: list of extracted skip n-grams
        """

        result = []

        if stop_words is not None:
            tokens = [word for word in tokens if word not in stop_words]

        for n in range(self.ngram_range[0], self.ngram_range[1] + 1):
            for k in range(self.skip_range[0], self.skip_range[1] + 1):
                result.append(list(skipgrams(tokens, n, k)))

        return [item for sublist in result for item in sublist]


class DependencyVectorizer(CountVectorizer, PickleCompliantVectorizer):
    """
    Extracts dependency features from given text.
    The following features are selected:

        1. Syntatic dependency features, i.e. triples like (head, relation, tail), where head and tail
         are words from text, whereas relation is the syntatic relation that the Stanford Dependency Parser
         has found between them.

        2. POS generalized dependency features, i.e. triples like the ones described in 1., but with the difference
        that the head element is substituted with its associated POS tag.

        3. Opinion generalized dependency features, i.e. triples like the ones described in 1., but with the difference
        that only triples containing sentiment words are selected. Moreover, sentiment words are replaced with their
        respective symbol ('+' for positive sentiment, '-' for negative sentiment)

    """

    def __init__(self, key, utils=None, use_mpqa=False, save_to_pickle=False,
                 pickle_mode=False, pickle_file=None, pre_loaded=False, pickle_folder=None):
        """
        :param utils: FeatureExtractorUtils instance
        :param key: which features to compute: Syntactic, POS generalized, Opinion generalized
        :param use_mpqa: _extract_opinion_generalized_dependencies parameter.
        It tells whether to use MPQA lexicon or VADER analyser (default) for
        sentiment words detection
        """

        self.key = key
        self.use_mpqa = use_mpqa

        self.method = {
            'syntactic': lambda text: self._extract_syntactic_dependencies(text),
            'POS_generalized': lambda text: self._extract_pos_generalized_dependencies(text),
            'opinion_generalized': lambda text: self._extract_opinion_generalized_dependencies(text, self.use_mpqa)
        }

        PickleCompliantVectorizer.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                           pickle_mode=pickle_mode, pickle_file=pickle_file,
                                           pre_loaded=pre_loaded, pickle_folder=pickle_folder)
        CountVectorizer.__init__(self)

    def _online_transform(self, doc):

        # print("Splitting it into sentences")
        dependency_features = []
        sentences = sent_tokenize(doc)
        for sentence in sentences:
            sent_dependency_features = self.method[self.key](sentence)
            dependency_features += sent_dependency_features

        print("Completed: ", doc)
        if self.pickle_data is None:
            self.pickle_data = {doc: dependency_features}
        else:
            self.pickle_data.update({doc: dependency_features})
        return dependency_features

    def build_analyzer(self):
        return lambda doc: self._check_speedup_analyzer(doc)

    def _extract_syntactic_dependencies(self, text):
        """
        Extracts syntactic dependencies via Stanford Dependency Parser.
        Each dependency triple is structured as follow: (head, relation, tail)
        where head and tail are words extracted from text and relation is the dependency relation between them

        :param text: text to parse
        :return: list of syntactic dependencies triples
        """

        result = self.utils.dependency_parser.raw_parse(text)
        dependencies = result.next()

        syntactic_dependencies = []

        for triple in list(dependencies.triples()):
            syntactic_dependencies.append((triple[0][0], triple[1], triple[2][0]))

        return ['-'.join(triple) for triple in syntactic_dependencies]

    def _extract_pos_generalized_dependencies(self, text):
        """
        Extracts POS generalized dependencies, i.e. syntactic dependencies whose head is replaced with its associated
        POS tag.

        :param text: text to parse
        :return: list of POS generalized dependencies triples
        """

        result = self.utils.dependency_parser.raw_parse(text)
        dependencies = result.next()

        pos_generalized_dependencies = []

        for triple in list(dependencies.triples()):
            pos_generalized_dependencies.append((triple[0][1], triple[1], triple[2][0]))

        return ['-'.join(triple) for triple in pos_generalized_dependencies]

    def _extract_opinion_generalized_dependencies(self, text, use_mpqa=False):
        """
        Extracts opinion generalized dependencies, i.e. syntactic dependencies whose head and/or tail are replaced
        with its/their associated sentiment symbols ('+' for positive sentiment, '-' for negative sentiment).
        If a certain triple has both head and tail with neutral sentiment, it is then not considered.
        Sentiment scores are computed via VADER analyzer or by consulting MPQA subjectivity lexicon.

        :param text: text to parse
        :return: list of opinion generalized dependencies triples
        """

        triples = self._extract_syntactic_dependencies(text)

        # Loading lexicon
        if use_mpqa:
            lexicon = self.utils.parse_mpqa_lexicon()
            filter = lambda word: lexicon[word] if word in lexicon else None
            checker = lambda score: True if score is not None else False
            mapper = lambda score: '+' if score == 'positive' else '-'
        else:   # use VADER
            sid = SentimentIntensityAnalyzer()
            filter = lambda word: sid.polarity_scores(word)["compound"]
            checker = lambda score: True if abs(score) >= 0.5 else False
            mapper = lambda score, original: '+' if score >= 0.5 else '-' if score <= -0.5 else original

        opinion_generalized_dependencies = []

        for triple in triples:
            head_score = filter(triple[0])
            tail_score = filter(triple[2])

            if checker(head_score) or checker(tail_score):
                relation = triple[1]

                head = mapper(head_score, triple[0])
                tail = mapper(tail_score, triple[2])

                opinion_generalized_dependencies.append((head, relation, tail))

        return ['-'.join(triple) for triple in opinion_generalized_dependencies]


class DiscourseCueVectorizer(CountVectorizer):
    """

    Simple CountVectorizer extension for discourse cues feature extraction.

    """

    def __init__(self, stemmer=None):
        super(DiscourseCueVectorizer, self).__init__()

        if stemmer is None:
            self.stemmer = PorterStemmer()
        else:
            self.stemmer = stemmer

    def build_analyzer(self):
        preprocess = self.build_preprocessor()
        stop_words = self.get_stop_words()
        tokenize = self._tokenize

        return lambda doc: self._extract_first_n_grams(compose(tokenize, preprocess)(doc), stop_words)

    def _tokenize(self, text):
        tokens = word_tokenize(text)
        stemmed = [self.stemmer.stem(token) for token in tokens]

        return stemmed

    def _extract_first_n_grams(self, tokens, stop_words=None):
        """
        Extracts the first unigram, bigram and trigram from given text

        :param tokens: list of tokens derived by text pre-processing
        :return: list containing the first specified n-grams, if they exist.
        """

        if stop_words is not None:
            tokens = [word for word in tokens if word not in stop_words]

        unigrams = list(ngrams(tokens, 1))
        bigrams = list(ngrams(tokens, 2))
        trigrams = list(ngrams(tokens, 3))

        return [iterable[0] for iterable in [unigrams, bigrams, trigrams] if iterable]


class SyntacticCountVectorizer(CountVectorizer, PickleCompliantVectorizer):

    """

    Simple CountVectorizer extension for syntactic n-grams feature extraction.

    """

    def __init__(self, utils, sngram_range=2, save_to_pickle=False, pickle_mode=False,
                 pickle_file=None, pre_loaded=False, pickle_folder=None):

        if sngram_range < 2:
            raise Exception("Minimum n for sn-grams is 2!")

        PickleCompliantVectorizer.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                           pickle_mode=pickle_mode, pickle_file=pickle_file, pre_loaded=pre_loaded,
                                           pickle_folder=pickle_folder)
        CountVectorizer.__init__(self)

        self.sngram_range = sngram_range

    def _online_transform(self, doc):

        # print("Splitting it into sentences")
        sn_grams = []
        sentences = sent_tokenize(doc)
        for sentence in sentences:
            sentence_sngrams = self._extract_sngrams(sentence)
            sn_grams += sentence_sngrams

        print("Completed: ", doc)
        if self.pickle_data is None:
            self.pickle_data = {doc: sn_grams}
        else:
            self.pickle_data.update({doc: sn_grams})
        return sn_grams

    def build_analyzer(self):
        return lambda doc: self._check_speedup_analyzer(doc)

    def _extract_sngrams(self, text):
        """
        Extracts syntactic n-grams from given input text. Sngrams are retrieved via Stanford Dependency Parser.

        :param text: input text
        :return: list of sngrams
        """

        parsed_doc = self.utils.dependency_parser.raw_parse(text)
        dependencies = parsed_doc.next()

        sn_bigrams = []

        # bigrams
        for triple in list(dependencies.triples()):
            sn_bigrams.append((triple[0][0], triple[2][0]))

        if self.sngram_range > 2:
            return sn_bigrams + self._build_higher_level_sngrams(sn_bigrams)
        else:
            return sn_bigrams

    def _build_higher_level_sngrams(self, sn_bigrams):
        """
        Utility function for computing sngrams where n is higher than 2 (default case).

        :param sn_bigrams: pre-computed syntactic bigrams
        :return: list of sngrams
        """

        result = []

        distance = self.sngram_range - 1
        for idx, bigram in enumerate(sn_bigrams):
            if idx + distance <= len(sn_bigrams):
                to_evaluate = sn_bigrams[idx:idx + distance]
                if self._evaluate_path(to_evaluate):
                    result.append(tuple(self._build_sngram_from_path(to_evaluate)))

        return result

    def _evaluate_path(self, path):
        """
        Checks whether the given list of couples forms a path, i.e. the tail of each couple equals the head of the
        following couple.

        :param path: list of couples
        :return: True if the tail of each couple equals the head of the following couple, False otherwise.
        """

        for current, next in pairwise(path):
            if current[1] != next[0]:
                return False

        return True

    def _build_sngram_from_path(self, path):
        """
        Builds sngrams from given valid path.

        :param path: valid list of couples. @see '_evaluate_path' for more info about validation of a path
        :return: list of sngrams
        """

        result = [path[0][0], path[0][1]]
        for bigram in path[1:]:
            result.append(bigram[1])

        return result

##############################
# Stab and Gurevych features #
##############################

# Structural features


class TokenCounterExtractor(PickleCompliantExtractor):

    def _online_transform(self, target_list):
        """
        Counts the number of tokens of each item in given list (texts).

        :param target_list:
        :return: list of integers
        """

        count_vector = []
        count_dict = {}

        for item in target_list:
            tokens_amount = FeatureExtractorUtils.count_tokens(item)
            count_vector.append(tokens_amount)
            count_dict[item] = tokens_amount

        self.save_data_to_pickle(count_dict)

        return count_vector

    def transform(self, target_list):
        return self._check_speedup(target_list)


class TokenCounterDifferenceExtractor(PickleCompliantExtractor):

    def _online_transform(self, couple_list):
        """
        Computes the tokens amount absolute difference between text couples in given list.

        :param couple_list:
        :return: list of positive integers
        """

        difference_vector = []
        difference_dict = {}

        for (target1, target2) in couple_list:
            target1_tokens_amount = FeatureExtractorUtils.count_tokens(target1)
            target2_tokens_amount = FeatureExtractorUtils.count_tokens(target2)
            token_amount_diff = abs(target1_tokens_amount - target2_tokens_amount)
            difference_vector.append(token_amount_diff)
            difference_dict[(target1, target2)] = token_amount_diff

        self.save_data_to_pickle(difference_dict)

        return difference_vector

    def transform(self, couple_list):
        return self._check_speedup(couple_list)


class PunctuationMarksExtractor(PickleCompliantExtractor):

    def _online_transform(self, target_list):
        """
        Counts punctuation marks for each text in given list.

        :param target_list:
        :return: integer list
        """

        punctuation_mark_vector = []
        punctuation_mark_dict = {}

        for item in target_list:
            punctuation_marks_count = FeatureExtractorUtils.count_punctuation_marks(item)
            punctuation_mark_vector.append(punctuation_marks_count)
            punctuation_mark_dict[item] = punctuation_marks_count

        self.save_data_to_pickle(punctuation_mark_dict)

        return punctuation_mark_vector

    def transform(self, target_list):
        return self._check_speedup(target_list)


class PunctuationMarksDifferenceExtractor(PickleCompliantExtractor):

    def _online_transform(self, couple_list):
        """
        Computes the punctuation marks amount absolute difference between text couples in given list.

        :param couple_list:
        :return:
        """

        difference_vector = []
        difference_dict = {}

        for (target1, target2) in couple_list:
            target1_punctuation_marks_count = FeatureExtractorUtils.count_punctuation_marks(target1)
            target2_punctuation_marks_count = FeatureExtractorUtils.count_punctuation_marks(target2)
            punctuation_marks_count_difference = abs(target1_punctuation_marks_count - target2_punctuation_marks_count)
            difference_vector.append(punctuation_marks_count_difference)
            difference_dict[(target1, target2)] = punctuation_marks_count_difference

        self.save_data_to_pickle(difference_dict)

        return difference_vector

    def transform(self, couple_list):
        return self._check_speedup(couple_list)


# Lexical features


class WordPairsBinaryVectorizer(CountVectorizer, PickleCompliantVectorizer):

    def __init__(self, utils, stop_words=None, save_to_pickle=False, pickle_mode=False,
                 pickle_file=None, pre_loaded=False, pickle_folder=None):
        CountVectorizer.__init__(self, binary=True, stop_words=stop_words)
        PickleCompliantVectorizer.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                           pickle_mode=pickle_mode, pickle_file=pickle_file, pre_loaded=pre_loaded,
                                           pickle_folder=pickle_folder)

        self.preprocess = self.build_preprocessor()
        self.stop_words = self.get_stop_words()
        self.tokenize = self.build_tokenizer()

    def _online_transform(self, couple):
        word_pairs_data = list(self._extract_word_pairs(compose(self.tokenize, self.preprocess)(couple[0]),
                                                        compose(self.tokenize, self.preprocess)(couple[1]),
                                                        self.stop_words))
        # print("Processed couple: {}".format(couple))
        if self.pickle_data is None:
            self.pickle_data = {couple: word_pairs_data}
        else:
            self.pickle_data.update({couple: word_pairs_data})
            # print(len(self.pickle_data))
        return word_pairs_data

    def build_analyzer(self):
        return lambda couple: self._check_speedup_analyzer(couple)

    def _extract_word_pairs(self, target1_tokens, target2_tokens, stop_words):
        """
        Computes all possible word pairs between two given token sequences.
        Tokens can be initially pre-processed by eliminating stop words if the latter is specified as a parameter

        :param target1_tokens:
        :param target2_tokens:
        :param stop_words:
        :return: iterator over word pairs
        """

        if stop_words is not None:
            target1_tokens = [token for token in target1_tokens if token not in stop_words]
            target2_tokens = [token for token in target2_tokens if token not in stop_words]

        return product(target1_tokens, target2_tokens)


class FirstWordBinaryExtractor(CountVectorizer):

    def __init__(self, stop_words=None):
        CountVectorizer.__init__(self, binary=True, stop_words=stop_words)

    def build_analyzer(self):
        preprocess = self.build_preprocessor()
        stop_words = self.get_stop_words()
        tokenize = self.build_tokenizer()

        return lambda doc: self._extract_first_word(compose(tokenize, preprocess)(doc), stop_words)

    def _extract_first_word(self, tokens, stop_words):
        """
        Extracts the first token from a given token sequence. The latter can be initially pre-processed by eliminating
         stop words if the associated parameter is specified.

        :param tokens:
        :param stop_words:
        :return: list containing the first word extracted
        """

        if stop_words is not None:
            tokens = [token for token in tokens if token not in stop_words]

        return [tokens[0]]


class FirstWordBinaryCoupleExtractor(CountVectorizer):

    def __init__(self, stop_words=None):
        CountVectorizer.__init__(self, binary=True, stop_words=stop_words)

    def build_analyzer(self):
        preprocess = self.build_preprocessor()
        stop_words = self.get_stop_words()
        tokenize = self.build_tokenizer()

        return lambda couple: self._extract_first_word_couple(compose(tokenize, preprocess)(couple[0]),
                                                              compose(tokenize, preprocess)(couple[1]),
                                                              stop_words)

    def _extract_first_word_couple(self, target1_tokens, target2_tokens, stop_words):
        """
        Extracts a first word couple from two given token sequences. The latter can be initially pre-processed by
        eliminating stop words.

        :param target1_tokens:
        :param target2_tokens:
        :param stop_words:
        :return: list containing the extracted first word couple
        """

        if stop_words is not None:
            target1_tokens = [token for token in target1_tokens if token not in stop_words]
            target2_tokens = [token for token in target2_tokens if token not in stop_words]

        return [(target1_tokens[0], target2_tokens[0])]


class ModalVerbBinaryExtractor(PickleCompliantExtractor):

    def _online_transform(self, target_list):
        """
        Checks whether each element in given list contains any modal verb.

        :param target_list:
        :return: list of boolean values (1 or 0), where 1 means that the associated item (text) contains a modal verb,
        0 otherwise.
        """

        modal_vector = []
        modal_dict = {}

        for item in target_list:
            item_tokens = [self.utils.wordnet_lemmatizer.lemmatize(token).lower() for token in word_tokenize(item)]
            if any(modal in item_tokens for modal in FeatureExtractorUtils.modals):
                modal_vector.append(1)
                modal_dict[item] = 1
            else:
                modal_vector.append(0)
                modal_dict[item] = 0

        self.save_data_to_pickle(modal_dict)

        return modal_vector

    def transform(self, target_list):
        return self._check_speedup(target_list)


class CommonElementsExtractor(PickleCompliantExtractor):

    def _online_transform(self, couple_list):
        """
        Counts all the common terms for each couple in given list.

        :param couple_list:
        :return: list of integer values, in which each number refers to the amount of common terms in the associated
        couple.
        """

        common_elements_vector = []
        common_elements_dict = {}

        for (target1, target2) in couple_list:
            target1_tokens = [self.utils.wordnet_lemmatizer.lemmatize(token).lower()
                              for token in word_tokenize(target1)]
            target2_tokens = [self.utils.wordnet_lemmatizer.lemmatize(token).lower()
                              for token in word_tokenize(target2)]

            common_tokens = list(set(target1_tokens).intersection(target2_tokens))
            common_elements_vector.append(len(common_tokens))
            common_elements_dict[(target1, target2)] = len(common_tokens)

        self.save_data_to_pickle(common_elements_dict)

        return common_elements_vector

    def transform(self, couple_list):
        return self._check_speedup(couple_list)

# Syntactic features


class ProductionRulesBinaryVectorizer(CountVectorizer, PickleCompliantVectorizer):

    def __init__(self, utils, save_to_pickle=False, pickle_mode=False,
                 pickle_file=None, pre_loaded=False, pickle_folder=None):
        CountVectorizer.__init__(self, binary=True)
        PickleCompliantVectorizer.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                           pickle_mode=pickle_mode, pickle_file=pickle_file, pre_loaded=pre_loaded,
                                           pickle_folder=pickle_folder)

    def _online_transform(self, doc):
        """
        Extracts production rules from given text. Stanford parser is used in order to retrieve production rules from
        text.

        :param doc:
        :return: list of extracted production rules
        """

        doc_production_rules = []
        for sentence in sent_tokenize(doc):
            tree = list(self.utils.parser.raw_parse(sentence))[0]
            production_rules = [rule.unicode_repr() for rule in tree.productions()]
            doc_production_rules += production_rules

        if self.pickle_data is None:
            self.pickle_data = {doc: doc_production_rules}
        else:
            self.pickle_data.update({doc: doc_production_rules})
        print("Processed text: {}".format(doc))

        return doc_production_rules

    def build_analyzer(self):
        return lambda doc: self._check_speedup_analyzer(doc)


##############################
#          Variants          #
##############################


class CosineSimilaritySEVariantExtractor(PickleCompliantExtractor):

    def __init__(self, a, dim=300, utils=None, save_to_pickle=False,
                 pickle_mode=False, pickle_file=None, pre_loaded=False, pickle_folder=None):
        PickleCompliantExtractor.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                          pickle_mode=pickle_mode, pickle_file=pickle_file,
                                          pre_loaded=pre_loaded, pickle_folder=pickle_folder)
        self.a = a
        self.dim = dim

    def _update_embedding_vector(self, vector, pca_components):
        return vector - pca_components * np.transpose(pca_components) * vector

    def _online_transform(self, couple_list):
        """
        Computes cosine similarity for each item in given list.
        Each item is a 2D-tuple of texts

        :param couple_list:
        :return: list of computed cosine similarities
        """

        # If the dataframe has duplicates, returning dict.values() is not correct
        cosine_similarity_dict = {}
        cosine_similarity_list = []

        sentence_list = []
        for couple in couple_list:
            sentence_list += list(couple)
        probabilities_dict = self.utils.compute_unigrams_corpus_probability(sentence_list)
        sentence_embeddings_dict = {}
        sentence_embedding_matrix = []

        for sentence in sentence_list:
            sentence_lemmas = self.utils.get_tokenized_lemmas(sentence)
            sentence_length = len(sentence_lemmas)
            word_embeddings_sum = self.utils.convert_text_to_vec_via_SE(sentence, self.a, probabilities_dict)
            sentence_embedding_vector = (1/float(sentence_length)) * word_embeddings_sum
            sentence_embeddings_dict[sentence] = sentence_embedding_vector
            sentence_embedding_matrix.append(sentence_embedding_vector)

        sentence_embedding_matrix = np.array(sentence_embedding_matrix)
        pca = PCA(n_components=1)
        pca.fit(sentence_embedding_matrix)
        first_singular_vector = pca.components_[0]

        for couple in couple_list:
            cosine_similarity = self.utils.calculate_cosine_similarity(
                self._update_embedding_vector(sentence_embeddings_dict[couple[0]], first_singular_vector),
                self._update_embedding_vector(sentence_embeddings_dict[couple[1]], first_singular_vector))
            cosine_similarity_dict[couple] = cosine_similarity
            cosine_similarity_list.append(cosine_similarity)

        self.save_data_to_pickle(cosine_similarity_dict)

        return cosine_similarity_list

    def transform(self, couple_list):
        return self._check_speedup(couple_list)


class CosineSimilarityABTTVariantExtractor(PickleCompliantExtractor):

    def __init__(self, D, dim=300, utils=None, save_to_pickle=False,
                 pickle_mode=False, pickle_file=None, pre_loaded=False, pickle_folder=None):
        PickleCompliantExtractor.__init__(self, utils=utils, save_to_pickle=save_to_pickle,
                                          pickle_mode=pickle_mode, pickle_file=pickle_file,
                                          pre_loaded=pre_loaded, pickle_folder=pickle_folder)
        self.D = D
        self.dim = dim

    def _update_word_embeddings(self, vector, mean_vector, pca_components):

        pca_sum = np.sum(np.array([(np.transpose(pca_component) * vector) * pca_component
                                   for pca_component in pca_components]), axis=0)
        return vector - mean_vector - pca_sum

    def _online_transform(self, couple_list):
        """
        Computes cosine similarity for each item in given list.
        Each item is a 2D-tuple of texts

        :param couple_list:
        :return: list of computed cosine similarities
        """

        # If the dataframe has duplicates, returning dict.values() is not correct
        cosine_similarity_dict = {}
        cosine_similarity_list = []

        sentence_list = []
        for couple in couple_list:
            sentence_list += list(couple)

        all_tokens = set()
        for sentence in sentence_list:
            sentence_lemmas = self.utils.get_tokenized_lemmas(sentence)
            all_tokens.update(sentence_lemmas)

        word_embeddings_dict = {token: self.utils.convert_word_to_vec(token, self.dim) for token in all_tokens}
        word_embeddings = np.array(word_embeddings_dict.values())
        mean_vector = np.mean(word_embeddings, axis=0)

        pca = PCA(n_components=self.D)
        pca.fit(word_embeddings)

        for token in word_embeddings_dict:
            word_embeddings_dict[token] = self._update_word_embeddings(word_embeddings_dict[token],
                                                                       mean_vector, pca.components_)

        for couple in couple_list:
            cosine_similarity = self.utils.calculate_cosine_similarity(
                self.utils.convert_text_to_vec_ABTT(couple[0], word_embeddings_dict),
                self.utils.convert_text_to_vec_ABTT(couple[1], word_embeddings_dict)
            )
            cosine_similarity_dict[couple] = cosine_similarity
            cosine_similarity_list.append(cosine_similarity)

        self.save_data_to_pickle(cosine_similarity_dict)

        return cosine_similarity_list

    def transform(self, couple_list):
        return self._check_speedup(couple_list)