"""

Separating initial compact pickle data by data-set

1. Emergent
2. CE-ACL-14
3. CE-EMNLP-15
4. FNC-1
5. Other

"""

import os

import pandas as pd

from Supervised_Approach.path_definitions import TRAIN_DIR, COMPLETE_DIR
from Supervised_Approach.Utils.pickle_utility import PickleUtility

datasets = {
    1: 'Emergent',
    2: 'CE-ACL-14',
    3: 'CE-EMNLP-15',
    # 4: 'Argument Structure Prediction'
}

datasets_paths = {
    'Emergent': os.path.join(TRAIN_DIR, 'url-versions-2015-06-14-clean.csv'),
    'CE-ACL-14': os.path.join(COMPLETE_DIR, 'ce_acl_14_joined-ids.csv'),
    'CE-EMNLP-15': os.path.join(COMPLETE_DIR, 'ce_emnlp_15_joined-ids.csv'),
    # 'Argument Structure Prediction': os.path.join(POST_PROCESS_DIR,
    #                                               'ce_emnlp_15_argument_structure_prediction_training.csv')
}

dataset_keys = {
    'Emergent': ['articleHeadline', 'claimHeadline'],
    'CE-ACL-14': ['Claim', 'CDE'],
    'CE-EMNLP-15': ['Claim', 'Evidence'],
    'Argument Structure Prediction': ['Claim', 'Evidence']
}

# Getting pickle file
# TODO: pickle_file may be a list?
pickle_file = input("Insert pickle file: ")
pickle_data = PickleUtility.load_pickle(pickle_file)

print("Loaded {}".format(pickle_file))

# Getting data-set keys -> CE-ACL-14 (Claim, CDE), CE-EMNLP-15 (Claim, Evidence),
#  Emergent (articleHeadline, claimHeadline)

# dataset_index = input("Please select data-set:\n{}\n: ".format(datasets))
#
# while dataset_index not in datasets:
#     dataset_index = input("Please select data-set:\n{}\n: ".format(datasets))

for dataset_index in datasets:

    print("Considering: {}".format(datasets[dataset_index]))

    dataset_name = datasets[dataset_index]
    dataset_path = datasets_paths[dataset_name]

    print("Path to data-set: {}".format(dataset_path))

    data = pd.read_csv(dataset_path, encoding="utf-8")
    keys = dataset_keys[dataset_name]
    first_values = data[keys[0]].values
    second_values = data[keys[1]].values
    couple_values = zip(first_values, second_values)

    # Extracting keys from pickle data

    extracted_data = dict({key: pickle_data[key] for key in pickle_data
                           if key in first_values or key in second_values or key in couple_values})
    print("Found {} items!".format(len(extracted_data.keys())))

    # Saving extracted data to new pickle file

    if len(extracted_data.keys()) > 0:
        folder = dataset_name
        PickleUtility.save_to_file(extracted_data, pickle_file, folder=folder)
        print("Saving extracted data to {}/{}".format(folder, pickle_file))

