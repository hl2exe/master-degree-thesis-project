"""

Testing word embedding + PCA

"""

from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from sklearn.decomposition import PCA
import numpy as np


def get_vec(model, word, dim=300):
    default_value = np.zeros(dim)

    try:
        vec = model[word]
    except KeyError:
        vec = default_value

    return vec


utils = FeatureExtractorUtils()
utils.load_w2v_model()

# utils.w2v_model.init_sims()

pairs = [('maculay', 'he'),
('her', 'his'),
('woman', 'man'),
('Mary', 'John'),
('herself', 'himself'),
('daughter', 'son'),
('mother', 'father'),
('gal', 'guy'),
('girl', 'boy'),
('female', 'male')]

default_value = np.zeros(300)
differences = [get_vec(utils.embeddings_model, a[0]) - get_vec(utils.embeddings_model, a[1]) for a in pairs]
differences.append(default_value)
differences.append(default_value)
difference_matrix = np.array(differences)

pca = PCA(n_components=10)
pca.fit(difference_matrix)
print(difference_matrix.shape)
print(difference_matrix)
# print(pca.explained_variance_ratio_)
# print(pca.components_[0])
# print(pca.components_[0].shape)
