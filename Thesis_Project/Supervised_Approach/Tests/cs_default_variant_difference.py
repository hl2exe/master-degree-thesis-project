"""

Verifying difference between:

1. Standard cosine similarity extractor data
2. Sentence embedding cosine similarity extractor data

"""

from Supervised_Approach.Utils.pickle_utility import PickleUtility
from Supervised_Approach.selector import CoupleSelector
import pandas as pd
from Supervised_Approach.Utils.dataset_utils import DatasetUtils

df_filename = "url-versions-2015-06-14-clean-train.csv"
df_name = 'emergent-train-set'
targets = ['claimHeadline', 'articleHeadline']
df_section = "train"
pickle_section = 'Emergent'

standard_data = PickleUtility.load_pickle('cosine_similarity.pickle', pickle_section)
variant_data = PickleUtility.load_pickle('cosine_similarity_ABTT.pickle', pickle_section)

selector = CoupleSelector(key_mapping={'x': ['claimHeadline', 'articleHeadline']})
# selector = ItemSelector(key_mapping={'x': 'Evidence'})

# Placeholder
path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
df = pd.read_csv(path_to_df, encoding="utf-8")
df.name = 'x'
target_list = selector.transform(dataframe=df)

differences = []
benefits = 0
for couple in target_list:
    difference = standard_data[couple] - variant_data[couple]
    differences.append(abs(difference))
    print(standard_data[couple], variant_data[couple])
    if difference < 0:
        benefits += 1

avg = sum(differences) / float(len(differences))
print('Average: ', avg)
print('Benefits rate: {} (total: {})'.format(float(benefits) / len(target_list), benefits))

