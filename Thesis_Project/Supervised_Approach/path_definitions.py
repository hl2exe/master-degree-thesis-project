"""

Simple script which defines all directories used in this project for saving/using data.

"""

import os

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

PICKLE_DIR = os.path.join(PROJECT_DIR,
                          'Pickle')

CONFIG_DIR = os.path.join(PROJECT_DIR,
                          'Configs')

STATISTICS_DIR = os.path.join(PROJECT_DIR,
                              'Statistics')

CLASSIFIER_CONFIGURATIONS_DIR = os.path.join(STATISTICS_DIR,
                                             'Classifier_configurations')

ANALYTICS_DIR = os.path.join(STATISTICS_DIR, 'Analytics')

DATASETS_DIR = os.path.join(PROJECT_DIR,
                            'Datasets')

PREDICTIONS_DIR = os.path.join(DATASETS_DIR,
                               'predictions')

GENERATED_DIR = os.path.join(DATASETS_DIR,
                             'generated')

TRAIN_DIR = os.path.join(DATASETS_DIR,
                         'train')

TEST_DIR = os.path.join(DATASETS_DIR,
                        'test')

PREBUILT_CV_DIR = os.path.join(DATASETS_DIR,
                               'prebuilt_cv')

RUNNABLES_DIR = os.path.join(PROJECT_DIR,
                             'Runnables')

POST_PROCESS_DIR = os.path.join(DATASETS_DIR,
                                'post_process')

EXT_DIR = os.path.join(PROJECT_DIR, 'Ext')

NLTK_DIR = os.path.join(EXT_DIR, 'nltk_data')

STANFORD_PARSER_DIR = os.path.join(EXT_DIR, 'stanford-parser-full-2017-06-09')

PPDB_DIR = os.path.join(EXT_DIR, 'ppdb')

DATASETS_DICT = {
    'generated': GENERATED_DIR,
    'prebuilt_cv': PREBUILT_CV_DIR,
    'predictions': PREDICTIONS_DIR,
    'train': TRAIN_DIR,
    'test': TEST_DIR,
    'post_process': POST_PROCESS_DIR
}