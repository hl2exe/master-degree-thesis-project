from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from Supervised_Approach.Neural_networks.rnn_models import WordEmbeddingLSTMSequential,\
    WordEmbeddingLSTMSequentialSiamese, WordEmbeddingsCNNSequential
from Supervised_Approach.Utils.json_utils import load_simplejson_from_file

"""

Simple class which implements the factory pattern.

"""


class ClassifierFactory(object):

    supported_classifiers = {
        SGDClassifier,
        LinearSVC,
        LogisticRegression,
        MultinomialNB,
        RandomForestClassifier
    }

    supported_networks = {
        WordEmbeddingLSTMSequential,
        WordEmbeddingLSTMSequentialSiamese,
        WordEmbeddingsCNNSequential

    }

    default_classifier_initialization_info = load_simplejson_from_file('classifiers_args.json')

    @staticmethod
    def factory(cl_type, model_type='classic', **kwargs):
        """
        Returns an instance of specified type, built with given, if any, parameters.

        :param cl_type: string name of the classifier class (not case sensitive)
        :param model_type
        :param kwargs: additional __init__ parameters
        :return: classifier instance
        """

        if model_type == 'classic':
            set_to_lookup = ClassifierFactory.supported_classifiers
        elif model_type == 'network':
            set_to_lookup = ClassifierFactory.supported_networks
        else:
            set_to_lookup = ClassifierFactory.supported_networks | ClassifierFactory.supported_networks

        for classifier in set_to_lookup:
            if cl_type.lower() == classifier.__name__.lower():
                return classifier(**kwargs)
        else:
            assert 0, "Bad type creation: {}".format(cl_type)

    @staticmethod
    def get_all_classifiers_types(classifiers_info=None, model_type='classic'):
        """
        Retrieves all supporter classifier types.

        :param classifiers_info: custom classifier args for init
        :param model_type: whether to look for standard classifier algorithms
        or custom neural network models
        :return: supported instantiated classifier algorithms
        """

        if classifiers_info is None:
            classifiers_info = ClassifierFactory.default_classifier_initialization_info

        if model_type == 'classic':
            set_to_lookup = ClassifierFactory.supported_classifiers
        elif model_type == 'network':
            set_to_lookup = ClassifierFactory.supported_networks
        else:
            set_to_lookup = ClassifierFactory.supported_networks | ClassifierFactory.supported_networks

        instances = []
        for classifier_type in set_to_lookup:
            classifier_name = type(classifier_type).__name__.lower()
            if classifier_name in classifiers_info:
                instances.append(classifier_type(**classifiers_info[classifier_name]))
            else:
                instances.append(classifier_type())

        return instances
