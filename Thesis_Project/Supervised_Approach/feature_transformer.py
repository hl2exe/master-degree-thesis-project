import itertools
import traceback

import numpy as np
from scipy.sparse import csr_matrix
from sklearn.base import TransformerMixin, BaseEstimator

from Utils.feature_transformer_utils import FeatureTransformerUtils


class BaseCustomTransformer(BaseEstimator, TransformerMixin):
    """
    Basic abstract class which defines the general behaviour of a transformer.
    """

    def __init__(self, utils=None):
        self.utils = utils

    def fit(self, X, y=None):
        return self

    def transform(self, data):
        pass


class CosineSimilarityTransformer(BaseCustomTransformer):

    def transform(self, cosine_similarity_data):
        """
        Computes the cosine similarity matrix

        :param cosine_similarity_data:
        :return: feature matrix
        """

        mat = np.zeros((len(cosine_similarity_data), 1))
        for i, item in enumerate(cosine_similarity_data):
            mat[i] = item

        return csr_matrix(mat)


class SVOTriplesTransformer(BaseCustomTransformer):

    def transform(self, triples_couple_list, **transform_params):
        """
        Computes SVO entailment matrix. For each SVO triples couple obtained by the product of
        the first target SVO triples and the second's, the PPDB relationship data is used in order to
        identify the entailment score between each S,V,O couple.

        :param triples_couple_list: list of tuples containing targets svo triples
        :return: SVO entailment matrix
        """

        columns = 3*len(set(FeatureTransformerUtils.entailment_map.values()))
        mat = np.zeros((len(triples_couple_list), columns))

        for id, (target_1_svos, target_2_svos) in enumerate(triples_couple_list):
            try:
                vec = np.zeros((1, columns))

                for (target1_svo, target2_svo) in itertools.product(target_1_svos, target_2_svos):

                    if not target1_svo or not target2_svo:
                        continue

                    entailments = {}
                    for key in target1_svo[0]:
                        entailments[key] = FeatureTransformerUtils.calc_entailment_vec(target1_svo[0][key][0], target2_svo[0][key][0])

                    for index, key in zip(range(0, 3), entailments):
                        start_value = len(set(FeatureTransformerUtils.entailment_map.values())) * index
                        end_value = start_value + len(set(FeatureTransformerUtils.entailment_map.values()))
                        vec[0, start_value:end_value] += entailments[key][0]

                mat[id, :] = vec

            except Exception, e:
                print(e)
                traceback.print_exc()
                pass

        return csr_matrix(mat)

##############################
# Stab and Gurevych features #
##############################

# Are the below transformers needed?


class TokenCounterTrasnformer(BaseCustomTransformer):

    def transform(self, token_counter_data):
        """
        Simple transformer that builds a csr_matrix from given data.

        :param token_counter_data: list of data, one for each input item.
        :return:
        """

        mat = np.zeros((len(token_counter_data), 1))
        for i, item in enumerate(token_counter_data):
            mat[i] = item

        return csr_matrix(mat)


class TokenCounterDifferenceTransformer(BaseCustomTransformer):

    def transform(self, token_difference_data):
        """
        Simple transformer that builds a csr_matrix from given data.

        :param token_difference_data: list of data, one for each input item.
        :return: csr_matrix
        """

        mat = np.zeros((len(token_difference_data), 1))
        for i, item in enumerate(token_difference_data):
            mat[i] = item

        return csr_matrix(mat)


class PunctuationMarksTransformer(BaseCustomTransformer):

    def transform(self, punctuation_marks_data):
        """
        Simple transformer that builds a csr_matrix from given data.

        :param punctuation_marks_data: list of data, one for each input item.
        :return: csr_matrix
        """

        mat = np.zeros((len(punctuation_marks_data), 1))
        for i, item in enumerate(punctuation_marks_data):
            mat[i] = item

        return csr_matrix(mat)


class PunctuationMarksDifferenceTransformer(BaseCustomTransformer):

    def transform(self, punctuation_marks_difference_data):
        """
        Simple transformer that builds a csr_matrix from given data.

        :param punctuation_marks_difference_data: list of data, one for each input item.
        :return: csr_matrix
        """

        mat = np.zeros((len(punctuation_marks_difference_data), 1))
        for i, item in enumerate(punctuation_marks_difference_data):
            mat[i] = item

        return csr_matrix(mat)


class ModalVerbBinaryTransformer(BaseCustomTransformer):

    def transform(self, modal_verb_data):
        """
        Simple transformer that builds a csr_matrix from given data.

        :param modal_verb_data: list of data, one for each input item.
        :return: csr_matrix
        """

        mat = np.zeros((len(modal_verb_data), 1))
        for i, item in enumerate(modal_verb_data):
            mat[i] = item

        return csr_matrix(mat)


class CommonElementsTransformer(BaseCustomTransformer):

    def transform(self, common_elements_data):
        """
        Simple transformer that builds a csr_matrix from given data.

        :param common_elements_data: list of data, one for each input item.
        :return: csr_matrix
        """

        mat = np.zeros((len(common_elements_data), 1))
        for i, item in enumerate(common_elements_data):
            mat[i] = item

        return csr_matrix(mat)
