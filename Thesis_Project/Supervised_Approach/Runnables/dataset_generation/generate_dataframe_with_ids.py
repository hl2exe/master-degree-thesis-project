"""

:author: Federico Ruggeri
:date: 06th April 2018
:description: Processes instance dataframe in order to build a feature extraction compliant dataset.
"""

import os

import pandas as pd

from Supervised_Approach.dataset_generator import DatasetGenerator
from Supervised_Approach.path_definitions import TRAIN_DIR

df_name = 'ce_emnlp_15_joined.csv'
df_folder = TRAIN_DIR
columns = ['Evidence']
filename = 'ce_emnlp_15_joined-ids.csv'

df_path = os.path.join(df_folder, df_name)

dataframe = pd.DataFrame.from_csv(df_path, encoding="utf-8")
DatasetGenerator.generate_ids(dataframe=dataframe, columns=columns, filename=filename)
