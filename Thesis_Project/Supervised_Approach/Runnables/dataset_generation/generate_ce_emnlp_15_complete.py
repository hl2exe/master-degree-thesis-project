"""

:author: Federico Ruggeri
:date: 06th April 2018
:description: Combines claim data with retrieved article data into a single .csv file.
"""

import os

from Supervised_Approach.dataset_generator import DatasetGenerator
from Supervised_Approach.path_definitions import GENERATED_DIR

claim_df_name = 'claims.csv'
claim_df_folder = ""
path_to_claim_csv = os.path.join(claim_df_folder, claim_df_name)

# generated associations filename (check generate_claim_article_association_ce_emnlp_15)
bodies_df_name = 'claim_article_associations_ce_emnlp_15.csv'
path_to_bodies_csv = os.path.join(GENERATED_DIR, bodies_df_name)

filename = "ce_emnlp_15_complete"

DatasetGenerator.generate_ce_emnlp_15_complete(path_to_claim=path_to_claim_csv, path_to_bodies=path_to_bodies_csv,
                                               filename=filename)
