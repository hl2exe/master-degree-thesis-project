"""

:author: Federico Ruggeri
:date: 06th April 2018
:description: Combines claim data with retrieved article data into a single .csv file.
"""

from Supervised_Approach.dataset_generator import DatasetGenerator
from Supervised_Approach.dataset_generator import GENERATED_DIR
import os

claim_df_name = '2014_7_18_ibm_CDCdata.csv'
claim_df_folder = ""
path_to_claim_csv = os.path.join(claim_df_folder, claim_df_name)

# generated_bodies_ce_acl_14.csv (check generate_bodies_ce_acl_14.py)
bodies_df_name = 'generated_bodies_ce_acl_14.csv'
path_to_bodies_csv = os.path.join(GENERATED_DIR, bodies_df_name)

filename = "ce_acl_14_complete.csv"

DatasetGenerator.generate_ce_acl_14_complete(path_to_claim=path_to_claim_csv, path_to_bodies=path_to_bodies_csv,
                                             filename=filename)
