"""

:author: Federico Ruggeri
:date: 06th April 2018
:description: Builds claim - article associations, i.e. retrieves article bodies stored in files in which the claim was
previously extracted. Associations are saved in .csv format speficied by given file name. Failed entries are saved in
.csv format as well.

"""

import os

import pandas as pd

from Supervised_Approach.dataset_generator import DatasetGenerator

# All taken from CE-EMNLP-2015.v3 folder

claim_df_name = 'claims.csv'
claim_df_folder = ""
claim_dataset_path = os.path.join(claim_df_folder, claim_df_name)

# articles.csv
article_df_name = 'articles.csv'
article_df_folder = ""
article_dataset_path = os.path.join(article_df_folder, article_df_name)

# articles folder
folder = ""

filename = "claim_article_associations_ce_emnlp_15"
failed_filename = "failed_entries_ce_emnlp_15.csv"

claim_dataset = pd.read_csv(claim_dataset_path, encoding="utf-8")
article_dataset = pd.read_csv(article_dataset_path, encoding="utf-8")

DatasetGenerator.generate_claim_article_association_ce_emnlp_15(claim_dataset=None, article_dataset=None, folder=None,
                                                                filename=None, failed_filename=None)
