"""

:author: Federico Ruggeri
:date: 06th April 2018
:description: Retrieves for each Body ID the associated text stored in a file within given folder path.
The results is saved in .csv format in specified file name.

"""

from Supervised_Approach.dataset_generator import DatasetGenerator

articles_folder = ""
filename = ""

DatasetGenerator.generate_bodies_ce_emnlp_15(folder=articles_folder, filename=filename)
