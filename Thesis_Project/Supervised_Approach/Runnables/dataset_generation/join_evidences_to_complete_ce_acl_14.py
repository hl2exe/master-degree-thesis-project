"""

:author: Federico Ruggeri
:date: 06th April 2018
:description: Adds evidence data to complete csv file.
"""

import os

from Supervised_Approach.dataset_generator import DatasetGenerator
from Supervised_Approach.path_definitions import GENERATED_DIR

complete_df_name = "ce_acl_14_complete.csv"
cde_df_name = ""
filename = "ce_acl_14_joined.csv"

path_to_complete = os.path.join(GENERATED_DIR, complete_df_name)
path_to_cde = ""

DatasetGenerator.join_evidences_to_complete_ce_acl_14(path_to_complete=path_to_complete, path_to_cde=path_to_cde,
                                                      filename=filename)
