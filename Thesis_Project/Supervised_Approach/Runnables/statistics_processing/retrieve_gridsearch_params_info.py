"""

:author: Federico Ruggeri
:date: 23rd March 2018
:description: retrieves best grid-search parameters for given configuration and
saves them in .csv format.

"""

import os

import pandas as pd

from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.Utils.json_utils import load_simplejson_from_file

configuration_name = 'emergent_all_linearsvc_1'
folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)

gridsearch_data = load_simplejson_from_file('gridsearch.json', folder)
gridsearch_dataset_name = gridsearch_data.keys()[0]

dataframe_data = {}

for param in gridsearch_data[gridsearch_dataset_name]['best_parameters']:
    featureunion_suffix, feature_name, block, param_name = param.split('__')
    if param_name in dataframe_data:
        dataframe_data[param_name].update(
            {feature_name: gridsearch_data[gridsearch_dataset_name]['best_parameters'][param]})
    else:
        dataframe_data[param_name] = {feature_name: gridsearch_data[gridsearch_dataset_name]['best_parameters'][param]}

path = os.path.join(folder, 'gridsearch_results_analysis.csv')
pd.DataFrame.from_dict(dataframe_data).to_csv(path, encoding="utf-8")
