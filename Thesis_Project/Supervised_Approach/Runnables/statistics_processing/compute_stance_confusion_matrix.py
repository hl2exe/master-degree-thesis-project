"""

:author: Federico Ruggeri
:date: 15th March 2018
:description: confusion matrix computation for each possible filtering condition
considered related to the 'observing' stance classification label within the
ASP data-sets in order to evaluate the importance of the stance labels informative content

"""

import os
from Supervised_Approach.post_processor import PostProcessor
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, ANALYTICS_DIR
from Supervised_Approach.collector import Collector

#####################
# Variables Section
#####################

df_name = 'ce_emnlp_15_joined_predictions.csv'
df_folder = POST_PROCESS_DIR
path_to_df = os.path.join(df_folder, df_name)

claim_stance_column = 'Claim stance'
evidence_stance_column = 'Evidence stance'
claim_column = 'Claim'
evidence_column = 'Evidence'
group_by_column = 'Topic'

modes = ['default', 'overcome-observing', 'ignore-observing']

# Saving

filename = 'confusion_matrix_stance_statistics'
path = ANALYTICS_DIR

#####################
# System Section
#####################

confusion_matrixes = {}
for mode in modes:
    confusion_matrix = PostProcessor.compute_stance_confusion_matrix(path_to_csv=path_to_df,
                                                                     claim_stance_column=claim_stance_column,
                                                                     evidence_stance_column=evidence_stance_column,
                                                                     claim_column=claim_column,
                                                                     evidence_column=evidence_column,
                                                                     group_by_column=group_by_column,
                                                                     mode=mode)
    confusion_matrixes[mode] = confusion_matrix.to_dict()

# Saving results

to_save = {df_name: confusion_matrixes}
Collector.save_results(to_save, filename, path, append=True)
