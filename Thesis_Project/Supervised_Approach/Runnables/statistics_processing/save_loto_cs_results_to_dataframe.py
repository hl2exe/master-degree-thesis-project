"""

:author: Federico Ruggeri
:date: 4th April 2018
:description: saves LOTO confusion matrix results in tabular format, i.e. .csv file

"""

import os

import numpy as np
import pandas as pd

from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR, ANALYTICS_DIR, TRAIN_DIR
from Supervised_Approach.Utils import json_utils


def flat_list(l):
    return [item for sublist in l for item in sublist]


configurations_names = ['test_config_loto']
results_key = "('MARGOT-ASP', 'CE-EMNLP-15-TT')"
data_filename = "loto_scoring.json"
confusion_matrix_labels = ['link', 'no-link']
csv_filename = 'loto_validation_confusion_matrix.csv'

# Retrieving topics info

motions_df = pd.read_csv(os.path.join(TRAIN_DIR, 'motions.csv'), encoding="utf-8")
train_and_test_topics = motions_df.loc[motions_df['Data-set'] == 'train and test'][['Topic id', 'Topic']].to_dict()
topic_mapping = {train_and_test_topics['Topic'][key]: key for key in train_and_test_topics['Topic']}
topic_mapping_ids = train_and_test_topics['Topic id']
topic_mapping_sorted = [(topic, topic_mapping[topic]) for topic in topic_mapping]
topic_mapping_sorted.sort(key=lambda couple: couple[1])

loto_dict = {}
topics = None
for config_name in configurations_names:
    folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, config_name)
    config_data = json_utils.load_simplejson_from_file(data_filename, folder)
    topic_results = config_data[results_key]['topic_results']
    loto_dict[config_name] = topic_results

    if topics is None:
        topics = set(topic_results.keys())

dataframe_columns = ['Topic graph id', 'Topic id']
for config_name in configurations_names:
    for label in confusion_matrix_labels:
        dataframe_columns.append('{} {}'.format(config_name, label))

dataframe_columns += ['Link', 'True']

df_out = pd.DataFrame(index=[val for val in topic_mapping_ids.values() for _ in (0, 1)],
                      columns=dataframe_columns, data=np.nan)

df_out['Topic id'] = pd.Series([val for val in topic_mapping_ids.values() for _ in (0, 1)]).values
df_out['Topic graph id'] = pd.Series([x for x in topic_mapping_ids.keys() for _ in (0, 1)]).values

for config_name in configurations_names:
    for label in confusion_matrix_labels:
        column_name = '{} {}'.format(config_name, label)
        column_values = flat_list([loto_dict[config_name][couple[0]]['confusion_matrix'][label].values()
                                   for couple in topic_mapping_sorted])
        df_out[column_name] = pd.Series(column_values).values

df_out['Link'] = pd.Series(
    ['no-link' if x % 2 == 0 else 'link' for x in topic_mapping_ids.values() for _ in (0, 1)]).values
df_out['True'] = pd.Series(['True' for x in topic_mapping_ids.values() for _ in (0, 1)]).values

df_out = df_out.set_index('Topic graph id')
df_out.to_csv(os.path.join(ANALYTICS_DIR, csv_filename), encoding='utf-8')
