"""

:author: Federico Ruggeri
:date: 25th March 2018
:description: claim and evidences stance classification predictions are evaluated
in order to find general information about classification labels distribution
along with possible inconsistencies, i.e. claim stance label differs from evidence
stance label in accordance to a given filtering condition.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd

from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, ANALYTICS_DIR
from Supervised_Approach.post_processor import PostProcessor


def hard_inconsistency(label1, label2):
    return label1 != 'observing' and label2 != 'observing'


df_name = 'ce_acl_14_joined_predictions.csv'
path_to_df = os.path.join(POST_PROCESS_DIR, df_name)
df = pd.read_csv(path_to_df, encoding="utf-8")

has_hard_inconsistency = True

filename = 'dataset_predictions_analysis'
path = ANALYTICS_DIR

# Label stats

columns_to_count = ['Claim stance', 'CDE stance']
columns_stats = {}

for column in columns_to_count:
    values = df[column].values
    stats = {label: count for label, count in zip(*np.unique(values, return_counts=True))}
    columns_stats[column] = stats

# Label inconsistencies

inconsistencies = PostProcessor.find_inconsistencies(df_name, columns_to_count)

soft_inconsistencies = 0
if inconsistencies is not None:
    soft_inconsistencies = len(inconsistencies)

hard_inconsistencies = None
if has_hard_inconsistency and inconsistencies is not None:
    values = [inconsistencies[column].values for column in columns_to_count]
    hard_inconsistencies = len([(label1, label2) for label1, label2 in zip(*values)
                                if hard_inconsistency(label1, label2)])

to_save = OrderedDict()
for column in columns_stats:
    if df_name in to_save:
        to_save[df_name].update({column: columns_stats[column]})
    else:
        to_save[df_name] = {column: columns_stats[column]}

to_save[df_name]['has_hard_inconsistencies'] = has_hard_inconsistency
to_save[df_name]['inconsistencies'] = {'soft': soft_inconsistencies,
                                       'hard': hard_inconsistencies}

to_save[df_name]['total'] = len(df)

Collector.save_results(to_save, filename, path, append=True)
