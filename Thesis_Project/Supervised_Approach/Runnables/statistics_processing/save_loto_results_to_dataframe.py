"""

:author: Federico Ruggeri
:date: 4th April 2018
:description: saves LOTO accuracy and f1-score results in tabular format, i.e. .csv file

"""

import os

import numpy as np
import pandas as pd

from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR, ANALYTICS_DIR, TRAIN_DIR
from Supervised_Approach.Utils import json_utils

configurations_names = ['test_config_loto', 'test_config_loto_2']
results_key = "CE-EMNLP-15-TT"
data_filename = "loto_validation.json"
metrics_to_compare = ['accuracy', 'f1-score']

csv_filename = 'loto_validation.csv'

# Retrieving topics info

motions_df = pd.read_csv(os.path.join(TRAIN_DIR, 'motions.csv'), encoding="utf-8")
train_and_test_topics = motions_df.loc[motions_df['Data-set'] == 'train and test'][['Topic id', 'Topic']].to_dict()
topic_mapping = {train_and_test_topics['Topic'][key]: key for key in train_and_test_topics['Topic']}
topic_mapping_ids = train_and_test_topics['Topic id']
topic_mapping_sorted = [(topic, topic_mapping[topic]) for topic in topic_mapping]
topic_mapping_sorted.sort(key=lambda couple: couple[1])

loto_dict = {}
topics = None
for config_name in configurations_names:
    folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, config_name)
    config_data = json_utils.load_simplejson_from_file(data_filename, folder)
    topic_results = config_data[results_key]['topic_results']
    loto_dict[config_name] = topic_results

    if topics is None:
        topics = set(topic_results.keys())

dataframe_columns = ['Topic graph id', 'Topic id']
for config_name in configurations_names:
    for metric in metrics_to_compare:
        dataframe_columns.append('{} {}'.format(config_name, metric))

df_out = pd.DataFrame(index=[x for x in range(0, len(topics))], columns=dataframe_columns, data=np.nan)

df_out['Topic id'] = pd.Series(topic_mapping_ids.values()).values
df_out['Topic graph id'] = pd.Series(topic_mapping_ids.keys()).values

for config_name in configurations_names:
    for metric in metrics_to_compare:
        column_name = '{} {}'.format(config_name, metric)
        column_values = [loto_dict[config_name][couple[0]][metric] for couple in topic_mapping_sorted]
        df_out[column_name] = pd.Series(column_values).values

df_out = df_out.set_index('Topic graph id')
df_out.to_csv(os.path.join(ANALYTICS_DIR, csv_filename), encoding='utf-8')
