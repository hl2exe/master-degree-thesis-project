"""

:author: Federico Ruggeri
:date: 4th April 2018
:description: LOTO accuracy and f1-score results are plotted via matplotlib in order to show
which classifier performs best in each topic

"""

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR, ANALYTICS_DIR, TRAIN_DIR
from Supervised_Approach.Utils import json_utils

configurations_names = ['test_config_loto', 'test_config_loto_2']
results_key = "CE-EMNLP-15-TT"
data_filename = "loto_validation.json"
figure_name = 'loto_validation_plot.png'
metrics_to_compare = ['accuracy', 'f1-score']

plt_figure_size = [16, 16]
plt_subplots = 211

# [Optional]
sort_by_best_estimator = True
best_estimator = {
    'accuracy': 'test_config_loto',
    'f1-score': 'test_config_loto'
}

# Retrieving topics info

motions_df = pd.read_csv(os.path.join(TRAIN_DIR, 'motions.csv'), encoding="utf-8")
train_and_test_topics = motions_df.loc[motions_df['Data-set'] == 'train and test'][['Topic id', 'Topic']].to_dict()
topic_mapping = {train_and_test_topics['Topic'][key]: key for key in train_and_test_topics['Topic']}
topic_mapping_ids = train_and_test_topics['Topic id']

# Retrieving LOTO results

loto_dict = {}
topics = None
for config_name in configurations_names:
    folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, config_name)
    config_data = json_utils.load_simplejson_from_file(data_filename, folder)
    topic_results = config_data[results_key]['topic_results']
    loto_dict[config_name] = topic_results

    if topics is None:
        topics = set(topic_results.keys())

# Plotting

plt.figure(1)

fig = plt.gcf()
fig.set_size_inches(*plt_figure_size)

for metric in metrics_to_compare:

    # [Optional] Sort data in accordance to best estimator
    if sort_by_best_estimator:
        best_estimator_data = loto_dict[best_estimator[metric]]
        metric_data = [(topic, best_estimator_data[topic][metric]) for topic in best_estimator_data]
        metric_data.sort(key=lambda couple: couple[1], reverse=True)
        current_topics = [couple[0] for couple in metric_data]
    else:
        current_topics = topics

    topics_indexes = [topic_mapping[topic] for topic in current_topics]

    ax = plt.subplot(plt_subplots)

    for config_name in configurations_names:
        plt.plot(range(0, len(current_topics)), [loto_dict[config_name][topic][metric] for topic in current_topics],
                 label=config_name)

    leg = plt.legend(loc='best', ncol=2, mode="expand", shadow=True, fancybox=True)

    plt.xlabel('topics')
    plt.ylabel(metric)
    plt.xticks(range(0, len(topics)), topics_indexes)
    plt.yticks(np.arange(0.0, 1.0, 0.1))

    plt.grid(linestyle='dotted')

    plt_subplots += 1

figure_path = os.path.join(ANALYTICS_DIR, figure_name)
fig.savefig(figure_path)

plt.show()
