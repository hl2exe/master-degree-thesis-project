"""

:author: Federico Ruggeri
:date: 15th March 2018
:description: finds topic grouped incoherent crossed pairs, i.e. evidence - claim
crossed pairs that have different stance labels in accordance to a given filtering
condition.

"""

import os

from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, ANALYTICS_DIR
from Supervised_Approach.post_processor import PostProcessor

#####################
# Variables Section
#####################

df_name = 'ce_acl_14_joined_predictions.csv'
df_folder = POST_PROCESS_DIR
path_to_df = os.path.join(df_folder, df_name)

group_by_column = 'Topic'
claim_stance_column = 'Claim stance'
evidence_stance_column = 'CDE stance'
claim_column = 'Claim'
evidence_column = 'CDE'

modes = ['default', 'ignore-observing', 'all']
filename = 'stance_crossed_pairs_statistics'
path = ANALYTICS_DIR

#####################
# System Section
#####################

crossed_pairs_counts = {}
for mode in modes:
    filtered_couples = PostProcessor.count_incoherent_crossed_pairs(path_to_csv=path_to_df,
                                                                    group_by_column=group_by_column,
                                                                    claim_stance_column=claim_stance_column,
                                                                    evidence_stance_column=evidence_stance_column,
                                                                    claim_column=claim_column,
                                                                    evidence_column=evidence_column,
                                                                    mode=mode)

    crossed_pairs_counts[mode] = len(filtered_couples)

to_save = {df_name: crossed_pairs_counts}
Collector.save_results(to_save, filename, path, append=True)
