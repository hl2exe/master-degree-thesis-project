"""

:author: Federico Ruggeri
:date: 23rd March 2018
:description: Ablation test results are analysed in order to build a .csv file
containing test cross validation scores and test evaluation performance for each
tested feature configuration

"""

from Supervised_Approach.Utils.json_utils import load_simplejson_from_file
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
import os
import pandas as pd
import numpy as np

configuration_name = 'best_emergent_divided'
folder = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)

ablation_data = load_simplejson_from_file('ablation.json', folder)

ablation_results = {}
for test in ablation_data:
    ablation_results[test] = {}
    for feature in ablation_data[test]:
        if feature not in ['general_test_info', 'specific_test_info']:
            ablation_results[test][feature] = {
                'cv test average accuracy': ablation_data[test][feature]['cross_validation']['test_Accuracy_average'],
                'cv test average f1score': ablation_data[test][feature]['cross_validation'][
                    'test_F1-score_average'],
                'test evaluation score': ablation_data[test][feature]['test_set_evaluation']}

for test in ablation_results:
    df_out = pd.DataFrame(index=['{}'.format(feature) for feature in ablation_results[test].keys()],
                          columns=['cv test average accuracy', 'cv test average f1score', 'test evaluation score'],
                          data=np.nan)
    for feature in ablation_results[test].keys():
        df_out.ix[feature, 'cv test average accuracy'] = ablation_results[test][feature]['cv test average accuracy']
        df_out.ix[feature, 'cv test average f1score'] = ablation_results[test][feature]['cv test average f1score']
        df_out.ix[feature, 'test evaluation score'] = ablation_results[test][feature]['test evaluation score']

    path = os.path.join(folder, 'ablation_results_{}.csv'.format(test))
    df_out.to_csv(path, encoding="utf-8")
