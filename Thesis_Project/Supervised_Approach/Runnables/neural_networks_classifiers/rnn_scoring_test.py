"""

:author: Federico Ruggeri
:data: 22nd March 2018
:description: classification test in which a trained classifier makes
 prediction towards a given target of a given input data-set.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (neural network model) is built via the custom framework APIs.

3) Output section: the trained classifier is asked to make predictions on given test set.
 Lastly, obtained results are saved on file.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score
from sklearn.utils import class_weight

from Supervised_Approach.classifier_factory import ClassifierFactory
from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Neural_networks.rnn_models import BaseNetwork

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Dataset

    datasets_info = {
        'train': {
            'df_filename': "ce_acl_14_argument_structure_prediction_training.csv",
            'df_section': 'post_process',
            'df_name': 'CE-ACL-14',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
        'test': {
            'df_filename': "MARGOT_ASP_default_training_balanced.csv",
            'df_section': 'post_process',
            'df_name': 'MARGOT-default',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
    }

    # Stance addition

    # Values: 'y' or 'n'
    add_stance_mode = 'y'
    stance_columns = {'Claim': 'Claim stance',
                      'Evidence': 'Evidence stance'}
    stance_labels = {'for', 'against', 'observing'}

    add_stance = False
    stance_vector = None

    # Network

    configuration_mode = 'auto'

    # Auto configuration

    configuration_name = 'rnn_test_config'
    configuration_folder = None

    # Manual configuration

    network_type = 'wordembeddinglstmsequential'
    network_params = {
        'lstm_neurons': 100,
        'metrics': ['accuracy'],
        'add_dropout': False,
        'dropout': 0.,
        'recurrent_dropout': 0.,
        'padding_mode': 'average'
    }
    network_verbose = 0

    # Cross-validation

    classification_labels = OrderedDict({
        'link': 1,
        'no-link': 0
    })

    batch_size = 1000
    epochs = 3
    verbose = 2
    shuffle = True

    ################################
    # System section
    ################################

    # Dataset

    data_info = {}
    for key in datasets_info:
        path_to_df = DatasetUtils.get_path_to_dataset(datasets_info[key]['df_filename'],
                                                      datasets_info[key]['df_section'])
        df_data = pd.read_csv(path_to_df, encoding="utf-8")
        data_info[key] = {
            'path_to_df': path_to_df,
            'df_data': df_data,
            'df_target': df_data[datasets_info[key]['target_column']].values,
            'df_column_data': {target: df_data[target] for target in datasets_info[key]['targets']}
        }

    # Stance

    if add_stance_mode.lower() == 'y':
        stances_data = {target: data_info['train']['df_data'][stance_columns[target]] for target in stance_columns}

        stance_vector = {}
        for target in datasets_info['train']['targets']:
            stance_vector[target] = [idx for val in stances_data[target] for idx, label in enumerate(stance_labels)
                                     if val == label]

        add_stance = True

    # Word2Vec

    utils = FeatureExtractorUtils()
    utils.load_w2v_model()

    # Network

    if configuration_mode == 'manual':
        network = ClassifierFactory.factory(network_type, model_type='network', **network_params)
    else:
        network = Trainer.load_network_configuration(configuration_name=configuration_name, folder=configuration_folder)

    # Input pre-processing

    input_info = {}
    for key in datasets_info:
        df_name = datasets_info[key]['df_name']
        input_info[key] = {target: data_info[key]['df_column_data'][target]
                           for target in datasets_info[key]['targets']}

    vocab_size = {}
    vocab_sizes = {}
    for key in input_info:
        for target in input_info[key]:
            current_vocab_size = BaseNetwork.get_vocab_size(input_info[key][target])
            if target not in vocab_sizes:
                vocab_sizes[target] = [current_vocab_size]
            else:
                vocab_sizes[target].append(current_vocab_size)

    for target in vocab_sizes:
        vocab_size[target] = max(vocab_sizes[target])

    build_info, train_info = network.preprocess_input(input_info=input_info['train'], w2v_model=utils.embeddings_model,
                                                      add_stance=add_stance, stance_labels=stance_labels,
                                                      stance_vector=stance_vector, vocab_size=vocab_size)

    # print('Build info:\n {}'.format(build_info))
    # print('Train info:\n {}'.format(train_info))

    build_info, test_info = network.preprocess_input(input_info=input_info['test'], w2v_model=utils.embeddings_model,
                                                     added_build_info=build_info)

    # Releasing memory
    utils.embeddings_model = None

    ################################
    # Output section:
    ################################

    translated_target = [classification_labels[val] for val in data_info['train']['df_target']]
    target_tran = np.array(translated_target)

    weights = class_weight.compute_class_weight('balanced', np.unique(translated_target), translated_target)
    weights_dict = dict(enumerate(weights))
    print("weight classes: {}".format(np.unique(translated_target)))
    print(weights_dict)

    model = network.build_model(build_info, network_verbose)

    model.fit(x=train_info.values(), y=target_tran, batch_size=batch_size, epochs=epochs, verbose=verbose,
              class_weight=weights_dict, shuffle=True)

    predictions = model.predict_classes(x=test_info.values(), batch_size=batch_size, verbose=verbose)
    predictions = np.reshape(predictions, predictions.shape[0])
    reversed_classification_labels = {value: key for key, value in classification_labels.items()}
    predictions = [reversed_classification_labels[val] for val in predictions]

    # Scoring

    report, confusion_matrix = Trainer.view_sklearn_metrics(predictions, data_info['test']['df_target'])
    accuracy = accuracy_score(y_pred=predictions, y_true=data_info['test']['df_target'])
    f1 = f1_score(y_pred=predictions, y_true=data_info['test']['df_target'], average='weighted')

    # Saving results

    # Results info

    key = str((datasets_info['train']['df_name'], datasets_info['test']['df_name']))
    to_save = OrderedDict({
        key: {
            'true_stats': {label: count for label, count in zip(*np.unique(data_info['test']['df_target'],
                                                                           return_counts=True))},
            'predicted_stats': {label: count for label, count in zip(*np.unique(predictions, return_counts=True))},
            'classification_report': report,
            'confusion_matrix': confusion_matrix.to_dict(),
            'accuracy': accuracy,
            'f1score': f1
        }
    })

    # Test info

    to_save[key]['general_test_info'] = {
        'dataset_info': datasets_info,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'classification_labels': classification_labels,
        'add_stance_mode': add_stance_mode
    }

    to_save[key]['specific_test_info'] = {
        'network_type': network_type,
        'network_params': network_params,
        'network_verbose': network_verbose,
        'batch_size': batch_size,
        'epochs': epochs,
        'verbose': verbose,
        'shuffle': shuffle
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'scoring', path, append=True)
