"""

:author: Federico Ruggeri
:date: 20th March 2018
:description: Leave One Topic Out (LOTO) training test.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (neural network model) is built via the custom framework APIs.

3) Output section: a LOTO test is held in which accuracy and f1-score metrics are computed.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score
from sklearn.model_selection import LeaveOneGroupOut
from sklearn.utils import class_weight

from Supervised_Approach.classifier_factory import ClassifierFactory
from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Dataset

    df_filename = "ce_emnlp_15_argument_structure_prediction_training.csv"
    df_name = 'EMNLP-15_training'
    targets = ['Claim', 'Evidence']
    df_section = "train"
    target_column = 'Argument relation'

    # Stance addition

    # Values: 'y' or 'n'
    add_stance_mode = 'y'
    stance_columns = ['Claim stance', 'Evidence stance']
    stance_labels = {'for', 'against', 'observing'}

    add_stance = False
    stance_vector = None

    # Word2vec

    w2v_mode = 2
    pickle_file = df_name.replace('.csv', '.pickle')

    # Network

    configuration_mode = 'auto'

    # Auto configuration

    configuration_name = 'rnn_test_config'
    configuration_folder = None

    # Manual configuration

    network_type = 'wordembeddinglstmsequential'
    network_params = {
        'lstm_neurons': 100,
        'metrics': ['accuracy'],
        'add_dropout': False,
        'dropout': 0.,
        'recurrent_dropout': 0.,
        'padding_mode': 'average'
    }
    network_verbose = 0

    # Leave One Group Out

    group_column = 'Topic'
    pos_label = 'link'

    classification_labels = OrderedDict({
        'link': 1,
        'no-link': 0
    })

    batch_size = 1000
    epochs = 3
    verbose = 2

    ################################
    # System section
    ################################

    # Dataset

    path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
    train_data = pd.read_csv(path_to_df, encoding="utf-8")
    train_target = train_data[target_column].values

    column_data = [train_data[target] for target in targets]

    # Stance

    if add_stance_mode.lower() == 'y':
        stances_data = [train_data[stance_column] for stance_column in stance_columns]

        stance_vector = {}
        for stance_data, key in zip(stances_data, targets):
            stance_vector[key] = [idx for val in stance_data for idx, label in enumerate(stance_labels)
                                  if val == label]

        add_stance = True

    # Word2Vec

    utils = FeatureExtractorUtils()

    if w2v_mode == 2:
        utils.load_w2v_model()
        load_pickle = False
    else:
        load_pickle = True

    # Network

    if configuration_mode == 'manual':
        network = ClassifierFactory.factory(network_type, model_type='network', **network_params)
    else:
        network = Trainer.load_network_configuration(configuration_name=configuration_name, folder=configuration_folder)

    # Input pre-processing

    input_info = {target: target_data for target, target_data in zip(targets, column_data)}
    build_info, train_info = network.preprocess_input(input_info=input_info, w2v_model=utils.embeddings_model,
                                                      pickle_file=pickle_file, load_pickle=load_pickle,
                                                      add_stance=add_stance, stance_labels=stance_labels,
                                                      stance_vector=stance_vector)

    print('Build info:\n {}'.format(build_info))
    print('Train info:\n {}'.format(train_info))

    # Releasing memory
    utils.embeddings_model = None

    ################################
    # Output section:
    ################################

    translated_target = [classification_labels[val] for val in train_target]
    target_tran = np.array(translated_target)
    target_tran = target_tran.reshape((len(train_target), 1))
    print(target_tran.shape)

    # LOTO test

    logo = LeaveOneGroupOut()
    topic_map = {topic: idx for idx, topic in enumerate(np.unique(train_data[group_column].values))}
    groups = [topic_map[topic] for topic in train_data[group_column].values]
    n_splits = logo.get_n_splits(X=train_data, y=train_target, groups=groups)

    scores = {}

    for idx, (train_index, test_index) in enumerate(logo.split(X=train_data, y=train_target, groups=groups)):
        print "Running Fold", idx+1, "/", n_splits

        group_excluded = np.unique(train_data.iloc[test_index][group_column].values)[0]
        print("Group excluded: {}".format(group_excluded))

        model = network.build_model(build_info, network_verbose)

        labels = np.unique(translated_target)
        weights = class_weight.compute_class_weight('balanced', labels, translated_target)
        weights_dict = {label: weight for (label, weight) in zip(labels, weights)}
        print("weight classes: {}".format(labels))
        print(weights_dict)

        X_train, y_train, X_test, y_test = network.transform_for_classification(train_info=train_info,
                                                                                train_target=target_tran,
                                                                                train_indexes=train_index,
                                                                                test_indexes=test_index)

        model.fit(x=X_train, y=y_train, batch_size=batch_size, epochs=epochs, verbose=verbose,
                  class_weight=weights_dict)
        predictions = model.predict_classes(x=X_test, batch_size=batch_size, verbose=verbose)

        f1 = f1_score(y_pred=predictions, y_true=y_test, pos_label=pos_label)
        accuracy = accuracy_score(y_pred=predictions, y_true=y_test)
        print("Fold scores:\n F1: {}\nAccuracy: {}".format(f1, accuracy))
        scores[group_excluded] = {'f1-score': f1, 'accuracy': accuracy}

    # Saving results

    to_save = OrderedDict({df_name: {'topic_results': scores}})

    # Test info

    to_save[df_name]['general_test_info'] = {
        'df_filename': df_filename,
        'df_name': df_name,
        'targets': targets,
        'df_section': df_section,
        'target_column': target_column,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'classification_labels': classification_labels,
        'w2v_mode': w2v_mode,
        'add_stance_mode': add_stance_mode
    }

    to_save[df_name]['specific_test_info'] = {
        'network_type': network_type,
        'network_params': network_params,
        'network_verbose': network_verbose,
        'n_splits': n_splits,
        'batch_size': batch_size,
        'epochs': epochs,
        'verbose': verbose,
        'group_column': group_column,
        'pos_label': pos_label
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'loto_validation', path)
