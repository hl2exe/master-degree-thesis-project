"""

:author: Federico Ruggeri
:data: 22nd March 2018
:description: rebuttal validation test based on argument structure prediction activity.
First, ASP labels are predicted on test set.
Second, predictions are used in order to define ASP at post level, i.e. ASP between
adjacent post in a given discussion thread.
Third, post level predictions are compared with true values and metrics like precision,
recall, f1-score and accuracy are computed.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score
from sklearn.utils import class_weight

from Supervised_Approach.classifier_factory import ClassifierFactory
from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Neural_networks.rnn_models import BaseNetwork

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Dataset

    datasets_info = {
        'train': {
            'df_filename': "ce_emnlp_15_ASP_training.csv",
            'df_section': 'train',
            'df_name': 'CE-EMNLP-15',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
        'test': {
            'df_filename': "MARGOT_ASP_default_training_balanced.csv",
            'df_section': 'test',
            'df_name': 'MARGOT-default',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
    }

    # Stance addition

    # Values: 'y' or 'n'
    add_stance_mode = 'y'
    stance_columns = {'Claim': 'Claim stance',
                      'Evidence': 'Evidence stance'}
    stance_labels = {'for', 'against', 'observing'}

    add_stance = False
    stance_vector = None

    # Network

    configuration_mode = 'auto'

    # Auto configuration

    configuration_name = 'rnn_test_config'
    configuration_folder = None

    # Manual configuration

    network_type = 'wordembeddinglstmsequential'
    network_params = {
        'lstm_neurons': 100,
        'metrics': ['accuracy'],
        'add_dropout': False,
        'dropout': 0.,
        'recurrent_dropout': 0.,
        'padding_mode': 'average'
    }
    network_verbose = 0

    # Cross-validation

    classification_labels = OrderedDict({
        'link': 1,
        'no-link': 0
    })

    batch_size = 1000
    epochs = 3
    verbose = 2
    shuffle = True

    ################################
    # System section
    ################################

    # Dataset

    data_info = {}
    for key in datasets_info:
        path_to_df = DatasetUtils.get_path_to_dataset(datasets_info[key]['df_filename'],
                                                      datasets_info[key]['df_section'])
        df_data = pd.read_csv(path_to_df, encoding="utf-8")
        data_info[key] = {
            'path_to_df': path_to_df,
            'df_data': df_data,
            'df_target': df_data[datasets_info[key]['target_column']].values,
            'df_column_data': {target: df_data[target] for target in datasets_info[key]['targets']}
        }

    # Stance

    if add_stance_mode.lower() == 'y':
        stances_data = {target: data_info['train']['df_data'][stance_columns[target]] for target in stance_columns}

        stance_vector = {}
        for target in datasets_info['train']['targets']:
            stance_vector[target] = [idx for val in stances_data[target] for idx, label in enumerate(stance_labels)
                                     if val == label]

        add_stance = True

    # Word2Vec

    utils = FeatureExtractorUtils()
    utils.load_w2v_model()

    # Network

    if configuration_mode == 'manual':
        network = ClassifierFactory.factory(network_type, model_type='network', **network_params)
    else:
        network = Trainer.load_network_configuration(configuration_name=configuration_name, folder=configuration_folder)

    # Input pre-processing

    input_info = {}
    for key in datasets_info:
        df_name = datasets_info[key]['df_name']
        input_info[df_name] = {target: data_info[key]['df_column_data'][target]
                               for target in datasets_info[key]['targets']}

    vocab_size = {}
    vocab_sizes = {}
    for key in input_info:
        for target in input_info[key]:
            current_vocab_size = BaseNetwork.get_vocab_size(input_info[key]['df_column_data'][target])
            if target not in vocab_sizes:
                vocab_sizes[target] = [current_vocab_size]
            else:
                vocab_sizes[target].append(current_vocab_size)

    for target in vocab_sizes:
        vocab_size[target] = max(vocab_sizes[target])

    build_info, train_info = network.preprocess_input(input_info=input_info['train'], w2v_model=utils.embeddings_model,
                                                      add_stance=add_stance, stance_labels=stance_labels,
                                                      stance_vector=stance_vector, vocab_size=vocab_size)

    print('Build info:\n {}'.format(build_info))
    print('Train info:\n {}'.format(train_info))

    updated_build_info, test_info = network.preprocess_input(input_info=input_info['test'], w2v_model=utils.embeddings_model,
                                                             vocab_size=vocab_size,
                                                             padding_max_length=build_info['input_length'],
                                                             embedding_matrix=build_info['weights'])

    build_info['weights'] = updated_build_info['weights']

    # Releasing memory
    utils.embeddings_model = None

    ################################
    # Output section:
    ################################

    translated_target = [classification_labels[val] for val in data_info['train']['df_target']]
    target_tran = np.array(translated_target)
    target_tran = target_tran.reshape((len(data_info['train']['df_target']), 1))
    print(target_tran.shape)

    weights = class_weight.compute_class_weight('balanced', np.unique(target_tran), target_tran)
    weights_dict = dict(enumerate(weights))
    print("weight classes: {}".format(np.unique(target_tran)))
    print(weights_dict)

    model = network.build_model(build_info, network_verbose)

    model.fit(x=train_info.values(), y=target_tran, batch_size=batch_size, epochs=epochs, verbose=verbose,
              class_weight=weights_dict, shuffle=True)

    predictions = model.predict_classes(x=test_info.values(), batch_size=batch_size, verbose=verbose)

    # Scoring

    # Retrieving true and predicted values for evaluation

    target_column = datasets_info['test']['target_column']
    data_info['test']['df_data'][target_column] = predictions

    grouped_test_data = data_info['test']['df_data'].groupby('Post couple')
    post_dict = {}

    print("Total post couples: ", len(grouped_test_data))
    for key, item in grouped_test_data:
        post_dict[key] = {}
        if len(item.loc[item[target_column] == 'link']) > 0:
            post_dict[key]['predicted'] = 'link'
        else:
            post_dict[key]['predicted'] = 'no-link'

        if item['Rebuttal'].values[0] == 'support':
            post_dict[key]['true'] = 'link'
        else:
            post_dict[key]['true'] = 'no-link'

    y_pred = [post_dict[key]['predicted'] for key in post_dict]
    y_true = [post_dict[key]['true'] for key in post_dict]

    precision = precision_score(y_true=y_true, y_pred=y_pred, pos_label='link')
    recall = recall_score(y_true=y_true, y_pred=y_pred, pos_label='link')
    accuracy = accuracy_score(y_true=y_true, y_pred=y_pred)
    f1score = f1_score(y_true=y_true, y_pred=y_pred, pos_label='link', average='binary')

    # Saving results

    # Results info

    key = str((datasets_info['train']['df_name'], datasets_info['test']['df_name']))
    to_save = OrderedDict({
        key: {
            'precision': precision,
            'recall': recall,
            'accuracy': accuracy,
            'f1score': f1score
        }
    })

    # Test info

    to_save[key]['general_test_info'] = {
        'dataset_info': datasets_info,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'classification_labels': classification_labels,
        'add_stance_mode': add_stance_mode
    }

    to_save[key]['specific_test_info'] = {
        'network_type': network_type,
        'network_params': network_params,
        'network_verbose': network_verbose,
        'batch_size': batch_size,
        'epochs': epochs,
        'verbose': verbose,
        'shuffle': shuffle
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'rebuttal', path, append=True)
