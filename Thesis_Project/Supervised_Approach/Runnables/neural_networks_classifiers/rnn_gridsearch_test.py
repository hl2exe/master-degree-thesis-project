"""

:author: Federico Ruggeri
:date: 22nd March 2018
:description: simple runnable script that allows the user to calibrate
a given classifier (manually defined or loaded) in order to find the most
suitable parameters for the given task.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (neural network model) is built via the custom
 framework APIs. Moreover, its calibration parameters are retrieved in order
  to define the final step of the script, i.e. the grid-search.

3) Output section: a grid-search process is held and eventually the achieved
 results are stored permanently.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score
from sklearn.utils import class_weight

from Supervised_Approach.classifier_factory import ClassifierFactory
from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.gridsearch_utils import GridSearchUtils

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Dataset

    df_filename = "url-versions-2015-06-14-clean-train.csv"
    df_name = 'emergent-train-set'
    targets = ['claimHeadline', 'articleHeadline']
    df_section = "train"
    target_column = 'articleHeadlineStance'

    # Stance addition

    # Values: 'y' or 'n'
    add_stance_mode = 'n'
    stance_columns = ['Claim stance', 'Evidence stance']
    stance_labels = {'for', 'against', 'observing'}

    add_stance = False
    stance_vector = None

    # Word2vec

    embeddings_mode = 1
    word_embeddings_mode = 'word2vec'
    # pickle_file = df_name.replace('.csv', '.pickle')
    pickle_file = "emergent-train-set.pickle"

    # Network

    configuration_mode = 'manual'

    # Auto configuration

    auto_configuration_name = ''

    # Manual configuration

    network_type = 'wordembeddingscnnsequential'
    network_params = {
        'num_filters': 32,
        'predictions_dim': 3,
        'loss': 'categorical_crossentropy',
        'metrics': ['accuracy'],
        'add_dropout': False,
        'dropout': 0.,
        'padding_mode': 'average',
        'trainable': False
    }
    network_verbose = 0

    # Grid-search

    gridsearch_params = None

    classification_labels = OrderedDict({
        'for': 1,
        'against': 0,
        'observing': 2
    })

    n_splits = 2
    batch_size = 64
    epochs = 15
    verbose = 2

    # CV

    cv_mode = "prebuilt_cv"
    # Note: required for 'prebuilt' cv_mode only
    prebuilt_cv_key = "emergent"

    if cv_mode == 'default':
        cv_data = {'n_splits': n_splits, 'shuffle': True}
    else:
        cv_data = {}

    # Saving

    configuration_name = 'cnn_we_emergent_fixed'
    configuration_folder = None

    ################################
    # System section
    ################################

    # Dataset

    path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
    train_data = pd.read_csv(path_to_df, encoding="utf-8")
    train_target = train_data[target_column].values

    column_data = [train_data[target].values for target in targets]

    # Stance

    if add_stance_mode.lower() == 'y':
        stances_data = [train_data[stance_column] for stance_column in stance_columns]

        stance_vector = {}
        for stance_data, key in zip(stances_data, targets):
            stance_vector[key] = [idx for val in stance_data for idx, label in enumerate(stance_labels)
                                  if val == label]

        add_stance = True

    # Word2Vec

    utils = FeatureExtractorUtils()

    if embeddings_mode == 2:
        utils.load_embeddings_model(mode=word_embeddings_mode)
        load_pickle = False
    else:
        load_pickle = True

    if configuration_mode == 'manual':
        network = ClassifierFactory.factory(network_type, model_type='network', **network_params)
    else:
        network = Trainer.load_network_configuration(configuration_name=auto_configuration_name,
                                                     folder=configuration_folder)

    ################################
    # Output section:
    ################################

    input_info = {target: target_data for target, target_data in zip(targets, column_data)}

    translated_target = [classification_labels[val] for val in train_target]
    target_tran = np.array(translated_target)
    target_tran = target_tran.reshape((len(train_target), 1))

    cv = Trainer.retrieve_cv(cv_mode, prebuilt_cv_key=prebuilt_cv_key, **cv_data)
    n_splits = cv.n_splits

    # Grid-search

    # Memo: possible values
    # 'params': requires: network_type, [gridsearch_params]
    args_dict = {
        'params': {'network_type': network_type},
    }
    parameters = GridSearchUtils.get_network_gridsearch_parameters(args_dict)
    print('Considering the following parameters:\n', parameters)

    params_combinations = GridSearchUtils.get_network_params_combinations(parameters)

    print("""Starting grid-search..
    Total parameters to test: {}
    Total fits: {}""".format(len(parameters), len(params_combinations) * n_splits))

    scores = {}

    # Memorizing best combination!
    best_comb = {'comb': None, 'score': None}

    for param_comb in params_combinations:
        print("Considering: {}".format(param_comb))

        # Network

        for key, value in param_comb.items():
            setattr(network, key, value)

        # Input pre-processing (very expensive if there's padding mode in grid-search)
        build_info, train_info = network.preprocess_input(input_info=input_info, w2v_model=utils.embeddings_model,
                                                          pickle_file=pickle_file, load_pickle=load_pickle,
                                                          add_stance=add_stance, stance_labels=stance_labels,
                                                          stance_vector=stance_vector)

        splitted_indices = cv.split(train_data, target_tran)

        param_comb_key = ', '.join(['{}: {}'.format(key, value) for key, value in param_comb.items()])
        scores[param_comb_key] = {}

        for i, (train, test) in enumerate(splitted_indices):
            print "Running Fold", i + 1, "/", n_splits
            model = None
            model = network.build_model(build_info, network_verbose)

            for key in train_info:
                print("{} fold shapes:\n"
                      "Train: {}\nTest: {}".format(key, train_info[key][train].shape,
                                                   train_info[key][test].shape))

            X_train, y_train, X_test, y_test = network.transform_for_classification(train_info=train_info,
                                                                                    train_target=target_tran,
                                                                                    train_indexes=train,
                                                                                    test_indexes=test)

            labels = np.unique(translated_target)
            weights = class_weight.compute_class_weight('balanced', labels, np.reshape(y_train, (y_train.shape[0],)))
            weights_dict = {label: weight for (label, weight) in zip(labels, weights)}
            print("weight classes: {}".format(labels))
            print(weights_dict)

            if network.predictions_dim > 1:
                y_train_reshaped = network.reshape_true_values_for_multiclass(y_train)
            else:
                y_train_reshaped = y_train

            model.fit(x=X_train, y=y_train_reshaped, batch_size=batch_size, epochs=epochs, verbose=verbose,
                      class_weight=weights_dict)
            # predictions = model.predict_classes(x=X_test, batch_size=batch_size, verbose=verbose)
            predictions = network.predict(model, X_test, batch_size=batch_size, verbose=verbose)
            f1 = f1_score(y_true=target_tran[test], y_pred=predictions, labels=classification_labels.values(),
                          average="weighted")
            acc = accuracy_score(y_true=target_tran[test], y_pred=predictions)
            print("Fold Scores:\nF1-score: {}\nAccuracy: {}".format(f1, acc))
            scores[param_comb_key][i] = [f1, acc]

        scores[param_comb_key]['average'] = np.mean(scores[param_comb_key].values(), axis=0)

        # TODO: best network on which metric?
        # Memorizing best network
        if best_comb['comb'] is None or best_comb['score'] < scores[param_comb_key]['average'][0]:
            best_comb['comb'] = param_comb
            best_comb['score'] = scores[param_comb_key]['average'][0]

    # Saving

    to_save = OrderedDict({df_name: {
        'scores': scores
    }})

    # Test info

    to_save[df_name]['general_test_info'] = {
        'df_filename':           df_filename,
        'df_name':               df_name,
        'targets':               targets,
        'df_section':            df_section,
        'target_column':         target_column,
        'configuration_mode':    configuration_mode,
        'configuration_name':    configuration_name,
        'classification_labels': classification_labels,
        'w2v_mode':              embeddings_mode,
        'add_stance_mode':       add_stance_mode
    }

    to_save[df_name]['specific_test_info'] = {
        'network_type': network_type,
        'network_params': network_params,
        'network_verbose': network_verbose,
        'n_splits': n_splits,
        'batch_size': batch_size,
        'epochs': epochs,
        'verbose': verbose,
        'cv_mode': cv_mode,
        'prebuilt_cv_key': prebuilt_cv_key,
        'gridsearch_args': args_dict,
        'best_combination': best_comb
    }

    # Best network achieved
    print("Best network achieved the following score: {}".format(best_comb['score']))
    for key, value in best_comb['comb'].items():
        setattr(network, key, value)

    Trainer.save_network_configuration(network=network, configuration_name=configuration_name,
                                       folder=configuration_folder)

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'gridsearch', path)
