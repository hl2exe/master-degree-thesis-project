"""

:author: Federico Ruggeri
:date: 20th March 2018
:description: simple runnable script that allows the user to evaluate a given
recurrent neural network model (manually defined or loaded) on a given data-set.

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (neural network model) is built via the custom framework APIs.

3) Output section: a cross-validation process is held and eventually the achieved results are stored permanently.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score
from sklearn.utils import class_weight

from Supervised_Approach.classifier_factory import ClassifierFactory
from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Dataset

    df_filename = "url-versions-2015-06-14-clean-train.csv"
    df_name = 'emergent-train-set'
    targets = ['claimHeadline', 'articleHeadline']
    df_section = "train"
    target_column = 'articleHeadlineStance'

    # Stance addition

    # Values: 'y' or 'n'
    add_stance_mode = 'n'
    stance_columns = ['Claim stance', 'Evidence stance']
    stance_labels = {'for', 'against', 'observing'}

    add_stance = False
    stance_vector = None

    # Word2vec

    w2v_mode = 1
    pickle_file = df_name.replace('.csv', '.pickle')

    # Network

    configuration_mode = 'manual'

    # Auto configuration

    configuration_name = 'rnn_emergent'
    configuration_folder = None

    # Manual configuration

    network_type = 'wordembeddinglstmsequential'
    network_params = {
        'lstm_neurons': 100,
        'predictions_dim': 3,
        'loss': 'categorical_crossentropy',
        'metrics': ['accuracy'],
        'add_dropout': False,
        'dropout': 0.,
        'recurrent_dropout': 0.,
        'padding_mode': 'average'
    }
    network_verbose = 0

    # Cross-validation

    classification_labels = OrderedDict({
        'for': 1,
        'against': 0,
        'observing': 2
    })

    n_splits = 3
    batch_size = 64
    epochs = 5
    verbose = 0

    # CV

    cv_mode = "default"
    # Note: required for 'prebuilt' cv_mode only
    prebuilt_cv_key = "emergent"

    if cv_mode == 'default':
        cv_data = {'n_splits': n_splits}
    else:
        cv_data = {}

    ################################
    # System section
    ################################

    # Dataset

    path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
    train_data = pd.read_csv(path_to_df, encoding="utf-8")
    train_target = train_data[target_column].values

    column_data = [train_data[target] for target in targets]

    # Stance

    if add_stance_mode.lower() == 'y':
        stances_data = [train_data[stance_column] for stance_column in stance_columns]

        stance_vector = {}
        for stance_data, key in zip(stances_data, targets):
            stance_vector[key] = [idx for val in stance_data for idx, label in enumerate(stance_labels)
                                  if val == label]

        add_stance = True

    # Word2Vec

    utils = FeatureExtractorUtils()

    if w2v_mode == 2:
        utils.load_w2v_model()
        load_pickle = False
    else:
        load_pickle = True

    # Network

    if configuration_mode == 'manual':
        network = ClassifierFactory.factory(network_type, model_type='network', **network_params)
    else:
        network = Trainer.load_network_configuration(configuration_name=configuration_name, folder=configuration_folder)

    # Input pre-processing

    input_info = {target: target_data for target, target_data in zip(targets, column_data)}
    build_info, train_info = network.preprocess_input(input_info=input_info, w2v_model=utils.embeddings_model,
                                                      pickle_file=pickle_file, load_pickle=load_pickle,
                                                      add_stance=add_stance, stance_labels=stance_labels,
                                                      stance_vector=stance_vector)

    print('Build info:\n {}'.format(build_info))
    print('Train info:\n {}'.format(train_info))

    # Releasing memory
    utils.embeddings_model = None

    ################################
    # Output section:
    ################################

    translated_target = [classification_labels[val] for val in train_target]
    target_tran = np.array(translated_target)
    target_tran = target_tran.reshape((len(train_target), 1))
    print(target_tran.shape)


    # CV

    cv = Trainer.retrieve_cv(cv_mode, prebuilt_cv_key=prebuilt_cv_key, **cv_data)

    # Cross validation
    splitted_indices = cv.split(train_info[targets[0]], target_tran)
    scores = []

    for i, (train, test) in enumerate(splitted_indices):
        print "Running Fold", i+1, "/", n_splits
        model = None
        model = network.build_model(build_info, network_verbose)

        # for key in train_info:
        #     print("Fold shapes:\n"
        #           "Train: {}\nTest: {}".format(train_info[key][train].shape,
        #                                        train_info[key][test].shape))

        X_train, y_train, X_test, y_test = network.transform_for_classification(train_info=train_info,
                                                                                train_target=target_tran,
                                                                                train_indexes=train,
                                                                                test_indexes=test)
        if network.is_multiclass:
            y_train_reshaped = network.reshape_true_values_for_multiclass(y_train)
        else:
            y_train_reshaped = y_train

        labels = np.unique(y_train)
        weights = class_weight.compute_class_weight('balanced', labels, y_train)
        weights_dict = {label: weight for (label, weight) in zip(labels, weights)}
        print("weight classes: {}".format(labels))
        print(weights_dict)

        model.fit(x=X_train, y=y_train_reshaped, batch_size=batch_size, epochs=epochs, verbose=verbose,
                  class_weight=weights_dict)
        predictions = network.predict(model, X_test, batch_size=batch_size, verbose=verbose)
        f1 = f1_score(y_true=target_tran[test], y_pred=predictions, labels=classification_labels.values(),
                      average='weighted')
        acc = accuracy_score(y_true=target_tran[test], y_pred=predictions)
        print("Fold Scores:\nF1-score: {}\nAccuracy: {}".format(f1, acc))
        scores.append([f1, acc])

    # Saving results

    average_scores = np.mean(scores, axis=0)

    to_save = OrderedDict({df_name: {
        'scores': scores,
        'average_scores': average_scores
    }})

    # Test info

    to_save[df_name]['general_test_info'] = {
        'df_filename': df_filename,
        'df_name': df_name,
        'targets': targets,
        'df_section': df_section,
        'target_column': target_column,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'classification_labels': classification_labels,
        'w2v_mode': w2v_mode,
        'add_stance_mode': add_stance_mode
    }

    to_save[df_name]['specific_test_info'] = {
        'network_type': network_type,
        'network_params': network_params,
        'network_verbose': network_verbose,
        'n_splits': n_splits,
        'batch_size': batch_size,
        'epochs': epochs,
        'verbose': verbose,
        'cv_mode': cv_mode,
        'prebuilt_cv_key': prebuilt_cv_key
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'cross_validation', path, append=True)
