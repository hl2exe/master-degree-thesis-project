"""

:author: Federico Ruggeri
:date: 11th March 2018
:description: classification test in which a trained classifier predicts stance towards a given target of
a given input data-set.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (pipeline model) is built via the custom framework APIs.

3) Output section: the trained classifier is asked to make predictions on given test set. Lastly, obtained results
are saved on file.


"""

import os
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
import pandas as pd

from Supervised_Approach.collector import Collector
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.pipeline_builder import PipelineBuilder
from collections import OrderedDict
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folder = 'Emergent'
features_names = [
    'cosine_similarity',
    'basic',
    'repeated_punctuation',
    'POS_generalized',
    'syntactic',
    'sngrams',
    'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = ['ppdb']
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    datasets_info = {
        'train': {
            'df_filename': "url-versions-2015-06-14-clean-train.csv",
            'df_section': 'train',
            'df_name': 'emergent-train-set',
            'targets': ['claimHeadline', 'articleHeadline']
        },
        'test': {
            'df_filename': "ce_acl_14_joined-ids.csv",
            'df_section': 'train',
            'df_name': 'CE-ACL-14',
            'targets': ['Claim', 'Topic']
        }
    }
    target_column = 'articleHeadlineStance'

    # Classifier (pipeline)

    configuration_mode = 'auto'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'single_ngrams',
        'single_skipgrams'
    }
    features_targets_info = {
        'single_ngrams': {1},
        'single_skipgrams': {2}
    }

    num_targets = len(datasets_info['train']['targets'])
    additional_args = None
    classifier_type = 'sgdclassifier'
    classifier_info = {'class_weight': 'balanced'}

    # Auto configuration

    configuration_name = 'best_emergent_divided_calibrated'
    configuration_folder = None

    # Prediction

    to_save_filename = 'ce_acl_14_claim_topic_predictions.csv'

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = \
        Trainer.get_associations_mapping_from_dict(datasets_info)

    path_to_train_df = DatasetUtils.get_path_to_dataset(datasets_info['train']['df_filename'],
                                                        datasets_info['train']['df_section'])
    train_data = pd.read_csv(path_to_train_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[datasets_info['train']['df_name']])
    train_target = train_data[target_column].values

    path_to_test_df = DatasetUtils.get_path_to_dataset(datasets_info['test']['df_filename'],
                                                       datasets_info['test']['df_section'])
    test_data = pd.read_csv(path_to_test_df, encoding='utf-8')
    test_data = test_data.rename(columns=columns_mapping[datasets_info['test']['df_name']])

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    pipeline = PipelineBuilder.build(features, args, classifier)

    print('System section successfully completed')

    ################################
    # Output section
    ################################

    # Prediction phase

    pipeline.fit(train_data, train_target)
    predictions = pipeline.predict(test_data)

    # Saving results

    columns = associations_mapping[datasets_info['test']['df_name']].values()

    key = str((datasets_info['train']['df_name'], datasets_info['test']['df_name']))
    stance_column = datasets_info['test']['targets'][0]

    prediction_column = Collector.save_prediction_results(predictions, columns, test_data,
                                                          inverted_columns_mapping,
                                                          stance_column, datasets_info['test']['df_name'],
                                                          to_save_filename)

    # Results info

    to_save = OrderedDict({
        key: {
            'to_save_filename': to_save_filename,
            'prediction_column': prediction_column}
    })

    # Test info

    to_save[key]['general_test_info'] = {
        'datasets_info': datasets_info,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'predictions', path, append=True)
