"""

:author: Federico Ruggeri
:date: 14th March 2018
:description: rebuttal validation test based on argument structure prediction activity.
First, ASP labels are predicted on test set.
Second, predictions are used in order to define ASP at post level, i.e. ASP between
adjacent post in a given discussion thread.
Third, post level predictions are compared with true values and metrics like precision,
recall, f1-score and accuracy are computed.

"""

import os
from collections import OrderedDict

import pandas as pd
from sklearn.metrics import precision_score, recall_score, f1_score, accuracy_score

from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.pipeline_builder import PipelineBuilder
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folder = 'Emergent'
features_names = [
    'cosine_similarity',
    'basic',
    'repeated_punctuation',
    'POS_generalized',
    'syntactic',
    'sngrams',
    'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = ['ppdb']
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    datasets_info = {
        'train': {
            'df_filename': "ce_emnlp_15_ASP_train_and_test_training.csv",
            'df_section': 'post_process',
            'df_name': 'CE-EMNLP-15-TT',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
        'test': {
            'df_filename': "MARGOT_ASP_rebuttal_training_balanced.csv",
            'df_section': 'post_process',
            'df_name': 'MARGOT_ASP_rebuttal',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
    }

    # Classifier (pipeline)

    configuration_mode = 'manual'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'single_ngrams'
    }
    features_targets_info = {
        'single_ngrams': {1, 2}
    }

    num_targets = len(datasets_info['train']['targets'])
    additional_args = None
    classifier_type = 'sgdclassifier'
    classifier_info = {'class_weight': 'balanced'}

    # Auto configuration

    configuration_name = 'test_config_rebuttal'
    configuration_folder = None

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = \
        Trainer.get_associations_mapping_from_dict(datasets_info)

    path_to_train_df = DatasetUtils.get_path_to_dataset(datasets_info['train']['df_filename'],
                                                        datasets_info['train']['df_section'])
    train_data = pd.read_csv(path_to_train_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[datasets_info['train']['df_name']])
    train_target = train_data[datasets_info['train']['target_column']].values

    path_to_test_df = DatasetUtils.get_path_to_dataset(datasets_info['test']['df_filename'],
                                                       datasets_info['test']['df_section'])
    test_data = pd.read_csv(path_to_test_df, encoding='utf-8')
    test_data = test_data.rename(columns=columns_mapping[datasets_info['test']['df_name']])

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    pipeline = PipelineBuilder.build(features, args, classifier)

    print('System section successfully completed')

    ################################
    # Output section
    ################################

    # predictions

    pipeline.fit(train_data, train_target)
    predictions = pipeline.predict(test_data)

    # Retrieving true and predicted values for evaluation

    test_data['Argument Relation'] = predictions

    grouped_test_data = test_data.groupby('Post couple')
    post_dict = {}

    print("Total post couples: ", len(grouped_test_data))
    for key, item in grouped_test_data:
        post_dict[key] = {}
        if len(item.loc[item['Argument Relation'] == 'link']) > 0:
            post_dict[key]['predicted'] = 'link'
        else:
            post_dict[key]['predicted'] = 'no-link'

        if item['Rebuttal'].values[0] == 'support':
            post_dict[key]['true'] = 'link'
        else:
            post_dict[key]['true'] = 'no-link'

    y_pred = [post_dict[key]['predicted'] for key in post_dict]
    y_true = [post_dict[key]['true'] for key in post_dict]

    precision = precision_score(y_true=y_true, y_pred=y_pred, pos_label='link')
    recall = recall_score(y_true=y_true, y_pred=y_pred, pos_label='link')
    accuracy = accuracy_score(y_true=y_true, y_pred=y_pred)
    f1score = f1_score(y_true=y_true, y_pred=y_pred, pos_label='link', average='binary')

    # Results info

    key = str((datasets_info['train']['df_name'], datasets_info['test']['df_name']))
    to_save = OrderedDict({
        key: {
            'precision': precision,
            'recall': recall,
            'accuracy': accuracy,
            'f1score': f1score
        }
    })

    # Test info

    to_save[key]['general_test_info'] = {
        'datasets_info': datasets_info,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'rebuttal', path, append=True)
