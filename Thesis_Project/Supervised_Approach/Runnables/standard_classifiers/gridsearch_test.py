"""

:author: Federico Ruggeri
:date: 10th March 2018
:description: simple runnable script that allows the user to calibrate a given classifier (manually defined or loaded)
in order to find the most suitable parameters for the given task.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (pipeline model) is built via the custom framework APIs. Moreover, its calibration
parameters are retrieved in order to define the final step of the script, i.e. the grid-search.

3) Output section: a grid-search process is held and eventually the achieved results are stored permanently.

"""

import os
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
import pandas as pd
from sklearn.metrics import make_scorer, f1_score
from sklearn.model_selection import GridSearchCV

from Supervised_Approach.collector import Collector
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.pipeline_builder import PipelineBuilder
from Supervised_Approach.Utils.gridsearch_utils import GridSearchUtils
from collections import OrderedDict
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folder = 'Emergent'
features_names = [
    'cosine_similarity',
    'basic',
    'repeated_punctuation',
    'POS_generalized',
    'syntactic',
    'sngrams',
    'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = [
    'ppdb'
]
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    df_filename = "url-versions-2015-06-14-clean-train.csv"
    df_name = 'emergent-train-set'
    targets = ['claimHeadline', 'articleHeadline']
    df_section = "train"
    target_column = 'articleHeadlineStance'

    # Classifier (pipeline)

    configuration_mode = 'manual'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'single_ngrams',
        'single_skipgrams',
        'cosine_similarity',
        'basic',
        'repeated_punctuation',
        'POS_generalized',
        'syntactic',
        'sngrams',
        'svo_triples'
    }
    features_targets_info = {
        'single_ngrams': {2},
        'single_skipgrams': {2},
        'basic': {2},
        'repeated_punctuation': {2},
        'POS_generalized': {2},
        'syntactic': {2},
        'sngrams': {2}
    }

    num_targets = len(targets)
    additional_args = None
    classifier_type = 'multinomialnb'
    classifier_info = {}

    # Auto configuration

    auto_configuration_name = 'emergent_divided_multinomialnb_2'

    # Saving

    configuration_name = 'emergent_divided_multinomialnb_2'
    configuration_folder = None

    # Grid-search

    gridsearch_features_set = {
        'single_ngrams',
        'single_skipgrams',
        # 'cosine_similarity',
        # 'basic',
        # 'repeated_punctuation',
    }
    gridsearch_features_targets_info = {
        'single_ngrams': {2},
        'single_skipgrams': {2},
        # 'basic': {1, 2},
        # 'repeated_punctuation': {1, 2}
    }

    # Note: ignored if average != binary
    pos_label = ''
    average = 'weighted'
    scoring = {'Accuracy': 'accuracy',
               'F1-score': make_scorer(f1_score, pos_label=pos_label, average=average)}
    refit = "Accuracy"
    n_jobs = 1
    verbose = 5

    # CV

    cv_mode = "prebuilt_cv"
    # Note: required for 'prebuilt' cv_mode only
    prebuilt_cv_key = "emergent"

    if cv_mode == 'default':
        cv_data = {'n_splits': 3}
    else:
        cv_data = {}

    # Preliminary controls

    if configuration_mode == 'manual' and not GridSearchUtils.verify_gridsearch_info(gridsearch_features_set,
                                                                                     features_set,
                                                                                     gridsearch_features_targets_info,
                                                                                     features_targets_info):
        exit(-1)

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = Trainer.get_associations_mapping(df_name, targets)

    path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
    train_data = pd.read_csv(path_to_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[df_name])
    train_target = train_data[target_column].values

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(auto_configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    pipeline = PipelineBuilder.build(features, args, classifier)

    # Parameters

    gridsearch_features, _, _ = Trainer.build_pipeline_info(gridsearch_features_set, gridsearch_features_targets_info,
                                                            associations_mapping, num_targets)

    # Memo: possible values
    # 'features': requires: features
    # 'classifier_algorithm': requires: classifier
    # 'classifier_types': requires: pipeline, [classifiers], [classifiers_info] (one entry for each classifier)
    # 'feature_weights': requires: features
    args_dict = {
        'features': {'features': gridsearch_features},
        # 'classifier_types': {'pipeline': pipeline, 'classifiers': None}
        # 'feature_weights': {'features': gridsearch_features}
        # 'classifier_algorithm': {'classifier': classifier}
    }
    parameters = GridSearchUtils.get_gridsearch_parameters(args_dict)
    print('Considering the following parameters:\n', parameters)

    print('System section successfully completed')

    ################################
    # Output section:
    ################################

    cv = Trainer.retrieve_cv(cv_mode, prebuilt_cv_key=prebuilt_cv_key, **cv_data)

    # Grid-search

    gridsearch = GridSearchCV(pipeline, parameters, cv=cv, n_jobs=n_jobs, verbose=verbose, scoring=scoring,
                              refit=refit, return_train_score=True)
    gridsearch.fit(train_data, train_target)

    # Saving results

    gridsearch_info = Collector.retrieve_gridsearch_info(gridsearch, parameters)

    # In case of feature weights calibration
    weighted_features = None
    if 'featureunion__transformer_weights' in gridsearch_info['best_parameters']:
        weighted_features = [feature_name for feature_name in
                             gridsearch_info['best_parameters']['featureunion__transformer_weights']
                             if
                             gridsearch_info['best_parameters']['featureunion__transformer_weights'][feature_name] == 1]

    Trainer.save_pipeline_configuration(gridsearch.best_estimator_, configuration_name, num_targets,
                                        configuration_folder, weighted_features=weighted_features)

    cv_results_filename = Collector.save_gridsearch_cv_results(gridsearch.cv_results_, df_name,
                                                               configuration_name)

    # Results info

    to_save = OrderedDict({df_name: gridsearch_info})
    to_save[df_name]['csv_results_file'] = cv_results_filename

    # Test info

    to_save[df_name]['general_test_info'] = {
        'df_filename': df_filename,
        'df_name': df_name,
        'targets': targets,
        'df_section': df_section,
        'target_column': target_column,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'auto_configuration_name': auto_configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    to_save[df_name]['specific_test_info'] = {
        'gridsearch_feature_set': gridsearch_features_set,
        'gridsearch_features_targets_info': gridsearch_features_targets_info,
        'scoring': {key: scoring[key] for key in scoring},
        'refit': refit,
        'n_jobs': n_jobs,
        'verbose': verbose,
        'cv_mode': cv_mode,
        'prebuilt_cv_key': prebuilt_cv_key
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'gridsearch', path)
