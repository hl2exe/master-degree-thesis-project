"""

:author: Federico Ruggeri
:date: 14th March 2018
:description: Leave One Topic Out (LOTO) evaluation test on test data.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (pipeline model) is built via the custom framework APIs.

3) Output section: a LOTO test is held in which accuracy and f1-score metrics are computed.

"""

import os
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, f1_score, precision_recall_fscore_support
from sklearn.model_selection import LeaveOneGroupOut

from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.collector import Collector
from Supervised_Approach.pipeline_builder import PipelineBuilder
from collections import OrderedDict
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folders = ['Argument Structure Prediction', 'margot_ASP']
features_names = [
    'cosine_similarity',
    # 'basic',
    # 'repeated_punctuation',
    # 'POS_generalized',
    # 'syntactic',
    # 'sngrams',
    # 'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folders)

utilities_names = [
    # 'ppdb'
]
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    datasets_info = {
        'train': {
            'df_filename': "MARGOT_ASP_default_training_balanced.csv",
            'df_section': 'post_process',
            'df_name': 'MARGOT-ASP',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
        'test': {
            'df_filename': "ce_emnlp_15_ASP_train_and_test_training.csv",
            'df_section': 'post_process',
            'df_name': 'CE-EMNLP-15-TT',
            'targets': ['Claim', 'Evidence'],
            'target_column': 'Argument relation'
        },
    }

    # Classifier (pipeline)

    configuration_mode = 'manual'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'cosine_similarity',
    }
    features_targets_info = {
        # 'single_ngrams': {1, 2}
    }

    num_targets = len(datasets_info['train']['targets'])
    additional_args = None
    classifier_type = 'sgdclassifier'
    classifier_info = {'class_weight': 'balanced'}

    # Auto configuration

    configuration_name = 'test_config_loto'
    configuration_folder = None

    # Leave One Group Out

    group_column = 'Topic'
    pos_label = 'link'

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = \
        Trainer.get_associations_mapping_from_dict(datasets_info)

    path_to_train_df = DatasetUtils.get_path_to_dataset(datasets_info['train']['df_filename'],
                                                        datasets_info['train']['df_section'])
    train_data = pd.read_csv(path_to_train_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[datasets_info['train']['df_name']])
    train_target = train_data[datasets_info['train']['target_column']].values

    path_to_test_df = DatasetUtils.get_path_to_dataset(datasets_info['test']['df_filename'],
                                                       datasets_info['test']['df_section'])
    test_data = pd.read_csv(path_to_test_df, encoding='utf-8')
    test_data = test_data.rename(columns=columns_mapping[datasets_info['test']['df_name']])
    test_target = test_data[datasets_info['test']['target_column']].values

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    print('System section successfully completed')

    ################################
    # Output section
    ################################

    pipeline = PipelineBuilder.build(features, args, classifier)
    pipeline.fit(train_data, train_target)

    # LOTO test

    logo = LeaveOneGroupOut()
    topic_map = {topic: idx for idx, topic in enumerate(np.unique(test_data[group_column].values))}
    groups = [topic_map[topic] for topic in test_data[group_column].values]
    n_splits = logo.get_n_splits(X=test_data, y=test_target, groups=groups)

    target_names = list(set(test_target))
    scores = {}

    for idx, (_, test_index) in enumerate(logo.split(X=test_data, y=test_target, groups=groups)):
        current_group = np.unique(test_data.iloc[test_index][group_column].values)[0]
        print("Predicting on group ({}/{}): {}".format(idx, n_splits, current_group))

        X_test = test_data.iloc[test_index]
        y_test = test_target[test_index]

        predictions = pipeline.predict(X_test)

        f1 = f1_score(y_pred=predictions, y_true=y_test, pos_label='link')
        accuracy = accuracy_score(y_pred=predictions, y_true=y_test)

        predicted_series = pd.Series(predictions, name='Predicted')
        true_series = pd.Series(y_test, name='True')
        confusion_matrix = pd.crosstab(true_series, predicted_series)

        report = {}
        precision, recall, f1score, support = precision_recall_fscore_support(y_true=y_test, y_pred=predictions,
                                                                              labels=target_names, average=None)

        for header_name, value_array in zip(['precision', 'recall', 'f1score', 'support'],
                                            [precision, recall, f1score, support]):
            report[header_name] = {target_name: header_value for target_name, header_value
                                   in zip(target_names, value_array.tolist())}

        report['precision'].update({'average': float(np.average(precision, weights=support))})
        report['recall'].update({'average': float(np.average(precision, weights=support))})
        report['f1score'].update({'average': float(np.average(precision, weights=support))})
        report['support'].update({'total': int(np.sum(support))})

        scores[current_group] = {'f1score': f1,
                                 'accuracy': accuracy,
                                 'report': report,
                                 'confusion_matrix': confusion_matrix.to_dict()}

    # Saving results

    keys = list(scores.keys())
    accumulator_confusion_matrix = pd.DataFrame.from_dict(scores[keys[0]]['confusion_matrix'])
    for idx in range(1, len(keys)):
        accumulator_confusion_matrix = accumulator_confusion_matrix.add(
            pd.DataFrame.from_dict(scores[keys[idx]]['confusion_matrix']),
            fill_value=0)

    per_class_avg_precision = np.mean([scores[idx]['report']['precision'].values() for idx in scores], axis=0)
    per_class_avg_recall = np.mean([scores[idx]['report']['recall'].values() for idx in scores], axis=0)
    per_class_avg_f1 = np.mean([scores[idx]['report']['f1score'].values() for idx in scores], axis=0)
    per_class_support = np.sum([scores[idx]['report']['support'].values() for idx in scores], axis=0)

    average_report = {}
    for header_name, value_array in zip(['precision', 'recall', 'f1score', 'support'],
                                        [per_class_avg_precision, per_class_avg_recall,
                                         per_class_avg_f1, per_class_support]):
        average_report[header_name] = {target_name: header_value for target_name, header_value
                                       in zip(target_names, value_array.tolist())}

    average_report['precision'].update({'average': float(np.average(per_class_avg_precision,
                                                                    weights=per_class_support))})
    average_report['recall'].update({'average': float(np.average(per_class_avg_recall, weights=per_class_support))})
    average_report['f1score'].update({'average': float(np.average(per_class_avg_f1, weights=per_class_support))})
    average_report['support'].update({'total': int(np.sum(per_class_support))})

    scores['average'] = {
        'f1score': np.mean([scores[idx]['f1score'] for idx in scores]),
        'accuracy': np.mean([scores[idx]['accuracy'] for idx in scores]),
        'report': average_report,
        'confusion_matrix': accumulator_confusion_matrix.to_dict()
    }

    # Results info

    key = str((datasets_info['train']['df_name'], datasets_info['test']['df_name']))
    to_save = OrderedDict({key: {'topic_results': scores}})

    # Test info

    to_save[key]['general_test_info'] = {
        'datasets_info': datasets_info,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    to_save[key]['specific_test_info'] = {
        'group_column': group_column,
        'pos_label': pos_label
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'loto_scoring', path, append=True)
