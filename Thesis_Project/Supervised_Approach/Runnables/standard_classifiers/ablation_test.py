"""

:author: Federico Ruggeri
:date: 11th March 2018
:description: ablation test on train set with classifier evaluation on test set.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: features, args and the classifier algorithm are retrieved from the given configuration.

3) Output section: an ablation test is held in order to evaluate the classifier performance on the train set. At the
same time, the same classifier is evaluated on the test set.

"""

import os
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
import numpy as np
import pandas as pd
from sklearn.metrics import make_scorer, f1_score
from sklearn.model_selection import cross_validate
from collections import OrderedDict

from Supervised_Approach.collector import Collector
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.pipeline_builder import PipelineBuilder
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folder = 'Emergent'
features_names = [
    'cosine_similarity',
    'basic',
    'repeated_punctuation',
    'POS_generalized',
    'syntactic',
    'sngrams',
    'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = ['ppdb']
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    datasets_info = {
        'train': {
            'df_filename': "url-versions-2015-06-14-clean-train.csv",
            'df_section': 'train',
            'df_name': 'emergent-train-set',
            'targets': ['claimHeadline', 'articleHeadline']
        },
        'test': {
            'df_filename': "url-versions-2015-06-14-clean-test.csv",
            'df_section': 'test',
            'df_name': 'emergent-test-set',
            'targets': ['claimHeadline', 'articleHeadline']
        }
    }
    target_column = 'articleHeadlineStance'

    # Classifier (pipeline)

    configuration_mode = 'auto'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'single_ngrams',
        'single_skipgrams',
        'cosine_similarity',
        'basic',
        'repeated_punctuation',
        'POS_generalized',
        'syntactic',
        'sngrams',
        'svo_triples'
    }
    features_targets_info = {
        'single_ngrams': {1, 2},
        'single_skipgrams': {1, 2},
        'basic': {1, 2},
        'repeated_punctuation': {1, 2},
        'POS_generalized': {1, 2},
        'syntactic': {1, 2},
        'sngrams': {1, 2}
    }

    num_targets = len(datasets_info['train']['targets'])
    additional_args = None
    classifier_type = 'linearsvc'
    classifier_info = {'class_weight': 'balanced'}

    # Auto configuration

    configuration_name = 'best_emergent_divided'
    configuration_folder = None

    # Scoring

    # Note: ignored if average != binary
    pos_label = ''
    average = 'weighted'
    scoring = {'Accuracy': 'accuracy',
               'F1-score': make_scorer(f1_score, pos_label=pos_label, average=average)}

    n_jobs = 1
    verbose = 5

    # CV

    cv_mode = "prebuilt_cv"
    # Note: required for 'prebuilt' cv_mode only
    prebuilt_cv_key = "emergent"

    if cv_mode == 'default':
        cv_data = {'n_splits': 3}
    else:
        cv_data = {}

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = \
        Trainer.get_associations_mapping_from_dict(datasets_info)

    path_to_train_df = DatasetUtils.get_path_to_dataset(datasets_info['train']['df_filename'],
                                                        datasets_info['train']['df_section'])
    train_data = pd.read_csv(path_to_train_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[datasets_info['train']['df_name']])
    train_target = train_data[target_column].values

    path_to_test_df = DatasetUtils.get_path_to_dataset(datasets_info['test']['df_filename'],
                                                       datasets_info['test']['df_section'])
    test_data = pd.read_csv(path_to_test_df, encoding='utf-8')
    test_data = test_data.rename(columns=columns_mapping[datasets_info['test']['df_name']])
    test_target = test_data[target_column].values

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    print('System section successfully completed')

    ################################
    # Output section
    ################################

    cv = Trainer.retrieve_cv(cv_mode, prebuilt_cv_key=prebuilt_cv_key, **cv_data)

    # Ablation

    ablation_results = {}

    for to_exclude in features:
        print("######\nExcluding feature: {}\n######".format(to_exclude))
        current_features_set = {f_key: features[f_key] for f_key in features if f_key != to_exclude}
        current_args = {f_key: args[f_key] for f_key in args if f_key != to_exclude}
        pipeline = PipelineBuilder.build(current_features_set, current_args, classifier)

        # Cross Validation

        cv_scores = cross_validate(estimator=pipeline, X=train_data, y=train_target,
                                   cv=cv,
                                   verbose=verbose,
                                   n_jobs=n_jobs,
                                   scoring=scoring,
                                   return_train_score=True)

        # Evaluation on test set

        pipeline.fit(train_data, train_target)
        test_score = pipeline.score(test_data, test_target)

        ablation_results[to_exclude] = {'cross_validation': cv_scores,
                                        'test_set_evaluation': test_score}

    # Saving results

    for feature_name in ablation_results:
        added_scores = {key: value.tolist() for key, value in
                        ablation_results[feature_name]['cross_validation'].items()}
        for key in ablation_results[feature_name]['cross_validation']:
            added_scores['{}_average'.format(key)] = np.mean(ablation_results[feature_name]['cross_validation'][key])

        ablation_results[feature_name]['cross_validation'] = OrderedDict(sorted(added_scores.items()))

    # Results info

    key = str((datasets_info['train']['df_name'], datasets_info['test']['df_name']))
    to_save = OrderedDict({key: ablation_results})

    # Test info

    to_save[key]['general_test_info'] = {
        'datasets_info': datasets_info,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    to_save[key]['specific_test_info'] = {
        'scoring': {key: scoring[key] for key in scoring},
        'n_jobs': n_jobs,
        'verbose': verbose,
        'cv_mode': cv_mode,
        'prebuilt_cv_key': prebuilt_cv_key
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'ablation', path, append=True)
