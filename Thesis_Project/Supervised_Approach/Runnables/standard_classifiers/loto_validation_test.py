"""

:author: Federico Ruggeri
:date: 12th March 2018
:description: Leave One Topic Out (LOTO) training test.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (pipeline model) is built via the custom framework APIs.

3) Output section: a LOTO test is held in which accuracy and f1-score metrics are computed.

"""

import os
from collections import OrderedDict

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import LeaveOneGroupOut

from Supervised_Approach.collector import Collector
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
from Supervised_Approach.pipeline_builder import PipelineBuilder
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folder = 'Argument Structure Prediction'
features_names = [
    # 'cosine_similarity',
    'basic',
    # 'repeated_punctuation',
    # 'POS_generalized',
    # 'syntactic',
    # 'sngrams',
    # 'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = [
    # 'ppdb'
]
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    df_filename = "ce_emnlp_15_ASP_train_and_test_training.csv"
    df_name = 'CE-EMNLP-15-TT'
    targets = ['Claim', 'Evidence']
    df_section = "post_process"
    target_column = 'Argument relation'

    # Classifier (pipeline)

    configuration_mode = 'manual'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'basic',
    }
    features_targets_info = {
        'basic': {1, 2}
    }

    num_targets = len(targets)
    additional_args = None
    classifier_type = 'sgdclassifier'
    classifier_info = {'class_weight': 'balanced'}

    # Auto configuration

    configuration_name = 'test_config_loto_2'
    configuration_folder = None

    # Leave One Group Out

    group_column = 'Topic'
    pos_label = 'link'

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = Trainer.get_associations_mapping(df_name, targets)

    path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
    train_data = pd.read_csv(path_to_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[df_name])
    train_target = train_data[target_column].values

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    print('System section successfully completed')

    ################################
    # Output section
    ################################

    # LOTO test

    logo = LeaveOneGroupOut()
    topic_map = {topic: idx for idx, topic in enumerate(np.unique(train_data[group_column].values))}
    groups = [topic_map[topic] for topic in train_data[group_column].values]
    n_splits = logo.get_n_splits(X=train_data, y=train_target, groups=groups)

    scores = {}

    for idx, (train_index, test_index) in enumerate(logo.split(X=train_data, y=train_target, groups=groups)):
        print "Running Fold", idx+1, "/", n_splits

        group_excluded = np.unique(train_data.iloc[test_index][group_column].values)[0]
        print("Group excluded: {}".format(group_excluded))

        X_train, X_test = train_data.iloc[train_index], train_data.iloc[test_index]
        y_train, y_test = train_target[train_index], train_target[test_index]

        pipeline = PipelineBuilder.build(features, args, classifier)
        pipeline.fit(X=X_train, y=y_train)
        predictions = pipeline.predict(X_test)

        f1 = f1_score(y_pred=predictions, y_true=y_test, pos_label=pos_label)
        accuracy = accuracy_score(y_pred=predictions, y_true=y_test)
        print("Fold scores:\n F1: {}\nAccuracy: {}".format(f1, accuracy))
        scores[group_excluded] = {'f1-score': f1, 'accuracy': accuracy}

    # Saving results

    # Results info

    to_save = OrderedDict({df_name: {'topic_results': scores}})

    # Test info

    to_save[df_name]['general_test_info'] = {
        'df_filename': df_filename,
        'df_name': df_name,
        'targets': targets,
        'df_section': df_section,
        'target_column': target_column,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    to_save[df_name]['specific_test_info'] = {
        'group_column': group_column,
        'pos_label': pos_label
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'loto_validation', path, append=True)
