"""

:author: Federico Ruggeri
:date: 10th March 2018
:description: simple runnable script that allows the user to evaluate a given classifier (manually defined or loaded)
on a given data-set.
In particular, it is possible to highlight three different steps:

1) Input section: the majority of the script variables are defined here.

2) System section: a classifier (pipeline model) is built via the custom framework APIs.

3) Output section: a cross-validation process is held and eventually the achieved results are stored permanently.

"""

import os
from Supervised_Approach.path_definitions import CLASSIFIER_CONFIGURATIONS_DIR
import pandas as pd
from sklearn.metrics import make_scorer, f1_score
from sklearn.model_selection import cross_validate
from collections import OrderedDict
import numpy as np

from Supervised_Approach.collector import Collector
from Supervised_Approach.trainer import Trainer
from Supervised_Approach.Utils.dataset_utils import DatasetUtils
from Supervised_Approach.pipeline_builder import PipelineBuilder
from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.Utils.feature_transformer_utils import FeatureTransformerUtils

# pickle pre-loading
folder = 'Emergent'
features_names = [
    'cosine_similarity',
    # 'basic',
    # 'repeated_punctuation',
    # 'POS_generalized',
    # 'syntactic',
    # 'sngrams',
    # 'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = [
    # 'ppdb'
]
FeatureTransformerUtils.load_pickle_data(utilities_names)

# Windows multiprocessing requires this, since every process
# runs the whole module.
if __name__ == '__main__':

    ################################
    # Input section
    ################################

    # Data-set

    df_filename = "url-versions-2015-06-14-clean-train.csv"
    df_name = 'emergent-train-set'
    targets = ['claimHeadline', 'articleHeadline']
    df_section = "train"
    target_column = 'articleHeadlineStance'

    # Classifier (pipeline)

    configuration_mode = 'manual'

    # Manual configuration

    # Note: general names (the ones indicated in the .json file)
    features_set = {
        'cosine_similarity'
    }
    features_targets_info = {
    }

    num_targets = len(targets)
    additional_args = None
    classifier_type = 'sgdclassifier'
    classifier_info = {'class_weight': 'balanced'}

    # Auto configuration

    configuration_name = 'test_config_cs'
    configuration_folder = None

    # Scoring

    # Note: ignored if average != binary
    pos_label = ''
    average = 'weighted'
    scoring = {'Accuracy': 'accuracy',
               'F1-score': make_scorer(f1_score, pos_label=pos_label, average=average)}

    n_jobs = 2
    verbose = 5

    # CV

    cv_mode = "prebuilt_cv"
    # Note: required for 'prebuilt' cv_mode only
    prebuilt_cv_key = "emergent"

    if cv_mode == 'default':
        cv_data = {'n_splits': 3}
    else:
        cv_data = {}

    print('Input section successfully completed!')

    ################################
    # System section
    ################################

    # Data-set

    # Note: requires all used data-sets (1 for gridsearch, 2 for predictions on other datasets -> dict structure)
    associations_mapping, columns_mapping, inverted_columns_mapping = Trainer.get_associations_mapping(df_name, targets)

    path_to_df = DatasetUtils.get_path_to_dataset(df_filename, df_section)
    train_data = pd.read_csv(path_to_df, encoding="utf-8")
    train_data = train_data.rename(columns=columns_mapping[df_name])
    train_target = train_data[target_column].values

    # Classifier (pipeline)

    # Manual configuration
    if configuration_mode == 'manual':
        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)
    # Auto configuration
    else:
        features_set, features_targets_info, additional_args, classifier_type, classifier_info = \
            Trainer.load_pipeline_configuration(configuration_name, configuration_folder)

        features, args, classifier = Trainer.build_pipeline_info(features_set, features_targets_info,
                                                                 associations_mapping,
                                                                 num_targets,
                                                                 additional_args=additional_args,
                                                                 classifier_type=classifier_type,
                                                                 classifier_info=classifier_info)

    pipeline = PipelineBuilder.build(features, args, classifier)

    print('System section successfully completed')

    ################################
    # Output section:
    ################################

    cv = Trainer.retrieve_cv(cv_mode, prebuilt_cv_key=prebuilt_cv_key, **cv_data)

    # Cross Validation

    scores = cross_validate(estimator=pipeline, X=train_data, y=train_target,
                            cv=cv,
                            verbose=verbose,
                            n_jobs=n_jobs,
                            scoring=scoring)

    # Saving results

    added_scores = {key: value.tolist() for key, value in scores.items()}
    for key in scores:
        added_scores['{}_average'.format(key)] = np.mean(scores[key])

    added_scores = OrderedDict(sorted(added_scores.items()))
    to_save = OrderedDict({df_name: added_scores})

    # Test info

    to_save[df_name]['general_test_info'] = {
        'df_filename': df_filename,
        'df_name': df_name,
        'targets': targets,
        'df_section': df_section,
        'target_column': target_column,
        'configuration_mode': configuration_mode,
        'configuration_name': configuration_name,
        'features_set': features_set,
        'features_targets_info': features_targets_info,
        'num_targets': num_targets,
        'additional_args': additional_args,
        'classifier_type': classifier_type,
        'classifier_info': classifier_info,
    }

    to_save[df_name]['specific_test_info'] = {
        'scoring': {key: scoring[key] for key in scoring},
        'n_jobs': n_jobs,
        'verbose': verbose,
        'cv_mode': cv_mode,
        'prebuilt_cv_key': prebuilt_cv_key
    }

    path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name)
    Collector.save_results(to_save, 'cross_validation', path, append=True)
