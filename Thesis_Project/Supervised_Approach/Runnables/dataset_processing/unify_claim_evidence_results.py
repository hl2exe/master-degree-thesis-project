"""

:author: Federico Ruggeri
:date: 15th March 2018
:description: unifies claim and evidence separate predictions on the same
data-set

"""

from Supervised_Approach.post_processor import PostProcessor
from Supervised_Approach.path_definitions import PREDICTIONS_DIR
import os

#####################
# Variables Section
#####################

path_to_claim_csv = os.path.join(PREDICTIONS_DIR, 'ce_acl_14_claim_topic_predictions.csv')
path_to_evidence_csv = os.path.join(PREDICTIONS_DIR, 'ce_acl_14_evidence_topic_predictions.csv')
unified_filename = 'ce_acl_14_joined_predictions.csv'

#####################
# System Section
#####################

PostProcessor.unify_claim_evidence_results_per_scenario(path_to_claim_csv, path_to_evidence_csv, unified_filename)
