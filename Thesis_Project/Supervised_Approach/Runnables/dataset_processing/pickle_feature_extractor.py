"""

:author: Federico Ruggeri
:date: 30th March 2018
:description: simple runnable script used for feature extraction.
Uncomment the extractor, then define the proper selector (Couple, Item, Concatenated)
and then run

"""

from Supervised_Approach.Utils.feature_extractor_utils import FeatureExtractorUtils
from Supervised_Approach.feature_extractor import SyntacticCountVectorizer, DependencyVectorizer, RepeatedPunctuationExtractor, BasicExtractor, CosineSimilarityExtractor, CosineSimilaritySEVariantExtractor, CosineSimilarityABTTVariantExtractor, ProductionRulesBinaryVectorizer, \
    ModalVerbBinaryExtractor, CommonElementsExtractor, TokenCounterExtractor, TokenCounterDifferenceExtractor, \
    PunctuationMarksExtractor, PunctuationMarksDifferenceExtractor, WordPairsBinaryVectorizer, SVOTriplesExtractor
from Supervised_Approach.selector import ItemSelector, CoupleSelector
import pandas as pd
import os
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, TRAIN_DIR, TEST_DIR

# Setup

utils = FeatureExtractorUtils()
pickle_folder = 'CE-ACL-14'
directory = TRAIN_DIR
dataset_name = 'ce_acl_14_joined-ids.csv'
df = pd.read_csv(os.path.join(directory, dataset_name), encoding='utf-8')

# Extractors

# Dependency: possible key parameter values are: 'POS_generalized', 'syntactic', 'opinion_generalized'
extractor = DependencyVectorizer(utils=utils, key="syntactic", save_to_pickle=True,
                                 pickle_file="syntactic.pickle", pickle_folder=pickle_folder)

# SVO triples
# extractor = SVOTriplesExtractor(utils=utils, save_to_pickle=True, pickle_file="svo_triples.pickle",
#                                 pickle_folder=pickle_folder)

# Cosine similarity
# Note: uncomment this if you are using CosineSimilarityExtractor or any custom extractor that requires Word2Vec model
# utils.load_w2v_model()
# extractor = CosineSimilarityExtractor(utils=utils, pickle_mode=False, pickle_file='cosine_similarity.pickle',
#                                       save_to_pickle=True, pickle_folder=pickle_folder)
# extractor = CosineSimilaritySEVariantExtractor(a=0.001, utils=utils, pickle_mode=False,
#                                                pickle_file='cosine_similarity_SE.pickle',
#                                                save_to_pickle=True, pickle_folder=pickle_folder)
# extractor = CosineSimilarityABTTVariantExtractor(D=3, utils=utils, pickle_mode=False,
#                                                  pickle_file='cosine_similarity_ABTT.pickle',
#                                                  save_to_pickle=True, pickle_folder=pickle_folder)

# Repeated Punctuation
# extractor = RepeatedPunctuationExtractor(utils=utils, pickle_file='repeated_punctuation.pickle', save_to_pickle=True,
#                                          pickle_folder=pickle_folder)

# Basic features
# extractor = BasicExtractor(utils=utils, pickle_file='basic.pickle', save_to_pickle=True, pickle_folder=pickle_folder)

# Sngrams
# extractor = SyntacticCountVectorizer(utils=utils, save_to_pickle=True, pickle_file='sngrams.pickle',
#                                      pickle_folder=pickle_folder)

# Ngrams -> hard to efficiently save and load: not used
# extractor = PickleCountVectorizer(utils=utils, ngram_range=(1, 3), save_to_pickle=True, pickle_file='ngrams.pickle',
#                                   pickle_folder=pickle_folder)

# Stab & Gurevych features

# Production rules
# extractor = ProductionRulesBinaryVectorizer(utils=utils, save_to_pickle=True,
#                                             pickle_file='production_rules_binary.pickle', pickle_folder=pickle_folder)

# Modal verbs binary
# extractor = ModalVerbBinaryExtractor(utils=utils, save_to_pickle=True, pickle_file='modal_verb_binary.pickle',
#                                      pickle_folder=pickle_folder)

# Common elements
# extractor = CommonElementsExtractor(utils=utils, save_to_pickle=True, pickle_file='common_elements.pickle',
#                                     pickle_folder=pickle_folder)

# Tokens count
# extractor = TokenCounterExtractor(utils=utils, save_to_pickle=True, pickle_file='token_count.pickle',
#                                   pickle_folder=pickle_folder)

# Tokens count difference
# extractor = TokenCounterDifferenceExtractor(utils=utils, save_to_pickle=True,
#                                             pickle_file='token_count_difference.pickle', pickle_folder=pickle_folder)

# Punctuation marks
# extractor = PunctuationMarksExtractor(utils=utils, save_to_pickle=True, pickle_file='punctuation_marks.pickle',
#                                       pickle_folder=pickle_folder)

# Punctuation marks difference
# extractor = PunctuationMarksDifferenceExtractor(utils=utils, save_to_pickle=True,
#                                                 pickle_file='punctuation_marks_difference.pickle',
#                                                 pickle_folder=pickle_folder)

# Word pairs binary
# extractor = WordPairsBinaryVectorizer(utils=utils, stop_words='english', save_to_pickle=True,
#                                       pickle_file='word_pairs_binary.pickle', pickle_folder=pickle_folder)

# Change target column names if needed
# selector = CoupleSelector(key_mapping={'x': {1: 'claimHeadline', 2: 'articleHeadline'}})
selector = ItemSelector(key_mapping={'x': 'CDE'})

# Placeholder
df.name = 'x'
target_list = selector.transform(dataframe=df)

print(len(target_list))
target_list = list(set(target_list))  # efficiency
print(len(target_list))

from Supervised_Approach.Utils.pickle_utility import PickleUtility
existing_data = PickleUtility.load_pickle('syntactic.pickle', pickle_folder)

target_list = [item for item in target_list if item not in existing_data]
print(len(target_list))

# Custom extractor
# extractor.transform(target_list)

# Vectorizer extractor
# Note: if extractor is pickle compliant vectorizer, in order to save data it is necessary to explicitly invoke
# save_data_to_pickle method
extractor.fit_transform(target_list)
extractor.save_data_to_pickle(extractor.pickle_data)
