"""

:author: Federico Ruggeri
:date: 15th March 2018

Builds CE-EMNLP-15 train and test and held-out sets from
CE-EMNLP-15 argument structure prediction data-set and
motions.csv

"""

import os
from Supervised_Approach.path_definitions import POST_PROCESS_DIR, TRAIN_DIR
from Supervised_Approach.post_processor import PostProcessor


#####################
# Variables Section
#####################

motions_name = 'motions.csv'
motions_folder = TRAIN_DIR
path_to_motions = os.path.join(motions_folder, motions_name)

emnlp_15_name = 'ce_emnlp_15_argument_structure_prediction_training.csv'
emnlp_15_folder = POST_PROCESS_DIR
path_to_emnlp_15 = os.path.join(emnlp_15_folder, emnlp_15_name)

# dataset_value: 'held-out' or 'train and test'
dataset_value = 'train and test'
filename = 'ce_emnlp_15_ASP_train_and_test_training.csv'

#####################
# System Section
#####################

PostProcessor.build_motions_ce_emnlp_15_datasets(path_to_motions=path_to_motions,
                                                 path_to_csv=path_to_emnlp_15,
                                                 dataset_value=dataset_value,
                                                 filename=filename)
