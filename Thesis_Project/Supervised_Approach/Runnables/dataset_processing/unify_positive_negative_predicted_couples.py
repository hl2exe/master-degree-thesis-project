"""

:author: Federico Ruggeri
:date: 15th March 2018
:description: unifies positive (supporting evidence - claim) and
negative (opposing evidence - claim) examples from the same data-set

"""

from Supervised_Approach.path_definitions import PREDICTIONS_DIR
from Supervised_Approach.post_processor import PostProcessor
import os

#####################
# Variables Section
#####################


negative_csv_name = ''
negative_csv_folder = PREDICTIONS_DIR
negative_csv_path = os.path.join(negative_csv_folder, negative_csv_name)

positive_csv_name = ''
positive_csv_folder = PREDICTIONS_DIR
positive_csv_path = os.path.join(positive_csv_folder, positive_csv_name)

claim_column = ''
evidence_column = ''
positive_claim_stance_column = ""
positive_evidence_stance_column = ""
negative_claim_stance_column = ""
negative_evidence_stance_column = ""
topic_column = ''

filename = 'ce_acl_14_argument_structure_prediction_training.csv'

#####################
# System Section
#####################

PostProcessor.unify_positive_negative_couples(negative_csv=negative_csv_path,
                                              positive_csv=positive_csv_path,
                                              claim_column=claim_column,
                                              evidence_column=evidence_column,
                                              positive_evidence_stance_column=positive_evidence_stance_column,
                                              positive_claim_stance_column=positive_claim_stance_column,
                                              negative_evidence_stance_column=negative_evidence_stance_column,
                                              negative_claim_stance_column=negative_claim_stance_column,
                                              topic_column=topic_column,
                                              filename=filename)
