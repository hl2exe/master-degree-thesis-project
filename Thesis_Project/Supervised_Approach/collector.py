import os

import pandas as pd

from path_definitions import CLASSIFIER_CONFIGURATIONS_DIR, PREDICTIONS_DIR
from Utils.json_utils import save_simplejson_to_file_from_path, load_simplejson_from_file_from_path


class Collector(object):

    """
    Simple utility class that save to file given data.
    Used in particular to save gridsearch and classifier results.
    """

    separator_length = 20
    separator_symbol = '#'

    @staticmethod
    def get_default_scorers():

        return [
            'accuracy',
            'average_precision',
            'f1',
            'f1_micro',
            'f1_macro',
            'precision',
            'recall',
            'roc_auc'
        ]

    @staticmethod
    def get_default_colours():

        return [
            'b',
            'g',
            'r',
            'c',
            'm',
            'y',
            'k',
            'w'
        ]

    # TODO: to test
    # @staticmethod
    # def plot_cv_results(cv_results, param, scoring, colours):
    #     """
    #     Taken from: http://scikit-learn.org/stable/auto_examples/model_selection/plot_multi_metric_evaluation.html
    #     :param cv_results:
    #     :param scoring
    #     :param colours
    #     :return:
    #     """
    #
    #     plt.figure(figsize=(13, 13))
    #     plt.title("GridSearchCV evaluating using multiple scorers simultaneously",
    #               fontsize=16)
    #
    #     plt.xlabel(param)
    #     plt.ylabel("Score")
    #     plt.grid()
    #
    #     ax = plt.axes()
    #     ax.set_xlim(0, 402)
    #     ax.set_ylim(0.65, 1)
    #
    #     # Get the regular numpy array from the MaskedArray
    #     X_axis = np.array(cv_results[param].data, dtype=float)
    #
    #     for scorer, color in zip(sorted(scoring), colours):
    #         for sample, style in (('train', '--'), ('test', '-')):
    #             sample_score_mean = cv_results['mean_%s_%s' % (sample, scorer)]
    #             sample_score_std = cv_results['std_%s_%s' % (sample, scorer)]
    #             ax.fill_between(X_axis, sample_score_mean - sample_score_std,
    #                             sample_score_mean + sample_score_std,
    #                             alpha=0.1 if sample == 'test' else 0, color=color)
    #             ax.plot(X_axis, sample_score_mean, style, color=color,
    #                     alpha=1 if sample == 'test' else 0.7,
    #                     label="%s (%s)" % (scorer, sample))
    #
    #         best_index = np.nonzero(cv_results['rank_test_%s' % scorer] == 1)[0][0]
    #         best_score = cv_results['mean_test_%s' % scorer][best_index]
    #
    #         # Plot a dotted vertical line at the best score for that scorer marked by x
    #         ax.plot([X_axis[best_index], ] * 2, [0, best_score],
    #                 linestyle='-.', color=color, marker='x', markeredgewidth=3, ms=8)
    #
    #         # Annotate the best score for that scorer
    #         ax.annotate("%0.2f" % best_score,
    #                     (X_axis[best_index], best_score + 0.005))
    #
    #     plt.legend(loc="best")
    #     plt.grid('off')
    #     plt.show()

    @staticmethod
    def save_gridsearch_cv_results(gridsearch_cv_results, dataset_name, configuration_name, index=None):
        """
        Saves GridSearchCV dict results in .csv format

        :param gridsearch_cv_results: GridSearchCV cv_results dictionary
        :param dataset_name: name of the data-set on which the grid-search activity
        has been held
        :param configuration_name: classifier configuration name
        :param index: which index to use during data saving
        :return: name of the .csv file name
        """

        df_out = pd.DataFrame.from_dict(gridsearch_cv_results)
        if index is not None:
            df_out = df_out.set_index(index)

        filename = 'gridsearch_{}.csv'.format(dataset_name)
        path = os.path.join(CLASSIFIER_CONFIGURATIONS_DIR, configuration_name, filename)
        df_out.to_csv(path, encoding="utf-8")

        return filename

    @staticmethod
    def retrieve_gridsearch_info(gridsearch, parameters):
        """
        Retrieves main gridsearch statistics and saves them.

        :param gridsearch:
        :param parameters:
        :return:
        """

        best_estimator_classifier = gridsearch.best_estimator_.get_params(deep=True)['steps'][-1][1]

        info = {
            'best_score': gridsearch.best_score_,
            'refit': gridsearch.refit,
            'best_estimator_name': type(best_estimator_classifier).__name__.lower(),
            'best_estimator_fields': {key: str(value) for key, value in best_estimator_classifier.__dict__.items()},
            'best_parameters': {param_name: gridsearch.best_params_[param_name]
                                for param_name in sorted(parameters.keys())},
            'scorer': gridsearch.scorer_.keys(),
            'n_splits': gridsearch.n_splits_
        }

        return info

    @staticmethod
    def save_prediction_results(predictions, columns, target_dataframe, inverted_columns_mapping, stance_column,
                                dataset_name, to_save_filename):
        """
        Saves predictions results on target data-set by defining a new data-set

        :param predictions: array of predicted labels
        :param columns: important columns to save (targets of classification)
        :param target_dataframe: data-set target of the classification
        :param inverted_columns_mapping: target data-set inverted columns mapping
        :param stance_column: name of the stance column
        :param dataset_name: target data-set name
        :param to_save_filename: new data-set file name
        :return:
        """

        to_save = target_dataframe[columns]
        to_save = to_save.rename(columns=inverted_columns_mapping[dataset_name])
        prediction_column = "{} stance".format(stance_column)
        to_save[prediction_column] = pd.Series(predictions).values

        to_save_path = os.path.join(PREDICTIONS_DIR, to_save_filename)
        to_save.to_csv(to_save_path, encoding="utf-8")

        return prediction_column

    @staticmethod
    def save_results(results, filename, path, append=False):
        """
        Saves generic data to file

        :param results: data to save
        :param filename: .json file name
        :param path: path to .json file
        :param append: whether to append or not results on existing .json file
        :return: None
        """

        if not os.path.isdir(path):
            os.mkdir(path)

        path = os.path.join(path, '{}.json'.format(filename))

        if append and os.path.isfile(path):
            data = load_simplejson_from_file_from_path(path)
            data.update(results)
            save_simplejson_to_file_from_path(path, data)
        else:
            save_simplejson_to_file_from_path(path, results)
