import hashlib
import io
import os

import pandas as pd

from Utils.dataset_utils import DatasetUtils
from path_definitions import GENERATED_DIR, TRAIN_DIR


class DatasetGenerator(object):
    """
    
    Generators
    
    """

    @staticmethod
    def generate_bodies_ce_acl_14(folder, filename):
        """
        Parses 2014_7_18_ibm_CDCdata.csv and produces a new dataset with
        the following structure:

            Index : Article
            Columns: [ Body ID, articleBody ]


        :param folder: folder containing article files
        :param filename: filename where to save result dataframe
        :return: None
        """

        bodies = {}
        filenames = []

        # Reading bodies
        for path, subdirs, files in os.walk(folder):
            for f in files:
                filepath = os.path.join(path, f)
                with open(filepath, 'r') as current_file:
                    body = current_file.read()
                    bodies[len(bodies.keys())] = body
                    filenames.append(f.replace("_", " "))

        to_write = pd.DataFrame(list(bodies.items()), columns=['Body ID', 'articleBody'])
        to_write['Article'] = pd.Series(filenames).values
        to_write = to_write.set_index('Article')
        to_write.to_csv(os.path.join(GENERATED_DIR, filename), encoding="utf-8")

    @staticmethod
    def generate_bodies_ce_emnlp_15(folder, filename):
        """
        Retrieves for each Body ID the associated text stored in a file within given folder path.
        The results is saved in .csv format in specified file name.
        This method is specific for CE-EMNLP-15 data-set and follows FNC-1 baseline classifier input format.

        :param folder: path to folder containing body files
        :param filename: name of the .csv where to save results.
        :return: None
        """

        bodies = {}
        filenames = []

        # Reading bodies
        for path, subdirs, files in os.walk(folder):
            for f in files:
                filepath = os.path.join(path, f)
                filenames.append(f.replace("clean_", "").replace(".txt", ""))
                with io.open(filepath, 'r', encoding="utf-8") as current_file:
                    body = current_file.read()
                    bodies[len(bodies.keys())] = body

        to_write = pd.DataFrame(list(bodies.items()), columns=['', 'articleBody'])
        to_write['Body ID'] = pd.Series(filenames).values
        to_write = to_write.set_index("Body ID")
        to_write = to_write[["articleBody"]]
        to_write.to_csv(os.path.join(GENERATED_DIR, filename), encoding="utf-8")

    @staticmethod
    def generate_claim_article_association_ce_emnlp_15(claim_dataset, article_dataset, folder, filename,
                                                       failed_filename):
        """
        Builds claim - article associations, i.e. retrieves article bodies stored in files in which the claim was
        previously extracted. Associations are saved in .csv format speficied by given file name. Failed entries
        are saved in .csv format as well.
        This method is specific for CE-EMNLP-15 data-set and follows FNC-1 baseline classifier input format.

        :param claim_dataset:
        :param article_dataset:
        :param folder:
        :param filename:
        :param failed_filename:
        :return:
        """

        # Step 1: retrieving claims
        claim_data = pd.DataFrame.from_csv(claim_dataset, encoding="utf-8", sep="\t")
        claims = claim_data["Claim original text"].values

        # Step 2: retrieving articles IDs
        article_data = pd.DataFrame.from_csv(article_dataset, encoding="utf-8", sep="\t")
        articles = article_data["article Id"].values
        articles = ["clean_{}".format(idx) for idx in articles]

        # Step 3: finding pairs
        pairs = {'Claim': [], 'articleBody': []}
        failed = []

        for claim in claims:
            found = False
            for article in articles:
                file_path = os.path.join(folder, "{}.txt".format(article))
                with io.open(file_path, "r", encoding="utf-8") as current_file:
                    data = current_file.read()
                    if claim.lower() in data.lower():
                        pairs["Claim"].append(claim)
                        pairs["articleBody"].append(data)
                        found = True
                        break

            if found is False:
                pairs["Claim"].append(claim)
                pairs["articleBody"].append("Not Found")
                failed.append(claim)

        print("Failed entries: {}".format(len(failed)))

        failed_to_save_series = pd.Series(failed)
        failed_to_save = pd.DataFrame(failed_to_save_series).reset_index()
        failed_to_save.columns = ["ID", "Claim"]
        failed_to_save.to_csv(os.path.join(GENERATED_DIR, failed_filename), encoding="utf-8")

        result = pd.DataFrame.from_dict(pairs, orient="columns")
        result = result.set_index("Claim")
        result.to_csv(os.path.join(GENERATED_DIR, filename), encoding="utf-8")

    """
    
    Complete
    
    """

    @staticmethod
    def generate_ce_acl_14_complete(path_to_claim, path_to_bodies, filename):
        """
        Combines claim data with retrieved article data into a single .csv file.
        This method is specific for CE-ACL-14 data-set.

        :param path_to_claim: claim data
        :param path_to_bodies: bodies data
        :param filename:
        :return: None
        """

        claim_data = pd.DataFrame.from_csv(path_to_claim, encoding="utf-8")
        body_data = pd.DataFrame.from_csv(path_to_bodies, encoding="utf-8")

        claim_data = claim_data[["Article", "Claim"]]
        article_dict = body_data.to_dict()

        articles = claim_data["Article"].values

        to_add = [article_dict["articleBody"][article] for article in articles]

        claim_data['Article Body'] = pd.Series(to_add).values
        claim_data.to_csv(os.path.join(TRAIN_DIR, filename), encoding="utf-8")

    @staticmethod
    def generate_ce_emnlp_15_complete(path_to_claim, path_to_bodies, filename):
        """
        Combines claim data with retrieved article data into a single .csv file.
        This method is specific for CE-EMNLP-15 data-set.

        :param path_to_claim: claim data
        :param path_to_bodies: bodies data
        :param filename:
        :return: None
        """

        claim_data = pd.DataFrame.from_csv(path_to_claim, encoding="utf-8", sep="\t")
        body_data = pd.DataFrame.from_csv(path_to_bodies, encoding="utf-8")

        body_dict = body_data.to_dict()

        to_add = [body_dict["articleBody"][claim] for claim in claim_data["Claim original text"].values]

        claim_data['Article Body'] = pd.Series(to_add).values
        claim_data.to_csv(os.path.join(TRAIN_DIR, filename), encoding="utf-8")

    @staticmethod
    def generate_ids(dataframe, columns, filename, filtering=False, encoding="utf-8"):
        """
        Processes instance dataframe in order to build a feature extraction compliant dataset.

        :param dataframe: pandas DataFrame view of a given dataset
        :param columns: column names to consider for id generation
        :param filename: filename where to save new dataframe
        :param filtering: whether pre-processing dataframe, i.e. selecting only
        given columns, or not.
        :param encoding: save encoding parameter
        :return: None
        """

        if filtering:
            filtered = DatasetUtils.extract_columns(dataframe, columns)
        else:
            filtered = dataframe

        for column in columns:
            ids = [hashlib.md5(value.encode('utf-8')).hexdigest() for value in filtered[column].values]
            column_id_name = "{} ID".format(column)
            filtered[column_id_name] = pd.Series(ids).values

        filtered.to_csv(os.path.join(TRAIN_DIR, filename), encoding=encoding)

    @staticmethod
    def join_evidences_to_complete_ce_acl_14(path_to_complete, path_to_cde, filename):
        """
        Adds evidence data to complete csv file.
        This method is specific for CE-ACL-14 data-set.

        :param path_to_complete: complete data
        :param path_to_cde: evidence data
        :param filename:
        :return: None
        """

        complete_df = pd.read_csv(path_to_complete, encoding="utf-8")
        cde_df = pd.read_csv(path_to_cde, encoding="utf-8")

        mod_complete_df = complete_df.set_index(['Topic', 'Claim'])
        mod_cde_df = cde_df.set_index(['Topic', 'Claim'])

        joined = mod_cde_df.join(mod_complete_df)
        joined.to_csv(os.path.join(TRAIN_DIR, filename), encoding="utf-8")

    @staticmethod
    def join_evidences_to_complete_ce_emnlp_15(path_to_complete, path_to_cde, filename):
        """
        Adds evidence data to complete csv file.
        This method is specific for CE-ACL-14 data-set.

        :param path_to_complete: complete data
        :param path_to_cde: evidence data
        :param filename:
        :return: None
        """

        complete_df = pd.read_csv(path_to_complete, encoding="utf-8")
        cde_df = pd.read_csv(path_to_cde, encoding="utf-8", sep="\t")

        mod_complete_df = complete_df.rename(columns={'Claim original text': 'Claim',
                                                      'Claim original text ID': 'Claim ID'})
        mod_complete_df = mod_complete_df.set_index(['Topic', 'Claim'])
        mod_cde_df = cde_df.set_index(['Topic', 'Claim'])

        joined = mod_cde_df.join(mod_complete_df)
        joined.to_csv(os.path.join(TRAIN_DIR, filename), encoding="utf-8")
