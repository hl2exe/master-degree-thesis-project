"""

:author: Federico Ruggeri
:date: 17th March 2018
:description:


"""

import itertools
import os

import numpy as np
import pandas as pd
import unicodecsv as csv

from path_definitions import POST_PROCESS_DIR


class PostProcessor(object):

    @staticmethod
    def unify_claim_evidence_results_per_scenario(path_to_claim_csv, path_to_evidence_csv,
                                                  unified_filename, folder=POST_PROCESS_DIR):
        """
        Combines results of coupled scenarios. Couples scenarios are 'evidence-XXX' and 'claim-XXX'.

        :param path_to_claim_csv:
        :param path_to_evidence_csv:
        :param unified_filename:
        :param folder:
        :return: None
        """

        claim_data = pd.read_csv(path_to_claim_csv, encoding="utf-8")
        evidence_data = pd.read_csv(path_to_evidence_csv, encoding="utf-8")
        common_columns = [col for col in claim_data.columns.values if col in evidence_data.columns.values]
        claim_data = claim_data.set_index(common_columns)
        evidence_data = evidence_data.set_index(common_columns)
        joined = claim_data.join(evidence_data)
        joined_path = os.path.join(folder, unified_filename)
        joined.to_csv(joined_path, encoding="utf-8")

    @staticmethod
    def count_incoherent_crossed_pairs(path_to_csv, group_by_column, claim_stance_column,
                                       evidence_stance_column, claim_column, evidence_column,
                                       mode='default'):
        """
        Considering entries that have a column value in common, expressed by group_by_column parameter,
        counts the number of crossed 'evidence-claim' pairs that have different stance according to a particular
        cond, managed via the mode parameter.
        Lastly, all the 'evidence-claim' pairs are saved in .csv format, if the parameter filename is given.

        :param path_to_csv:
        :param group_by_column: column by which a groupby action is held -> topic
        :param claim_stance_column:
        :param evidence_stance_column:
        :param claim_column:
        :param evidence_column:
        :param mode: manages the type of cond that determines incoherent crossed pairs. This parameter can assume
        2 different values:
            1. Default: pairs that have different stance value are considered as incoherent ones
            2. Ignore-observing: pairs that have different stance value and each stance value is different from
             'observing' are considered as incoherent ones
        :return: None
        """

        if mode == 'default':
            cond = lambda claim_stance, evidence_stance: claim_stance != evidence_stance
        else:
            if mode == 'ignore-observing':
                cond = lambda claim_stance, evidence_stance: claim_stance != evidence_stance and \
                                                             claim_stance != 'observing' \
                                                             and evidence_stance != 'observing'
            else:
                if mode == 'all':
                    cond = lambda claim_stance, evidence_stance: True
                else:
                    print("Invalid mode!")
                    return

        df = pd.read_csv(path_to_csv, encoding="utf-8")

        grouped = df.groupby(group_by_column)
        total = 0
        filtered_couples = []

        for key, item in grouped:
            claim_values = zip(item[claim_stance_column].values, item[claim_column].values)
            evidence_values = zip(item[evidence_stance_column].values, item[evidence_column].values)

            claim_evidence_crossed_couples = list(itertools.product(claim_values, evidence_values))
            claim_evidence_incoherent_couples = [couple for couple in claim_evidence_crossed_couples
                                                 if cond(couple[0][0], couple[1][0])]

            print("Found for key '{}': {}".format(key, len(claim_evidence_incoherent_couples)))
            total += len(set(claim_evidence_incoherent_couples))
            filtered_couples += [(key, couple[0][0], couple[0][1], couple[1][0], couple[1][1])
                                 for couple in claim_evidence_incoherent_couples]

        print("Total incoherent crossed pairs: {}".format(total))
        filtered_couples = list(set(filtered_couples))
        print("Total distinct incoherent crossed pairs: {}".format(len(filtered_couples)))

        return filtered_couples

    @staticmethod
    def compute_stance_confusion_matrix(path_to_csv, evidence_stance_column, claim_stance_column, evidence_column,
                                        claim_column, group_by_column, mode='default'):
        """
        Computes confusion matrix related to the stance label. The aim is to verify if the stance can be used as a
        feature in order to improve argument structure prediction classification. Therefore, couples that have stance
        values in accordance to the specified filtering condition are considered either as supportive or opposing
        examples.

        :param path_to_csv:
        :param evidence_stance_column:
        :param claim_stance_column:
        :param evidence_column:
        :param claim_column:
        :param group_by_column: column by which a groupby action is held -> topic
        :param mode: manages the type of condition that determines incoherent crossed pairs. This parameter can assume
        3 different values:
            1. Default: couples that have same stance are considered as supporting examples
            2. Overcome-observing: couples that have same stance or one of them is of type 'observing' are considered
            as supporting examples
            3. Ignore-observing: couples that have same stance and neither of them is of type 'observing' are considered
            as supporting examples
        :return: confusion matrix as pandas DataFrame
        """

        df = pd.read_csv(path_to_csv, encoding="utf-8")

        if mode == 'default':
            condition = lambda claim_stance, evidence_stance: claim_stance == evidence_stance
        else:
            if mode == 'overcome-observing':
                condition = lambda claim_stance, evidence_stance: claim_stance == evidence_stance \
                                                                  or claim_stance == 'observing' \
                                                                  or evidence_stance == 'observing'
            else:
                if mode == 'ignore-observing':
                    condition = lambda claim_stance, evidence_stance: claim_stance == evidence_stance and \
                                                                      claim_stance != 'observing' \
                                                                      and evidence_stance != 'observing'
                else:
                    print("Invalid mode!")
                    return

        # Horizontal couples
        claim_stances = df[claim_stance_column].values
        evidence_stances = df[evidence_stance_column].values

        # Crossed couples
        grouped = df.groupby(group_by_column)
        claim_evidence_crossed_couples = []

        for key, item in grouped:
            claim_values = zip(item[claim_stance_column].values, item[claim_column].values)
            evidence_values = zip(item[evidence_stance_column].values, item[evidence_column].values)

            current_crossed_couples = list(itertools.product(claim_values, evidence_values))
            current_crossed_couples = list(set(current_crossed_couples))
            claim_evidence_crossed_couples += ['link' if condition(couple[0][0], couple[1][0]) else 'no-link'
                                               for couple in current_crossed_couples]

        print("Total crossed couples: {}".format(len(claim_evidence_crossed_couples)))

        predicted_values = ['link' if condition(claim_stance, evidence_stance) else 'no-link'
                            for claim_stance, evidence_stance in zip(claim_stances, evidence_stances)]
        predicted_values += claim_evidence_crossed_couples

        true_values = ['link' for _ in range(0, len(claim_stances))]
        true_values += ['no-link' for _ in range(0, len(claim_evidence_crossed_couples))]

        predicted_series = pd.Series(predicted_values, name='Predicted')
        true_series = pd.Series(true_values, name='True')

        return pd.crosstab(true_series, predicted_series)

    @staticmethod
    def find_inconsistencies(filename, columns, folder=POST_PROCESS_DIR):
        """
        Counts the number of entries that have non-identical stances.
        Found entries are saved in .csv format.

        :param filename:
        :param columns: stance columns names
        :param folder:
        :return: sub-dataframe containing only indexes in which
        a incoherent stance comparison has been detected
        """

        if not os.path.isdir(POST_PROCESS_DIR):
            os.mkdir(POST_PROCESS_DIR)
            print("Creating post process folder!")

        print("Considering: {}".format(filename))
        indexes_to_save = []

        df = pd.read_csv(os.path.join(folder, filename), encoding="utf-8")
        stance_values = [df[column].values for column in columns]

        for idx, stances in enumerate(zip(*stance_values)):
            # print(set(stances))
            if len(set(stances)) > 1:
                indexes_to_save.append(idx)

        print("Found {} incoherent predictions!".format(len(indexes_to_save)))
        if len(indexes_to_save) > 0:
            return df.iloc[indexes_to_save]
        else:
            return None

    @staticmethod
    def unify_positive_negative_couples(negative_csv, positive_csv, claim_column,
                                        evidence_column, negative_claim_stance_column,
                                        positive_claim_stance_column,
                                        negative_evidence_stance_column,
                                        positive_evidence_stance_column,
                                        topic_column,
                                        filename):
        """
        Unifies found positive and negative couple examples related to argument structure prediction along with their
        stance values and their argument relation. Lastly, the whole data is saved in .csv format, specified by the
        filename parameter.

        :param negative_csv: path to .csv file containing negative examples
        :param positive_csv: path to .csv file containing positive examples
        :param claim_column:
        :param evidence_column:
        :param negative_claim_stance_column:
        :param positive_claim_stance_column:
        :param negative_evidence_stance_column:
        :param positive_evidence_stance_column:
        :param topic_column:
        :param filename:
        :return: None
        """

        negative_df = pd.read_csv(negative_csv, encoding="utf-8")
        positive_df = pd.read_csv(positive_csv, encoding="utf-8")

        all_claims = negative_df[claim_column].append(positive_df[claim_column])
        all_evidences = negative_df[evidence_column].append(positive_df[evidence_column])
        all_claims_stances = negative_df[negative_claim_stance_column].append(positive_df[positive_claim_stance_column])
        all_evidences_stances = negative_df[negative_evidence_stance_column] \
            .append(positive_df[positive_evidence_stance_column])
        all_topics = negative_df[topic_column].append(positive_df[topic_column])

        argument_relations = ['no-link' for _ in range(0, len(negative_df))] + \
                             ['link' for _ in range(0, len(positive_df))]
        index = range(0, len(all_claims))

        df_out = pd.DataFrame(index=index, columns=['Claim', 'Evidence', 'Argument relation'], data=np.nan)
        df_out['Claim'] = all_claims.values
        df_out['Evidence'] = all_evidences.values
        df_out['Claim stance'] = all_claims_stances.values
        df_out['Evidence stance'] = all_evidences_stances.values
        df_out['Argument relation'] = pd.Series(argument_relations).values
        df_out['Topic'] = all_topics.values

        df_out.to_csv(os.path.join(POST_PROCESS_DIR, filename), encoding="utf-8")

    @staticmethod
    def build_motions_ce_emnlp_15_datasets(path_to_motions, path_to_csv, dataset_value, filename):
        """

        :param path_to_motions:
        :param path_to_csv:
        :param dataset_value: name of the data-set set ('train and test' or 'held-out')
        :param filename:
        :return:
        """

        motions_df = pd.read_csv(path_to_motions, encoding="utf-8", sep='\t')
        topics = motions_df.loc[motions_df['Data-set'] == dataset_value]['Topic'].values
        print("Topics with value equal to {}: {}".format(dataset_value, len(topics)))

        ibm_df = pd.read_csv(path_to_csv, encoding="utf-8")
        ibm_df = ibm_df.loc[ibm_df['Topic'].isin(topics)]
        print("Selected sub-dataset dimension: {}".format(ibm_df.shape))
        print("Positive examples: {}".format(len(ibm_df.loc[ibm_df['Argument relation'] == 'link'])))
        print("Negative examples: {}".format(len(ibm_df.loc[ibm_df['Argument relation'] == 'no-link'])))

        ibm_df.to_csv(os.path.join(POST_PROCESS_DIR, filename), encoding="utf-8")
