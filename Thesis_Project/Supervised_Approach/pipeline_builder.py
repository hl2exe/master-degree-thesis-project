
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer
from sklearn.pipeline import make_pipeline, FeatureUnion

from feature_extractor import CosineSimilarityExtractor, BasicExtractor, SVOTriplesExtractor, SkipGramVectorizer, \
    DependencyVectorizer, RepeatedPunctuationExtractor, DiscourseCueVectorizer, SyntacticCountVectorizer, \
    TokenCounterExtractor, \
    TokenCounterDifferenceExtractor, PunctuationMarksExtractor, PunctuationMarksDifferenceExtractor, \
    WordPairsBinaryVectorizer, \
    FirstWordBinaryExtractor, FirstWordBinaryCoupleExtractor, ModalVerbBinaryExtractor, CommonElementsExtractor, \
    ProductionRulesBinaryVectorizer, CosineSimilaritySEVariantExtractor
from feature_transformer import CosineSimilarityTransformer, SVOTriplesTransformer, TokenCounterTrasnformer, \
    TokenCounterDifferenceTransformer, PunctuationMarksTransformer, PunctuationMarksDifferenceTransformer, \
    ModalVerbBinaryTransformer, CommonElementsTransformer
from selector import ItemSelector, CoupleSelector, ConcatenateSelector


class PipelineBuilder(object):

    default_feature_dict = {
        'cosine_similarity': [CoupleSelector,
                              CosineSimilarityExtractor,
                              CosineSimilarityTransformer],

        'cosine_similarity_SE': [CoupleSelector,
                                 CosineSimilaritySEVariantExtractor,
                                 CosineSimilarityTransformer],

        'svo_triples': [CoupleSelector,
                        SVOTriplesExtractor,
                        SVOTriplesTransformer],

        'basic': [ItemSelector,
                  BasicExtractor,
                  DictVectorizer,
                  TfidfTransformer],

        'ngrams': [ConcatenateSelector,
                   TfidfVectorizer],

        'skipgrams': [ConcatenateSelector,
                      SkipGramVectorizer,
                      TfidfTransformer],

        'single_ngrams': [ItemSelector,
                          TfidfVectorizer],

        'single_skipgrams': [ItemSelector,
                             SkipGramVectorizer,
                             TfidfTransformer],

        'dependency_relations': [ItemSelector,
                                 DependencyVectorizer,
                                 TfidfTransformer],

        'repeated_punctuation': [ItemSelector,
                                 RepeatedPunctuationExtractor,
                                 DictVectorizer,
                                 TfidfTransformer],

        'discourse_cues': [ItemSelector,
                           DiscourseCueVectorizer,
                           TfidfTransformer],

        'sngrams': [ItemSelector,
                    SyntacticCountVectorizer,
                    TfidfTransformer],

        'count_tokens': [ItemSelector,
                         TokenCounterExtractor,
                         TokenCounterTrasnformer],

        'difference_tokens': [CoupleSelector,
                              TokenCounterDifferenceExtractor,
                              TokenCounterDifferenceTransformer],

        'punctuation_marks': [ItemSelector,
                              PunctuationMarksExtractor,
                              PunctuationMarksTransformer],

        'difference_punctuation_marks': [CoupleSelector,
                                         PunctuationMarksDifferenceExtractor,
                                         PunctuationMarksDifferenceTransformer],

        'word_pairs_binary': [CoupleSelector,
                              WordPairsBinaryVectorizer],

        'first_word_binary': [ItemSelector,
                              FirstWordBinaryExtractor],

        'couple_first_word_binary': [CoupleSelector,
                                     FirstWordBinaryCoupleExtractor],

        'modal_verbs_binary': [ItemSelector,
                               ModalVerbBinaryExtractor,
                               ModalVerbBinaryTransformer],

        'common_elements': [CoupleSelector,
                            CommonElementsExtractor,
                            CommonElementsTransformer],

        'production_rules_binary': [ItemSelector,
                                    ProductionRulesBinaryVectorizer]
    }

    @staticmethod
    def _build_steps_with_args(features, args):
        """
        Instantiates each step of the pipeline associated
        with the specified used features with their arguments
        respectively.

        :param features: list of feature names
        :param args: dictionary containing init args for each feature pipeline block
        :return: list of instantiated pipeline steps
        """

        instantiated_feature_list = []
        for block_name in features:
            args_list = args[block_name]
            steps = []
            for idx, step in enumerate(PipelineBuilder.default_feature_dict[features[block_name]]):
                args_dict = args_list[idx] if idx < len(args_list) else {}
                steps.append(step(**args_dict))

            instantiated_feature_list.append(steps)

        return instantiated_feature_list

    @staticmethod
    def build(features, args, classifier, transformer_weights=None):
        """
        Builds a Pipeline instance from the given features list.
        For each feature list element, its pipeline steps are retrieved. Successively, a pipeline is built via
        make_pipeline method. Then, a FeatureUnion object is created according to the previously created
        list of pipelines. Lastly, the result pipeline is built by invoking make_pipeline, taking the FeatureUnion
        object as the first step.

        :param features: list of feature names
        :param args: dictionary containing init args for each feature pipeline block
        :param classifier:
        :param transformer_weights:
        :return: pipeline object
        """

        instantiated_feature_list = PipelineBuilder._build_steps_with_args(features, args)
        pipelines = []

        for block_name, steps in zip(features, instantiated_feature_list):
            pipeline = make_pipeline(*steps)
            pipelines.append((block_name, pipeline))

        union = FeatureUnion(transformer_list=pipelines, transformer_weights=transformer_weights, n_jobs=1)

        return make_pipeline(union, classifier)
