import numpy as np
from keras import regularizers
from keras.layers import Dense, Dropout, Merge, Conv1D, MaxPool1D, GlobalMaxPool1D
from keras.layers.embeddings import Embedding
from keras.models import Sequential
from keras.optimizers import Adam
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer

from Supervised_Approach.Neural_networks.Utils.model_metrics import get_metrics
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from basic import BaseNetwork


class WordEmbeddingsCNNSequential(BaseNetwork):

    def __init__(self, num_filters, predictions_dim, dense_dim=32, kernel_size=7, pool_size=2, weight_decay=1e-4, trainable=False, loss='binary_crossentropy', learning_rate=0.001, metrics=None,
                 add_dropout=False, recurrent_dropout=0., dropout=0., padding_mode='average'):
        super(WordEmbeddingsCNNSequential, self).__init__(predictions_dim)
        self.loss = loss
        self.learning_rate = learning_rate
        self.metrics = metrics
        self.add_dropout = add_dropout
        self.recurrent_dropout = recurrent_dropout
        self.dropout = dropout
        self.padding_mode = padding_mode
        self.num_filters = num_filters
        self.kernel_size = kernel_size
        self.pool_size = pool_size
        self.weight_decay = weight_decay
        self.trainable = trainable
        self.dense_dim = dense_dim

    def build_model(self, build_info, verbose=0):
        branches = []

        if isinstance(self.num_filters, int):
            self.num_filters = {key: self.num_filters for key in build_info}

        for key in build_info:
            model = Sequential()
            model.add(Embedding(build_info[key]['vocab_size'], 300, weights=[build_info[key]['weights']],
                                input_length=build_info[key]['input_length'], trainable=self.trainable))
            if self.add_dropout:
                model.add(Dropout(self.dropout))

            model.add(Conv1D(self.num_filters[key], self.kernel_size, activation='relu', padding='same'))
            model.add(MaxPool1D(self.pool_size))
            model.add(Conv1D(self.num_filters[key] / 2, self.kernel_size, activation='relu', padding='same'))
            model.add(GlobalMaxPool1D())

            if self.add_dropout:
                model.add(Dropout(self.dropout))

            if verbose:
                print(model.summary())

            branches.append(model)

        merged = Sequential()
        merged.add(Merge(branches, mode='concat'))
        merged.add(Dense(self.dense_dim, activation='relu', kernel_regularizer=regularizers.l2(self.weight_decay)))
        merged.add(Dense(self.predictions_dim, activation='softmax'))

        optimizer = Adam(lr=self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
        merged.compile(loss=self.loss, optimizer=optimizer, metrics=get_metrics(self.metrics))

        if verbose:
            print(merged.summary())

        return merged

    def preprocess_input(self, input_info, w2v_model=None, pickle_file=None, load_pickle=False,
                         add_stance=False, stance_labels=None, stance_vector=None,
                         vocab_size=None, added_build_info=None):
        """
        Processes given documents (input_info) in order to build padded documents and obtain
        general information about them such as vocabulary size, padding length and eventually
        define the word embedding matrix.

        :param input_info: dictionary containing evidences and claims sentences
        :param w2v_model: Word2Vec pre-built model
        :param pickle_file: pickle file name in which to store the word embedding matrix for
         fast re-computation
        :param load_pickle: whether to load pre-calculated word embedding matrix or not.
        :param add_stance: whether to add or not the stance labels in the computed padded docs
        :param stance_labels: set of stance classification labels
        :param stance_vector: list of stance labels related to each given data-set entry, i.e.
        claim - evidence couple.
        :param vocab_size: size of the vocabulary built from text via Keras tokenizer
        :param added_build_info: existing build_info to update. This is usually done when doing
        train - test prediction tests
        :return: dictionary (build_info) containing the general info about input pre-processing;
        dictionary (classification_info) containg for each target of classification its related
        padded docs matrix
        """

        build_info = {}
        classification_info = {}

        for key in input_info:
            tokenizer = Tokenizer()
            tokenizer.fit_on_texts(input_info[key])

            if vocab_size is None:
                if added_build_info is None:
                    current_vocab_size = len(tokenizer.word_index) + 1
                else:
                    current_vocab_size = added_build_info[key]['vocab_size']
            else:
                current_vocab_size = vocab_size[key]

            encoded_data = tokenizer.texts_to_sequences(input_info[key])

            if added_build_info is None:
                if self.padding_mode == 'average':
                    current_padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
                elif self.padding_mode == 'max':
                    current_padding_max_length = max([len(row) for row in encoded_data])
                elif self.padding_mode == 'min':
                    current_padding_max_length = min([len(row) for row in encoded_data])
                else:
                    raise ValueError('Unrecognised padding mode! Found {}. Possible values: average, max, min'
                                     .format(self.padding_mode))
            else:
                current_padding_max_length = added_build_info[key]['input_length']

            padded_docs = pad_sequences(encoded_data, maxlen=current_padding_max_length, padding='post')

            # Add stances values in padded_docs: shifting indexes by len(stance_labels),
            #  i.e. reserving values for stances
            if add_stance:
                current_padding_max_length += 1
                current_vocab_size += len(stance_labels)
                new_padded_docs = np.zeros((padded_docs.shape[0], padded_docs.shape[1] + 1), dtype=np.int32)
                for idx, row in enumerate(padded_docs):
                    new_padded_docs[idx] = [val + len(stance_labels) if val != 0 else val for val in row] \
                                           + [stance_vector[key][idx]]
                padded_docs = new_padded_docs

            if not load_pickle:
                if added_build_info is None:
                    current_embedding_matrix = np.zeros((current_vocab_size, 300))
                else:
                    current_embedding_matrix = added_build_info[key]['weights']
                for word, i in tokenizer.word_index.items():
                    try:
                        embedding_vector = w2v_model[word]
                    except KeyError:
                        embedding_vector = np.zeros(300)

                    current_embedding_matrix[i] = embedding_vector

                if pickle_file is not None:
                    if PickleUtility.verify_file(pickle_file):
                        existing_data = PickleUtility.load_pickle(pickle_file)
                        existing_data.update({key: current_embedding_matrix})
                        PickleUtility.save_to_file(existing_data, pickle_file)
                    else:
                        PickleUtility.save_to_file({key: current_embedding_matrix}, pickle_file)
            else:
                current_embedding_matrix = PickleUtility.load_pickle(pickle_file)[key]

            build_info[key] = {'vocab_size': current_vocab_size,
                               'input_length': current_padding_max_length,
                               'weights': current_embedding_matrix
                               }
            classification_info[key] = padded_docs

        return build_info, classification_info

    def transform_for_classification(self, train_info, train_target, train_indexes, test_indexes):
        """
        Elaborates given train and test information in accordance to its specific model input

        :param train_info: classification_info computed by preprocess_input method
        :param train_target: target array for train set
        :param train_indexes: train indexes computed by specific applied cv
        :param test_indexes: test indexes computed by specific applied cv
        :return: train set, train set target, test set, test set target
        """

        X_train = [train_info[key][train_indexes] for key in train_info]
        y_train = train_target[train_indexes]
        X_test = [train_info[key][test_indexes] for key in train_info]
        y_test = train_target[test_indexes]

        return X_train, y_train, X_test, y_test
