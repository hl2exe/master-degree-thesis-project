import numpy as np
from keras.layers import Dense, Dropout, LSTM, Merge
from keras.layers.embeddings import Embedding
from keras.models import Sequential
from keras.optimizers import RMSprop
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer

from Supervised_Approach.Neural_networks.Utils.model_metrics import get_metrics
from Supervised_Approach.Utils.pickle_utility import PickleUtility
from basic import BaseNetwork



class WordEmbeddingLSTMSequential(BaseNetwork):

    def __init__(self, lstm_neurons, predictions_dim, trainable=False, loss='binary_crossentropy', learning_rate=0.001, metrics=None,
                 add_dropout=False, recurrent_dropout=0., dropout=0., padding_mode='average'):
        super(WordEmbeddingLSTMSequential, self).__init__(predictions_dim)
        self.loss = loss
        self.learning_rate = learning_rate
        self.metrics = metrics
        self.add_dropout = add_dropout
        self.recurrent_dropout = recurrent_dropout
        self.dropout = dropout
        self.padding_mode = padding_mode
        self.lstm_neurons = lstm_neurons
        self.trainable = trainable

    def build_model(self, build_info, verbose=0):
        branches = []

        if isinstance(self.lstm_neurons, int):
            self.lstm_neurons = {key: self.lstm_neurons for key in build_info}

        for key in build_info:
            model = Sequential()
            model.add(Embedding(build_info[key]['vocab_size'], 300, weights=[build_info[key]['weights']],
                                input_length=build_info[key]['input_length'], trainable=self.trainable))
            if self.add_dropout:
                model.add(Dropout(self.dropout))

            model.add(LSTM(self.lstm_neurons[key], recurrent_dropout=self.recurrent_dropout, dropout=self.dropout,
                           unit_forget_bias=True))

            if self.add_dropout:
                model.add(Dropout(self.dropout))

            if verbose:
                print(model.summary())

            branches.append(model)

        merged = Sequential()
        merged.add(Merge(branches, mode='concat'))
        merged.add(Dense(self.predictions_dim, activation='sigmoid'))

        optimizer = RMSprop(lr=self.learning_rate)
        merged.compile(loss=self.loss, optimizer=optimizer, metrics=get_metrics(self.metrics))

        if verbose:
            print(merged.summary())

        return merged

    def preprocess_input(self, input_info, w2v_model=None, pickle_file=None, load_pickle=False,
                         add_stance=False, stance_labels=None, stance_vector=None,
                         vocab_size=None, added_build_info=None):
        """
        Processes given documents (input_info) in order to build padded documents and obtain
        general information about them such as vocabulary size, padding length and eventually
        define the word embedding matrix.

        :param input_info: dictionary containing evidences and claims sentences
        :param w2v_model: Word2Vec pre-built model
        :param pickle_file: pickle file name in which to store the word embedding matrix for
         fast re-computation
        :param load_pickle: whether to load pre-calculated word embedding matrix or not.
        :param add_stance: whether to add or not the stance labels in the computed padded docs
        :param stance_labels: set of stance classification labels
        :param stance_vector: list of stance labels related to each given data-set entry, i.e.
        claim - evidence couple.
        :param vocab_size: size of the vocabulary built from text via Keras tokenizer
        :param added_build_info: existing build_info to update. This is usually done when doing
        train - test prediction tests
        :return: dictionary (build_info) containing the general info about input pre-processing;
        dictionary (classification_info) containg for each target of classification its related
        padded docs matrix
        """

        build_info = {}
        classification_info = {}

        for key in input_info:
            tokenizer = Tokenizer()
            tokenizer.fit_on_texts(input_info[key])

            if vocab_size is None:
                if added_build_info is None:
                    current_vocab_size = len(tokenizer.word_index) + 1
                else:
                    current_vocab_size = added_build_info[key]['vocab_size']
            else:
                current_vocab_size = vocab_size[key]

            encoded_data = tokenizer.texts_to_sequences(input_info[key])

            if added_build_info is None:
                if self.padding_mode == 'average':
                    current_padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
                elif self.padding_mode == 'max':
                    current_padding_max_length = max([len(row) for row in encoded_data])
                elif self.padding_mode == 'min':
                    current_padding_max_length = min([len(row) for row in encoded_data])
                else:
                    raise ValueError('Unrecognised padding mode! Found {}. Possible values: average, max, min'
                                     .format(self.padding_mode))
            else:
                current_padding_max_length = added_build_info[key]['input_length']

            padded_docs = pad_sequences(encoded_data, maxlen=current_padding_max_length, padding='post')

            # Add stances values in padded_docs: shifting indexes by len(stance_labels),
            #  i.e. reserving values for stances
            if add_stance:
                current_padding_max_length += 1
                current_vocab_size += len(stance_labels)
                new_padded_docs = np.zeros((padded_docs.shape[0], padded_docs.shape[1] + 1), dtype=np.int32)
                for idx, row in enumerate(padded_docs):
                    new_padded_docs[idx] = [val + len(stance_labels) if val != 0 else val for val in row] \
                                           + [stance_vector[key][idx]]
                padded_docs = new_padded_docs

            if not load_pickle:
                if added_build_info is None:
                    current_embedding_matrix = np.zeros((current_vocab_size, 300))
                else:
                    current_embedding_matrix = added_build_info[key]['weights']
                for word, i in tokenizer.word_index.items():
                    try:
                        embedding_vector = w2v_model[word]
                    except KeyError:
                        embedding_vector = np.zeros(300)

                    current_embedding_matrix[i] = embedding_vector

                if pickle_file is not None:
                    if PickleUtility.verify_file(pickle_file):
                        existing_data = PickleUtility.load_pickle(pickle_file)
                        existing_data.update({key: current_embedding_matrix})
                        PickleUtility.save_to_file(existing_data, pickle_file)
                    else:
                        PickleUtility.save_to_file({key: current_embedding_matrix}, pickle_file)
            else:
                current_embedding_matrix = PickleUtility.load_pickle(pickle_file)[key]

            build_info[key] = {'vocab_size': current_vocab_size,
                               'input_length': current_padding_max_length,
                               'weights': current_embedding_matrix
                               }
            classification_info[key] = padded_docs

        return build_info, classification_info

    def transform_for_classification(self, train_info, train_target, train_indexes, test_indexes):
        """
        Elaborates given train and test information in accordance to its specific model input

        :param train_info: classification_info computed by preprocess_input method
        :param train_target: target array for train set
        :param train_indexes: train indexes computed by specific applied cv
        :param test_indexes: test indexes computed by specific applied cv
        :return: train set, train set target, test set, test set target
        """

        X_train = [train_info[key][train_indexes] for key in train_info]
        y_train = train_target[train_indexes]
        X_test = [train_info[key][test_indexes] for key in train_info]
        y_test = train_target[test_indexes]

        return X_train, y_train, X_test, y_test


class WordEmbeddingLSTMSequentialSiamese(BaseNetwork):
    """
    Model info:

    The vocab_size is the maximum between claim and evidence vocab_size.

    The input_length could be computed as follows: minimum, maximum or average
    between claim and evidence input_length.

    The word embedding matrix (weights) must contain embeddings from both targets
    (claim and evidence).
    """

    def __init__(self, lstm_neurons, predictions_dim, trainable=False, loss='binary_crossentropy', learning_rate=0.001, metrics=None,
                 add_dropout=False, recurrent_dropout=0., dropout=0., padding_mode='average'):
        super(WordEmbeddingLSTMSequentialSiamese, self).__init__(predictions_dim)
        self.loss = loss
        self.learning_rate = learning_rate
        self.metrics = metrics
        self.add_dropout = add_dropout
        self.recurrent_dropout = recurrent_dropout
        self.dropout = dropout
        self.padding_mode = padding_mode
        self.lstm_neurons = lstm_neurons
        self.trainable = trainable

    def build_model(self, build_info, verbose=0):
        branches = []

        shared_model = Sequential()
        shared_model.add(Embedding(build_info['vocab_size'], 300, weights=[build_info['weights']],
                                   input_length=build_info['input_length'], trainable=self.trainable))
        if self.add_dropout:
            shared_model.add(Dropout(self.dropout))

        shared_model.add(LSTM(self.lstm_neurons,
                              recurrent_dropout=self.recurrent_dropout,
                              dropout=self.dropout,
                              unit_forget_bias=True))

        if self.add_dropout:
            shared_model.add(Dropout(self.dropout))

        for idx in build_info['ids']:
            model = Sequential(name=idx)
            shared_model.name = idx
            model.add(shared_model)

            if verbose:
                print(model.summary())

            branches.append(model)

        merged = Sequential()
        merged.add(Merge(branches, mode='concat'))
        merged.add(Dense(self.predictions_dim, activation='sigmoid'))

        optimizer = RMSprop(lr=self.learning_rate)
        merged.compile(loss=self.loss, optimizer=optimizer, metrics=get_metrics(self.metrics))

        if verbose:
            print(merged.summary())

        return merged

    def _build_padded_docs(self, sequence, key, tokenizer, padding_max_length, vocab_size=None, add_stance=False,
                           stance_vector=None, stance_labels=None):
        """
        Builds input padded docs for classification phase

        :param sequence: input sequence to analyse
        :param key: which target to consider (claim or evidence)
        :param tokenizer: keras tokenizer for input processing
        :param padding_max_length: padded docs length
        :param vocab_size: size of the vocabulary built from text via Keras tokenizer
        :param add_stance: whether to add or not the stance labels in the computed padded docs
        :param stance_labels: set of stance classification labels
        :param stance_vector: list of stance labels related to each given data-set entry, i.e.
        claim - evidence couple.
        :return: input padded docs
        """

        encoded_data = tokenizer.texts_to_sequences(sequence)
        padded_docs = pad_sequences(encoded_data, maxlen=padding_max_length, padding='post')

        # Add stances values in padded_docs: shifting indexes by len(stance_labels),
        #  i.e. reserving values for stances
        if add_stance:
            padding_max_length += 1

            vocab_size += len(stance_labels)
            new_padded_docs = np.zeros((padded_docs.shape[0], padded_docs.shape[1] + 1), dtype=np.int32)
            for idx, row in enumerate(padded_docs):
                new_padded_docs[idx] = [val + len(stance_labels) if val != 0 else val for val in row] + \
                                       [stance_vector[key][idx]]
            padded_docs = new_padded_docs

        return padded_docs

    def preprocess_input(self, input_info, w2v_model, load_pickle=False, pickle_file=None,
                         add_stance=False, stance_labels=None, stance_vector=None,
                         vocab_size=None, added_build_info=None):
        """
        Processes given documents (input_info) in order to build padded documents and obtain
        general information about them such as vocabulary size, padding length and eventually
        define the word embedding matrix.

        :param input_info: dictionary containing evidences and claims sentences
        :param w2v_model: Word2Vec pre-built model
        :param pickle_file: pickle file name in which to store the word embedding matrix for
         fast re-computation
        :param load_pickle: whether to load pre-calculated word embedding matrix or not.
        :param add_stance: whether to add or not the stance labels in the computed padded docs
        :param stance_labels: set of stance classification labels
        :param stance_vector: list of stance labels related to each given data-set entry, i.e.
        claim - evidence couple.
        :param vocab_size: size of the vocabulary built from text via Keras tokenizer
        :param added_build_info: existing build_info to update. This is usually done when doing
        train - test prediction tests
        :return: dictionary (build_info) containing the general info about input pre-processing;
        dictionary (classification_info) containg for each target of classification its related
        padded docs matrix
        """

        sequence = np.concatenate(tuple(input_info.values()), axis=0)
        print(sequence.shape)

        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(sequence)

        if vocab_size is None:
            if added_build_info is None:
                vocab_size = len(tokenizer.word_index) + 1
            else:
                vocab_size = added_build_info['vocab_size']

        encoded_data = tokenizer.texts_to_sequences(sequence)

        if added_build_info is None:
            if self.padding_mode == 'average':
                padding_max_length = sum([len(row) for row in encoded_data]) / len(encoded_data)
            elif self.padding_mode == 'max':
                padding_max_length = max([len(row) for row in encoded_data])
            elif self.padding_mode == 'min':
                padding_max_length = min([len(row) for row in encoded_data])
            else:
                raise ValueError('Unrecognised padding mode! Found {}. Possible values: average, max, min'
                                 .format(self.padding_mode))
        else:
            padding_max_length = added_build_info['input_length']
            
        if not load_pickle:
            if added_build_info is None:
                embedding_matrix = np.zeros((vocab_size, 300))
            else:
                embedding_matrix = added_build_info['weights']
            for word, i in tokenizer.word_index.items():
                try:
                    embedding_vector = w2v_model[word]
                except KeyError:
                    embedding_vector = np.zeros(300)
                embedding_matrix[i] = embedding_vector

            if pickle_file is not None:
                if PickleUtility.verify_file(pickle_file):
                    existing_data = PickleUtility.load_pickle(pickle_file)
                    existing_data.update(embedding_matrix)
                    PickleUtility.save_to_file(existing_data, pickle_file)
                else:
                    PickleUtility.save_to_file(embedding_matrix, pickle_file)
        else:
            embedding_matrix = PickleUtility.load_pickle(pickle_file)

        build_info = {'vocab_size': vocab_size,
                      'input_length': padding_max_length,
                      'weights': embedding_matrix,
                      'ids': input_info.keys()
                      }

        train_info = {}
        for key in input_info:
            train_info[key] = self._build_padded_docs(sequence=input_info[key], key=key, tokenizer=tokenizer,
                                                      padding_max_length=padding_max_length, vocab_size=vocab_size,
                                                      add_stance=add_stance, stance_labels=stance_labels,
                                                      stance_vector=stance_vector)

        return build_info, train_info

    def transform_for_classification(self, train_info, train_target, train_indexes, test_indexes):
        """
        Elaborates given train and test information in accordance to its specific model input

        :param train_info: classification_info computed by preprocess_input method
        :param train_target: target array for train set
        :param train_indexes: train indexes computed by specific applied cv
        :param test_indexes: test indexes computed by specific applied cv
        :return: train set, train set target, test set, test set target
        """

        X_train = [train_info[key][train_indexes] for key in train_info]
        y_train = train_target[train_indexes]
        X_test = [train_info[key][test_indexes] for key in train_info]
        y_test = train_target[test_indexes]

        return X_train, y_train, X_test, y_test
