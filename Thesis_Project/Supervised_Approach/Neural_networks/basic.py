import numpy as np
from keras.preprocessing.text import Tokenizer


class BaseNetwork(object):

    def __init__(self, predictions_dim):
        self.predictions_dim = predictions_dim

    def build_model(self, **kwargs):
        pass

    def preprocess_input(self, **kwargs):
        pass

    def transform_for_classification(self, **kwargs):
        pass

    def reshape_true_values_for_multiclass(self, values):
        reshaped_values = np.zeros(shape=(values.shape[0], self.predictions_dim))
        indexes = [idx * self.predictions_dim + value for idx, value in enumerate(values)]
        reshaped_values.put(indexes, [1 for _ in values])
        return reshaped_values

    def predict(self, model, input_data, **kwargs):
        if self.predictions_dim:
            predictions = model.predict(input_data, **kwargs)
            return [np.argmax(prediction) for prediction in predictions]
        else:
            return model.predict_classes(input_data)

    @staticmethod
    def get_vocab_size(sequence):
        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(sequence)

        return len(tokenizer.word_index) + 1
    