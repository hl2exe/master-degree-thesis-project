# Master Degree Thesis Project:
===========================================================

Original Title: 'Predizione della struttura di un argomento con feature di stance classification'

English translation: 'Argument structure prediction with stance classification features'

## Brief Summary
-------------

This project approaches 3 different kinds of problems:

* **Stance classification (SC)**: a pipeline based classifier for stance classification is proposed in order to achieve state of art results on Emergent data-set. Moreover, it is possible to add new corpora (see examples section) as to expand the experimental context of interest.

* **Argument structure prediction (ASP)**: the stance information is tested as an additional feature in order to solve the argument structure prediction problem on IBM corpora (CE-ACL-14 and CE-EMNLP-15). Additionally, the following project defines 5 different type of classifiers in order to solve the aforementioned problem:

    * Baseline n-grams
    * Baseline cosine similarity
    * Classifier based on a subset of Christian Stab & Irina Gurevych features described in 'Identifying argumentative discourse structures in persuasive essays.'
    * Classifier based on a subset of most common stance classification features as described in the first experimental part dedicated to stance classification
    * Recurrent neural network model (LSTM) with Embedding layers. In particular, two variants of the following model are considered in order to achieve better classification results: (1) adds Dropout layers and (2) adds stance information at the input level

* **Introduction of a new corpus for ASP**: with reference to Create Debate corpus (see 'Stance Classification of Ideological Debates: Data, Models, Features and Constraints' [Hasan and Ng 2013]) and MARGOT argument detection tool (see 'MARGOT a web server for argumentation mining' [Lippi and Torroni 2016]), a new corpus for ASP is built. Successively, the previously introduced 5 classifiers are tested and evaluated on the given data-set and, finally, transfer learning classification tests are held in order to obtain results comparable between multiple corpora.

## Project info
-------------

The project was created via Pycharm IDE and it is composed of two main directories.

### Project structure

* **MARGOT_parsing**: Dedicated to MARGOT usage in order to process documents and build the new corpus for ASP as described in the 3rd experimental part.

    * [Folder] Configs: contains configuration files used by system scripts. **Please check them before usage**
    
    * [Folder] Dataset_creation_info: contains data-set descriptive files in .json format. More precisely, whenever a MARGOT specific data-set is built, information like the amount of positive and negative claim-evidence couples is stored via JSON API.

    * [Folder] Runnables: contains test scripts. In particular, *extract_arguments.py* allows the user to use MARGOT in order to parse a specific data-set.

    * [Folder] Utils: contains data-set specific utils for information retrieval

    * [Script] analyzer.py: a general-purpose script that allows to find valid documents in terms of the number of detected claims and evidence (1+ claim and evidence) or to extract them.

    * [Script] cleaner.py: defines several data-set filtering methods

    * [Script] path_definitions.py: contains all project relative directories paths in order to access local data more easily

    * [Script] reader.py: the core system script that invokes MARGOT for document analysis.

    * [Script] selector.py: defines utility methods in order to access to data-set documents properly

    * **[Downloadable Folder]** Datasets: contains corpora of interest like CreateDebateCustom data-set.

    * **[Downloadable Folder]** ParsedDatasets: it contains parsed version of the data-set analysed via MARGOT  

* **Supervised_Approach**: Dedicated to all the aforementioned experimental parts.
    
    * [Folder] Configs: contains configuration files used by system scripts. **Please check them before usage**

    * [Folder] Ext: contains external imported scripts and **tools**! Check [Project external dependencies](#markdown-header-project-external-dependencies) section for more info

    * [Folder] Neural_networks: contains recurrent neural networks definitions (*rnn_models.py*) along with additional utils (*Utils* directory)

    * [Folder] Runnables: contains test scripts. In particular we have: 

        * *dataset_generation*: for data-set (CE-ACL-14 and CE-EMNLP-15) pre-processing

        * *dataset_processing*: for data-set analysis (post-processing)

        * *neural_network_classifiers*: stance classification and argument structure prediction test scripts for recurrent neural networks

        * *standard_classifiers*:  stance classification and argument structure prediction test scripts for sklearn standard algorithms (SVM, MultinomialNB, RandomForest, etc..)

        * *statistics_processing*: classification results are parsed in order to define tabular, plots and other types of resume reports

    * [Folder] Utils: contains several types of utility methods for different purposes, like feature extraction, data saving, information retrieval and data processing

    * [Script] classifier_factory.py: simple factory pattern for classifier algorithms (neural networks and standard algorithms)

    * [Script] collector.py: utility script for saving information like classification results or classifiers configurations

    * [Script] dataset_generator.py: utility script for data-set pre-processing

    * [Script] feature_extractor.py: contains all supported extractors (from cosine similarity to more specific extractors like the ones introduced by Christian Stab and Irina Gurevych), acting as the intermediate step of the feature extraction pipeline

    * [Script]: feature_transformer.py: contains all supported feature transformers, acting as the final step of the feature extraction pipeline

    * [Script] path_definitions.py: contains all project relative directories paths in order to access local data more easily

    * [Script] pipeline_builder.py: core system script for Pipeline object definition, i.e. the classifier

    * [Script] post_processor.py: utility script for data-set post-processing

    * [Script] selector.py: contains all supported text extractors, acting as the first step of the feature extraction pipeline

    * [Script] trainer.py: core system script containing useful methods for classification

    * **[Downloadable Folder]** Datasets: contains all used data-set for classification tests or pre/post-processing analysis.

    * **[Downloadable Folder]** Pickle: contains all pre-computed features for each data-set of interest

    * **[Downloadable Sub-folders]** nltk_data, ppdb, stanford-parser-full-2017-06-09: external tools or data for classification and data-set analysis

### Project external dependencies

The project is built upon some important external tools like NLTK, Word2Vec and WordNet and so on.

* [NLTK](https://www.nltk.org/)

* [Word2Vec](https://code.google.com/archive/p/word2vec/)

* [WordNet](https://wordnet.princeton.edu/)

* [Keras](https://keras.io/)

* [Theano](http://deeplearning.net/software/theano/): you can also use Tensorflow as a replacement since everything has been written via Keras

* [MPQA Subjectivity Lexicon](http://mpqa.cs.pitt.edu/lexicons/subj_lexicon/)

## Installing
-------------

Installing external tools and downloading missing folders is quite simple. Please check this [link](https://drive.google.com/open?id=19YLdx-d6ZfsjhbZYZkoPnp4ztZI_RY_-)
and download all folders. Lastly, copy them into the project respecting the given hierarchy.
Moreover, other tools are downloadable as follows:

* [Word2Vec](https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing): download it and modify *Configs/extractor.ini* afterwards

* [Keras](https://keras.io/): either install it via anaconda or separately

* [Theano/Tensorflow]: same procedure as described for Keras

* [MPQA Subjectivity Lexicon](http://mpqa.cs.pitt.edu/lexicons/subj_lexicon/): compile the form, download it and modify *Configs/extractor.ini* afterwards. **Please be aware that this data is required only if you want to extract Opinion dependencies with MPQA and not with VADER!!**

## Running tests
-------------

Runnables scripts should be (hopefully) sufficiently documented and self-explanatory in some parts. However, there is some information which is required.

### Pickle pre-loading

Using pickle complaint extractors/transformers during classification requires a pickle pre-loading phase in which previously pre-computed features are loaded in memory.

```
# pickle pre-loading
folder = 'Emergent'
features_names = [
    'cosine_similarity',
    'basic',
    'repeated_punctuation',
    'POS_generalized',
    'syntactic',
    'sngrams',
    'svo_triples'
]
FeatureExtractorUtils.load_pickle_data(features_names=features_names, folders=folder)

utilities_names = ['ppdb']
FeatureTransformerUtils.load_pickle_data(utilities_names)
```

**Please note that the pre-loading phase is repeated for each heavy process in Windows (necessary), which may slow down the initial phase of the classification.**

### Classifier configurations (saving/loading)

A configuration is merely a combination of features to use along with a specific classification algorithm. Configurations are saved after a grid search phase and can be
automatically loaded in several runnable scripts. However, it is also possible to manually define a classifier configuration along with its specific features arguments.
Configurations are saved in JSON format for readability while maintaining ease of use at the same time. Lastly, each configuration is stored in *Statistics* directory and it is identified by a unique name. 

### Input section

It is possible to define things like:

* Data-sets for classification
 
* Configuration mode: whether to load a previously saved configuration or not
 
* Manual configuration: features to use, targets for each feature, additional args for each feature, classifier algorithm and so on
 
* Autoconfiguration: name of the configuration file to load (JSON format)

* Specific test info like metrics, number of processes, verbose, custom cross validators and so on

### System section

In this phase, classifiers are usually built in order to be properly configured for the last section.

### Output section

Specific tests actions are usually defined here. All obtained results are usually saved in the same folder of the classifier configuration.

## Project libraries versions
-------------

Python interpreter version used: 2.7.13 (Anaconda).
Main libraries:

```
configparser==3.5.0
dill==0.2.7.1
gensim==3.1.0
keras==2.1.1
matplotlib==2.1.0
nltk==3.2.5
numpy==1.13.3
pandas==0.21.0
scikit-learn==0.19.1
scipy==1.0.0
simplejson==3.11.1
sklearn==0.0
Theano==1.0.0
toolz==0.8.2
unicodecsv==0.14.1
urllib3==1.22
```

## License
-------------

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## FAQ
-------------

### How to add a new data-set

Adding and testing on a new data-set is very simple. Just add it to any of the Datasets sub-folders (preferably train and test folders if you are planning to do some classification tests) and properly modify the input section of the runnables scripts of your interest.

### How to add a new feature

Simply define a new extractor in *feature_extractor.py*. If your extractor requires pickle saving, please select the proper superclass among **PickleCompliantExtractor** and **PickleCompliantVectorizer**. Please check also *dataset_processing/pickle_feature_extractor.py* script for features pre-computation and saving.
Whereupon, modify *default_feature_dict* in *pipeline_builder* by adding your new feature pipeline definition. Lastly, modify *Configs/features.json*, *Configs/features_args.json*, *Configs/features_gridsearch.json* with your new feature information.

### How to add a new classifier algorithm

Add it to *classifier_factory* *supported_classifiers* or *supported_networks*. Then modify *Configs/classifiers_args.json* and *Configs/classifiers_gridsearch.json* and you are done.

## Acknowledgements

This work represents my master degree thesis at the University of Bologna (UNIBO). I would like to thank my advisor Prof. Paolo Torroni along with my co-advisor Marco Lippi for all the support and suggestions during the development process.
I would like to thank also PhD student Andrea Galassi for his precious feedback.

To my dear Valentina, without her nothing would have been possible.

## TODO
-------------

I'm planning on defining jupyter notebook version of the runnables folder along with a Docker project image in order to work more easily without worrying about external dependencies or
packages versions.
